﻿using System;
namespace TotalApp.Package
{
    /// <summary>
    /// *Autor:Larry Ramirez
    /// *Fecha de creacion:09-01-2018
    /// *Version:1.1.20180109
    /// *Sinopsis:Clase de tipo atibuto, permite asignar y capturar valores de un objeto (Type)Enum
    /// </summary>
    [System.AttributeUsage(System.AttributeTargets.All, AllowMultiple = true, Inherited = true)]
    public class TStringValueAttribute : Attribute
    {
        #region "Constantes"
        #endregion

        #region "Variables"
        protected string _sValue;
        #endregion

        #region "Propiedades"
        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        public string Value
        {
            get { return _sValue; }
        }
        #endregion

        #region "Funciones"
        #endregion

        #region "Metodos & constructores"
        public TStringValueAttribute()
        {

        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sValue"></param>
        public TStringValueAttribute(string sValue)
        {
            _sValue = sValue;
        }
        #endregion

        #region "Eventos"
        #endregion

        #region "Destructor"
        ~TStringValueAttribute()
        {

        }
        #endregion
    }
}
