﻿using System;
using System.Linq.Expressions;

namespace TotalApp.Package.TLINQ
{
    /// <summary>
    /// *Autor:Larry Ramirez
    /// *Fecha de creacion:09-01-2018
    /// *Version:1.1.20180109
    /// *Sinopsis:
    /// </summary>
    public class TExpressionRule<T> where T : class
    {
        #region "Constantes"
        #endregion

        #region "Variables"
        #endregion

        #region "Propiedades"

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        public Func<T, bool> Expression { get; private set; }
        #endregion

        #region "Funciones"

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="oItem"></param>
        public static implicit operator Func<T, bool>(TExpressionRule<T> oItem)
        {
            return oItem.Expression;
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="oItem"></param>
        /// <returns></returns>
        public bool Evaluate(T oItem)
        {
            return Expression(oItem);
        }
        #endregion

        #region "Metodos & constructores"
        public TExpressionRule()
        {

        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="oExp"></param>
        public TExpressionRule(Expression<Func<T, bool>> oExp)
        {
            Expression = oExp.Compile();
        }
        #endregion

        #region "Eventos"
        #endregion

        #region "Destructor"
        ~TExpressionRule()
        {

        }
        #endregion
    }
}
