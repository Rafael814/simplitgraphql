﻿using System;
using System.Collections.Generic;

namespace TotalApp.Package.TLINQ
{
    /// <summary>
    /// *Autor:Larry Ramirez
    /// *Fecha de creacion:09-01-2018
    /// *Version:1.1.20180109
    /// *Sinopsis:
    /// </summary>
    public static partial class THelperLinq
    {
        #region "Funciones"
        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="oData"></param>
        /// <param name="oPred"></param>
        /// <returns></returns>
        public static IEnumerable<T> TWhereIf<T>(this IEnumerable<T> oData, Func<T,int, bool> oPred)
        {
            if (oData.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oData));
            if (oPred.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oPred));

            var nIndex = -1;
            foreach (var oElement in oData)
            {
                checked
                {
                    nIndex++;
                }

                if (oPred(oElement, nIndex))
                {
                    yield return oElement;
                }
            }
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="oData"></param>
        /// <param name="oPred"></param>
        /// <returns></returns>
        public static IEnumerable<T> TWhereIfNot<T>(this IEnumerable<T> oData, Func<T, int, bool> oPred)
        {
            if (oData.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oData));
            if (oPred.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oPred));

            var nIndex = -1;
            foreach (var oElement in oData)
            {
                checked
                {
                    nIndex++;
                }

                if (!oPred(oElement, nIndex))
                {
                    yield return oElement;
                }
            }
        }
        #endregion
    }
}
