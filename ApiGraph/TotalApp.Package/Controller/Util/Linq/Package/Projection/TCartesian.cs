﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace TotalApp.Package.TLINQ
{
    /// <summary>
    /// *Autor:Larry Ramirez
    /// *Fecha de creacion:09-01-2018
    /// *Version:1.1.20180109
    /// *Sinopsis:
    /// </summary>
    public static partial class THelperLinq
    {
        #region "Funciones"
        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="U"></typeparam>
        /// <typeparam name="W"></typeparam>
        /// <param name="oFirst"></param>
        /// <param name="oSecond"></param>
        /// <param name="oResultSelector"></param>
        /// <returns></returns>
        public static IEnumerable<W> TCartesian<T, U, W>(this IEnumerable<T> oFirst
                                                       , IEnumerable<U> oSecond
                                                       , Func<T, U, W> oResultSelector)
        {
            if (oFirst.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oFirst));
            if (oSecond.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oSecond));
            if (oResultSelector.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oResultSelector));

            return from oItem1 in oFirst
                   from oItem2 in oSecond
                   select oResultSelector(oItem1, oItem2);
        }
        #endregion
    }
}
