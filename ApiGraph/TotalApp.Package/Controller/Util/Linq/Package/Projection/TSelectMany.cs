﻿using System;
using System.Collections.Generic;

namespace TotalApp.Package.TLINQ
{
    /// <summary>
    /// *Autor:Larry Ramirez
    /// *Fecha de creacion:09-01-2018
    /// *Version:1.1.20180109
    /// *Sinopsis:
    /// </summary>
    public static partial class THelperLinq
    {
        #region "Funciones"

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="U"></typeparam>
        /// <typeparam name="T"></typeparam>
        /// <param name="oData"></param>
        /// <param name="oSelect"></param>
        /// <returns></returns>
        public static IEnumerable<T> TSelectMany<U, T>(this IEnumerable<U> oData
                                                      , Func<U, IEnumerable<T>> oSelect)
        {
            if (oData == null)
            {
                throw new ArgumentNullException("oData");
            }
            if (oSelect == null)
            {
                throw new ArgumentNullException("oSelect");
            }
            return SelectManyImpl(oData,
                                  (oValue, nIndex) => oSelect(oValue),
                                  (oElement, oSubElement) => oSubElement);
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="U"></typeparam>
        /// <typeparam name="T"></typeparam>
        /// <param name="oData"></param>
        /// <param name="oSelect"></param>
        /// <returns></returns>
        private static IEnumerable<T> SelectManyImpl<U, T>(IEnumerable<U> oData
                                                         ,Func<U, IEnumerable<T>> oSelect)
        {
            foreach (U oItem in oData)
            {
                foreach (T oResult in oSelect(oItem))
                {
                    yield return oResult;
                }
            }
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="U"></typeparam>
        /// <typeparam name="TCollection"></typeparam>
        /// <typeparam name="T"></typeparam>
        /// <param name="oData"></param>
        /// <param name="oCollect"></param>
        /// <param name="oResultSelect"></param>
        /// <returns></returns>
        private static IEnumerable<T> SelectManyImpl<U, TCollection, T>(IEnumerable<U> oData
                                                                      , Func<U, int, IEnumerable<TCollection>> oCollect
                                                                      , Func<U, TCollection, T> oResultSelect)
        {
            int nIndex = 0;
            foreach (U oItem in oData)
            {
                foreach (TCollection oCollectItem in oCollect(oItem, nIndex++))
                {
                    yield return oResultSelect(oItem, oCollectItem);
                }
            }
        }
        #endregion
    }
}
