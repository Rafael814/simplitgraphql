﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace TotalApp.Package.TLINQ
{
    /// <summary>
    /// *Autor:Larry Ramirez
    /// *Fecha de creacion:09-01-2018
    /// *Version:1.1.20180109
    /// *Sinopsis:
    /// </summary>
    public static partial class THelperLinq
    {
        #region "Funciones"
        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="oData"></param>
        /// <param name="oVal"></param>
        /// <returns></returns>
        public static bool TContains<T>(this IEnumerable<T> oData, T oVal) => oData is ICollection<T>
                                                                            ? ((ICollection<T>)oData).Contains(oVal)
                                                                            : TContains(oData, oVal, null);


        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="oData"></param>
        /// <param name="oVal"></param>
        /// <param name="oComparer"></param>
        /// <returns></returns>
        public static bool TContains<T>(this IEnumerable<T> oData, T oVal, IEqualityComparer<T> oComparer)
        {
            if (oComparer == null)
            {
                oComparer = EqualityComparer<T>.Default;
            }

            if (oData.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oData));

            foreach (var oElement in oData)
            {
                if (oComparer.Equals(oElement, oVal))
                {
                    return true;
                }
            }

            return false;
        }


        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="oData"></param>
        /// <param name="oItem"></param>
        /// <returns></returns>
        public static bool TContains<T>(this IEnumerable oData, T oItem)
        {
            foreach (var obj in oData)
            {
                if (Equals(obj, oItem))
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="TColumn"></typeparam>
        /// <param name="oData"></param>
        /// <param name="oPred"></param>
        /// <param name="sCrit"></param>
        /// <returns></returns>
        public static IEnumerable<T> TContains<T, TColumn>(this IEnumerable<T> oData
                                                         , Func<T, TColumn> oPred
                                                         , string sCrit)
        {
            if (oData.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oData));
            if (oPred.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oPred));

            return oData.TWhereIf(x => oPred(x).ToString().Contains(sCrit));

        }

        #endregion
    }
}
