﻿using System.Collections.Generic;


namespace TotalApp.Package.TLINQ
{
    /// <summary>
    /// *Autor:Larry Ramirez
    /// *Fecha de creacion:09-01-2018
    /// *Version:1.1.20180109
    /// *Sinopsis:
    /// </summary>
    public static partial class THelperLinq
    {
        #region "Funciones"
        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="oData"></param>
        /// <returns></returns>
        public static IEnumerable<T> TReverse<T>(this IEnumerable<T> oData)
        {
            LinkedList<T> oList = new LinkedList<T>(oData);
            LinkedListNode<T> oNode = oList.Last;
            while (oNode != null)
            {
                yield return oNode.Value;
                oNode = oNode.Previous;
            }
        }
        #endregion
    }
}
