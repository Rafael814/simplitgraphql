﻿using System;
using System.Collections.Generic;

namespace TotalApp.Package.TLINQ
{
    /// <summary>
    /// *Autor:Larry Ramirez
    /// *Fecha de creacion:09-01-2018
    /// *Version:1.1.20180109
    /// *Sinopsis:
    /// </summary>
    public static partial class THelperLinq
    {
        #region "Funciones"

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="oData"></param>
        /// <returns></returns>
        public static T TLastOrDefault<T>(this IEnumerable<T> oData)
        {
            if (oData.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oData));

            T oReturn = default(T);

            IList<T> oList = oData as IList<T>;

            if (oList != null)
            {
                if (oList.Count == 0)
                {
                    return oReturn;
                }

                int nCount = oList.Count;
                if (nCount > 0)
                {
                    return oList[nCount - 1];
                }

            }

            using (IEnumerator<T> oIter = oData.GetEnumerator())
            {
                if (!oIter.MoveNext())
                {
                    throw new InvalidOperationException("Sequence was empty");
                }

                oReturn = oIter.Current;

                while (oIter.MoveNext())
                {
                    oReturn = oIter.Current;
                }
            }

            return oReturn;
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="oData"></param>
        /// <param name="oPred"></param>
        /// <returns></returns>
        public static T TLastOrDefault<T>(this IEnumerable<T> oData, Func<T, bool> oPred)
        {
            if (oData.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oData));
            if (oPred.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oPred));

            T oReturn = default(T);

            foreach (T oItem in oData)
            {
                if (oPred(oItem))
                {
                    oReturn = oItem;
                }
            }
            return oReturn;
        }
        #endregion

    }
}
