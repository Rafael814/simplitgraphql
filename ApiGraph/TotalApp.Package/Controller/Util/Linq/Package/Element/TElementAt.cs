﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace TotalApp.Package.TLINQ
{
    /// <summary>
    /// *Autor:Larry Ramirez
    /// *Fecha de creacion:09-01-2018
    /// *Version:1.1.20180109
    /// *Sinopsis:
    /// </summary>
    public static partial class THelperLinq
    {
        #region "Funciones"

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="oData"></param>
        /// <param name="nIndex"></param>
        /// <returns></returns>
        public static T TElementAt<T>(this IEnumerable<T> oData, int nIndex)
        {
            T oReturn;
            if (!TryElementAt(oData, nIndex, out oReturn))
            {
                throw new ArgumentOutOfRangeException("nIndex");
            }
            return oReturn;
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="oData"></param>
        /// <param name="nIndex"></param>
        /// <returns></returns>
        public static T TElementAtOrDefault<T>(this IEnumerable<T> oData,int nIndex)
        {
            T oReturn;
           
            TryElementAt(oData, nIndex, out oReturn);
            return oReturn;
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="oData"></param>
        /// <param name="nIndex"></param>
        /// <param name="oElement"></param>
        /// <returns></returns>
        private static bool TryElementAt<T>(IEnumerable<T> oData,int nIndex,out T oElement)
        {
            if (oData.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oData));

            oElement = default(T);

            if (nIndex < 0)
            {
                return false;
            }

            ICollection<T> oCollect = oData as ICollection<T>;

            if (oCollect != null)
            {
                int nCount = oCollect.Count;
                if (nIndex >= nCount)
                {
                    return false;
                }
               
                IList<T> oList = oData as IList<T>;
                if (oList != null)
                {
                    oElement = oList[nIndex];
                    return true;
                }
            }

            ICollection oNonGenericCollection = oData as ICollection;

            if (oNonGenericCollection != null)
            {
                int nCount = oNonGenericCollection.Count;
                if (nIndex >= nCount)
                {
                    return false;
                }
            }
           
            using (IEnumerator<T> oIter = oData.GetEnumerator())
            {
                for (int i = -1; i < nIndex; i++)
                {
                    if (!oIter.MoveNext())
                    {
                        return false;
                    }
                }
                oElement = oIter.Current;
                return true;
            }
        }
        #endregion
    }
}
