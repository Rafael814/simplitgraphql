﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace TotalApp.Package.TLINQ
{
    /// <summary>
    /// *Autor:Larry Ramirez
    /// *Fecha de creacion:09-01-2018
    /// *Version:1.1.20180109
    /// *Sinopsis:
    /// </summary>
    public static partial class THelperLinq
    {
        #region "Funciones"

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="oData"></param>
        /// <param name="oActionExec"></param>
        /// <returns></returns>
        public static IEnumerable<T> TForEachReturnElement<T>(this IEnumerable<T> oData, Action<T> oActionExec)
        {
            if (oData.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oData));
            if (oActionExec.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oActionExec));

            foreach (var oElement in oData)
            {
                yield return oElement;
                oActionExec(oElement);
            }
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="oData"></param>
        /// <param name="oActionExec"></param>
        /// <returns></returns>
        public static IEnumerable<T> TForEachReturnElementAsync<T>(this IEnumerable<T> oData, Action<T> oActionExec)
        {
            var oTask = new Task<IEnumerable<T>>(() => {
                return TForEachReturnElement(oData, oActionExec);
            });
            oTask.Start();

            return oTask.Result;
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="oData"></param>
        /// <param name="oPred"></param>
        /// <param name="oActionExec"></param>
        /// <returns></returns>
        public static IEnumerable<T> TForEachReturnElementIf<T>(this IEnumerable<T> oData
                                                              , Func<T, bool> oPred
                                                              , Action<T> oActionExec)
        {
            if (oData.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oData));
            if (oActionExec.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oActionExec));

            foreach (var oElement in oData)
            {
                if (oPred(oElement))
                {
                    yield return oElement;
                    oActionExec(oElement);
                }
            }
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="oData"></param>
        /// <param name="oPred"></param>
        /// <param name="oActionExec"></param>
        /// <returns></returns>
        public static IEnumerable<T> TForEachReturnElementIfAsync<T>(this IEnumerable<T> oData
                                                                   , Func<T, bool> oPred
                                                                   , Action<T> oActionExec)
        {
            var oTask = new Task<IEnumerable<T>>(() => {
                return TForEachReturnElementIf(oData, oPred, oActionExec);
            });
            oTask.Start();

            return oTask.Result;
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="oData"></param>
        /// <param name="oPred"></param>
        /// <param name="oActionExec"></param>
        /// <param name="oActionElse"></param>
        /// <returns></returns>
        public static IEnumerable<T> TForEachReturnElementIf<T>(this IEnumerable<T> oData
                                                              , Func<T, bool> oPred
                                                              , Action<T> oActionExec
                                                              , Action<T> oActionElse)
        {
            if (oData.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oData));
            if (oActionExec.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oActionExec));
            if (oActionElse.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oActionElse));

            foreach (var oElement in oData)
            {
                yield return oElement;

                if (oPred(oElement))
                {
                    oActionExec(oElement);
                }
                else
                {
                    oActionElse(oElement);
                }
            }
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="oData"></param>
        /// <param name="oPred"></param>
        /// <param name="oActionExec"></param>
        /// <param name="oActionElse"></param>
        /// <returns></returns>
        public static IEnumerable<T> TForEachReturnElementIfAsync<T>(this IEnumerable<T> oData
                                                                    , Func<T, bool> oPred
                                                                    , Action<T> oActionExec
                                                                    , Action<T> oActionElse)
        {
            var oTask = new Task<IEnumerable<T>>(() => {
                return TForEachReturnElementIf(oData, oPred, oActionExec, oActionElse);
            });
            oTask.Start();

            return oTask.Result;
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="oData"></param>
        /// <param name="oPred"></param>
        /// <param name="oActionExec"></param>
        /// <returns></returns>
        public static IEnumerable<T> TForEachReturnElementIfNot<T>(this IEnumerable<T> oData, Func<T, bool> oPred, Action<T> oActionExec)
        {
            if (oData.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oData));
            if (oActionExec.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oActionExec));

            foreach (var oElement in oData)
            {
                if (!oPred(oElement))
                {
                    yield return oElement;
                    oActionExec(oElement);
                }
            }
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="oData"></param>
        /// <param name="oPred"></param>
        /// <param name="oActionExec"></param>
        /// <returns></returns>
        public static IEnumerable<T> TForEachReturnElementIfNotAsync<T>(this IEnumerable<T> oData, Func<T, bool> oPred, Action<T> oActionExec)
        {
            var oTask = new Task<IEnumerable<T>>(() => {
                return TForEachReturnElementIfNot(oData, oPred, oActionExec);
            });
            oTask.Start();

            return oTask.Result;
        }


        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="oData"></param>
        /// <param name="oPred"></param>
        /// <param name="oActionExec"></param>
        /// <param name="oActionElse"></param>
        /// <returns></returns>
        public static IEnumerable<T> TForEachReturnElementIfNot<T>(this IEnumerable<T> oData
                                                              , Func<T, bool> oPred
                                                              , Action<T> oActionExec
                                                              , Action<T> oActionElse)
        {
            if (oData.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oData));
            if (oActionExec.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oActionExec));
            if (oActionElse.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oActionElse));

            foreach (var oElement in oData)
            {
                yield return oElement;

                if (!oPred(oElement))
                {
                    oActionExec(oElement);
                }
                else
                {
                    oActionElse(oElement);
                }
            }
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="oData"></param>
        /// <param name="oPred"></param>
        /// <param name="oActionExec"></param>
        /// <param name="oActionElse"></param>
        /// <returns></returns>
        public static IEnumerable<T> TForEachReturnElementIfNotAsync<T>(this IEnumerable<T> oData
                                                                      , Func<T, bool> oPred
                                                                      , Action<T> oActionExec
                                                                      , Action<T> oActionElse)
        {
            var oTask = new Task<IEnumerable<T>>(() => {
                return TForEachReturnElementIfNot(oData, oPred, oActionExec, oActionElse);
            });
            oTask.Start();

            return oTask.Result;
        }
        #endregion

        #region "Metodos"

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="oData"></param>
        /// <param name="oActionExec"></param>
        public static void TForEach<T>(this IEnumerable<T> oData, Action<T> oActionExec)
        {
            if (oData.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oData));
            if (oActionExec.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oActionExec));

            foreach (var oElement in oData)
            {
                oActionExec(oElement);
            }
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="oData"></param>
        /// <param name="oActionExec"></param>
        public static void TForEachAsync<T>(this IEnumerable<T> oData, Action<T> oActionExec)
        {
            var oTask = new Task(() => {
                TForEach(oData, oActionExec);
            });
            oTask.Start();
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="oData"></param>
        /// <param name="oActionExec"></param>
        public static void TForEachParallel<T>(this IEnumerable<T> oData, Action<T> oActionExec)
        {
            if (oData.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oData));
            if (oActionExec.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oActionExec));

            Parallel.ForEach(oData, new ParallelOptions(), (oItem) => oActionExec(oItem));
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="oData"></param>
        /// <param name="oActionExec"></param>
        public static void TForEach<T>(this IEnumerable<T> oData, params Action<T>[] oActionExec)
        {
            if (oData.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oData));
            if (oActionExec.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oActionExec));

            foreach (var oElement in oData)
            {
                for (int nIndex = 0; nIndex < oActionExec.Length; nIndex++)
                {
                    oActionExec[nIndex](oElement);
                }
            }
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="oData"></param>
        /// <param name="oActionExec"></param>
        public static void TForEachAsync<T>(this IEnumerable<T> oData, params Action<T>[] oActionExec)
        {
            var oTask = new Task(() => {
                TForEach(oData, oActionExec);
            });
            oTask.Start();
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="oData"></param>
        /// <param name="oActionExec"></param>
        public static void TForEach<T>(this IEnumerable<T> oData, Action<T, int> oActionExec)
        {
            if (oData.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oData));
            if (oActionExec.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oActionExec));

            var nIndex = 0;
            foreach (var oElement in oData)
            {
                oActionExec(oElement, nIndex++);
            }
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="oData"></param>
        /// <param name="oActionExec"></param>
        public static void TForEachAsync<T>(this IEnumerable<T> oData, Action<T, int> oActionExec)
        {
            var oTask = new Task(() => {
                TForEach(oData, oActionExec);
            });
            oTask.Start();
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="oData"></param>
        /// <param name="oPred"></param>
        /// <param name="oActionExec"></param>
        public static void TForEachIf<T>(this IEnumerable<T> oData, Func<T, bool> oPred, Action<T> oActionExec)
        {
            if (oData.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oData));
            if (oPred.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oPred));
            if (oActionExec.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oActionExec));

            foreach (var oElement in oData)
            {
                if (oPred(oElement))
                {
                    oActionExec(oElement);
                }
              
            }
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="oData"></param>
        /// <param name="oPred"></param>
        /// <param name="oActionExec"></param>
        public static void TForEachIfAsync<T>(this IEnumerable<T> oData, Func<T, bool> oPred, Action<T> oActionExec)
        {
            var oTask = new Task(() => {
                TForEachIf(oData, oPred, oActionExec);
            });
            oTask.Start();
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="oData"></param>
        /// <param name="oPred"></param>
        /// <param name="oActionExec"></param>
        /// <param name="oActionElse"></param>
        public static void TForEachIf<T>(this IEnumerable<T> oData
                                       , Func<T, bool> oPred
                                       , Action<T> oActionExec
                                       , Action<T> oActionElse)
        {
            if (oData.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oData));
            if (oPred.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oPred));
            if (oActionExec.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oActionExec));
            if (oActionElse.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oActionElse));

            foreach (var oElement in oData)
            {
                if (oPred(oElement))
                {
                    oActionExec(oElement);
                }
                else
                {
                    oActionElse(oElement);
                }

            }
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="oData"></param>
        /// <param name="oPred"></param>
        /// <param name="oActionExec"></param>
        /// <param name="oActionElse"></param>
        public static void TForEachIfAsync<T>(this IEnumerable<T> oData
                                           , Func<T, bool> oPred
                                           , Action<T> oActionExec
                                           , Action<T> oActionElse)
        {
            var oTask = new Task(() => {
                TForEachIf(oData, oPred, oActionExec, oActionElse);
            });
            oTask.Start();
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="oData"></param>
        /// <param name="oPred"></param>
        /// <param name="oActionExec"></param>
        public static void TForEachIfNot<T>(this IEnumerable<T> oData, Func<T, bool> oPred, Action<T> oActionExec)
        {
            if (oData.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oData));
            if (oPred.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oPred));
            if (oActionExec.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oActionExec));

            foreach (var oElement in oData)
            {
                if (!oPred(oElement))
                {
                    oActionExec(oElement);
                }
            }
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="oData"></param>
        /// <param name="oPred"></param>
        /// <param name="oActionExec"></param>
        public static void TForEachIfNotAsync<T>(this IEnumerable<T> oData, Func<T, bool> oPred, Action<T> oActionExec)
        {
            var oTask = new Task(() => {
                TForEachIfNot(oData, oPred, oActionExec);
            });
            oTask.Start();
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="oData"></param>
        /// <param name="oPred"></param>
        /// <param name="oActionExec"></param>
        /// <param name="oActionElse"></param>
        public static void TForEachIfNot<T>(this IEnumerable<T> oData
                                          , Func<T, bool> oPred
                                          , Action<T> oActionExec
                                          , Action<T> oActionElse)
        {
            if (oData.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oData));
            if (oPred.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oPred));
            if (oActionExec.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oActionExec));
            if (oActionElse.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oActionElse));

            foreach (var oElement in oData)
            {
                if (!oPred(oElement))
                {
                    oActionExec(oElement);
                }
                else
                {
                    oActionElse(oElement);
                }

            }
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="oData"></param>
        /// <param name="oPred"></param>
        /// <param name="oActionExec"></param>
        /// <param name="oActionElse"></param>
        public static void TForEachIfNotAsync<T>(this IEnumerable<T> oData
                                                , Func<T, bool> oPred
                                                , Action<T> oActionExec
                                                , Action<T> oActionElse)
        {
            var oTask = new Task(() => {
                TForEachIfNot(oData, oPred, oActionExec, oActionElse);
            });
            oTask.Start();
        }
        #endregion
    }



}
