﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace TotalApp.Package.TLINQ
{
    /// <summary>
    /// *Autor:Larry Ramirez
    /// *Fecha de creacion:09-01-2018
    /// *Version:1.1.20180109
    /// *Sinopsis:
    /// </summary>
    public static partial class THelperLinq
    {
        #region "Metodos"
        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="oData"></param>
        /// <param name="oPred"></param>
        /// <returns></returns>
        public static void TRemoveItem<T>(this IEnumerable<T> oData, Func<T, bool> oPred)
        {
            if (oData.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oData));
            if (oPred.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oPred));

            if (oData is IList<T> oList)
            {
                var oResp = oData.TFirstOrDefaultIf(oPred);

                if (!oResp.TIsNull())
                {
                    oResp.TAction((x) =>
                    {
                        if (x != null)
                        {
                            oList.Remove(x);
                        }

                    });
                }
            }
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="oData"></param>
        /// <param name="oPred"></param>
        /// <returns></returns>
        public static void TRemoveItemAsync<T>(this IEnumerable<T> oData, Func<T, bool> oPred)
        {
            var oTask = new Task(() => {
                TRemoveItem(oData, oPred);
            });
            oTask.Start();
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="oData"></param>
        /// <param name="oPred"></param>
        /// <returns></returns>
        public static void TRemoveAll<T>(this IEnumerable<T> oData, Func<T, bool> oPred)
        {
            if (oData.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oData));
            if (oPred.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oPred));

            if (oData is IList<T> oList)
            {
                foreach (var oElement in oData)
                {
                    if (oElement != null)
                    {
                        oList.TRemoveItem(oPred);
                    }
                }
            }

        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="oData"></param>
        /// <param name="oPred"></param>
        /// <returns></returns>
        public static void TRemoveAllAsync<T>(this IEnumerable<T> oData, Func<T, bool> oPred)
        {
            var oTask = new Task(() => {
                TRemoveAll(oData, oPred);
            });
            oTask.Start();

        }
        #endregion
    }
}
