﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace TotalApp.Package.TLINQ
{
    /// <summary>
    /// *Autor:Larry Ramirez
    /// *Fecha de creacion:09-01-2018
    /// *Version:1.1.20180109
    /// *Sinopsis:
    /// </summary>
    public static partial class THelperLinq
    {
        #region "Metodos"
        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="oData"></param>
        /// <param name="oPred"></param>
        /// <param name="oActionExec"></param>
        /// <returns></returns>
        public static void TUpdateAllItem<T>(this IEnumerable<T> oData, Func<T, bool> oPred, Action<T> oActionExec)
        {
            if (oData.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oData));
            if (oPred.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oPred));

            if (oData is IList<T> oList)
            {
                foreach (var oElement in oData)
                {
                    if (oElement != null)
                    {
                        if (oPred(oElement))
                        {
                            oActionExec(oElement);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="oData"></param>
        /// <param name="oPred"></param>
        /// <param name="oActionExec"></param>
        /// <returns></returns>
        public static void TUpdateAllItemAsync<T>(this IEnumerable<T> oData, Func<T, bool> oPred, Action<T> oActionExec)
        {
            var oTask = new Task(() => {
                TUpdateAllItem(oData, oPred, oActionExec);
            });
            oTask.Start();
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="oData"></param>
        /// <param name="oPred"></param>
        /// <param name="oActionExec"></param>
        /// <returns></returns>
        public static void TUpdateItem<T>(this IEnumerable<T> oData, Func<T, bool> oPred, Action<T> oActionExec)
        {
            if (oData.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oData));
            if (oPred.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oPred));

            if (oData is IList<T> oList)
            {
                var oResp = oData.TFirstOrDefaultIf(oPred);

                if (!oResp.TIsNull())
                {
                    oResp.TAction((x) =>
                    {
                        if (x != null)
                        {
                            oActionExec(x);
                        }

                    });
                }
            }
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="oData"></param>
        /// <param name="oPred"></param>
        /// <param name="oActionExec"></param>
        /// <returns></returns>
        public static void TUpdateItemAsync<T>(this IEnumerable<T> oData, Func<T, bool> oPred, Action<T> oActionExec)
        {
            var oTask = new Task(() => {
                TUpdateItem(oData, oPred, oActionExec);
            });
            oTask.Start();
        }
        #endregion
    }
}
