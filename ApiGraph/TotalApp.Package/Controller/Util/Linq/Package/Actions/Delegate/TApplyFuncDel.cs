﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace TotalApp.Package.TLINQ
{
    /// <summary>
    /// *Autor:Larry Ramirez
    /// *Fecha de creacion:09-01-2018
    /// *Version:1.1.20180109
    /// *Sinopsis:
    /// </summary>
    public static partial class THelperLinq
    {
        #region "Funciones"
        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="oData"></param>
        /// <param name="oDel"></param>
        /// <returns></returns>
        public static IEnumerable<T> TApplyFuncDelIf<T>(this IEnumerable<T> oData
                                                      , TLQDelFunc<T> oDel)
        {
            foreach (var oItem in oData)
            {
                if (oDel(oItem))
                {
                    yield return oItem;
                }
            }
            yield break;
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="oData"></param>
        /// <param name="oDel"></param>
        /// <returns></returns>
        public static IEnumerable<T> TApplyFuncDelIfAsync<T>(this IEnumerable<T> oData
                                                           , TLQDelFunc<T> oDel)
        {
            var oTask = new Task<IEnumerable<T>>(() => {
               return TApplyFuncDelIf(oData, oDel);
            });
            oTask.Start();

            return oTask.Result;
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="oData"></param>
        /// <param name="oDel"></param>
        /// <returns></returns>
        public static IEnumerable<T> TApplyFuncDelIfNot<T>(this IEnumerable<T> oData
                                                         , TLQDelFunc<T> oDel)
        {
            foreach (var oItem in oData)
            {
                if (!oDel(oItem))
                {
                    yield return oItem;
                }
            }
            yield break;
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="oData"></param>
        /// <param name="oDel"></param>
        /// <returns></returns>
        public static IEnumerable<T> TApplyFuncDelIfNotAsync<T>(this IEnumerable<T> oData
                                                         , TLQDelFunc<T> oDel)
        {

            var oTask = new Task<IEnumerable<T>>(() => {
                return TApplyFuncDelIfNot(oData, oDel);
            });
            oTask.Start();

            return oTask.Result;
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="oData"></param>
        /// <param name="oDel"></param>
        /// <returns></returns>
        public static T TApplyFuncItemDelIf<T>(this IEnumerable<T> oData
                                             , TLQDelFunc<T> oDel)
        {
            foreach (var oItem in oData)
            {
                if (oDel(oItem))
                {
                    return oItem;
                }
            }
            return default(T);
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="oData"></param>
        /// <param name="oDel"></param>
        /// <returns></returns>
        public static T TApplyFuncItemDelIfAsync<T>(this IEnumerable<T> oData
                                                   , TLQDelFunc<T> oDel)
        {
            var oTask = new Task<T>(() => {
                return TApplyFuncItemDelIf(oData, oDel);
            });
            oTask.Start();

            return oTask.Result;
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="oData"></param>
        /// <param name="oDel"></param>
        /// <returns></returns>
        public static T TApplyFuncItemDelIfNot<T>(this IEnumerable<T> oData
                                                , TLQDelFunc<T> oDel)
        {
            foreach (var oItem in oData)
            {
                if (!oDel(oItem))
                {
                    return oItem;
                }
            }
            return default(T);
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="oData"></param>
        /// <param name="oDel"></param>
        /// <returns></returns>
        public static T TApplyFuncItemDelIfNotAsync<T>(this IEnumerable<T> oData
                                                     , TLQDelFunc<T> oDel)
        {
            var oTask = new Task<T>(() => {
                return TApplyFuncItemDelIfNot(oData, oDel);
            });
            oTask.Start();

            return oTask.Result;
        }
        #endregion


    }
}
