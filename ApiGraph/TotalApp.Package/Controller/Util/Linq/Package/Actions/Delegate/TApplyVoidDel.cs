﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace TotalApp.Package.TLINQ
{
    /// <summary>
    /// *Autor:Larry Ramirez
    /// *Fecha de creacion:09-01-2018
    /// *Version:1.1.20180109
    /// *Sinopsis:
    /// </summary>
    public static partial class THelperLinq
    {
        #region "Metodos"
        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj"></param>
        /// <param name="oDel"></param>
        public static void TApplyVoidDel<T>(this T obj, TLQDelVoid<T> oDel)
        {
            if (obj.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(obj));
            if (oDel.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oDel));

            oDel(obj);
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj"></param>
        /// <param name="oDel"></param>
        public static void TApplyVoidDelAsync<T>(this T obj, TLQDelVoid<T> oDel)
        {
            var oTask = new Task(() => {
                TApplyVoidDel(obj, oDel);
            });
            oTask.Start();
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj"></param>
        /// <param name="oDel"></param>
        public static void TApplyVoidDel<T>(this T obj, params TLQDelVoid<T>[] oDel)
        {
            if (obj.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(obj));
            if (oDel.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oDel));

            for (int nIndex = 0; nIndex < oDel.Length; nIndex++)
            {
                oDel[nIndex](obj);
            }
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj"></param>
        /// <param name="oDel"></param>
        public static void TApplyVoidDelAsync<T>(this T obj, params TLQDelVoid<T>[] oDel)
        {
            var oTask = new Task(() => {
                TApplyVoidDel(obj, oDel);
            });
            oTask.Start();
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj"></param>
        /// <param name="oPred"></param>
        /// <param name="oDel"></param>
        public static void TApplyVoidDelIf<T>(this T obj, Func<T, bool> oPred, TLQDelVoid<T> oDel)
        {
            if (obj.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(obj));
            if (oPred.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oPred));
            if (oDel.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oDel));

            if (oPred(obj))
            {
                oDel(obj);
            }
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj"></param>
        /// <param name="oPred"></param>
        /// <param name="oDel"></param>
        public static void TApplyVoidDelIfAsync<T>(this T obj, Func<T, bool> oPred, TLQDelVoid<T> oDel)
        {
            var oTask = new Task(() => {
                TApplyVoidDelIf(obj, oPred, oDel);
            });
            oTask.Start();
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj"></param>
        /// <param name="oPred"></param>
        /// <param name="oDel"></param>
        /// <param name="oActionElse"></param>
        public static void TApplyVoidDelIf<T>(this T obj, Func<T, bool> oPred, TLQDelVoid<T> oDel, TLQDelVoid<T> oDelElse)
        {
            if (obj.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(obj));
            if (oPred.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oPred));
            if (oDel.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oDel));
            if (oDelElse.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oDelElse));

            if (oPred(obj))
            {
                oDel(obj);
            }
            else
            {
                oDelElse(obj);
            }
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj"></param>
        /// <param name="oPred"></param>
        /// <param name="oDel"></param>
        /// <param name="oActionElse"></param>
        public static void TApplyVoidDelIfAsync<T>(this T obj, Func<T, bool> oPred, TLQDelVoid<T> oDel, TLQDelVoid<T> oDelElse)
        {
            var oTask = new Task(() => {
                TApplyVoidDelIf(obj, oPred, oDel, oDelElse);
            });
            oTask.Start();
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj"></param>
        /// <param name="oPred"></param>
        /// <param name="oDel"></param>
        public static void TApplyVoidDelIfNot<T>(this T obj, Func<T, bool> oPred, TLQDelVoid<T> oDel)
        {
            if (obj.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(obj));
            if (oPred.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oPred));
            if (oDel.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oDel));

            if (!oPred(obj))
            {
                oDel(obj);
            }
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj"></param>
        /// <param name="oPred"></param>
        /// <param name="oDel"></param>
        public static void TApplyVoidDelIfNotAsync<T>(this T obj, Func<T, bool> oPred, TLQDelVoid<T> oDel)
        {
            var oTask = new Task(() => {
                TApplyVoidDelIfNot(obj, oPred, oDel);
            });
            oTask.Start();
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj"></param>
        /// <param name="oPred"></param>
        /// <param name="oDel"></param>
        /// <param name="oActionElse"></param>
        public static void TApplyVoidDelIfNot<T>(this T obj, Func<T, bool> oPred, TLQDelVoid<T> oDel, TLQDelVoid<T> oDelElse)
        {
            if (obj.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(obj));
            if (oPred.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oPred));
            if (oDel.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oDel));
            if (oDelElse.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oDelElse));

            if (!oPred(obj))
            {
                oDel(obj);
            }
            else
            {
                oDelElse(obj);
            }
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj"></param>
        /// <param name="oPred"></param>
        /// <param name="oDel"></param>
        /// <param name="oActionElse"></param>
        public static void TApplyVoidDelIfNotAsync<T>(this T obj, Func<T, bool> oPred, TLQDelVoid<T> oDel, TLQDelVoid<T> oDelElse)
        {
            var oTask = new Task(() => {
                TApplyVoidDelIfNotAsync(obj, oPred, oDel, oDelElse);
            });
            oTask.Start();
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="oData"></param>
        /// <param name="oDel"></param>
        public static void TApplyVoidDel<T>(this IEnumerable<T> oData
                                          , TLQDelVoid<T> oDel)
        {
            foreach (var oItem in oData)
            {
                oDel(oItem);
            }
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="oData"></param>
        /// <param name="oDel"></param>
        public static void TApplyVoidDelAsync<T>(this IEnumerable<T> oData
                                               , TLQDelVoid<T> oDel)
        {
            var oTask = new Task(() => {
                TApplyVoidDel(oData, oDel);
            });
            oTask.Start();
        }
        #endregion
    }
}
