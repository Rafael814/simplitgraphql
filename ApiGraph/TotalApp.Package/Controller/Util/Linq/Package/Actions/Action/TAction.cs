﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace TotalApp.Package.TLINQ
{
    /// <summary>
    /// *Autor:Larry Ramirez
    /// *Fecha de creacion:09-01-2018
    /// *Version:1.1.20180109
    /// *Sinopsis:
    /// </summary>
    public static partial class THelperLinq
    {
        #region "Metodos"
        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="oActionExec"></param>
        public static void TRunAsAction(Action oActionExec)
        {
            try
            {
                oActionExec();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="oActionExec"></param>
        public static void TRunAsActionAsync(Action oActionExec)
        {
            try
            {
                var oTask = new Task(oActionExec);
                oTask.Start();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj"></param>
        /// <param name="oActionExec"></param>
        public static void TAction<T>(this T obj, Action<T> oActionExec)
        {
            if (obj.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(obj));
            if (oActionExec.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oActionExec));

            oActionExec(obj);
        }


        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj"></param>
        /// <param name="oActionExec"></param>
        public static void TActionAsync<T>(this T obj, Action<T> oActionExec)
        {
            var oTask = new Task(() => {
                TAction(obj, oActionExec);
            });
            oTask.Start();
        }


        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj"></param>
        /// <param name="oActionExec"></param>
        public static void TAction<T>(this T obj, params Action<T>[] oActionExec)
        {
            if (obj.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(obj));
            if (oActionExec.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oActionExec));

            for (int nIndex = 0; nIndex < oActionExec.Length; nIndex++)
            {
                oActionExec[nIndex](obj);
            }
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj"></param>
        /// <param name="oActionExec"></param>
        public static void TActionAsync<T>(this T obj, params Action<T>[] oActionExec)
        {
            var oTask = new Task(() => {
                TAction(obj, oActionExec);
            });
            oTask.Start();
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj"></param>
        /// <param name="oPred"></param>
        /// <param name="oActionExec"></param>
        public static void TActionIf<T>(this T obj, Func<T, bool> oPred, Action<T> oActionExec)
        {
            if (obj.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(obj));
            if (oPred.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oPred));
            if (oActionExec.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oActionExec));

            if (oPred(obj))
            {
                oActionExec(obj);
            }
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj"></param>
        /// <param name="oPred"></param>
        /// <param name="oActionExec"></param>
        public static void TActionIfAsync<T>(this T obj, Func<T, bool> oPred, Action<T> oActionExec)
        {
            var oTask = new Task(() => {
                TActionIf(obj, oPred, oActionExec);
            });
            oTask.Start();
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj"></param>
        /// <param name="oPred"></param>
        /// <param name="oActionExec"></param>
        /// <param name="oActionElse"></param>
        public static void TActionIf<T>(this T obj, Func<T, bool> oPred, Action<T> oActionExec, Action<T> oActionElse)
        {
            if (obj.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(obj));
            if (oPred.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oPred));
            if (oActionExec.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oActionExec));
            if (oActionElse.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oActionElse));

            if (oPred(obj))
            {
                oActionExec(obj);
            }
            else
            {
                oActionElse(obj);
            }
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj"></param>
        /// <param name="oPred"></param>
        /// <param name="oActionExec"></param>
        /// <param name="oActionElse"></param>
        public static void TActionIfAsync<T>(this T obj, Func<T, bool> oPred, Action<T> oActionExec, Action<T> oActionElse)
        {
            var oTask = new Task(() => {
                TActionIf(obj, oPred, oActionExec, oActionElse);
            });
            oTask.Start();
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj"></param>
        /// <param name="oPred"></param>
        /// <param name="oActionExec"></param>
        public static void TActionIfNot<T>(this T obj, Func<T, bool> oPred, Action<T> oActionExec)
        {
            if (obj.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(obj));
            if (oPred.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oPred));
            if (oActionExec.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oActionExec));

            if (!oPred(obj))
            {
                oActionExec(obj);
            }
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj"></param>
        /// <param name="oPred"></param>
        /// <param name="oActionExec"></param>
        public static void TActionIfNotAsync<T>(this T obj, Func<T, bool> oPred, Action<T> oActionExec)
        {
            var oTask = new Task(() => {
                TActionIfNot(obj, oPred, oActionExec);
            });
            oTask.Start();
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj"></param>
        /// <param name="oPred"></param>
        /// <param name="oActionExec"></param>
        /// <param name="oActionElse"></param>
        public static void TActionIfNot<T>(this T obj, Func<T, bool> oPred, Action<T> oActionExec, Action<T> oActionElse)
        {
            if (obj.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(obj));
            if (oPred.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oPred));
            if (oActionExec.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oActionExec));
            if (oActionElse.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oActionElse));

            if (!oPred(obj))
            {
                oActionExec(obj);
            }
            else
            {
                oActionElse(obj);
            }
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj"></param>
        /// <param name="oPred"></param>
        /// <param name="oActionExec"></param>
        /// <param name="oActionElse"></param>
        public static void TActionIfNotAsync<T>(this T obj, Func<T, bool> oPred, Action<T> oActionExec, Action<T> oActionElse)
        {
            var oTask = new Task(() => {
                TActionIfNot(obj, oPred, oActionExec, oActionElse);
            });
            oTask.Start();

        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="oPred"></param>
        /// <returns></returns>
        public static T ExecuteFunc<T>(Func<T> oPred)
        {
            try
            {
                T oResult = oPred();
                return oResult;
            }
            catch
            {
                return default(T);
            }
        }
        #endregion
    }
}
