﻿using System;
using System.Collections.Generic;

namespace TotalApp.Package.TLINQ
{
    /// <summary>
    /// *Autor:Larry Ramirez
    /// *Fecha de creacion:09-01-2018
    /// *Version:1.1.20180109
    /// *Sinopsis:
    /// </summary>
    public static partial class THelperLinq
    {
        #region "Funciones"

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="oData"></param>
        /// <param name="oPred"></param>
        /// <returns></returns>
        public static bool TIfFunc<T>(this T oData, Func<T, bool> oPred)
        {
            if (oData.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oData));
            if (oPred.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oPred));

            if (oPred(oData))
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="oData"></param>
        /// <param name="oPred"></param>
        /// <returns></returns>
        public static bool TIfAnyFunc<T>(this T oData, params Func<T, bool>[] oPred)
        {
            if (oData.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oData));
            if (oPred.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oPred));

            for (int nIndex = 0; nIndex < oPred.Length; nIndex++)
            {
                if (oPred[nIndex](oData))
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="oData"></param>
        /// <param name="oPred"></param>
        /// <returns></returns>
        public static bool TIfNotFunc<T>(this T oData, Func<T, bool> oPred)
        {
            if (oData.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oData));
            if (oPred.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oPred));

            if (!oPred(oData))
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="oData"></param>
        /// <param name="oPred"></param>
        /// <returns></returns>
        public static bool TIfNotFunc<T>(this T oData, params Func<T, bool>[] oPred)
        {
            if (oData.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oData));
            if (oPred.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oPred));


            for (int nIndex = 0; nIndex < oPred.Length; nIndex++)
            {
                if (!oPred[nIndex](oData))
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="oData"></param>
        /// <param name="oPred"></param>
        /// <returns></returns>
        public static bool TIfAnyFunc<T>(this IEnumerable<T> oData, params Func<T, bool>[] oPred)
        {
            if (oData.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oData));
            if (oPred.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oPred));

            foreach (var oElement in oData)
            {
                foreach (var oP in oPred)
                {
                    if (oP(oElement))
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="oData"></param>
        /// <param name="oPred"></param>
        /// <returns></returns>
        public static bool TIfAnyNotFunc<T>(this IEnumerable<T> oData, params Func<T, bool>[] oPred)
        {
            if (oData.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oData));
            if (oPred.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oPred));

            foreach (var oElement in oData)
            {
                foreach (var oP in oPred)
                {
                    if (!oP(oElement))
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="oData"></param>
        /// <param name="oPred"></param>
        /// <returns></returns>
        public static bool TIfAllFunc<T>(this IEnumerable<T> oData, params Func<T, bool>[] oPred)
        {
            if (oData.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oData));
            if (oPred.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oPred));

            foreach (var oElement in oData)
            {
                for (int nIndex = 0; nIndex < oPred.Length; nIndex++)
                {
                    if (!oPred[nIndex](oElement))
                    {
                        return false;
                    }
                }
            }

            return true;
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="oData"></param>
        /// <param name="oPred"></param>
        /// <returns></returns>
        public static bool TIfAllNotFunc<T>(this IEnumerable<T> oData, params Func<T, bool>[] oPred)
        {
            if (oData.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oData));
            if (oPred.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oPred));

            foreach (var oElement in oData)
            {
                for (int nIndex = 0; nIndex < oPred.Length; nIndex++)
                {
                    if (oPred[nIndex](oElement))
                    {
                        return false;
                    }
                }
            }

            return true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="oPred"></param>
        /// <returns></returns>
        public static Func<T, bool> TIf<T>(this Func<T, bool> oPred) => oValor => oPred(oValor);


        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="oPred"></param>
        /// <returns></returns>
        public static Func<T, bool> TNot<T>(this Func<T, bool> oPred) => oValor => !oPred(oValor);


        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="oLeft"></param>
        /// <param name="oRight"></param>
        /// <returns></returns>
        public static Func<T, bool> TAnd<T>(this Func<T, bool> oLeft, Func<T, bool> oRight) => oValor => oLeft(oValor) && oRight(oValor);


        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="oLeft"></param>
        /// <param name="oRight"></param>
        /// <returns></returns>
        public static Func<T, bool> TOr<T>(this Func<T, bool> oLeft, Func<T, bool> oRight) => oValor => oLeft(oValor) || oRight(oValor);

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="U"></typeparam>
        /// <typeparam name="W"></typeparam>
        /// <param name="oLeft"></param>
        /// <param name="oRight"></param>
        /// <returns></returns>
        public static Func<T, W> TOr<T, U, W>(this Func<U, W> oLeft, Func<T, U> oRight) => oValue => oLeft(oRight(oValue));

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="U"></typeparam>
        /// <param name="oPred"></param>
        /// <returns></returns>
        public static Func<T, U> TCFunc<T, U>(Func<T, U> oPred) => oPred;
        #endregion
    }
}
