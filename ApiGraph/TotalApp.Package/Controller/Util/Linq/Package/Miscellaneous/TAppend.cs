﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace TotalApp.Package.TLINQ
{
    /// <summary>
    /// *Autor:Larry Ramirez
    /// *Fecha de creacion:09-01-2018
    /// *Version:1.1.20180109
    /// *Sinopsis:
    /// </summary>
    public static partial class THelperLinq
    {
        #region "Funciones"
        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:23-01-2018
        /// *Version:1.1.20180123
        /// *Sinopsis:
        /// </summary>
        /// <param name="oData"></param>
        /// <param name="oParams"></param>
        /// <returns></returns>
        public static IEnumerable TAppend(this IEnumerable oData, params object[] oParams)
        {
            return oData.TOfType<object>().Concat(oParams);
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:23-01-2018
        /// *Version:1.1.20180123
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="oData"></param>
        /// <param name="oParams"></param>
        /// <returns></returns>
        public static IEnumerable<T> TAppend<T>(this IEnumerable<T> oData, params T[] oParams)
        {
            return oData.Concat(oParams);
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:23-01-2018
        /// *Version:1.1.20180123
        /// *Sinopsis:
        /// </summary>
        /// <param name="oData"></param>
        /// <param name="oParams"></param>
        /// <returns></returns>
        public static IEnumerable TPrepend(this IEnumerable oData, params object[] oParams)
        {
            return oParams.Concat(oData.TOfType<object>());
        }

        #endregion
    }
}
