﻿using System;
using System.Collections.Generic;

namespace TotalApp.Package.TLINQ
{
    /// <summary>
    /// *Autor:Larry Ramirez
    /// *Fecha de creacion:09-01-2018
    /// *Version:1.1.20180109
    /// *Sinopsis:
    /// </summary>
    public static partial class THelperLinq
    {
        #region "Funciones"
        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="oArray"></param>
        /// <param name="oValue"></param>
        /// <returns></returns>
        public static int TLqIndexOf<T>(this T[] oArray, T oValue) where T : class
        {
            if (oArray.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oArray));
            if (oValue.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oValue));

            for (int nIndex = 0; nIndex < oArray.Length; nIndex++)
            {
                if (oArray[nIndex] == oValue)
                {
                    return nIndex;
                }
            }

            return -1;
        }


        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="oData"></param>
        /// <param name="oPred"></param>
        /// <returns></returns>
        public static int TLqIndexOf<T>(this IEnumerable<T> oData, Func<T, bool> oPred)
        {
            if (oData.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oData));
            if (oPred.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oPred));

            int nIndex = 0;
            foreach (var oItem in oData)
            {
                if (oPred(oItem))
                {
                    return nIndex;
                }
                nIndex++;
            }
            return -1;
        }


        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="oData"></param>
        /// <param name="oItem"></param>
        /// <returns></returns>
        public static int TLqIndexOf<T>(this IEnumerable<T> oData, T oItem)
        {
            return oData.TLqIndexOf(nIndex => EqualityComparer<T>.Default.Equals(oItem, nIndex));
        }
        #endregion
    }
}
