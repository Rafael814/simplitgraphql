﻿using System.Text;
using System.Collections.Generic;

namespace TotalApp.Package.TLINQ
{
    /// <summary>
    /// *Autor:Larry Ramirez
    /// *Fecha de creacion:09-01-2018
    /// *Version:1.1.20180109
    /// *Sinopsis:
    /// </summary>
    public static partial class THelperLinq
    {
        #region "Funciones"

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="oData"></param>
        /// <param name="sSeparador"></param>
        /// <returns></returns>
        public static string TLqToString<T>(this IEnumerable<T> oData, string sSeparador)
        {
            if (oData.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oData));
            if (sSeparador.TIsNullOrEmpty()) THelperInfoEstatus.ArgumentNull(nameof(sSeparador));

            StringBuilder sSb = new StringBuilder();
            foreach (var obj in oData)
            {
                if (sSb.Length > 0)
                {
                    sSb.Append(sSeparador);
                }
                sSb.Append(obj);
            }
            return sSb.ToString();
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="oData"></param>
        /// <returns></returns>
        public static string TLqToString<T>(this IEnumerable<T> oData)
        {
            if (oData.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oData));

            StringBuilder sSb = new StringBuilder();
            foreach (var obj in oData)
            {
                sSb.Append(obj);
            }
            return sSb.ToString();
        }
        #endregion
    }
}
