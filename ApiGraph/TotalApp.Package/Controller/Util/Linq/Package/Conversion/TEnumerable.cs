﻿using System.Collections.Generic;
using System.Collections.ObjectModel;


namespace TotalApp.Package.TLINQ
{
    /// <summary>
    /// *Autor:Larry Ramirez
    /// *Fecha de creacion:09-01-2018
    /// *Version:1.1.20180109
    /// *Sinopsis:
    /// </summary>
    public static partial class THelperLinq
    {
        #region "Funciones"
        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="oData"></param>
        /// <returns></returns>
        public static IEnumerable<T> TAsEnumerable<T>(this IEnumerable<T> oData) => oData;


        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="oData"></param>
        /// <returns></returns>
        public static IEnumerable<T> TAsEnumerable<T>(this object oData)
        {

            if (oData is System.Collections.IEnumerable oIe)
            {
                return oIe.TCast<T>();
            }

            if (oData is IEnumerable<T> oList)
            {
                return oList.TCast<T>();
            }

            if (oData is T oD)
            {
                return _(); IEnumerable<T> _()
                {
                    yield return oD;
                }
            }

            return default(IEnumerable<T>);
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:23-01-2018
        /// *Version:1.1.20180123
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="oData"></param>
        /// <returns></returns>
        public static IEnumerable<T> TOf<T>(T oData)
        {
            if (oData.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oData));
            return new List<T> { oData };
        }


        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:23-01-2018
        /// *Version:1.1.20180123
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="oData"></param>
        /// <param name="oItems"></param>
        /// <returns></returns>
        public static IEnumerable<T> TWithItems<T>(this IEnumerable<T> oData, params T[] oItems)
        {
            if (oData.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oData));

            return _(); IEnumerable<T> _()
            {
                foreach (var oItem in oData)
                {
                    yield return oItem;
                }
            }
        }


        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:23-01-2018
        /// *Version:1.1.20180123
        /// *Sinopsis:Convertir directamente un objeto de tipo IEnumerable en ObservableCollection 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="oData"></param>
        /// <returns></returns>
        public static ObservableCollection<T> TAsObservableCollection<T>(this IEnumerable<T> oData)
        {
            if (oData.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oData));

            return new ObservableCollection<T>(oData);
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:23-01-2018
        /// *Version:1.1.20180123
        /// *Sinopsis:Agregar uno por uno los elementos del objeto de tipo IEnumerable en ObservableCollection
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="oData"></param>
        /// <returns></returns>
        public static ObservableCollection<T> TAsObservableCollectionItemSource<T>(this IEnumerable<T> oData)
        {
            if (oData.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oData));

            var oItemSource = new ObservableCollection<T>();

            foreach (var oItem in oData)
            {
                oItemSource.Add(oItem);
            }
            return oItemSource;
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:23-01-2018
        /// *Version:1.1.20180123
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="oData"></param>
        /// <returns></returns>
        public static IEnumerable<T> TAsCollectionItems<T>(this IEnumerable<T> oData)
        {
            if (oData.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oData));

            foreach (var oItem in oData)
            {
                yield return (T)oItem;
            }
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:23-01-2018
        /// *Version:1.1.20180123
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="oData"></param>
        /// <returns></returns>
        public static HashSet<T> TAsHashSet<T>(this IEnumerable<T> oData)
        {
            return new HashSet<T>(oData);
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:23-01-2018
        /// *Version:1.1.20180123
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="oData"></param>
        /// <param name="nSkip"></param>
        /// <param name="nTake"></param>
        /// <returns></returns>
        public static IEnumerable<T> TPaginate<T>(this IEnumerable<T> oData, int nSkip, int nTake) => oData.TSkip(nSkip).TTake(nTake);

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:23-01-2018
        /// *Version:1.1.20180123
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="oData"></param>
        /// <param name="nSkip"></param>
        /// <param name="nTake"></param>
        /// <returns></returns>
        public static List<T> TPaginateToList<T>(this IEnumerable<T> oData, int nSkip, int nTake) => oData.TSkip(nSkip).TTake(nTake).TToList();
        #endregion

        #region "Metodos"
        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:23-01-2018
        /// *Version:1.1.20180123
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="oData"></param>
        /// <returns></returns>
        public static void TConsumeColletion<T>(this IEnumerable<T> oData)
        {
            if (oData.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oData));

            foreach (var oItem in oData)
            {

            }
        }
        #endregion
    }
}
