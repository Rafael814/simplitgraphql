﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace TotalApp.Package.TLINQ
{
    /// <summary>
    /// *Autor:Larry Ramirez
    /// *Fecha de creacion:09-01-2018
    /// *Version:1.1.20180109
    /// *Sinopsis:
    /// </summary>
    public static partial class THelperLinq
    {
        #region "Funciones"
        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="oData"></param>
        /// <returns></returns>
        public static List<T> TToList<T>(this IEnumerable<T> oData)
        {
            if (oData.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oData));

            return new List<T>(oData);
        }


        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// *Ejemplo: var oList = typeof(Enum).TToList();
        /// </summary>
        /// <param name="oEnumType"></param>
        /// <returns></returns>
        public static IEnumerable<KeyValuePair<string, string>> TToList(this Type oEnumType)

        {
            if (oEnumType.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oEnumType));

            return Enum.GetNames(oEnumType).Select((sName, nIndex) =>
                   new KeyValuePair<string, string>(Enum.GetValues(oEnumType).GetValue(nIndex).GetHashCode().ToString(), sName)).TToList();

        }


        #endregion
    }
}
