﻿using System;
using System.Collections.Generic;

namespace TotalApp.Package.TLINQ
{
    /// <summary>
    /// *Autor:Larry Ramirez
    /// *Fecha de creacion:09-01-2018
    /// *Version:1.1.20180109
    /// *Sinopsis:
    /// </summary>
    public static partial class THelperLinq
    {
        #region "Funciones"
        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="oData"></param>
        /// <returns></returns>
        public static T[] TToArray<T>(this IEnumerable<T> oData)
        {
            if (oData.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oData));

            ICollection<T> oCollet = oData as ICollection<T>;

            if (oCollet != null)
            {
                T[] oTmp = new T[oCollet.Count];
                oCollet.CopyTo(oTmp, 0);
                return oTmp;
            }

            T[] oResp = new T[16];
            int nCount = 0;
            foreach (T oItem in oData)
            {
                if (nCount == oResp.Length)
                {
                    Array.Resize(ref oResp, oResp.Length * 2);
                }
                oResp[nCount++] = oItem;
            }


            if (nCount != oResp.Length)
            {
                Array.Resize(ref oResp, nCount);
            }
            return oResp;
        }
        #endregion
    }
}
