﻿using System;
using System.Collections.Generic;

namespace TotalApp.Package.TLINQ
{
    /// <summary>
    /// *Autor:Larry Ramirez
    /// *Fecha de creacion:09-01-2018
    /// *Version:1.1.20180109
    /// *Sinopsis:
    /// </summary>
    public static partial class THelperLinq
    {
        #region "Funciones"
        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="ToF"></typeparam>
        /// <typeparam name="ToS"></typeparam>
        /// <typeparam name="TResult"></typeparam>
        /// <param name="oF"></param>
        /// <param name="oS"></param>
        /// <param name="oFunc"></param>
        /// <returns></returns>
        public static IEnumerable<TResult> TZip<ToF, ToS, TResult>(this IEnumerable<ToF> oF, IEnumerable<ToS> oS, Func<ToF, ToS, TResult> oFunc)
        {
            if (oF == null) THelperInfoEstatus.ArgumentNull(nameof(oF));
            if (oS == null) THelperInfoEstatus.ArgumentNull(nameof(oS));
            if (oFunc == null) THelperInfoEstatus.ArgumentNull(nameof(oFunc));

            return ZipIterator(oF, oS, oFunc);
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="ToF"></typeparam>
        /// <typeparam name="ToS"></typeparam>
        /// <typeparam name="TResult"></typeparam>
        /// <param name="oF"></param>
        /// <param name="oS"></param>
        /// <param name="oFunc"></param>
        /// <returns></returns>
        private static IEnumerable<TResult> ZipIterator<ToF, ToS, TResult>(IEnumerable<ToF> oF, IEnumerable<ToS> oS, Func<ToF, ToS, TResult> oFunc)
        {
            using (IEnumerator<ToF> e1 = oF.GetEnumerator())
            {
                using (IEnumerator<ToS> e2 = oS.GetEnumerator())
                {
                    while (e1.MoveNext() && e2.MoveNext())
                    {
                        yield return oFunc(e1.Current, e2.Current);
                    }
                }
            }

        }
        #endregion
    }

}
