﻿using System;
using System.Collections.Generic;

namespace TotalApp.Package.TLINQ
{
    /// <summary>
    /// *Autor:Larry Ramirez
    /// *Fecha de creacion:09-01-2018
    /// *Version:1.1.20180109
    /// *Sinopsis:
    /// </summary>
    public static partial class THelperLinq
    {
        #region "Funciones"

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="oData"></param>
        /// <param name="nCount"></param>
        /// <returns></returns>
        public static bool TLeast<T>(this IEnumerable<T> oData, int nCount)
        {
            if (nCount < 0) THelperInfoEstatus.ArgumentOutOfRange(nameof(nCount));

            return QuantityIterator(oData, nCount, n => n >= nCount);
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="oData"></param>
        /// <param name="nCount"></param>
        /// <returns></returns>
        public static bool TMost<T>(this IEnumerable<T> oData, int nCount)
        {
            if (nCount < 0) THelperInfoEstatus.ArgumentOutOfRange(nameof(nCount));

            return QuantityIterator(oData, nCount + 1, n => n <= nCount);
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="oData"></param>
        /// <param name="nCount"></param>
        /// <returns></returns>
        public static bool TExactly<T>(this IEnumerable<T> oData, int nCount)
        {
            if (nCount < 0) THelperInfoEstatus.ArgumentOutOfRange(nameof(nCount));

            return QuantityIterator(oData, nCount + 1, n => n == nCount);
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="oData"></param>
        /// <param name="nMin"></param>
        /// <param name="nMax"></param>
        /// <returns></returns>
        public static bool TCountBetween<T>(this IEnumerable<T> oData, int nMin, int nMax)
        {
            if (nMin < 0) THelperInfoEstatus.ArgumentOutOfRange(nameof(nMin));
            if (nMax < nMin) THelperInfoEstatus.ArgumentOutOfRange(nameof(nMax));

            return QuantityIterator(oData, nMax + 1, n => nMin <= n && n <= nMax);
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="oData"></param>
        /// <param name="nLimit"></param>
        /// <param name="oPred"></param>
        /// <returns></returns>
        private static bool QuantityIterator<T>(IEnumerable<T> oData, int nLimit, Func<int, bool> oPred)
        {
            if (oData == null) throw new ArgumentNullException(nameof(oData));

            var oCollection = oData as ICollection<T>;
            if (oCollection != null)
            {
                return oPred(oCollection.Count);
            }

            var nCount = 0;

            using (var e = oData.GetEnumerator())
            {
                while (e.MoveNext())
                {
                    if (++nCount == nLimit)
                    {
                        break;
                    }
                }
            }

            return oPred(nCount);
        }
        #endregion
    }
}
