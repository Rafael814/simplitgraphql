﻿using System;
using System.Collections.Generic;

namespace TotalApp.Package.TLINQ
{
    /// <summary>
    /// *Autor:Larry Ramirez
    /// *Fecha de creacion:09-01-2018
    /// *Version:1.1.20180109
    /// *Sinopsis:
    /// </summary>
    public static partial class THelperLinq
    {
        #region "Funciones"

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="U"></typeparam>
        /// <param name="oData"></param>
        /// <param name="oSelect"></param>
        /// <returns></returns>
        public static IEnumerable<KeyValuePair<U, int>> TCountBy<T, U>(this IEnumerable<T> oData
                                                                      , Func<T, U> oSelect)
        {
            return oData.TCountBy(oSelect, null);
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="U"></typeparam>
        /// <param name="oData"></param>
        /// <param name="oSelect"></param>
        /// <param name="oComparer"></param>
        /// <returns></returns>
        public static IEnumerable<KeyValuePair<U, int>> TCountBy<T, U>(this IEnumerable<T> oData
                                                                     , Func<T, U> oSelect
                                                                     , IEqualityComparer<U> oComparer)
        {
            if (oData.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oData));
            if (oSelect.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oSelect));

            return _(); IEnumerable<KeyValuePair<U, int>> _()
            {
                List<U> oListKeys;
                List<int> oListCounts;

                Loop(oComparer ?? EqualityComparer<U>.Default);

                for (var nIndex = 0; nIndex < oListKeys.Count; nIndex++)
                    yield return new KeyValuePair<U, int>(oListKeys[nIndex]
                                                        , oListCounts[nIndex]);

                void Loop(IEqualityComparer<U> oCmp)
                {
                    var oDic = new Dictionary<U, int>(oCmp);
                    oListKeys = new List<U>();
                    oListCounts = new List<int>();
                    var oHavePrevKey = false;
                    var oPrevKey = default(U);
                    var nIdx = 0;

                    foreach (var oItem in oData)
                    {
                        var key = oSelect(oItem);

                        if (
                            (oHavePrevKey && oCmp.GetHashCode(oPrevKey) == oCmp.GetHashCode(key)
                                          && oCmp.Equals(oPrevKey, key))
                           
                            || oDic.TryGetValue(key, out nIdx))
                        {
                            oListCounts[nIdx]++;
                        }
                        else
                        {
                            oDic[key] = oListKeys.Count;
                            nIdx = oListKeys.Count;
                            oListKeys.Add(key);
                            oListCounts.Add(1);
                        }

                        oPrevKey = key;
                        oHavePrevKey = true;
                    }
                }
            }
        }
        #endregion
    }
}
