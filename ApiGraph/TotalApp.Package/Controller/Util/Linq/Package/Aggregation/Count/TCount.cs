﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace TotalApp.Package.TLINQ
{
    /// <summary>
    /// *Autor:Larry Ramirez
    /// *Fecha de creacion:09-01-2018
    /// *Version:1.1.20180109
    /// *Sinopsis:
    /// </summary>
    public static partial class THelperLinq
    {
        #region "Funciones"
        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="oData"></param>
        /// <returns></returns>
        public static int TCountOrDefault<T>(this IEnumerable<T> oData)
        {
            if (oData.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oData));

            int nCount = 0;

            if (oData is ICollection<T>)
            {
                return ((ICollection<T>)oData).Count;
            }

            if (oData is ICollection)
            {
                return ((ICollection)oData).Count;
            }

            using (IEnumerator<T> e = oData.GetEnumerator())
            {
                checked
                {
                    while (e.MoveNext())
                    {
                        nCount++;
                    }
                }
            }

            return nCount;
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="oData"></param>
        /// <returns></returns>
        public static long TLongCount<T>(this IEnumerable<T> oData)
        {
            if (oData.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oData));

            long nCount = 0;

            if (oData is ICollection<T>)
            {
                return ((ICollection<T>)oData).Count;
            }

            if (oData is ICollection)
            {
                return ((ICollection)oData).Count;
            }

            using (IEnumerator<T> e = oData.GetEnumerator())
            {
                checked
                {
                    while (e.MoveNext())
                    {
                        nCount++;
                    }
                }
            }

            return nCount;
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="oData"></param>
        /// <param name="oPred"></param>
        /// <returns></returns>
        public static int TCountOrDefaultIf<T>(this IEnumerable<T> oData, Func<T, bool> oPred)
        {
            if (oData.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oData));
            if (oPred.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oPred));

            int nCount = 0;
            foreach (var oElement in oData)
            {
                checked
                {
                    if (oPred(oElement))
                    {
                        nCount++;
                    }
                }
            }

            return nCount;
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="oData"></param>
        /// <param name="oPred"></param>
        /// <returns></returns>
        public static long TLongCountIf<T>(this IEnumerable<T> oData, Func<T, bool> oPred)
        {
            if (oData.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oData));
            if (oPred.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oPred));

            long nCount = 0;
            foreach (var oElement in oData)
            {
                checked
                {
                    if (oPred(oElement))
                    {
                        nCount++;
                    }
                }
            }

            return nCount;
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="oData"></param>
        /// <param name="oPred"></param>
        /// <returns></returns>
        public static int TCountOrDefaultIfNot<T>(this IEnumerable<T> oData, Func<T, bool> oPred)
        {
            if (oData.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oData));
            if (oPred.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oPred));

            int nCount = 0;
            foreach (var oElement in oData)
            {
                checked
                {
                    if (!oPred(oElement))
                    {
                        nCount++;
                    }
                }
            }

            return nCount;
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="oData"></param>
        /// <param name="oPred"></param>
        /// <returns></returns>
        public static long TLongCountIfNot<T>(this IEnumerable<T> oData, Func<T, bool> oPred)
        {
            if (oData.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oData));
            if (oPred.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oPred));

            long nCount = 0;
            foreach (var oElement in oData)
            {
                checked
                {
                    if (!oPred(oElement))
                    {
                        nCount++;
                    }
                }
            }

            return nCount;
        }
        #endregion
    }
}
