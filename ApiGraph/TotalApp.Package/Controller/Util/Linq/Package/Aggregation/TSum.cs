﻿using System;
using System.Collections.Generic;

namespace TotalApp.Package.TLINQ
{
    /// <summary>
    /// *Autor:Larry Ramirez
    /// *Fecha de creacion:09-01-2018
    /// *Version:1.1.20180109
    /// *Sinopsis:
    /// </summary>
    public static partial class THelperLinq
    {
        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="oData"></param>
        /// <returns></returns>
        public static int TSumOrDefault(this IEnumerable<int> oData)
        {
            if (oData.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oData));

            int nRet = 0;
            checked
            {
                foreach (int nItem in oData)
                {
                    nRet += nItem;
                }
            }

            return nRet;
        }

        

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="oData"></param>
        /// <returns></returns>
        public static int? TSumOrDefault(this IEnumerable<int?> oData)
        {
            if (oData.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oData));

            int nRet  = 0;
            checked
            {
                foreach (int? nItem in oData)
                {
                    if (nItem != null)
                    {
                        nRet += nItem.GetValueOrDefault();
                    }
                }
            }

            return nRet;
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="oData"></param>
        /// <returns></returns>
        public static long TSumOrDefault(this IEnumerable<long> oData)
        {
            if (oData.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oData));

            long nRet  = 0;
            checked
            {
                foreach (long nItem in oData)
                {
                    nRet += nItem;
                }
            }

            return nRet;
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="oData"></param>
        /// <returns></returns>
        public static long? TSumOrDefault(this IEnumerable<long?> oData)
        {
            if (oData.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oData));

            long nRet  = 0;
            checked
            {
                foreach (long? nItem in oData)
                {
                    if (nItem != null)
                    {
                        nRet += nItem.GetValueOrDefault();
                    }
                }
            }

            return nRet;
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="oData"></param>
        /// <returns></returns>
        public static float TSumOrDefault(this IEnumerable<float> oData)
        {
            if (oData.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oData));

            double nRet = 0;
            foreach (float nItem in oData)
            {
                nRet += nItem;
            }

            return (float)nRet;
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="oData"></param>
        /// <returns></returns>
        public static float? TSumOrDefault(this IEnumerable<float?> oData)
        {
            if (oData.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oData));

            double nRet = 0;
            foreach (float? nItem in oData)
            {
                if (nItem != null)
                {
                    nRet += nItem.GetValueOrDefault();
                }
            }

            return (float)nRet;
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="oData"></param>
        /// <returns></returns>
        public static double TSumOrDefault(this IEnumerable<double> oData)
        {
            if (oData.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oData));

            double nRet = 0;
            foreach (double nItem in oData)
            {
                nRet += nItem;
            }

            return nRet;
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="oData"></param>
        /// <returns></returns>
        public static double? TSumOrDefault(this IEnumerable<double?> oData)
        {
            if (oData.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oData));

            double nRet = 0;
            foreach (double? nItem in oData)
            {
                if (nItem != null)
                {
                    nRet += nItem.GetValueOrDefault();
                }
            }

            return nRet;
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="oData"></param>
        /// <returns></returns>
        public static decimal TSumOrDefault(this IEnumerable<decimal> oData)
        {
            if (oData.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oData));

            decimal nRet = 0;
            foreach (decimal nItem in oData)
            {
                nRet += nItem;
            }

            return nRet;
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="oData"></param>
        /// <returns></returns>
        public static decimal? TSumOrDefault(this IEnumerable<decimal?> oData)
        {
            if (oData.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oData));

            decimal nRet = 0;
            foreach (decimal? nItem in oData)
            {
                if (nItem != null)
                {
                    nRet += nItem.GetValueOrDefault();
                }
            }

            return nRet;
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="oData"></param>
        /// <param name="oPred"></param>
        /// <returns></returns>
        public static int TSumOrDefault<T>(this IEnumerable<T> oData, Func<T, int> oPred)
        {
            if (oData.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oData));
            if (oPred.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oPred));

            int nRet  = 0;
            checked
            {
                foreach (T oItem in oData)
                {
                    nRet += oPred(oItem);
                }
            }

            return nRet;
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="oData"></param>
        /// <param name="oPred"></param>
        /// <returns></returns>
        public static int? TSumOrDefault<T>(this IEnumerable<T> oData, Func<T, int?> oPred)
        {
            if (oData.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oData));
            if (oPred.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oPred));

            int nRet  = 0;
            checked
            {
                foreach (T oItem in oData)
                {
                    int? nNum = oPred(oItem);
                    if (nNum != null)
                    {
                        nRet += nNum.GetValueOrDefault();
                    }
                }
            }

            return nRet;
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="oData"></param>
        /// <param name="oPred"></param>
        /// <returns></returns>
        public static long TSumOrDefault<T>(this IEnumerable<T> oData, Func<T, long> oPred)
        {
            if (oData.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oData));
            if (oPred.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oPred));

            long nRet  = 0;
            checked
            {
                foreach (T oItem in oData)
                {
                    nRet += oPred(oItem);
                }
            }

            return nRet;
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="oData"></param>
        /// <param name="oPred"></param>
        /// <returns></returns>
        public static long? TSumOrDefault<T>(this IEnumerable<T> oData, Func<T, long?> oPred)
        {
            if (oData.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oData));
            if (oPred.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oPred));

            long nRet  = 0;
            checked
            {
                foreach (T oItem in oData)
                {
                    long? nNum = oPred(oItem);
                    if (nNum != null)
                    {
                        nRet += nNum.GetValueOrDefault();
                    }
                }
            }

            return nRet;
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="oData"></param>
        /// <param name="oPred"></param>
        /// <returns></returns>
        public static float TSumOrDefault<T>(this IEnumerable<T> oData, Func<T, float> oPred)
        {
            if (oData.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oData));
            if (oPred.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oPred));

            double nRet = 0;
            foreach (T oItem in oData)
            {
                nRet += oPred(oItem);
            }

            return (float)nRet;
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="oData"></param>
        /// <param name="oPred"></param>
        /// <returns></returns>
        public static float? TSumOrDefault<T>(this IEnumerable<T> oData, Func<T, float?> oPred)
        {
            if (oData.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oData));
            if (oPred.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oPred));

            double nRet = 0;
            foreach (T oItem in oData)
            {
                float? nNum = oPred(oItem);
                if (nNum != null)
                {
                    nRet += nNum.GetValueOrDefault();
                }
            }

            return (float)nRet;
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="oData"></param>
        /// <param name="oPred"></param>
        /// <returns></returns>
        public static double TSumOrDefault<T>(this IEnumerable<T> oData, Func<T, double> oPred)
        {
            if (oData.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oData));
            if (oPred.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oPred));

            double nRet = 0;
            foreach (T oItem in oData)
            {
                nRet += oPred(oItem);
            }

            return nRet;
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="oData"></param>
        /// <param name="oPred"></param>
        /// <returns></returns>
        public static double? TSumOrDefault<T>(this IEnumerable<T> oData, Func<T, double?> oPred)
        {
            if (oData.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oData));
            if (oPred.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oPred));

            double nRet = 0;
            foreach (T oItem in oData)
            {
                double? nNum = oPred(oItem);
                if (nNum != null)
                {
                    nRet += nNum.GetValueOrDefault();
                }
            }

            return nRet;
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="oData"></param>
        /// <param name="oPred"></param>
        /// <returns></returns>
        public static decimal TSumOrDefault<T>(this IEnumerable<T> oData, Func<T, decimal> oPred)
        {
            if (oData.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oData));
            if (oPred.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oPred));

            decimal nRet = 0;
            foreach (T oItem in oData)
            {
                nRet += oPred(oItem);
            }

            return nRet;
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="oData"></param>
        /// <param name="oPred"></param>
        /// <returns></returns>
        public static decimal? TSumOrDefault<T>(this IEnumerable<T> oData, Func<T, decimal?> oPred)
        {
            if (oData.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oData));
            if (oPred.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oPred));

            decimal nRet = 0;
            foreach (T oItem in oData)
            {
                decimal? nNum = oPred(oItem);
                if (nNum != null)
                {
                    nRet += nNum.GetValueOrDefault();
                }
            }

            return nRet;
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="oData"></param>
        /// <returns></returns>
        public static double TSumAnyNumberOrDefault(this IEnumerable<object> oData)
        {
            if (oData.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oData));

            double nRet = 0;
            checked
            {
                foreach (var nItem in oData)
                {
                    if (nItem != null)
                    {
                        nRet += nItem.ToString().TToDoubleOrDefault();
                    }
                }
            }

            return nRet;
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="oData"></param>
        /// <returns></returns>
        public static double TSumAnyNumberOrDefault(this IEnumerable<object> oData, Func<object, bool> oPred)
        {
            if (oData.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oData));
            if (oPred.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oPred));

            double nRet = 0;
            checked
            {
                foreach (var nItem in oData)
                {
                    if (nItem != null)
                    {
                        if (oPred(nItem))
                        {
                            nRet += nItem.ToString().TToDoubleOrDefault();
                        }
                    }
                }
            }

            return nRet;
        }

    }
}
