﻿using System;
using System.Collections.Generic;

namespace TotalApp.Package.TLINQ
{
    /// <summary>
    /// *Autor:Larry Ramirez
    /// *Fecha de creacion:09-01-2018
    /// *Version:1.1.20180109
    /// *Sinopsis: 
    /// </summary>
    public static partial class THelperLinq
    {
        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis: 
        /// </summary>
        /// <param name="oData"></param>
        /// <returns></returns>
        public static double TAvgOrDefault(this IEnumerable<int> oData)
        {
            if (oData.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oData));

            using (IEnumerator<int> oItem = oData.GetEnumerator())
            {
                if (!oItem.MoveNext())
                {
                     THelperInfoEstatus.NoElements();
                }

                long nRet = oItem.Current;
                long nCount = 1;
                checked
                {
                    while (oItem.MoveNext())
                    {
                        nRet += oItem.Current;
                        ++nCount;
                    }
                }

                return (double)nRet / nCount;
            }
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis: 
        /// </summary>
        /// <param name="oData"></param>
        /// <returns></returns>
        public static double? TAvgOrDefault(this IEnumerable<int?> oData)
        {
            if (oData.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oData));

            using (IEnumerator<int?> oItem = oData.GetEnumerator())
            {
                while (oItem.MoveNext())
                {
                    int? nVal = oItem.Current;
                    if (nVal.HasValue)
                    {
                        long nRet = nVal.GetValueOrDefault();
                        long nCount = 1;
                        checked
                        {
                            while (oItem.MoveNext())
                            {
                                nVal = oItem.Current;
                                if (nVal.HasValue)
                                {
                                    nRet += nVal.GetValueOrDefault();
                                    ++nCount;
                                }
                            }
                        }

                        return (double)nRet / nCount;
                    }
                }
            }

            return null;
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis: 
        /// </summary>
        /// <param name="oData"></param>
        /// <returns></returns>
        public static double TAvgOrDefault(this IEnumerable<long> oData)
        {
            if (oData.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oData));

            using (IEnumerator<long> oItem = oData.GetEnumerator())
            {
                if (!oItem.MoveNext())
                {
                     THelperInfoEstatus.NoElements();
                }

                long nRet = oItem.Current;
                long nCount = 1;
                checked
                {
                    while (oItem.MoveNext())
                    {
                        nRet += oItem.Current;
                        ++nCount;
                    }
                }

                return (double)nRet / nCount;
            }
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis: 
        /// </summary>
        /// <param name="oData"></param>
        /// <returns></returns>
        public static double? TAvgOrDefault(this IEnumerable<long?> oData)
        {
            if (oData.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oData));

            using (IEnumerator<long?> oItem = oData.GetEnumerator())
            {
                while (oItem.MoveNext())
                {
                    long? nVal = oItem.Current;
                    if (nVal.HasValue)
                    {
                        long nRet = nVal.GetValueOrDefault();
                        long nCount = 1;
                        checked
                        {
                            while (oItem.MoveNext())
                            {
                                nVal = oItem.Current;
                                if (nVal.HasValue)
                                {
                                    nRet += nVal.GetValueOrDefault();
                                    ++nCount;
                                }
                            }
                        }

                        return (double)nRet / nCount;
                    }
                }
            }

            return null;
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis: 
        /// </summary>
        /// <param name="oData"></param>
        /// <returns></returns>
        public static float TAvgOrDefault(this IEnumerable<float> oData)
        {
            if (oData.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oData));

            using (IEnumerator<float> oItem = oData.GetEnumerator())
            {
                if (!oItem.MoveNext())
                {
                     THelperInfoEstatus.NoElements();
                }

                double nRet = oItem.Current;
                long nCount = 1;
                while (oItem.MoveNext())
                {
                    nRet += oItem.Current;
                    ++nCount;
                }

                return (float)(nRet / nCount);
            }
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis: 
        /// </summary>
        /// <param name="oData"></param>
        /// <returns></returns>
        public static float? TAvgOrDefault(this IEnumerable<float?> oData)
        {
            if (oData.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oData));

            using (IEnumerator<float?> oItem = oData.GetEnumerator())
            {
                while (oItem.MoveNext())
                {
                    float? nVal = oItem.Current;
                    if (nVal.HasValue)
                    {
                        double nRet = nVal.GetValueOrDefault();
                        long nCount = 1;
                        checked
                        {
                            while (oItem.MoveNext())
                            {
                                nVal = oItem.Current;
                                if (nVal.HasValue)
                                {
                                    nRet += nVal.GetValueOrDefault();
                                    ++nCount;
                                }
                            }
                        }

                        return (float)(nRet / nCount);
                    }
                }
            }

            return null;
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis: 
        /// </summary>
        /// <param name="oData"></param>
        /// <returns></returns>
        public static double TAvgOrDefault(this IEnumerable<double> oData)
        {
            if (oData.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oData));

            using (IEnumerator<double> oItem = oData.GetEnumerator())
            {
                if (!oItem.MoveNext())
                {
                     THelperInfoEstatus.NoElements();
                }

                double nRet = oItem.Current;
                long nCount = 1;
                while (oItem.MoveNext())
                {
                    nRet += oItem.Current;
                    ++nCount;
                }

                return nRet / nCount;
            }
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis: 
        /// </summary>
        /// <param name="oData"></param>
        /// <returns></returns>
        public static double? TAvgOrDefault(this IEnumerable<double?> oData)
        {
            if (oData.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oData));

            using (IEnumerator<double?> oItem = oData.GetEnumerator())
            {
                while (oItem.MoveNext())
                {
                    double? nVal = oItem.Current;
                    if (nVal.HasValue)
                    {
                        double nRet = nVal.GetValueOrDefault();
                        long nCount = 1;
                        checked
                        {
                            while (oItem.MoveNext())
                            {
                                nVal = oItem.Current;
                                if (nVal.HasValue)
                                {
                                    nRet += nVal.GetValueOrDefault();
                                    ++nCount;
                                }
                            }
                        }

                        return nRet / nCount;
                    }
                }
            }

            return null;
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis: 
        /// </summary>
        /// <param name="oData"></param>
        /// <returns></returns>
        public static decimal TAvgOrDefault(this IEnumerable<decimal> oData)
        {
            if (oData.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oData));

            using (IEnumerator<decimal> oItem = oData.GetEnumerator())
            {
                if (!oItem.MoveNext())
                {
                     THelperInfoEstatus.NoElements();
                }

                decimal nRet = oItem.Current;
                long nCount = 1;
                while (oItem.MoveNext())
                {
                    nRet += oItem.Current;
                    ++nCount;
                }

                return nRet / nCount;
            }
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis: 
        /// </summary>
        /// <param name="oData"></param>
        /// <returns></returns>
        public static decimal? TAvgOrDefault(this IEnumerable<decimal?> oData)
        {
            if (oData.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oData));

            using (IEnumerator<decimal?> oItem = oData.GetEnumerator())
            {
                while (oItem.MoveNext())
                {
                    decimal? nVal = oItem.Current;
                    if (nVal.HasValue)
                    {
                        decimal nRet = nVal.GetValueOrDefault();
                        long nCount = 1;
                        while (oItem.MoveNext())
                        {
                            nVal = oItem.Current;
                            if (nVal.HasValue)
                            {
                                nRet += nVal.GetValueOrDefault();
                                ++nCount;
                            }
                        }

                        return nRet / nCount;
                    }
                }
            }

            return null;
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis: 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="oData"></param>
        /// <param name="oPred"></param>
        /// <returns></returns>
        public static double TAvgOrDefault<T>(this IEnumerable<T> oData, Func<T, int> oPred)
        {
            if (oData.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oData));

            if (oPred.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oPred));

            using (IEnumerator<T> oItem = oData.GetEnumerator())
            {
                if (!oItem.MoveNext())
                {
                     THelperInfoEstatus.NoElements();
                }

                long nRet = oPred(oItem.Current);
                long nCount = 1;
                checked
                {
                    while (oItem.MoveNext())
                    {
                        nRet += oPred(oItem.Current);
                        ++nCount;
                    }
                }

                return (double)nRet / nCount;
            }
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis: 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="oData"></param>
        /// <param name="oPred"></param>
        /// <returns></returns>
        public static double? TAvgOrDefault<T>(this IEnumerable<T> oData, Func<T, int?> oPred)
        {
            if (oData.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oData));

            if (oPred.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oPred));

            using (IEnumerator<T> oItem = oData.GetEnumerator())
            {
                while (oItem.MoveNext())
                {
                    int? nVal = oPred(oItem.Current);
                    if (nVal.HasValue)
                    {
                        long nRet = nVal.GetValueOrDefault();
                        long nCount = 1;
                        checked
                        {
                            while (oItem.MoveNext())
                            {
                                nVal = oPred(oItem.Current);
                                if (nVal.HasValue)
                                {
                                    nRet += nVal.GetValueOrDefault();
                                    ++nCount;
                                }
                            }
                        }

                        return (double)nRet / nCount;
                    }
                }
            }

            return null;
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis: 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="oData"></param>
        /// <param name="oPred"></param>
        /// <returns></returns>
        public static double TAvgOrDefault<T>(this IEnumerable<T> oData, Func<T, long> oPred)
        {
            if (oData.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oData));

            if (oPred.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oPred));

            using (IEnumerator<T> oItem = oData.GetEnumerator())
            {
                if (!oItem.MoveNext())
                {
                     THelperInfoEstatus.NoElements();
                }

                long nRet = oPred(oItem.Current);
                long nCount = 1;
                checked
                {
                    while (oItem.MoveNext())
                    {
                        nRet += oPred(oItem.Current);
                        ++nCount;
                    }
                }

                return (double)nRet / nCount;
            }
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis: 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="oData"></param>
        /// <param name="oPred"></param>
        /// <returns></returns>
        public static double? TAvgOrDefault<T>(this IEnumerable<T> oData, Func<T, long?> oPred)
        {
            if (oData.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oData));

            if (oPred.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oPred));

            using (IEnumerator<T> oItem = oData.GetEnumerator())
            {
                while (oItem.MoveNext())
                {
                    long? nVal = oPred(oItem.Current);
                    if (nVal.HasValue)
                    {
                        long nRet = nVal.GetValueOrDefault();
                        long nCount = 1;
                        checked
                        {
                            while (oItem.MoveNext())
                            {
                                nVal = oPred(oItem.Current);
                                if (nVal.HasValue)
                                {
                                    nRet += nVal.GetValueOrDefault();
                                    ++nCount;
                                }
                            }
                        }

                        return (double)nRet / nCount;
                    }
                }
            }

            return null;
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis: 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="oData"></param>
        /// <param name="oPred"></param>
        /// <returns></returns>
        public static float TAvgOrDefault<T>(this IEnumerable<T> oData, Func<T, float> oPred)
        {
            if (oData.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oData));

            if (oPred.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oPred));

            using (IEnumerator<T> oItem = oData.GetEnumerator())
            {
                if (!oItem.MoveNext())
                {
                     THelperInfoEstatus.NoElements();
                }

                double nRet = oPred(oItem.Current);
                long nCount = 1;
                while (oItem.MoveNext())
                {
                    nRet += oPred(oItem.Current);
                    ++nCount;
                }

                return (float)(nRet / nCount);
            }
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis: 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="oData"></param>
        /// <param name="oPred"></param>
        /// <returns></returns>
        public static float? TAvgOrDefault<T>(this IEnumerable<T> oData, Func<T, float?> oPred)
        {
            if (oData.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oData));

            if (oPred.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oPred));

            using (IEnumerator<T> oItem = oData.GetEnumerator())
            {
                while (oItem.MoveNext())
                {
                    float? nVal = oPred(oItem.Current);
                    if (nVal.HasValue)
                    {
                        double nRet = nVal.GetValueOrDefault();
                        long nCount = 1;
                        checked
                        {
                            while (oItem.MoveNext())
                            {
                                nVal = oPred(oItem.Current);
                                if (nVal.HasValue)
                                {
                                    nRet += nVal.GetValueOrDefault();
                                    ++nCount;
                                }
                            }
                        }

                        return (float)(nRet / nCount);
                    }
                }
            }

            return null;
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis: 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="oData"></param>
        /// <param name="oPred"></param>
        /// <returns></returns>
        public static double TAvgOrDefault<T>(this IEnumerable<T> oData, Func<T, double> oPred)
        {
            if (oData.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oData));

            if (oPred.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oPred));

            using (IEnumerator<T> oItem = oData.GetEnumerator())
            {
                if (!oItem.MoveNext())
                {
                     THelperInfoEstatus.NoElements();
                }

                double nRet = oPred(oItem.Current);
                long nCount = 1;
                while (oItem.MoveNext())
                {
                    nRet += oPred(oItem.Current);
                    ++nCount;
                }

                return nRet / nCount;
            }
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis: 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="oData"></param>
        /// <param name="oPred"></param>
        /// <returns></returns>
        public static double? TAvgOrDefault<T>(this IEnumerable<T> oData, Func<T, double?> oPred)
        {
            if (oData.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oData));

            if (oPred.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oPred));

            using (IEnumerator<T> oItem = oData.GetEnumerator())
            {
                while (oItem.MoveNext())
                {
                    double? nVal = oPred(oItem.Current);
                    if (nVal.HasValue)
                    {
                        double nRet = nVal.GetValueOrDefault();
                        long nCount = 1;
                        checked
                        {
                            while (oItem.MoveNext())
                            {
                                nVal = oPred(oItem.Current);
                                if (nVal.HasValue)
                                {
                                    nRet += nVal.GetValueOrDefault();
                                    ++nCount;
                                }
                            }
                        }

                        return nRet / nCount;
                    }
                }
            }

            return null;
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis: 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="oData"></param>
        /// <param name="oPred"></param>
        /// <returns></returns>
        public static decimal TAvgOrDefault<T>(this IEnumerable<T> oData, Func<T, decimal> oPred)
        {
            if (oData.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oData));

            if (oPred.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oPred));

            using (IEnumerator<T> oItem = oData.GetEnumerator())
            {
                if (!oItem.MoveNext())
                {
                     THelperInfoEstatus.NoElements();
                }

                decimal nRet = oPred(oItem.Current);
                long nCount = 1;
                while (oItem.MoveNext())
                {
                    nRet += oPred(oItem.Current);
                    ++nCount;
                }

                return nRet / nCount;
            }
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis: 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="oData"></param>
        /// <param name="oPred"></param>
        /// <returns></returns>
        public static decimal? TAvgOrDefault<T>(this IEnumerable<T> oData, Func<T, decimal?> oPred)
        {
            if (oData.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oData));

            if (oPred.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oPred));

            using (IEnumerator<T> oItem = oData.GetEnumerator())
            {
                while (oItem.MoveNext())
                {
                    decimal? nVal = oPred(oItem.Current);
                    if (nVal.HasValue)
                    {
                        decimal nRet = nVal.GetValueOrDefault();
                        long nCount = 1;
                        while (oItem.MoveNext())
                        {
                            nVal = oPred(oItem.Current);
                            if (nVal.HasValue)
                            {
                                nRet += nVal.GetValueOrDefault();
                                ++nCount;
                            }
                        }

                        return nRet / nCount;
                    }
                }
            }

            return null;
        }


        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis: 
        /// </summary>
        /// <param name="oData"></param>
        /// <returns></returns>
        public static double TAvgAnyNumberOrDefault(this IEnumerable<object> oData)
        {
            if (oData.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oData));

            using (IEnumerator<object> oItem = oData.GetEnumerator())
            {
                if (!oItem.MoveNext())
                {
                    THelperInfoEstatus.NoElements();
                }

                if (oItem.Current != null)
                {
                    
                    double nRet = oItem.Current.ToString().TToDoubleOrDefault();
                    long nCount = 1;
                    checked
                    {
                        while (oItem.MoveNext())
                        {
                            if (oItem.Current != null)
                            {
                                nRet += oItem.Current.ToString().TToDoubleOrDefault();
                                ++nCount;
                            }  
                        }
                    }

                    return nRet / nCount;
                }
                
            }

            return 0;
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis: 
        /// </summary>
        /// <param name="oData"></param>
        /// <returns></returns>
        public static double TAvgAnyNumberOrDefault(this IEnumerable<object> oData, Func<object, bool> oPred)
        {
            if (oData.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oData));
            if (oPred.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oPred));

            using (IEnumerator<object> oItem = oData.GetEnumerator())
            {
                if (!oItem.MoveNext())
                {
                    THelperInfoEstatus.NoElements();
                }

                if (oItem.Current != null)
                {
                    double nRet = 0;
                    long nCount = 0;

                    if (oPred(oItem.Current))
                    {
                        nRet = oItem.Current.ToString().TToDoubleOrDefault();
                        nCount = 1;
                    }

                    checked
                    {
                        while (oItem.MoveNext())
                        {
                            if (oItem.Current != null)
                            {
                                if (oPred(oItem.Current))
                                {
                                    nRet += oItem.Current.ToString().TToDoubleOrDefault();
                                    ++nCount;
                                } 
                            }
                        }
                    }

                    return nRet / nCount;
                }

            }

            return 0;
        }
    }
}
