﻿using System;
using System.Collections.Generic;

namespace TotalApp.Package.TLINQ
{
    /// <summary>
    /// *Autor:Larry Ramirez
    /// *Fecha de creacion:09-01-2018
    /// *Version:1.1.20180109
    /// *Sinopsis:
    /// </summary>
    public static partial class THelperLinq
    {
        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="oData"></param>
        /// <returns></returns>
        public static int TMinOrDefault(this IEnumerable<int> oData)
        {
            if (oData.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oData));

            int nRet = 0;
            using (IEnumerator<int> oItem = oData.GetEnumerator())
            {
                if (!oItem.MoveNext())
                {
                    THelperInfoEstatus.NoElements();
                }

                nRet = oItem.Current;
                while (oItem.MoveNext())
                {
                    int x = oItem.Current;
                    if (x < nRet)
                    {
                        nRet = x;
                    }
                }
            }

            return nRet;
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="oData"></param>
        /// <returns></returns>
        public static int? TMinOrDefault(this IEnumerable<int?> oData)
        {
            if (oData.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oData));

            int? nRet = null;
            using (IEnumerator<int?> oItem = oData.GetEnumerator())
            {
                
                do
                {
                    if (!oItem.MoveNext())
                    {
                        return nRet;
                    }

                    nRet = oItem.Current;
                }
                while (!nRet.HasValue);

                
                int nVal = nRet.GetValueOrDefault();
                while (oItem.MoveNext())
                {
                    int? nCur = oItem.Current;
                    int x = nCur.GetValueOrDefault();

                    
                    if (nCur.HasValue & x < nVal)
                    {
                        nVal = x;
                        nRet = nCur;
                    }
                }
            }

            return nRet;
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="oData"></param>
        /// <returns></returns>
        public static long TMinOrDefault(this IEnumerable<long> oData)
        {
            if (oData.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oData));

            long nRet = 0;
            using (IEnumerator<long> oItem = oData.GetEnumerator())
            {
                if (!oItem.MoveNext())
                {
                    THelperInfoEstatus.NoElements();
                }

                nRet = oItem.Current;
                while (oItem.MoveNext())
                {
                    long x = oItem.Current;
                    if (x < nRet)
                    {
                        nRet = x;
                    }
                }
            }

            return nRet;
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="oData"></param>
        /// <returns></returns>
        public static long? TMinOrDefault(this IEnumerable<long?> oData)
        {
            if (oData.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oData));

            long? nRet = null;
            using (IEnumerator<long?> oItem = oData.GetEnumerator())
            {
                do
                {
                    if (!oItem.MoveNext())
                    {
                        return nRet;
                    }

                    nRet = oItem.Current;
                }
                while (!nRet.HasValue);

                long nVal = nRet.GetValueOrDefault();
                while (oItem.MoveNext())
                {
                    long? nCur = oItem.Current;
                    long x = nCur.GetValueOrDefault();

                   
                    if (nCur.HasValue & x < nVal)
                    {
                        nVal = x;
                        nRet = nCur;
                    }
                }
            }

            return nRet;
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="oData"></param>
        /// <returns></returns>
        public static float TMinOrDefault(this IEnumerable<float> oData)
        {
            if (oData.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oData));

            float nRet = 0;
            using (IEnumerator<float> oItem = oData.GetEnumerator())
            {
                if (!oItem.MoveNext())
                {
                    THelperInfoEstatus.NoElements();
                }

                nRet = oItem.Current;
                while (oItem.MoveNext())
                {
                    float x = oItem.Current;
                    if (x < nRet)
                    {
                        nRet = x;
                    }

                    
                    else if (float.IsNaN(x))
                    {
                        return x;
                    }
                }
            }

            return nRet;
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="oData"></param>
        /// <returns></returns>
        public static float? TMinOrDefault(this IEnumerable<float?> oData)
        {
            if (oData.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oData));

            float? nRet = null;
            using (IEnumerator<float?> oItem = oData.GetEnumerator())
            {
                do
                {
                    if (!oItem.MoveNext())
                    {
                        return nRet;
                    }

                    nRet = oItem.Current;
                }
                while (!nRet.HasValue);

                float nVal = nRet.GetValueOrDefault();
                while (oItem.MoveNext())
                {
                    float? nCur = oItem.Current;
                    if (nCur.HasValue)
                    {
                        float x = nCur.GetValueOrDefault();
                        if (x < nVal)
                        {
                            nVal = x;
                            nRet = nCur;
                        }
                        else if (float.IsNaN(x))
                        {
                            return nCur;
                        }
                    }
                }
            }

            return nRet;
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="oData"></param>
        /// <returns></returns>
        public static double TMinOrDefault(this IEnumerable<double> oData)
        {
            if (oData.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oData));

            double nRet = 0;
            using (IEnumerator<double> oItem = oData.GetEnumerator())
            {
                if (!oItem.MoveNext())
                {
                    THelperInfoEstatus.NoElements();
                }

                nRet = oItem.Current;
                while (oItem.MoveNext())
                {
                    double x = oItem.Current;
                    if (x < nRet)
                    {
                        nRet = x;
                    }
                    else if (double.IsNaN(x))
                    {
                        return x;
                    }
                }
            }

            return nRet;
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="oData"></param>
        /// <returns></returns>
        public static double? TMinOrDefault(this IEnumerable<double?> oData)
        {
            if (oData.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oData));

            double? nRet = null;
            using (IEnumerator<double?> oItem = oData.GetEnumerator())
            {
                do
                {
                    if (!oItem.MoveNext())
                    {
                        return nRet;
                    }

                    nRet = oItem.Current;
                }
                while (!nRet.HasValue);

                double nVal = nRet.GetValueOrDefault();
                while (oItem.MoveNext())
                {
                    double? nCur = oItem.Current;
                    if (nCur.HasValue)
                    {
                        double x = nCur.GetValueOrDefault();
                        if (x < nVal)
                        {
                            nVal = x;
                            nRet = nCur;
                        }
                        else if (double.IsNaN(x))
                        {
                            return nCur;
                        }
                    }
                }
            }

            return nRet;
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="oData"></param>
        /// <returns></returns>
        public static decimal TMinOrDefault(this IEnumerable<decimal> oData)
        {
            if (oData.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oData));

            decimal nRet = 0;
            using (IEnumerator<decimal> oItem = oData.GetEnumerator())
            {
                if (!oItem.MoveNext())
                {
                    THelperInfoEstatus.NoElements();
                }

                nRet = oItem.Current;
                while (oItem.MoveNext())
                {
                    decimal x = oItem.Current;
                    if (x < nRet)
                    {
                        nRet = x;
                    }
                }
            }

            return nRet;
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="oData"></param>
        /// <returns></returns>
        public static decimal? TMinOrDefault(this IEnumerable<decimal?> oData)
        {
            if (oData.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oData));

            decimal? nRet = null;
            using (IEnumerator<decimal?> oItem = oData.GetEnumerator())
            {
                do
                {
                    if (!oItem.MoveNext())
                    {
                        return nRet;
                    }

                    nRet = oItem.Current;
                }
                while (!nRet.HasValue);

                decimal nVal = nRet.GetValueOrDefault();
                while (oItem.MoveNext())
                {
                    decimal? nCur = oItem.Current;
                    decimal x = nCur.GetValueOrDefault();
                    if (nCur.HasValue && x < nVal)
                    {
                        nVal = x;
                        nRet = nCur;
                    }
                }
            }

            return nRet;
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="oData"></param>
        /// <returns></returns>
        public static T TMinOrDefault<T>(this IEnumerable<T> oData)
        {
            if (oData.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oData));

            Comparer<T> oCmp = Comparer<T>.Default;
            T nRet = default(T);
            if (nRet == null)
            {
                using (IEnumerator<T> oItem = oData.GetEnumerator())
                {
                    do
                    {
                        if (!oItem.MoveNext())
                        {
                            return nRet;
                        }

                        nRet = oItem.Current;
                    }
                    while (nRet == null);

                    while (oItem.MoveNext())
                    {
                        T x = oItem.Current;
                        if (x != null && oCmp.Compare(x, nRet) < 0)
                        {
                            nRet = x;
                        }
                    }
                }
            }
            else
            {
                using (IEnumerator<T> oItem = oData.GetEnumerator())
                {
                    if (!oItem.MoveNext())
                    {
                        THelperInfoEstatus.NoElements();
                    }

                    nRet = oItem.Current;
                    while (oItem.MoveNext())
                    {
                        T x = oItem.Current;
                        if (oCmp.Compare(x, nRet) < 0)
                        {
                            nRet = x;
                        }
                    }
                }
            }

            return nRet;
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="oData"></param>
        /// <param name="oPred"></param>
        /// <returns></returns>
        public static int TMinOrDefault<T>(this IEnumerable<T> oData, Func<T, int> oPred)
        {
            if (oData.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oData));

            if (oPred.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oPred));

            int nRet = 0;
            using (IEnumerator<T> oItem = oData.GetEnumerator())
            {
                if (!oItem.MoveNext())
                {
                    THelperInfoEstatus.NoElements();
                }

                nRet = oPred(oItem.Current);
                while (oItem.MoveNext())
                {
                    int x = oPred(oItem.Current);
                    if (x < nRet)
                    {
                        nRet = x;
                    }
                }
            }

            return nRet;
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="oData"></param>
        /// <param name="oPred"></param>
        /// <returns></returns>
        public static int? TMinOrDefault<T>(this IEnumerable<T> oData, Func<T, int?> oPred)
        {
            if (oData.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oData));

            if (oPred.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oPred));

            int? nRet = null;
            using (IEnumerator<T> oItem = oData.GetEnumerator())
            {
               
                do
                {
                    if (!oItem.MoveNext())
                    {
                        return nRet;
                    }

                    nRet = oPred(oItem.Current);
                }
                while (!nRet.HasValue);

                
                int nVal = nRet.GetValueOrDefault();
                while (oItem.MoveNext())
                {
                    int? nCur = oPred(oItem.Current);
                    int x = nCur.GetValueOrDefault();

                   
                    if (nCur.HasValue & x < nVal)
                    {
                        nVal = x;
                        nRet = nCur;
                    }
                }
            }

            return nRet;
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="oData"></param>
        /// <param name="oPred"></param>
        /// <returns></returns>
        public static long TMinOrDefault<T>(this IEnumerable<T> oData, Func<T, long> oPred)
        {
            if (oData.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oData));

            if (oPred.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oPred));

            long nRet = 0;
            using (IEnumerator<T> oItem = oData.GetEnumerator())
            {
                if (!oItem.MoveNext())
                {
                    THelperInfoEstatus.NoElements();
                }

                nRet = oPred(oItem.Current);
                while (oItem.MoveNext())
                {
                    long x = oPred(oItem.Current);
                    if (x < nRet)
                    {
                        nRet = x;
                    }
                }
            }

            return nRet;
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="oData"></param>
        /// <param name="oPred"></param>
        /// <returns></returns>
        public static long? TMinOrDefault<T>(this IEnumerable<T> oData, Func<T, long?> oPred)
        {
            if (oData.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oData));

            if (oPred.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oPred));

            long? nRet = null;
            using (IEnumerator<T> oItem = oData.GetEnumerator())
            {
                do
                {
                    if (!oItem.MoveNext())
                    {
                        return nRet;
                    }

                    nRet = oPred(oItem.Current);
                }
                while (!nRet.HasValue);

                long nVal = nRet.GetValueOrDefault();
                while (oItem.MoveNext())
                {
                    long? nCur = oPred(oItem.Current);
                    long x = nCur.GetValueOrDefault();

                    
                    if (nCur.HasValue & x < nVal)
                    {
                        nVal = x;
                        nRet = nCur;
                    }
                }
            }

            return nRet;
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="oData"></param>
        /// <param name="oPred"></param>
        /// <returns></returns>
        public static float TMinOrDefault<T>(this IEnumerable<T> oData, Func<T, float> oPred)
        {
            if (oData.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oData));

            if (oPred.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oPred));

            float nRet = 0;
            using (IEnumerator<T> oItem = oData.GetEnumerator())
            {
                if (!oItem.MoveNext())
                {
                    THelperInfoEstatus.NoElements();
                }

                nRet = oPred(oItem.Current);
                while (oItem.MoveNext())
                {
                    float x = oPred(oItem.Current);
                    if (x < nRet)
                    {
                        nRet = x;
                    }

                   
                    else if (float.IsNaN(x))
                    {
                        return x;
                    }
                }
            }

            return nRet;
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="oData"></param>
        /// <param name="oPred"></param>
        /// <returns></returns>
        public static float? TMinOrDefault<T>(this IEnumerable<T> oData, Func<T, float?> oPred)
        {
            if (oData.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oData));

            if (oPred.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oPred));

            float? nRet = null;
            using (IEnumerator<T> oItem = oData.GetEnumerator())
            {
                do
                {
                    if (!oItem.MoveNext())
                    {
                        return nRet;
                    }

                    nRet = oPred(oItem.Current);
                }
                while (!nRet.HasValue);

                float nVal = nRet.GetValueOrDefault();
                while (oItem.MoveNext())
                {
                    float? nCur = oPred(oItem.Current);
                    if (nCur.HasValue)
                    {
                        float x = nCur.GetValueOrDefault();
                        if (x < nVal)
                        {
                            nVal = x;
                            nRet = nCur;
                        }
                        else if (float.IsNaN(x))
                        {
                            return nCur;
                        }
                    }
                }
            }

            return nRet;
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="oData"></param>
        /// <param name="oPred"></param>
        /// <returns></returns>
        public static double TMinOrDefault<T>(this IEnumerable<T> oData, Func<T, double> oPred)
        {
            if (oData.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oData));

            if (oPred.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oPred));

            double nRet = 0;
            using (IEnumerator<T> oItem = oData.GetEnumerator())
            {
                if (!oItem.MoveNext())
                {
                    THelperInfoEstatus.NoElements();
                }

                nRet = oPred(oItem.Current);
                while (oItem.MoveNext())
                {
                    double x = oPred(oItem.Current);
                    if (x < nRet)
                    {
                        nRet = x;
                    }
                    else if (double.IsNaN(x))
                    {
                        return x;
                    }
                }
            }

            return nRet;
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="oData"></param>
        /// <param name="oPred"></param>
        /// <returns></returns>
        public static double? TMinOrDefault<T>(this IEnumerable<T> oData, Func<T, double?> oPred)
        {
            if (oData.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oData));

            if (oPred.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oPred));

            double? nRet = null;
            using (IEnumerator<T> oItem = oData.GetEnumerator())
            {
                do
                {
                    if (!oItem.MoveNext())
                    {
                        return nRet;
                    }

                    nRet = oPred(oItem.Current);
                }
                while (!nRet.HasValue);

                double nVal = nRet.GetValueOrDefault();
                while (oItem.MoveNext())
                {
                    double? nCur = oPred(oItem.Current);
                    if (nCur.HasValue)
                    {
                        double x = nCur.GetValueOrDefault();
                        if (x < nVal)
                        {
                            nVal = x;
                            nRet = nCur;
                        }
                        else if (double.IsNaN(x))
                        {
                            return nCur;
                        }
                    }
                }
            }

            return nRet;
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="oData"></param>
        /// <param name="oPred"></param>
        /// <returns></returns>
        public static decimal TMinOrDefault<T>(this IEnumerable<T> oData, Func<T, decimal> oPred)
        {
            if (oData.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oData));

            if (oPred.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oPred));

            decimal nRet = 0;
            using (IEnumerator<T> oItem = oData.GetEnumerator())
            {
                if (!oItem.MoveNext())
                {
                    THelperInfoEstatus.NoElements();
                }

                nRet = oPred(oItem.Current);
                while (oItem.MoveNext())
                {
                    decimal x = oPred(oItem.Current);
                    if (x < nRet)
                    {
                        nRet = x;
                    }
                }
            }

            return nRet;
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="oData"></param>
        /// <param name="oPred"></param>
        /// <returns></returns>
        public static decimal? TMinOrDefault<T>(this IEnumerable<T> oData, Func<T, decimal?> oPred)
        {
            if (oData.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oData));

            if (oPred.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oPred));

            decimal? nRet = null;
            using (IEnumerator<T> oItem = oData.GetEnumerator())
            {
                do
                {
                    if (!oItem.MoveNext())
                    {
                        return nRet;
                    }

                    nRet = oPred(oItem.Current);
                }
                while (!nRet.HasValue);

                decimal nVal = nRet.GetValueOrDefault();
                while (oItem.MoveNext())
                {
                    decimal? nCur = oPred(oItem.Current);
                    decimal x = nCur.GetValueOrDefault();
                    if (nCur.HasValue && x < nVal)
                    {
                        nVal = x;
                        nRet = nCur;
                    }
                }
            }

            return nRet;
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="U"></typeparam>
        /// <param name="oData"></param>
        /// <param name="oPred"></param>
        /// <returns></returns>
        public static U TMinOrDefault<T, U>(this IEnumerable<T> oData, Func<T, U> oPred)
        {
            if (oData.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oData));

            if (oPred.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oPred));

            Comparer<U> oCmp = Comparer<U>.Default;
            U nRet = default(U);
            if (nRet == null)
            {
                using (IEnumerator<T> oItem = oData.GetEnumerator())
                {
                    do
                    {
                        if (!oItem.MoveNext())
                        {
                            return nRet;
                        }

                        nRet = oPred(oItem.Current);
                    }
                    while (nRet == null);

                    while (oItem.MoveNext())
                    {
                        U x = oPred(oItem.Current);
                        if (x != null && oCmp.Compare(x, nRet) < 0)
                        {
                            nRet = x;
                        }
                    }
                }
            }
            else
            {
                using (IEnumerator<T> oItem = oData.GetEnumerator())
                {
                    if (!oItem.MoveNext())
                    {
                        THelperInfoEstatus.NoElements();
                    }

                    nRet = oPred(oItem.Current);
                    while (oItem.MoveNext())
                    {
                        U x = oPred(oItem.Current);
                        if (oCmp.Compare(x, nRet) < 0)
                        {
                            nRet = x;
                        }
                    }
                }
            }

            return nRet;
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="oData"></param>
        /// <returns></returns>
        public static double TMinAnyNumberOrDefault(this IEnumerable<object> oData)
        {
            if (oData.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oData));

            double nRet = 0;
            using (IEnumerator<object> oItem = oData.GetEnumerator())
            {
                if (!oItem.MoveNext())
                {
                    THelperInfoEstatus.NoElements();
                }

                if (oItem.Current != null)
                {
                    nRet = oItem.Current.ToString().TToDoubleOrDefault();
                    while (oItem.MoveNext())
                    {
                        if (oItem.Current != null)
                        {
                            double x = oItem.Current.ToString().TToDoubleOrDefault();
                            if (x < nRet)
                            {
                                nRet = x;
                            }
                        }
                          
                    }
                }
                
            }

            return nRet;
        }



        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="oData"></param>
        /// <param name="oPred"></param>
        /// <returns></returns>
        public static double TMinAnyNumberOrDefault(this IEnumerable<object> oData, Func<object,bool> oPred)
        {
            if (oData.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oData));
            if (oPred.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oPred));

            double nRet = 0;
            using (IEnumerator<object> oItem = oData.GetEnumerator())
            {
                if (!oItem.MoveNext())
                {
                    THelperInfoEstatus.NoElements();
                }

                if (oItem.Current != null)
                {
                   
                    if (oPred(oItem.Current))
                    {
                        nRet = oItem.Current.ToString().TToDoubleOrDefault();
                    }

                    while (oItem.MoveNext())
                    {
                        if (oItem.Current != null)
                        {
                            if (oPred(oItem.Current))
                            {
                                double x = oItem.Current.ToString().TToDoubleOrDefault();
                                if (x < nRet)
                                {
                                    nRet = x;
                                }
                            } 
                        }
                    }
                }

            }

            return nRet;
        }
    }
}
