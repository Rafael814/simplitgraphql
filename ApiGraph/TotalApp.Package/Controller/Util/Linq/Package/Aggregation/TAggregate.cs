﻿using System;
using System.Collections.Generic;

namespace TotalApp.Package.TLINQ
{
    /// <summary>
    /// *Autor:Larry Ramirez
    /// *Fecha de creacion:09-01-2018
    /// *Version:1.1.20180109
    /// *Sinopsis:
    /// </summary>
    public static partial class THelperLinq
    {
        #region "Funciones"

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="oData"></param>
        /// <param name="oPred"></param>
        /// <returns></returns>
        public static T TAggregate<T>(this IEnumerable<T> oData, Func<T, T, T> oPred)
        {
            if (oData.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oData));
            if (oPred.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oPred));

            using (IEnumerator<T> e = oData.GetEnumerator())
            {
                if (!e.MoveNext())
                {
                    throw THelperInfoEstatus.NoElements();
                }

                T oResult = e.Current;
                while (e.MoveNext())
                {
                    oResult = oPred(oResult, e.Current);
                }

                return oResult;
            }
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="TAccumulate"></typeparam>
        /// <param name="oData"></param>
        /// <param name="oSeed"></param>
        /// <param name="oPred"></param>
        /// <returns></returns>
        public static TAccumulate TAggregate<T, TAccumulate>(this IEnumerable<T> oData, TAccumulate oSeed, Func<TAccumulate, T, TAccumulate> oPred)
        {
            if (oData.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oData));
            if (oPred.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oPred));
            if (oSeed.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oSeed));

            TAccumulate oResult = oSeed;
            foreach (T oElement in oData)
            {
                oResult = oPred(oResult, oElement);
            }

            return oResult;
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="TAccumulate"></typeparam>
        /// <typeparam name="TResult"></typeparam>
        /// <param name="oData"></param>
        /// <param name="oSeed"></param>
        /// <param name="oPred"></param>
        /// <param name="oResultSelector"></param>
        /// <returns></returns>
        public static TResult TAggregate<T, TAccumulate, TResult>(this IEnumerable<T> oData
                                                                , TAccumulate oSeed
                                                                , Func<TAccumulate, T, TAccumulate> oPred
                                                                , Func<TAccumulate, TResult> oResultSelector)
        {
            if (oData.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oData));
            if (oPred.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oPred));
            if (oSeed.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oSeed));
            if (oResultSelector.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oResultSelector));

            TAccumulate oResult = oSeed;
            foreach (T oElement in oData)
            {
                oResult = oPred(oResult, oElement);
            }

            return oResultSelector(oResult);
        }
        #endregion
    }
}
