﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace TotalApp.Package.TLINQ
{
    /// <summary>
    /// *Autor:Larry Ramirez
    /// *Fecha de creacion:09-01-2018
    /// *Version:1.1.20180109
    /// *Sinopsis:
    /// </summary>
    public static partial class THelperLinq
    {
        #region "Funciones"
        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="ToData"></typeparam>
        /// <typeparam name="TKey"></typeparam>
        /// <typeparam name="TElement"></typeparam>
        /// <param name="oData"></param>
        /// <param name="oKeySelector"></param>
        /// <param name="oElementSelector"></param>
        /// <param name="oComparer"></param>
        /// <returns></returns>
        public static IEnumerable<IGrouping<TKey, TElement>> TGroupBy<ToData, TKey, TElement>(this IEnumerable<ToData> oData
                                                                                             ,Func<ToData, TKey> oKeySelector
                                                                                             ,Func<ToData, TElement> oElementSelector
                                                                                             ,IEqualityComparer<TKey> oComparer)
        {
            if (oData.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oData));
            if (oKeySelector.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oKeySelector));
            if (oElementSelector.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oElementSelector));

            return GroupByImpl(oData
                            , oKeySelector
                            , oElementSelector
                            , oComparer ?? EqualityComparer<TKey>.Default);
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="ToData"></typeparam>
        /// <typeparam name="TKey"></typeparam>
        /// <typeparam name="TElement"></typeparam>
        /// <param name="oData"></param>
        /// <param name="oKeySelector"></param>
        /// <param name="oElementSelector"></param>
        /// <param name="oComparer"></param>
        /// <returns></returns>
        private static IEnumerable<IGrouping<TKey, TElement>> GroupByImpl<ToData, TKey, TElement>(IEnumerable<ToData> oData
                                                                                                 , Func<ToData, TKey> oKeySelector
                                                                                                 , Func<ToData, TElement> oElementSelector
                                                                                                 , IEqualityComparer<TKey> oComparer)
        {
            var oLookup = oData.ToLookup(oKeySelector, oElementSelector, oComparer);
            foreach (var oResult in oLookup)
            {
                yield return oResult;
            }
        }
        #endregion
    }
}
