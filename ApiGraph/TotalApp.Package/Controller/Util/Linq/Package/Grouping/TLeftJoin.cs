﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace TotalApp.Package.TLINQ
{
    /// <summary>
    /// *Autor:Larry Ramirez
    /// *Fecha de creacion:09-01-2018
    /// *Version:1.1.20180109
    /// *Sinopsis:
    /// </summary>
    public static partial class THelperLinq
    {
        #region "Funciones"

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="TKey"></typeparam>
        /// <typeparam name="W"></typeparam>
        /// <param name="oFirst"></param>
        /// <param name="oSecond"></param>
        /// <param name="oKeySelect"></param>
        /// <param name="oFirstSelector"></param>
        /// <param name="oBothSelect"></param>
        /// <returns></returns>
        public static IEnumerable<W> TLeftJoin<T, TKey, W>(
            this IEnumerable<T> oFirst,
            IEnumerable<T> oSecond,
            Func<T, TKey> oKeySelect,
            Func<T, W> oFirstSelector,
            Func<T, T, W> oBothSelect)
        {
            if (oKeySelect == null) throw new ArgumentNullException(nameof(oKeySelect));

            return oFirst.TLeftJoin(oSecond, oKeySelect,
                                  oFirstSelector, oBothSelect,
                                  null);
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="TKey"></typeparam>
        /// <typeparam name="W"></typeparam>
        /// <param name="oFirst"></param>
        /// <param name="oSecond"></param>
        /// <param name="oKeySelect"></param>
        /// <param name="oFirstSelector"></param>
        /// <param name="oBothSelect"></param>
        /// <param name="oComper"></param>
        /// <returns></returns>
        public static IEnumerable<W> TLeftJoin<T, TKey, W>(
            this IEnumerable<T> oFirst,
            IEnumerable<T> oSecond,
            Func<T, TKey> oKeySelect,
            Func<T, W> oFirstSelector,
            Func<T, T, W> oBothSelect,
            IEqualityComparer<TKey> oComper)
        {
            if (oKeySelect == null) throw new ArgumentNullException(nameof(oKeySelect));
            return oFirst.TLeftJoin(oSecond,
                                  oKeySelect, oKeySelect,
                                  oFirstSelector, oBothSelect,
                                  oComper);
        }


        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="ToFirst"></typeparam>
        /// <typeparam name="ToSecond"></typeparam>
        /// <typeparam name="TKey"></typeparam>
        /// <typeparam name="W"></typeparam>
        /// <param name="oFirst"></param>
        /// <param name="oSecond"></param>
        /// <param name="oFirstoKeySelect"></param>
        /// <param name="oSecondoKeySelect"></param>
        /// <param name="oFirstSelector"></param>
        /// <param name="oBothSelect"></param>
        /// <returns></returns>
        public static IEnumerable<W> TLeftJoin<ToFirst, ToSecond, TKey, W>(
            this IEnumerable<ToFirst> oFirst,
            IEnumerable<ToSecond> oSecond,
            Func<ToFirst, TKey> oFirstoKeySelect,
            Func<ToSecond, TKey> oSecondoKeySelect,
            Func<ToFirst, W> oFirstSelector,
            Func<ToFirst, ToSecond, W> oBothSelect) =>
            oFirst.TLeftJoin(oSecond,
                           oFirstoKeySelect, oSecondoKeySelect,
                           oFirstSelector, oBothSelect,
                           null);



        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="ToFirst"></typeparam>
        /// <typeparam name="ToSecond"></typeparam>
        /// <typeparam name="TKey"></typeparam>
        /// <typeparam name="W"></typeparam>
        /// <param name="oFirst"></param>
        /// <param name="oSecond"></param>
        /// <param name="oFirstoKeySelect"></param>
        /// <param name="oSecondoKeySelect"></param>
        /// <param name="oFirstSelector"></param>
        /// <param name="oBothSelect"></param>
        /// <param name="oComper"></param>
        /// <returns></returns>
        public static IEnumerable<W> TLeftJoin<ToFirst, ToSecond, TKey, W>(
            this IEnumerable<ToFirst> oFirst,
            IEnumerable<ToSecond> oSecond,
            Func<ToFirst, TKey> oFirstoKeySelect,
            Func<ToSecond, TKey> oSecondoKeySelect,
            Func<ToFirst, W> oFirstSelector,
            Func<ToFirst, ToSecond, W> oBothSelect,
            IEqualityComparer<TKey> oComper)
        {
            if (oFirst == null) throw new ArgumentNullException(nameof(oFirst));
            if (oSecond == null) throw new ArgumentNullException(nameof(oSecond));
            if (oFirstoKeySelect == null) throw new ArgumentNullException(nameof(oFirstoKeySelect));
            if (oSecondoKeySelect == null) throw new ArgumentNullException(nameof(oSecondoKeySelect));
            if (oFirstSelector == null) throw new ArgumentNullException(nameof(oFirstSelector));
            if (oBothSelect == null) throw new ArgumentNullException(nameof(oBothSelect));

            KeyValuePair<TK, TV> Pair<TK, TV>(TK k, TV v) => new KeyValuePair<TK, TV>(k, v);

            return 
                from j in oFirst.GroupJoin(oSecond, oFirstoKeySelect, oSecondoKeySelect,
                                          (f, ss) => Pair(f, from s in ss select Pair(true, s)),
                                          oComper)
                from s in j.Value.DefaultIfEmpty()
                select s.Key ? oBothSelect(j.Key, s.Value) : oFirstSelector(j.Key);
        }
        #endregion
    }
}
