﻿using System.Collections.Generic;

namespace TotalApp.Package.TLINQ
{
    /// <summary>
    /// *Autor:Larry Ramirez
    /// *Fecha de creacion:09-01-2018
    /// *Version:1.1.20180109
    /// *Sinopsis:
    /// </summary>
    public static partial class THelperLinq
    {
        #region "Funciones"


        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="nStart"></param>
        /// <param name="nCount"></param>
        /// <returns></returns>
        public static IEnumerable<int> TRange(int nStart, int nCount)
        {
            if (nCount < 0)
            {
                if (nCount.TIsNull()) THelperInfoEstatus.ArgumentOutOfRange(nameof(nCount));
            }
            
            if ((((long)nStart + (long)nCount) - 1L) > int.MaxValue)
            {
                if (nCount.TIsNull()) THelperInfoEstatus.ArgumentOutOfRange(nameof(nCount));
            }

            return RangeImpl(nStart, nCount);
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="nStart"></param>
        /// <param name="nCount"></param>
        /// <returns></returns>
        private static IEnumerable<int> RangeImpl(int nStart, int nCount)
        {
            for (int nIndex = 0; nIndex < nCount; nIndex++)
            {
                yield return nStart + nIndex;
            }
        }


        #endregion


    }
}
