﻿using System.Collections.Generic;


namespace TotalApp.Package.TLINQ
{
    /// <summary>
    /// *Autor:Larry Ramirez
    /// *Fecha de creacion:09-01-2018
    /// *Version:1.1.20180109
    /// *Sinopsis:
    /// </summary>
    public static partial class THelperLinq
    {
        #region "Funciones"



        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="oData"></param>
        /// <returns></returns>
        public static IEnumerable<T> TDefaultIfEmpty<T>(this IEnumerable<T> oData) => TDefaultIfEmpty(oData, default(T));


        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="oData"></param>
        /// <param name="oDefaultValue"></param>
        /// <returns></returns>
        public static IEnumerable<T> TDefaultIfEmpty<T>(this IEnumerable<T> oData, T oDefaultValue)
        {
            if (oData.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oData));

            return DefaultIfEmptyImpl(oData, oDefaultValue);
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="oData"></param>
        /// <param name="oDefaultValue"></param>
        /// <returns></returns>
        private static IEnumerable<T> DefaultIfEmptyImpl<T>(IEnumerable<T> oData,T oDefaultValue)
        {
            using (IEnumerator<T> oIter = oData.GetEnumerator())
            {
                if (!oIter.MoveNext())
                {
                    yield return oDefaultValue;
                    yield break; 
                }
                yield return oIter.Current;
                while (oIter.MoveNext())
                {
                    yield return oIter.Current;
                }
            }
        }

        #endregion


    }
}
