﻿using System.Collections.Generic;

namespace TotalApp.Package.TLINQ
{
    /// <summary>
    /// *Autor:Larry Ramirez
    /// *Fecha de creacion:09-01-2018
    /// *Version:1.1.20180109
    /// *Sinopsis:
    /// </summary>
    public static partial class THelperLinq
    {
        #region "Funciones"

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="oElement"></param>
        /// <param name="nCount"></param>
        /// <returns></returns>
        public static IEnumerable<T> TRepeat<T>(T oElement, int nCount)
        {
            if (nCount < 0)
            {
                if (nCount.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(nCount));
            }
            return RepeatImpl(oElement, nCount);
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="oElement"></param>
        /// <param name="nCount"></param>
        /// <returns></returns>
        private static IEnumerable<T> RepeatImpl<T>(T oElement, int nCount)
        {
            for (int nIndex = 0; nIndex < nCount; nIndex++)
            {
                yield return oElement;
            }
        }


        #endregion


    }
}
