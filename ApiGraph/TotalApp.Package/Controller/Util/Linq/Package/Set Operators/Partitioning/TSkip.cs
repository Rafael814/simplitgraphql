﻿using System;
using System.Collections.Generic;

namespace TotalApp.Package.TLINQ
{
    /// <summary>
    /// *Autor:Larry Ramirez
    /// *Fecha de creacion:09-01-2018
    /// *Version:1.1.20180109
    /// *Sinopsis:
    /// </summary>
    public static partial class THelperLinq
    {
        #region "Funciones"
        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="oData"></param>
        /// <param name="nCount"></param>
        /// <returns></returns>
        public static IEnumerable<T> TSkip<T>(this IEnumerable<T> oData, int nCount)
        {
            if (oData.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oData));

            using (IEnumerator<T> oIterador = oData.GetEnumerator())
            {
                for (int nIndex = 0; nIndex < nCount; nIndex++)
                {
                    if (!oIterador.MoveNext())
                    {
                        yield break;
                    }
                }
                while (oIterador.MoveNext())
                {
                    yield return oIterador.Current;
                }
            }
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="oData"></param>
        /// <param name="nCount"></param>
        /// <param name="oPred"></param>
        /// <returns></returns>
        public static IEnumerable<T> TSkipIf<T>(this IEnumerable<T> oData, int nCount, Func<T, bool> oPred)
        {

            if (oData.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oData));
            
            if (oPred.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oPred));

            using (IEnumerator<T> oIterador = oData.GetEnumerator())
            {
                for (int nIndex = 0; nIndex < nCount; nIndex++)
                {
                    if (!oIterador.MoveNext())
                    {
                        yield break;
                    }
                }
                while (oIterador.MoveNext())
                {
                    if (oPred(oIterador.Current))
                    {
                        yield return oIterador.Current;
                    }
                }
            }
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="oData"></param>
        /// <param name="nCount"></param>
        /// <param name="oPred"></param>
        /// <returns></returns>
        public static IEnumerable<T> TSkipIfNot<T>(this IEnumerable<T> oData, int nCount, Func<T, bool> oPred)
        {

            if (oData.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oData));
            if (oPred.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oPred));

            using (IEnumerator<T> oIterador = oData.GetEnumerator())
            {
                for (int nIndex = 0; nIndex < nCount; nIndex++)
                {
                    if (!oIterador.MoveNext())
                    {
                        yield break;
                    }
                }
                while (oIterador.MoveNext())
                {
                    if (!oPred(oIterador.Current))
                    {
                        yield return oIterador.Current;
                    }
                }
            }
        }


        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="oData"></param>
        /// <param name="nCount"></param>
        /// <returns></returns>
        public static IEnumerable<T> TSkipLast<T>(this IEnumerable<T> oData, int nCount)
        {
            THelperInfoEstatus.ArgumentNull(nameof(oData));

            if (nCount < 1)
            {
                return oData;
            }


            return
                oData is ICollection<T> oColl
                ? oColl.TTake(oColl.Count - nCount)
                : _(); IEnumerable<T> _()
            {
                var oQueue = new Queue<T>(nCount);

                foreach (var oItem in oData)
                {
                    if (oQueue.Count < nCount)
                    {
                        oQueue.Enqueue(oItem);
                        continue;
                    }

                    yield return oQueue.Dequeue();
                    oQueue.Enqueue(oItem);
                }
            }
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="ToData"></typeparam>
        /// <param name="oData"></param>
        /// <param name="oPred"></param>
        /// <returns></returns>
        public static IEnumerable<T> TSkipUntil<T>(this IEnumerable<T> oData, Func<T, bool> oPred)
        {
            if (oData.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oData));
            if (oPred.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oPred));

            return _(); IEnumerable<T> _()
            {
                using (var oIterador = oData.GetEnumerator())
                {
                    while (oIterador.MoveNext())
                    {
                        if (oPred(oIterador.Current))
                        {
                            break;
                        }

                    }

                    while (oIterador.MoveNext())
                    {
                        yield return oIterador.Current;
                    }

                }
            }
        }
        #endregion
    }
}
