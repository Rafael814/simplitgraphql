﻿using System;
using System.Collections.Generic;

namespace TotalApp.Package.TLINQ
{
    /// <summary>
    /// *Autor:Larry Ramirez
    /// *Fecha de creacion:09-01-2018
    /// *Version:1.1.20180109
    /// *Sinopsis:
    /// </summary>
    public static partial class THelperLinq
    {
        #region "Funciones"
        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="oData"></param>
        /// <param name="nCount"></param>
        /// <returns></returns>
        public static IEnumerable<T> TTake<T>(this IEnumerable<T> oData, int nCount)
        {

            if (oData.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oData));
            if (nCount == 0) THelperInfoEstatus.ArgumentNull("Elemento vacio:" + nameof(nCount));


            using (IEnumerator<T> oIterador = oData.GetEnumerator())
            {
                for (int nIndex = 0; nIndex < nCount && oIterador.MoveNext(); nIndex++)
                {
                    yield return oIterador.Current;
                }
            }
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="oData"></param>
        /// <param name="nCount"></param>
        /// <param name="oPred"></param>
        /// <returns></returns>
        public static IEnumerable<T> TTakeIf<T>(this IEnumerable<T> oData, int nCount, Func<T, bool> oPred)
        {

            if (oData.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oData));
            if (nCount == 0) THelperInfoEstatus.ArgumentNull("Elemento vacio:" + nameof(nCount));
            if (oPred.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oPred));

            using (IEnumerator<T> oIterador = oData.GetEnumerator())
            {
                for (int nIndex = 0; nIndex < nCount && oIterador.MoveNext(); nIndex++)
                {
                    if (oPred(oIterador.Current))
                    {
                        yield return oIterador.Current;
                    }
                }
            }
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="oData"></param>
        /// <param name="nCount"></param>
        /// <param name="oPred"></param>
        /// <returns></returns>
        public static IEnumerable<T> TTakeIfNot<T>(this IEnumerable<T> oData, int nCount, Func<T, bool> oPred)
        {

            if (oData.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oData));
            if (nCount == 0) THelperInfoEstatus.ArgumentNull("Elemento vacio:" + nameof(nCount));
            if (oPred.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oPred));

            using (IEnumerator<T> oIterador = oData.GetEnumerator())
            {
                for (int nIndex = 0; nIndex < nCount && oIterador.MoveNext(); nIndex++)
                {
                    if (!oPred(oIterador.Current))
                    {
                        yield return oIterador.Current;
                    }
                }
            }
        }


        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="oData"></param>
        /// <param name="nCount"></param>
        /// <returns></returns>
        public static IEnumerable<T> TTakeLast<T>(this IEnumerable<T> oData, int nCount)
        {
            if (oData.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oData));
            if (nCount == 0) THelperInfoEstatus.ArgumentNull("Elemento vacio:" + nameof(nCount));

            return _(); IEnumerable<T> _()
            {
                if (nCount <= 0)
                {
                    yield break;
                }


                var oQ = new Queue<T>(nCount);

                foreach (var item in oData)
                {
                    if (oQ.Count == nCount)
                    {
                        oQ.Dequeue();
                    }
                    oQ.Enqueue(item);
                }

                foreach (var oItem in oQ)
                {
                    yield return oItem;
                }

            }
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="oData"></param>
        /// <param name="oPred"></param>
        /// <returns></returns>
        public static IEnumerable<T> TTakeUntil<T>(this IEnumerable<T> oData, Func<T, bool> oPred)
        {
            if (oData.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oData));
            if (oPred.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oPred));

            return _(); IEnumerable<T> _()
            {
                foreach (var oItem in oData)
                {
                    yield return oItem;

                    if (oPred(oItem))
                    {
                        yield break;
                    }

                }
            }
        }
        #endregion
    }
}
