﻿using System;
using System.Collections.Generic;

namespace TotalApp.Package.TLINQ
{
    /// <summary>
    /// *Autor:Larry Ramirez
    /// *Fecha de creacion:09-01-2018
    /// *Version:1.1.20180109
    /// *Sinopsis:
    /// </summary>
    public static partial class THelperLinq
    {
        #region "Funciones"

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="TKey"></typeparam>
        /// <param name="oData"></param>
        /// <param name="oPred"></param>
        /// <returns></returns>
        public static IEnumerable<T> TDistinctBy<T, TKey>(this IEnumerable<T> oData
                                                        , Func<T, TKey> oPred)
        {
            return oData.TDistinctBy(oPred, null);
        }


        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="TKey"></typeparam>
        /// <param name="oData"></param>
        /// <param name="oPred"></param>
        /// <param name="oComparer"></param>
        /// <returns></returns>
        public static IEnumerable<T> TDistinctBy<T, TKey>(this IEnumerable<T> oData
                                                       , Func<T, TKey> oPred
                                                       , IEqualityComparer<TKey> oComparer)
        {
            if (oData.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oData));
            if (oPred.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oPred));

            return _(); IEnumerable<T> _()
            {
                var oKeys = new HashSet<TKey>(oComparer);

                foreach (var oElement in oData)
                {
                    if (oKeys.Add(oPred(oElement)))
                    {
                        yield return oElement;
                    }
                }
            }
        }
        #endregion
    }
}
