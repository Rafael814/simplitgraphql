﻿using System;
using System.Collections.Generic;

namespace TotalApp.Package.TLINQ
{
    /// <summary>
    /// *Autor:Larry Ramirez
    /// *Fecha de creacion:09-01-2018
    /// *Version:1.1.20180109
    /// *Sinopsis:
    /// </summary>
    public static partial class THelperLinq
    {
        #region "Funciones"
        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="oFirst"></param>
        /// <param name="oSecond"></param>
        /// <returns></returns>
        public static IEnumerable<T> TExcept<T>(this IEnumerable<T> oFirst, IEnumerable<T> oSecond)
        {
            if (oFirst.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oFirst));
            if (oSecond.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oSecond));

            return ExceptIterator(oFirst, oSecond, null);
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="oFirst"></param>
        /// <param name="oSecond"></param>
        /// <param name="comparer"></param>
        /// <returns></returns>
        public static IEnumerable<T> Except<T>(this IEnumerable<T> oFirst, IEnumerable<T> oSecond, IEqualityComparer<T> comparer)
        {
            if (oFirst.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oFirst));
            if (oSecond.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oSecond));

            return ExceptIterator(oFirst, oSecond, comparer);
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="oFirst"></param>
        /// <param name="oSecond"></param>
        /// <param name="oComparer"></param>
        /// <returns></returns>
        private static IEnumerable<T> ExceptIterator<T>(IEnumerable<T> oFirst, IEnumerable<T> oSecond, IEqualityComparer<T> oComparer)
        {
            HashSet<T> oSet = new HashSet<T>(oComparer);

            foreach (T oElement in oSecond)
            {
                oSet.Add(oElement);
            }

            foreach (T oElement in oFirst)
            {
                if (oSet.Add(oElement))
                {
                    yield return oElement;
                }
            }
        }

        #endregion
    }
}
