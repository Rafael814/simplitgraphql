﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace TotalApp.Package.TLINQ
{
    /// <summary>
    /// *Autor:Larry Ramirez
    /// *Fecha de creacion:09-01-2018
    /// *Version:1.1.20180109
    /// *Sinopsis:
    /// </summary>
    public static partial class THelperLinq
    {
        #region "Funciones"

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.0
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="U"></typeparam>
        /// <param name="oData"></param>
        /// <param name="oRowSelect"></param>
        /// <returns></returns>
        public static IEnumerable<TGrouped<U, T>> TGroup<T, U>(this IEnumerable<T> oData
                                                             , Expression<Func<T, U>> oRowSelect)

        {
            if (oData.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oData));
            if (oRowSelect == null) THelperInfoEstatus.ArgumentNull(nameof(oRowSelect));

            return oData.GroupBy(oRowSelect.Compile()
                               , (x, y) => new TGrouped<U, T>
                               {
                                   Key = x
                                                              ,
                                   Values = y
                               });
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.0
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="U"></typeparam>
        /// <param name="oData"></param>
        /// <param name="oRowSelect"></param>
        /// <returns></returns>
        public static Dictionary<U, List<T>> TGroupToToDictionary<T, U>(this IEnumerable<T> oData
                                                                      , Func<T, U> oRowSelect)
        {
            if (oData.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oData));
            if (oRowSelect == null) THelperInfoEstatus.ArgumentNull(nameof(oRowSelect));

            return oData.GroupBy(oRowSelect).ToDictionary(x => x.Key
                                                        , x => x.ToList());
        }
        #endregion
    }
}
