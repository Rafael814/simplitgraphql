﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace TotalApp.Package.TLINQ
{
    /// <summary>
    /// *Autor:Larry Ramirez
    /// *Fecha de creacion:09-01-2018
    /// *Version:1.1.20180109
    /// *Sinopsis:
    /// </summary>
    public static partial class THelperLinq
    {
        #region "Funciones"


        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.0
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="U"></typeparam>
        /// <param name="oData"></param>
        /// <param name="oRowSelect"></param>
        /// <param name="oPredSum"></param>
        /// <param name="oPredCount"></param>
        /// <returns></returns>
        public static IEnumerable<TGroupedCountAndSumfloat<U>> TGroupCountAndSum<T, U>(this IEnumerable<T> oData
                                                                                      , Expression<Func<T, U>> oRowSelect
                                                                                      , Func<T, float> oPredSum
                                                                                      , Func<T, bool> oPredCount = null)

        {
            if (oData.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oData));
            if (oRowSelect == null) THelperInfoEstatus.ArgumentNull(nameof(oRowSelect));
            if (oPredSum == null) THelperInfoEstatus.ArgumentNull(nameof(oPredSum));

            return oData.GroupBy(oRowSelect.Compile()
                               , (x, y) => new TGroupedCountAndSumfloat<U>
                               {
                                   Key = x
                             ,
                                   Sum = y.Sum(oPredSum)
                             ,
                                   Count = oPredCount == null ? y.Count() : y.Count(oPredCount)
                               });
        }


        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.0
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="U"></typeparam>
        /// <param name="oData"></param>
        /// <param name="oRowSelect"></param>
        /// <param name="oPredSum"></param>
        /// <param name="oPredCount"></param>
        /// <returns></returns>
        public static IEnumerable<TGroupedCountAndSumfloat<U>> TGroupCountAndSum<T, U>(this IEnumerable<T> oData
                                                                                      , Expression<Func<T, U>> oRowSelect
                                                                                      , Func<T, float?> oPredSum
                                                                                      , Func<T, bool> oPredCount = null)

        {
            if (oData.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oData));
            if (oRowSelect == null) THelperInfoEstatus.ArgumentNull(nameof(oRowSelect));
            if (oPredSum == null) THelperInfoEstatus.ArgumentNull(nameof(oPredSum));

            return oData.GroupBy(oRowSelect.Compile()
                               , (x, y) => new TGroupedCountAndSumfloat<U>
                               {
                                   Key = x
                             ,
                                   Sum = y.Sum(oPredSum) ?? 0
                             ,
                                   Count = oPredCount == null ? y.Count() : y.Count(oPredCount)
                               });
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.0
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="U"></typeparam>
        /// <param name="oData"></param>
        /// <param name="oRowSelect"></param>
        /// <param name="oPredSum"></param>
        /// <param name="oPredCount"></param>
        /// <returns></returns>
        public static IEnumerable<TGroupedDataCountAndSumfloat<U, T>> TGroupDataCountAndSum<T, U>(this IEnumerable<T> oData
                                                                                                , Expression<Func<T, U>> oRowSelect
                                                                                                , Func<T, float> oPredSum
                                                                                                , Func<T, bool> oPredCount = null)

        {
            if (oData.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oData));
            if (oRowSelect == null) THelperInfoEstatus.ArgumentNull(nameof(oRowSelect));
            if (oPredSum == null) THelperInfoEstatus.ArgumentNull(nameof(oPredSum));

            return oData.GroupBy(oRowSelect.Compile()
                               , (x, y) => new TGroupedDataCountAndSumfloat<U, T>
                               {
                                   Key = x
                             ,
                                   Values = y
                             ,
                                   Sum = y.Sum(oPredSum)
                             ,
                                   Count = oPredCount == null ? y.Count() : y.Count(oPredCount)
                               });
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.0
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="U"></typeparam>
        /// <param name="oData"></param>
        /// <param name="oRowSelect"></param>
        /// <param name="oPredSum"></param>
        /// <param name="oPredCount"></param>
        /// <returns></returns>
        public static IEnumerable<TGroupedDataCountAndSumfloat<U, T>> TGroupDataCountAndSum<T, U>(this IEnumerable<T> oData
                                                                                                , Expression<Func<T, U>> oRowSelect
                                                                                                , Func<T, float?> oPredSum
                                                                                                , Func<T, bool> oPredCount = null)

        {
            if (oData.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oData));
            if (oRowSelect == null) THelperInfoEstatus.ArgumentNull(nameof(oRowSelect));
            if (oPredSum == null) THelperInfoEstatus.ArgumentNull(nameof(oPredSum));

            return oData.GroupBy(oRowSelect.Compile()
                               , (x, y) => new TGroupedDataCountAndSumfloat<U, T>
                               {
                                   Key = x
                             ,
                                   Values = y
                             ,
                                   Sum = y.Sum(oPredSum) ?? 0
                             ,
                                   Count = oPredCount == null ? y.Count() : y.Count(oPredCount)
                               });
        }
        #endregion
    }
}
