﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace TotalApp.Package.TLINQ
{
    /// <summary>
    /// *Autor:Larry Ramirez
    /// *Fecha de creacion:09-01-2018
    /// *Version:1.1.20180109
    /// *Sinopsis:
    /// </summary>
    public static partial class THelperLinq
    {
        #region "Funciones"
        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.0
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="U"></typeparam>
        /// <param name="oData"></param>
        /// <param name="oRowSelect"></param>
        /// <param name="oPred"></param>
        /// <returns></returns>
        public static IEnumerable<TGroupedSumint<U>> TGroupSum<T, U>(this IEnumerable<T> oData
                                                                    , Expression<Func<T, U>> oRowSelect
                                                                    , Func<T, int> oPred)

        {
            if (oData.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oData));
            if (oRowSelect == null) THelperInfoEstatus.ArgumentNull(nameof(oRowSelect));
            if (oPred == null) THelperInfoEstatus.ArgumentNull(nameof(oPred));

            
            return oData.GroupBy(oRowSelect.Compile()
                               , (x, y) => new TGroupedSumint<U>
                               {
                                   Key = x
                             ,
                                   Sum =  y.Sum(oPred)
                               });
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.0
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="U"></typeparam>
        /// <param name="oData"></param>
        /// <param name="oRowSelect"></param>
        /// <param name="oPred"></param>
        /// <returns></returns>
        public static IEnumerable<TGroupedSumint<U>> TGroupSum<T, U>(this IEnumerable<T> oData
                                                                    , Expression<Func<T, U>> oRowSelect
                                                                    , Func<T, int?> oPred)

        {
            if (oData.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oData));
            if (oRowSelect == null) THelperInfoEstatus.ArgumentNull(nameof(oRowSelect));
            if (oPred == null) THelperInfoEstatus.ArgumentNull(nameof(oPred));

            return oData.GroupBy(oRowSelect.Compile()
                               , (x, y) => new TGroupedSumint<U>
                               {
                                   Key = x
                             ,
                                   Sum = y.Sum(oPred) ?? 0
                               });
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.0
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="U"></typeparam>
        /// <param name="oData"></param>
        /// <param name="oRowSelect"></param>
        /// <param name="oPred"></param>
        /// <returns></returns>
        public static IEnumerable<TGroupedDataSumint<U, T>> TGroupDataSum<T, U>(this IEnumerable<T> oData
                                                                              , Expression<Func<T, U>> oRowSelect
                                                                              , Func<T, int> oPred)

        {
            if (oData.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oData));
            if (oRowSelect == null) THelperInfoEstatus.ArgumentNull(nameof(oRowSelect));
            if (oPred == null) THelperInfoEstatus.ArgumentNull(nameof(oPred));

            return oData.GroupBy(oRowSelect.Compile()
                               , (x, y) => new TGroupedDataSumint<U, T>
                               {
                                   Key = x
                             ,
                                   Values = y
                             ,
                                  Sum = y.Sum(oPred)
                               });
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.0
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="U"></typeparam>
        /// <param name="oData"></param>
        /// <param name="oRowSelect"></param>
        /// <param name="oPred"></param>
        /// <returns></returns>
        public static IEnumerable<TGroupedDataSumint<U, T>> TGroupDataSum<T, U>(this IEnumerable<T> oData
                                                                              , Expression<Func<T, U>> oRowSelect
                                                                              , Func<T, int?> oPred)

        {
            if (oData.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oData));
            if (oRowSelect == null) THelperInfoEstatus.ArgumentNull(nameof(oRowSelect));
            if (oPred == null) THelperInfoEstatus.ArgumentNull(nameof(oPred));

            return oData.GroupBy(oRowSelect.Compile()
                               , (x, y) => new TGroupedDataSumint<U, T>
                               {
                                   Key = x
                             ,
                                   Values = y
                             ,
                                   Sum = y.Sum(oPred) ?? 0
                               });
        }
        #endregion
    }
}
