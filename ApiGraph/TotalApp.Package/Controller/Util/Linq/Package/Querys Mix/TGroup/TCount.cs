﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace TotalApp.Package.TLINQ
{
    /// <summary>
    /// *Autor:Larry Ramirez
    /// *Fecha de creacion:09-01-2018
    /// *Version:1.1.20180109
    /// *Sinopsis:
    /// </summary>
    public static partial class THelperLinq
    {
        #region "Funciones"
        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.0
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="U"></typeparam>
        /// <param name="oData"></param>
        /// <param name="oRowSelect"></param>
        /// <param name="oPred"></param>
        /// <returns></returns>
        public static IEnumerable<TGroupedCount<U>> TGroupCount<T, U>(this IEnumerable<T> oData
                                                                     , Expression<Func<T, U>> oRowSelect
                                                                     , Func<T, bool> oPred = null)

        {
            if (oData.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oData));
            if (oRowSelect == null) THelperInfoEstatus.ArgumentNull(nameof(oRowSelect));

            return oData.GroupBy(oRowSelect.Compile()
                               , (x, y) => new TGroupedCount<U>
                               {
                                   Key = x
                             ,
                                   Count = oPred == null ? y.Count() : y.Count(oPred)
                               });
        }


        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.0
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="U"></typeparam>
        /// <param name="oData"></param>
        /// <param name="oRowSelect"></param>
        /// <param name="oPred"></param>
        /// <returns></returns>
        public static IEnumerable<TGroupedDataCount<U, T>> TGroupDataCount<T, U>(this IEnumerable<T> oData
                                                                           , Expression<Func<T, U>> oRowSelect
                                                                           , Func<T, bool> oPred = null)

        {
            if (oData.TIsNull()) THelperInfoEstatus.ArgumentNull(nameof(oData));
            if (oRowSelect == null) THelperInfoEstatus.ArgumentNull(nameof(oRowSelect));

            return oData.GroupBy(oRowSelect.Compile()
                               , (x, y) => new TGroupedDataCount<U, T>
                               {
                                   Key = x
                             ,
                                   Values = y
                             ,
                                   Count = oPred == null ? y.Count() : y.Count(oPred)
                               });
        }

      
        #endregion
    }
}
