﻿using System;
using System.Collections.Generic;

namespace TotalApp.Package.TLINQ
{
    /// <summary>
    /// *Autor:Larry Ramirez
    /// *Fecha de creacion:09-01-2018
    /// *Version:1.1.20180109
    /// *Sinopsis:
    /// </summary>
    public static partial class THelperLinq
    {
        #region "Funciones"

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="U"></typeparam>
        /// <param name="oData"></param>
        /// <param name="oWhere"></param>
        /// <param name="oSelect"></param>
        /// <returns></returns>
        public static IEnumerable<U> TQueryIf<T,U>(this IEnumerable<T> oData
                                                , Func<T, bool> oWhere
                                                , Func<T, U> oSelect)
        {
            try
            {
                return oData
                      .TWhereIf(oWhere)
                      .TSelect(oSelect);
            }
            catch
            {

                return default(IEnumerable<U>);
            }
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="U"></typeparam>
        /// <param name="oData"></param>
        /// <param name="oWhere"></param>
        /// <param name="oSelect"></param>
        /// <returns></returns>
        public static IEnumerable<U> TQueryIfNot<T, U>(this IEnumerable<T> oData
                                                     , Func<T, bool> oWhere
                                                     , Func<T, U> oSelect)
        {
            try
            {
                return oData
                      .TWhereIfNot(oWhere)
                      .TSelect(oSelect);
            }
            catch
            {

                return default(IEnumerable<U>);
            }
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="oData"></param>
        /// <returns></returns>
        public static int TCountAllRecords(this System.Data.DataSet oData) => oData
                                                                             .Tables
                                                                             .TOfType<System.Data.DataTable>()
                                                                             .TSumOrDefault(x => x.Rows.Count);


        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="oData"></param>
        /// <returns></returns>
        public static int TCountAllRecords<T>(params IEnumerable<T>[] oData) => oData
                                                                               .TSumOrDefault(x => x.TCountOrDefault());


        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="oData"></param>
        /// <returns></returns>
        public static bool TAnyRecords<T>(params IEnumerable<T>[] oData)
        {
            foreach (var oItemData in oData)
            {
                if (!oItemData.TAny())
                {
                    return false;
                }
            }
            return true;
        }
        #endregion
    }
}
