﻿using Newtonsoft.Json;

namespace TotalApp.Package.UTIL
{
    /// <summary>
    /// *Autor:Larry Ramirez
    /// *Fecha de creacion:09-01-2018
    /// *Version:1.1.20180109
    /// *Sinopsis:
    /// </summary>
    internal class BypassListManagement
    {
        #region "Constantes"
        #endregion

        #region "Variables"
        private bool _bIsEnable;
        #endregion

        #region "Propiedades"
        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        [JsonProperty(PropertyName = "enable")]
        public bool Enable
        {
            get
            {
                return _bIsEnable;
            }

            set
            {
                _bIsEnable = value;
            }
        }
        #endregion

        #region "Funciones"
        #endregion

        #region "Metodos"
        #endregion

        #region "Eventos"
        #endregion

        #region "Destructor"
        #endregion

    }
}

