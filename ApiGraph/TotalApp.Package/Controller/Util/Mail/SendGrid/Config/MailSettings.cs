﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;


namespace TotalApp.Package.UTIL
{

    /// <summary>
    /// *Autor:Larry Ramirez
    /// *Fecha de creacion:09-01-2018
    /// *Version:1.1.20180109
    /// *Sinopsis:
    /// </summary>
    internal class MailSettings
    {
        #region "Constantes"
        #endregion

        #region "Variables"
        private BCCSettings _oBccSettings;
        private BypassListManagement _oBypassListManagement;
        private FooterSettings _oFooterSettings;
        private SandboxMode _oSandboxMode;
        private SpamCheck _oSpamCheck;
        #endregion

        #region "Propiedades"
        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        [JsonProperty(PropertyName = "bcc")]
        public BCCSettings BccSettings
        {
            get
            {
                return _oBccSettings;
            }

            set
            {
                _oBccSettings = value;
            }
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        [JsonProperty(PropertyName = "bypass_list_management")]
        public BypassListManagement BypassListManagement
        {
            get
            {
                return _oBypassListManagement;
            }

            set
            {
                _oBypassListManagement = value;
            }
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        [JsonProperty(PropertyName = "footer")]
        public FooterSettings FooterSettings
        {
            get
            {
                return _oFooterSettings;
            }

            set
            {
                _oFooterSettings = value;
            }
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        [JsonProperty(PropertyName = "sandbox_mode")]
        public SandboxMode SandboxMode
        {
            get
            {
                return _oSandboxMode;
            }

            set
            {
                _oSandboxMode = value;
            }
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        [JsonProperty(PropertyName = "spam_check")]
        public SpamCheck SpamCheck
        {
            get
            {
                return _oSpamCheck;
            }

            set
            {
                _oSpamCheck = value;
            }
        }
        #endregion

        #region "Funciones"
        #endregion

        #region "Metodos"
        #endregion

        #region "Eventos"
        #endregion

        #region "Destructor"
        #endregion
    }
}

