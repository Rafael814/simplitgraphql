﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;


namespace TotalApp.Package.UTIL
{
    /// <summary>
    /// *Autor:Larry Ramirez
    /// *Fecha de creacion:09-01-2018
    /// *Version:1.1.20180109
    /// *Sinopsis:
    /// </summary>
    internal class Mail
    {
        #region "Constantes"
        #endregion

        #region "Variables"
        private Email _oFrom;
        private string _sSubject;
        private List<Personalization> _oPersonalizations;
        private List<Content> _oContents;
        private List<Attachment> _oAttachments;
        private string _sTemplateId;
        private Dictionary<string, string> _oHeaders;
        private Dictionary<string, string> _oSections;
        private List<string> _oCategories;
        private Dictionary<string, string> _oCustomArgs;
        private long _nSendAt;
        private ASM _oAsm;
        private string _sBatchId;
        private string _sSetIpPoolId;
        private MailSettings _oMailSettings;
        private TrackingSettings _oTrackingSettings;
        private Email _oReplyTo;
        #endregion

        #region "Propiedades"
        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        [JsonProperty(PropertyName = "from")]
        public Email From
        {
            get
            {
                return this._oFrom;
            }

            set
            {
                _oFrom = value;
            }
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        [JsonProperty(PropertyName = "subject")]
        public string Subject
        {
            get
            {
                return this._sSubject;
            }

            set
            {
                _sSubject = value;
            }
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        [JsonProperty(PropertyName = "personalizations")]
        public List<Personalization> Personalization
        {
            get
            {
                return this._oPersonalizations;
            }

            set
            {
                _oPersonalizations = value;
            }
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        [JsonProperty(PropertyName = "content")]
        public List<Content> Contents
        {
            get
            {
                return this._oContents;
            }

            set
            {
                _oContents = value;
            }
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        [JsonProperty(PropertyName = "attachments")]
        public List<Attachment> Attachments
        {
            get
            {
                return this._oAttachments;
            }

            set
            {
                _oAttachments = value;
            }
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        [JsonProperty(PropertyName = "template_id")]
        public string TemplateId
        {
            get
            {
                return this._sTemplateId;
            }

            set
            {
                _sTemplateId = value;
            }
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        [JsonProperty(PropertyName = "headers")]
        public Dictionary<string, string> Headers
        {
            get
            {
                return this._oHeaders;
            }

            set
            {
                _oHeaders = value;
            }
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        [JsonProperty(PropertyName = "sections")]
        public Dictionary<string, string> Sections
        {
            get
            {
                return this._oSections;
            }

            set
            {
                _oSections = value;
            }
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        [JsonProperty(PropertyName = "categories")]
        public List<string> Categories
        {
            get
            {
                return this._oCategories;
            }

            set
            {
                _oCategories = value;
            }
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        [JsonProperty(PropertyName = "custom_args")]
        public Dictionary<string, string> CustomArgs
        {
            get
            {
                return this._oCustomArgs;
            }

            set
            {
                _oCustomArgs = value;
            }
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        [JsonProperty(PropertyName = "send_at")]
        public long SendAt
        {
            get
            {
                return this._nSendAt;
            }

            set
            {
                _nSendAt = value;
            }
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        [JsonProperty(PropertyName = "asm")]
        public ASM Asm
        {
            get
            {
                return this._oAsm;
            }

            set
            {
                _oAsm = value;
            }
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        [JsonProperty(PropertyName = "batch_id")]
        public string BatchId
        {
            get
            {
                return this._sBatchId;
            }

            set
            {
                _sBatchId = value;
            }
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        [JsonProperty(PropertyName = "ip_pool_name")]
        public string SetIpPoolId
        {
            get
            {
                return this._sSetIpPoolId;
            }

            set
            {
                _sSetIpPoolId = value;
            }
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        [JsonProperty(PropertyName = "mail_settings")]
        public MailSettings MailSettings
        {
            get
            {
                return this._oMailSettings;
            }

            set
            {
                _oMailSettings = value;
            }
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        [JsonProperty(PropertyName = "tracking_settings")]
        public TrackingSettings TrackingSettings
        {
            get
            {
                return this._oTrackingSettings;
            }

            set
            {
                _oTrackingSettings = value;
            }
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        [JsonProperty(PropertyName = "reply_to")]
        public Email ReplyTo
        {
            get
            {
                return this._oReplyTo;
            }

            set
            {
                _oReplyTo = value;
            }
        }
        #endregion

        #region "Funciones"
        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <returns></returns>
        public String GetJson()
        {
            return JsonConvert.SerializeObject(this,
                                Formatting.None,
                                new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.Ignore, StringEscapeHandling = StringEscapeHandling.EscapeHtml });
        }
        #endregion

        #region "Metodos"
        public Mail()
        {
            return;
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="oFrom"></param>
        /// <param name="sSubject"></param>
        /// <param name="oTo"></param>
        /// <param name="oContent"></param>
        public Mail(Email oFrom, string sSubject, Email oTo, Content oContent)
        {
            this.From = oFrom;
            Personalization oPersonalization = new Personalization();
            oPersonalization.AddTo(oTo);
            this.AddPersonalization(oPersonalization);
            this.Subject = sSubject;
            this.AddContent(oContent);
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="oPersonalization"></param>
        public void AddPersonalization(Personalization oPersonalization)
        {
            if (Personalization == null)
            {
                Personalization = new List<Personalization>();
            }
            Personalization.Add(oPersonalization);
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="oContent"></param>
        public void AddContent(Content oContent)
        {
            if (Contents == null)
            {
                Contents = new List<Content>();
            }
            Contents.Add(oContent);
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="oAttachment"></param>
        public void AddAttachment(Attachment oAttachment)
        {
            if (Attachments == null)
            {
                Attachments = new List<Attachment>();
            }
            Attachments.Add(oAttachment);
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sKey"></param>
        /// <param name="sValue"></param>
        public void AddHeader(string sKey, string sValue)
        {
            if (this._oHeaders == null)
            {
                _oHeaders = new Dictionary<string, string>();
            }
            _oHeaders.Add(sKey, sValue);
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sKey"></param>
        /// <param name="sValue"></param>
        public void AddSection(string sKey, string sValue)
        {
            if (this._oSections == null)
            {
                _oSections = new Dictionary<string, string>();
            }
            _oSections.Add(sKey, sValue);
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sCategory"></param>
        public void AddCategory(string sCategory)
        {
            if (Categories == null)
            {
                Categories = new List<String>();
            }
            Categories.Add(sCategory);
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sKey"></param>
        /// <param name="sValue"></param>
        public void AddCustomArgs(string sKey, string sValue)
        {
            if (this._oCustomArgs == null)
            {
                _oCustomArgs = new Dictionary<string, string>();
            }
            _oCustomArgs.Add(sKey, sValue);
        }
        #endregion

        #region "Eventos"
        #endregion

        #region "Destructor"
        #endregion

    }

}

