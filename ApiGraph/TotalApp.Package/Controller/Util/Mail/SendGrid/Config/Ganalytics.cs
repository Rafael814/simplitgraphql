﻿using Newtonsoft.Json;


namespace TotalApp.Package.UTIL
{

    /// <summary>
    /// *Autor:Larry Ramirez
    /// *Fecha de creacion:09-01-2018
    /// *Version:1.1.20180109
    /// *Sinopsis:
    /// </summary>
    internal class Ganalytics
    {
        #region "Constantes"
        #endregion

        #region "Variables"
        private bool _bIsEnable;
        private string _sUtmSource;
        private string _sUtmMedium;
        private string _sUtmTerm;
        private string _sUtmContent;
        private string _sUtmCampaign;
        #endregion

        #region "Propiedades"
        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        [JsonProperty(PropertyName = "enable")]
        public bool Enable
        {
            get
            {
                return this._bIsEnable;
            }

            set
            {
                _bIsEnable = value;
            }
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        [JsonProperty(PropertyName = "utm_source")]
        public string UtmSource
        {
            get
            {
                return _sUtmSource;
            }

            set
            {
                _sUtmSource = value;
            }
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        [JsonProperty(PropertyName = "utm_medium")]
        public string UtmMedium
        {
            get
            {
                return _sUtmMedium;
            }

            set
            {
                _sUtmMedium = value;
            }
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        [JsonProperty(PropertyName = "utm_term")]
        public string UtmTerm
        {
            get
            {
                return _sUtmTerm;
            }

            set
            {
                _sUtmTerm = value;
            }
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        [JsonProperty(PropertyName = "utm_content")]
        public string UtmContent
        {
            get
            {
                return _sUtmContent;
            }

            set
            {
                _sUtmContent = value;
            }
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        [JsonProperty(PropertyName = "utm_campaign")]
        public string UtmCampaign
        {
            get
            {
                return _sUtmCampaign;
            }

            set
            {
                _sUtmCampaign = value;
            }
        }
        #endregion

        #region "Funciones"
        #endregion

        #region "Metodos"
        #endregion

        #region "Eventos"
        #endregion

        #region "Destructor"
        #endregion

    }
    
}

