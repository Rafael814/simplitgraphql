﻿using System;
using Newtonsoft.Json;


namespace TotalApp.Package.UTIL
{

    /// <summary>
    /// *Autor:Larry Ramirez
    /// *Fecha de creacion:09-01-2018
    /// *Version:1.1.20180109
    /// *Sinopsis:
    /// </summary>
    internal class Content
    {
        #region "Constantes"
        #endregion

        #region "Variables"
        private string _sType;
        private String _sValue;
        #endregion

        #region "Propiedades"
        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        [JsonProperty(PropertyName = "type")]
        public string Type
        {
            get
            {
                return _sType;
            }

            set
            {
                _sType = value;
            }
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        [JsonProperty(PropertyName = "value")]
        public String Value
        {
            get
            {
                return _sValue;
            }

            set
            {
                this._sValue = value;
            }
        }
        #endregion

        #region "Funciones"
        #endregion

        #region "Metodos"
        public Content()
        {
            return;
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sType"></param>
        /// <param name="sValue"></param>
        public Content(string sType, String sValue)
        {
            this.Type = sType;
            this.Value = sValue;
        }
        #endregion

        #region "Eventos"
        #endregion

        #region "Destructor"
        #endregion

    }
}

