﻿using System;
using Newtonsoft.Json;


namespace TotalApp.Package.UTIL
{

    /// <summary>
    /// *Autor:Larry Ramirez
    /// *Fecha de creacion:09-01-2018
    /// *Version:1.1.20180109
    /// *Sinopsis:
    /// </summary>
    internal class SpamCheck
    {
        #region "Constantes"
        #endregion

        #region "Variables"
        private bool _bIsEnable;
        private int _nThreshold;
        private String _sPostToUrl;
        #endregion

        #region "Propiedades"
        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        [JsonProperty(PropertyName = "enable")]
        public bool Enable
        {
            get
            {
                return _bIsEnable;
            }

            set
            {
                _bIsEnable = value;
            }
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        [JsonProperty(PropertyName = "threshold")]
        public int Threshold
        {
            get
            {
                return _nThreshold;
            }

            set
            {
                _nThreshold = value;
            }
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        [JsonProperty(PropertyName = "post_to_url")]
        public string PostToUrl
        {
            get
            {
                return _sPostToUrl;
            }

            set
            {
                _sPostToUrl = value;
            }
        }
        #endregion

        #region "Funciones"
        #endregion

        #region "Metodos"
        #endregion

        #region "Eventos"
        #endregion

        #region "Destructor"
        #endregion

    }
}

