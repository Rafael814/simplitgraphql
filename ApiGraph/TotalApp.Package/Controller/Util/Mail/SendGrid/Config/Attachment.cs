﻿using System;
using Newtonsoft.Json;


namespace TotalApp.Package.UTIL
{

    /// <summary>
    /// *Autor:Larry Ramirez
    /// *Fecha de creacion:09-01-2018
    /// *Version:1.1.20180109
    /// *Sinopsis:
    /// </summary>
    internal class Attachment
    {
        #region "Constantes"
        #endregion

        #region "Variables"
        private String _sContent;
        private string _sType;
        private string _sFilename;
        private string _sDisposition;
        private string _sContentId;
        #endregion

        #region "Propiedades"
        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        [JsonProperty(PropertyName = "content")]
        public String Content
        {
            get
            {
                return _sContent;
            }

            set
            {
                _sContent = value;
            }
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        [JsonProperty(PropertyName = "type")]
        public string Type
        {
            get
            {
                return _sType;
            }

            set
            {
                _sType = value;
            }
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        [JsonProperty(PropertyName = "filename")]
        public string Filename
        {
            get
            {
                return _sFilename;
            }

            set
            {
                _sFilename = value;
            }
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        [JsonProperty(PropertyName = "disposition")]
        public string Disposition
        {
            get
            {
                return _sDisposition;
            }

            set
            {
                _sDisposition = value;
            }
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        [JsonProperty(PropertyName = "content_id")]
        public string ContentId
        {
            get
            {
                return _sContentId;
            }

            set
            {
                _sContentId = value;
            }
        }
        #endregion

        #region "Funciones"

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sRuta"></param>
        /// <returns></returns>
        public string MimeType(string sRuta)
        {
            string sMimeType = "application/unknown";
            try
            {

                string sExt = System.IO.Path.GetExtension(sRuta).ToLower();
                Microsoft.Win32.RegistryKey oRegKey = Microsoft.Win32.Registry.ClassesRoot.OpenSubKey(sExt);

                if (oRegKey != null && oRegKey.GetValue("Content Type") != null)
                {
                    sMimeType = oRegKey.GetValue("Content Type").ToString();
                }

                if (sMimeType == "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
                {
                    sMimeType = "application/unknown";
                }

            }
            catch (Exception ex)
            {
                sMimeType = THelper.Empty;
            }
            finally
            {

            }

            return sMimeType;
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sRuta"></param>
        /// <returns></returns>
        public string Base64String(string sRuta) => THelperBase64.TBase64StringFile(sRuta);

        #endregion

        #region "Metodos"
        #endregion

        #region "Eventos"
        #endregion

        #region "Destructor"
        #endregion

    }
}

