﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;


namespace TotalApp.Package.UTIL
{
    /// <summary>
    /// *Autor:Larry Ramirez
    /// *Fecha de creacion:09-01-2018
    /// *Version:1.1.20180109
    /// *Sinopsis:
    /// </summary>
    internal class Email
    {
        #region "Constantes"
        #endregion

        #region "Variables"
        private string _sName;
        private string _sAddress;
        #endregion

        #region "Propiedades"
        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        [JsonProperty(PropertyName = "name")]
        public string Name
        {
            get
            {
                return _sName;
            }

            set
            {
                _sName = value;
            }
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        [JsonProperty(PropertyName = "email")]
        public string Address
        {
            get
            {
                return _sAddress;
            }

            set
            {
                _sAddress = value;
            }
        }
        #endregion

        #region "Funciones"
        #endregion

        #region "Metodos"
        public Email()
        {
            return;
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sEmail"></param>
        /// <param name="sName"></param>
        public Email(string sEmail, string sName = null)
        {
            this.Address = sEmail;
            this.Name = sName;
        }
        #endregion

        #region "Eventos"
        #endregion

        #region "Destructor"
        #endregion
    }

}

