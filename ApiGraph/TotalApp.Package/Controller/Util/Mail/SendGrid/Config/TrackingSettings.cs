﻿using Newtonsoft.Json;

namespace TotalApp.Package.UTIL
{

    /// <summary>
    /// *Autor:Larry Ramirez
    /// *Fecha de creacion:09-01-2018
    /// *Version:1.1.20180109
    /// *Sinopsis:
    /// </summary>
    internal class TrackingSettings
    {
        #region "Constantes"
        #endregion

        #region "Variables"
        private ClickTracking _oClickTracking;
        private OpenTracking _oOpenTracking;
        private SubscriptionTracking _oSubscriptionTracking;
        private Ganalytics _oGanalytics;
        #endregion

        #region "Propiedades"
        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        [JsonProperty(PropertyName = "click_tracking")]
        public ClickTracking ClickTracking
        {
            get
            {
                return _oClickTracking;
            }

            set
            {
                _oClickTracking = value;
            }
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        [JsonProperty(PropertyName = "open_tracking")]
        public OpenTracking OpenTracking
        {
            get
            {
                return _oOpenTracking;
            }

            set
            {
                _oOpenTracking = value;
            }
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        [JsonProperty(PropertyName = "subscription_tracking")]
        public SubscriptionTracking SubscriptionTracking
        {
            get
            {
                return _oSubscriptionTracking;
            }

            set
            {
                _oSubscriptionTracking = value;
            }
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        [JsonProperty(PropertyName = "ganalytics")]
        public Ganalytics Ganalytics
        {
            get
            {
                return _oGanalytics;
            }

            set
            {
                _oGanalytics = value;
            }
        }
        #endregion

        #region "Funciones"
        #endregion

        #region "Metodos"
        #endregion

        #region "Eventos"
        #endregion

        #region "Destructor"
        #endregion

    }
  
}

