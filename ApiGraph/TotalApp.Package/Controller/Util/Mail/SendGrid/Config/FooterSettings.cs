﻿using System;
using Newtonsoft.Json;


namespace TotalApp.Package.UTIL
{
    /// <summary>
    /// *Autor:Larry Ramirez
    /// *Fecha de creacion:09-01-2018
    /// *Version:1.1.20180109
    /// *Sinopsis:
    /// </summary>
    internal class FooterSettings
    {
        #region "Constantes"
        #endregion

        #region "Variables"
        private bool _bIsEnable;
        private String _sText;
        private String _sHtml;
        #endregion

        #region "Propiedades"
        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        [JsonProperty(PropertyName = "enable")]
        public bool Enable
        {
            get
            {
                return _bIsEnable;
            }

            set
            {
                _bIsEnable = value;
            }
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        [JsonProperty(PropertyName = "text")]
        public string Text
        {
            get
            {
                return _sText;
            }

            set
            {
                _sText = value;
            }
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        [JsonProperty(PropertyName = "html")]
        public string Html
        {
            get
            {
                return _sHtml;
            }

            set
            {
                _sHtml = value;
            }
        }
        #endregion

        #region "Funciones"
        #endregion

        #region "Metodos"
        #endregion

        #region "Eventos"
        #endregion

        #region "Destructor"
        #endregion
    }
}

