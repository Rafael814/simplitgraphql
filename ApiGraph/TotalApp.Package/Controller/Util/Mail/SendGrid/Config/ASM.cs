﻿using System.Collections.Generic;
using Newtonsoft.Json;


namespace TotalApp.Package.UTIL
{
    /// <summary>
    /// *Autor:Larry Ramirez
    /// *Fecha de creacion:09-01-2018
    /// *Version:1.1.20180109
    /// *Sinopsis:
    /// </summary>
    internal class ASM
    {
        #region "Constantes"
        #endregion

        #region "Variables"
        private int _nGroupId;
        private List<int> _oGroupsToDisplay;
        #endregion

        #region "Propiedades"
        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        [JsonProperty(PropertyName = "group_id")]
        public int GroupId
        {
            get
            {
                return _nGroupId;
            }

            set
            {
                _nGroupId = value;
            }
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        [JsonProperty(PropertyName = "groups_to_display")]
        public List<int> GroupsToDisplay
        {
            get
            {
                return _oGroupsToDisplay;
            }

            set
            {
                _oGroupsToDisplay = value;
            }
        }
        #endregion

        #region "Funciones"
        #endregion

        #region "Metodos"
        #endregion

        #region "Eventos"
        #endregion

        #region "Destructor"
        #endregion

    }
}

