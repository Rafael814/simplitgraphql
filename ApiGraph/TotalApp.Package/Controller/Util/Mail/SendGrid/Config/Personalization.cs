﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;


namespace TotalApp.Package.UTIL
{

    /// <summary>
    /// *Autor:Larry Ramirez
    /// *Fecha de creacion:09-01-2018
    /// *Version:1.1.20180109
    /// *Sinopsis:
    /// </summary>
    internal class Personalization
    {
        #region "Constantes"
        #endregion

        #region "Variables"
        private List<Email> _oTos;
        private List<Email> _oCcs;
        private List<Email> _oBccs;
        private string _sSubject;
        private Dictionary<String, String> _oHeaders;
        private Dictionary<String, String> _oSubstitutions;
        private Dictionary<String, String> _oCustomArgs;
        private long _nSendAt;
        #endregion

        #region "Propiedades"
        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        [JsonProperty(PropertyName = "to")]
        public List<Email> Tos
        {
            get
            {
                return _oTos;
            }

            set
            {
                _oTos = value;
            }
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        [JsonProperty(PropertyName = "cc")]
        public List<Email> Ccs
        {
            get
            {
                return _oCcs;
            }

            set
            {
                _oCcs = value;
            }
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        [JsonProperty(PropertyName = "bcc")]
        public List<Email> Bccs
        {
            get
            {
                return _oBccs;
            }

            set
            {
                _oBccs = value;
            }
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        [JsonProperty(PropertyName = "subject")]
        public string Subject
        {
            get
            {
                return _sSubject;
            }

            set
            {
                _sSubject = value;
            }
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        [JsonProperty(PropertyName = "headers")]
        public Dictionary<string, string> Headers
        {
            get
            {
                return _oHeaders;
            }

            set
            {
                _oHeaders = value;
            }
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        [JsonProperty(PropertyName = "substitutions")]
        public Dictionary<string, string> Substitutions
        {
            get
            {
                return _oSubstitutions;
            }

            set
            {
                _oSubstitutions = value;
            }
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        [JsonProperty(PropertyName = "custom_args")]
        public Dictionary<string, string> CustomArgs
        {
            get
            {
                return _oCustomArgs;
            }

            set
            {
                _oCustomArgs = value;
            }
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        [JsonProperty(PropertyName = "send_at")]
        public long SendAt
        {
            get
            {
                return _nSendAt;
            }

            set
            {
                _nSendAt = value;
            }
        }
        #endregion

        #region "Funciones"

        #endregion

        #region "Metodos"
        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="oEmail"></param>
        public void AddTo(Email oEmail)
        {
            if (this._oTos == null)
            {
                _oTos = new List<Email>();

            }
            _oTos.Add(oEmail);
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="oEmail"></param>
        public void AddCc(Email oEmail)
        {
            if (this._oCcs == null)
            {
                _oCcs = new List<Email>();
            }
            _oCcs.Add(oEmail);
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="oEmail"></param>
        public void AddBcc(Email oEmail)
        {
            if (this._oBccs == null)
            {
                _oBccs = new List<Email>();
            }
            _oBccs.Add(oEmail);
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sKey"></param>
        /// <param name="sValue"></param>
        public void AddHeader(string sKey, string sValue)
        {
            if (this._oHeaders == null)
            {
                _oHeaders = new Dictionary<String, String>();
            }
            _oHeaders.Add(sKey, sValue);
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sKey"></param>
        /// <param name="sValue"></param>
        public void AddSubstitution(string sKey, string sValue)
        {
            if (this._oSubstitutions == null)
            {
                _oSubstitutions = new Dictionary<String, String>();
            }
            _oSubstitutions.Add(sKey, sValue);
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sKey"></param>
        /// <param name="sValue"></param>
        public void AddCustomArgs(string sKey, string sValue)
        {
            if (this._oCustomArgs == null)
            {
                _oCustomArgs = new Dictionary<String, String>();
            }
            _oCustomArgs.Add(sKey, sValue);
        }
        #endregion

        #region "Eventos"
        #endregion

        #region "Destructor"
        #endregion
    }
}

