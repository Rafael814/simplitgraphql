﻿using System;

namespace TotalApp.Package.UTIL
{
    /// <summary>
    /// *Autor:Larry Ramirez
    /// *Fecha de creacion:09-01-2018
    /// *Version:1.1.20180109
    /// *Sinopsis:
    /// </summary>
    public static class THelperSendMail
    {
        #region "Constantes"
        #endregion

        #region "Variables"

        private static bool _bIsExportar = false;


        #region "Variables Lock"
        private static readonly object _oObjectLock = new object();
        #endregion

        #endregion

        #region "Propiedades"

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        public static bool Error
        {
            get; set;
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        public static string Mensaje
        {
            get; set;
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        public static bool ExportarError
        {
            get
            {
                return _bIsExportar;
            }
            set
            {
                _bIsExportar = value;
            }
        }
        #endregion

        #region "Funciones"
        #endregion

        #region "Metodos & constructores"
        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sFrom"></param>
        /// <param name="sTo"></param>
        /// <param name="sSubject"></param>
        /// <param name="sBody"></param>
        /// <param name="sTitulo"></param>
        /// <param name="bIsHtml"></param>
        /// <param name="bIsAddCurrentDate"></param>
        public static void SendGrid(string sFrom
                                  , string sTo
                                  , string sSubject
                                  , String sBody
                                  , string sTitulo
                                  , bool bIsHtml = true
                                  , bool bIsAddCurrentDate = false)
        {

            lock (_oObjectLock)
            {
                if (sFrom.TIsMail() && sTo.TIsMail())
                {
                    using (var oSendGrid = new TInvokeSendGrid(sFrom
                                                             , sTo
                                                             , sSubject
                                                             , sBody
                                                             , sTitulo
                                                             , bIsHtml
                                                             , bIsAddCurrentDate))
                    {
                      
                        Error = oSendGrid.Error;
                        Mensaje = oSendGrid.Mensaje;

                        oSendGrid.Dispose();
                    }
                }
                else
                {
                    Error = true;
                    Mensaje = "Tanto el From como el To, deben tener un formato valido";
                }

                try
                {
                    if (Error && ExportarError)
                    {
                        string sMensaje = "sFrom:" + sFrom
                                        + "\r\n"
                                        + "sTo:" + sTo
                                        + "\r\n"
                                        + "sSubject:" + sSubject
                                        + "\r\n"
                                        + "sTitulo:" + sTitulo
                                        + "\r\n"
                                        + "Mensaje de error:" + Mensaje
                                        + "\r\n"
                                        + "===============================================================";

                        using (ITLog oLog = new TLog())
                        {
                            oLog.LogWrite("SendGrid"
                                        , sMensaje
                                        , true
                                        , true
                                        , ""
                                        , "");

                            oLog.Dispose();
                        }
                        ExportarError = false;
                    }
                }
                catch
                {

                }


            }
        }
        #endregion

        #region "Eventos"
        #endregion

        #region "Destructor"
        #endregion
    }
}
