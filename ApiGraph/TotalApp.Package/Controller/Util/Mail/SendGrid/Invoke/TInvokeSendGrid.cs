﻿using System;
using System.Text;
using System.IO;
using System.Net;
using System.Collections.Generic;

namespace TotalApp.Package.UTIL
{
    /// <summary>
    /// *Autor:Larry Ramirez
    /// *Fecha de creacion:09-01-2018
    /// *Version:1.1.20180109
    /// *Sinopsis:
    /// </summary>
    public class TInvokeSendGrid : IDisposable
    {
        #region "Constantes"
        #endregion

        #region "Variables"
        private string _sUrl = THelper.Empty;
        private string _sMetodo = THelper.Empty;
        private string _sContentType = THelper.Empty;
        private int _nTimeOut = 0;
        private String _sJson = THelper.Empty;
        private bool _bIsComprimirJson = false;
        private bool _bIsError = false;
        private string _sMensaje = THelper.Empty;

        #region "Variables de configuracion"
        private short _nLimiteDeCorreosAEnviar = 1000;
        #endregion

        #region "Variables de correo"
        private Dictionary<string, String> _oDicCorreo = new Dictionary<string, String>();
        private string _sFrom = THelper.Empty;
        private string _sNameForm = THelper.Empty;
        private List<string> _sTo = new List<string>();
        private string _sAsuntoInterno = THelper.Empty;
        private string _sSubject = THelper.Empty;
        private String _sBody = THelper.Empty;
        private string _sTitulo = THelper.Empty;
        private bool _bIsHtml = false;
        private long _nTotalDeMegas = 0;

        private static bool _bIsAddCurrentDate = false;
        private Dictionary<string,string> _oAttachment = new Dictionary<string, string>();
        #endregion
        #endregion

        #region "Propiedades"

        #region "Control  de errores"
        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        public bool Error
        {
            get
            {
                return _bIsError;
            }
            set
            {
                _bIsError = value;
            }
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        public string Mensaje
        {
            get
            {
                return _sMensaje;
            }
            set
            {
                _sMensaje = value;
            }
        }
        #endregion

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        public string Url
        {
            get
            {
                return _sUrl;
            }
            set
            {
                _sUrl = value;
            }
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        public string Metodo
        {
            get
            {
                return _sMetodo;
            }
            set
            {
                _sMetodo = value;
            }
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        public string ContentType
        {
            get
            {
                return _sContentType;
            }
            set
            {
                _sContentType = value;
            }
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        public int TimeOut
        {
            get
            {
                return _nTimeOut;
            }
            set
            {
                _nTimeOut = value;
            }
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        public String Json
        {
            get
            {
                return _sJson;
            }
            set
            {
                _sJson = value;
            }
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        public bool ComprimirJson
        {
            get
            {
                return _bIsComprimirJson;
            }
            set
            {
                _bIsComprimirJson = value;
            }
        }



        #region "Propiedades de correo"
        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        public string From
        {
            get
            {
                return _sFrom;
            }
            set
            {
                _sFrom = value;
            }
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        public string NameForm
        {
            get
            {
                return _sNameForm;
            }
            set
            {
                _sNameForm = value;
            }
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        public List<string> To
        {
            get
            {
                return _sTo;
            }
            set
            {
                _sTo = value;
            }
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        public string AsuntoInterno
        {
            get
            {
                return _sAsuntoInterno;
            }
            set
            {
                _sAsuntoInterno = value;
            }
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        public string Subject
        {
            get
            {
                return _sSubject;
            }
            set
            {
                _sSubject = value;
            }
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        public String Body
        {
            get
            {
                return _sBody;
            }
            set
            {
                _sBody = value;
            }
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        public string Titulo
        {
            get
            {
                return _sTitulo;
            }
            set
            {
                _sTitulo = value;
            }
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        public bool IsHtml
        {
            get
            {
                return _bIsHtml;
            }
            set
            {
                _bIsHtml = value;
            }
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        public bool AddCurrentDate
        {
            get
            {
                return _bIsAddCurrentDate;
            }
            set
            {
                _bIsAddCurrentDate = value;
            }
        }
        #endregion

        #endregion

        #region "Funciones"

        #region "Funciones generales"

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <returns></returns>
        private bool ValidateVDos()
        {
            Error = false;
            Mensaje = THelper.Empty;

            try
            {
                //Valores por defecto
                ContentType = string.IsNullOrEmpty(ContentType) ? "application/x-www-form-urlencoded" : ContentType.Trim().Length > 0 ? ContentType : "application/x-www-form-urlencoded";

                if (TimeOut == 0)
                {

                    TimeOut = 600000;
                }

                //Validaciones
                if (string.IsNullOrEmpty(Url) || Url.Length == 0)
                {
                    Error = true;
                    Mensaje = "#1 - Error: Debe especificar la URL.";
                }
                else if (string.IsNullOrEmpty(From) || From.Length == 0)
                {
                    Error = true;
                    Mensaje = "#2 - Error: Debe especificar el From.";
                }
                else if (To.Count <= 0)
                {
                    Error = true;
                    Mensaje = "#3 - Error: Debe especificar el To.";
                }
                else if (string.IsNullOrEmpty(Subject) || Subject.Length == 0)
                {
                    Error = true;
                    Mensaje = "#4 - Error: Debe especificar el Subject.";
                }
                else if (string.IsNullOrEmpty(Body) || Body.Length == 0)
                {
                    Error = true;
                    Mensaje = "#5 - Error: Debe especificar el Body.";
                }
                else if (!From.TIsMail())
                {
                    Error = true;
                    Mensaje = "#6 - Error: El From contiene un formato de correo invalido.";
                }
                else
                {


                    string sTo = THelper.Empty;
                    short nCount = 1;

                    if (To.Count > 0 || To != null)
                    {
                        foreach (string sT in To)
                        {
                            if (sT.TIsMail())
                            {
                                if (nCount == 1)
                                {
                                    sTo = sT;
                                }
                                else
                                {
                                    sTo = sTo + ";" + sT;
                                }
                            }
                            else
                            {
                                if (nCount == 1)
                                {
                                    Error = true;
                                    Mensaje = "#7 - Error: El formato del primer correo es invalido.";
                                    break;
                                }

                            }


                            nCount++;
                        }
                    }


                    if (Error == false)
                    {
                        //Configuracion y parametros de correo.
                        _oDicCorreo.Add("api_user", "totalaplicaciones");
                        _oDicCorreo.Add("api_key", "$ta365284$");

                        Subject = Subject + " - " + THelper.FechaHora();
                        _oDicCorreo.Add("to", sTo);

                        _oDicCorreo.Add("subject", Subject);

                        if (IsHtml)
                        {

                            _oDicCorreo.Add("html", Body);
                        }
                        else
                        {
                            _oDicCorreo.Add("text", Body);
                        }

                        _oDicCorreo.Add("from", From);

                        if (!string.IsNullOrEmpty(Titulo) && Titulo.Length > 0)
                        {
                            _oDicCorreo.Add("fromname", Titulo);
                        }


                    }


                }

            }
            catch (Exception ex)
            {
                Error = true;
                Mensaje = ex.Message.ToString();
            }

            return Error;
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <returns></returns>
        private bool ValidateVTres()
        {
            Error = false;
            Mensaje = THelper.Empty;

            try
            {
                //Valores por defecto
                ContentType = string.IsNullOrEmpty(ContentType) ? "application/json" : ContentType.Trim().Length > 0 ? ContentType : "application/json";

                if (TimeOut == 0)
                {

                    TimeOut = 600000;
                }

                //Validaciones
                if (string.IsNullOrEmpty(Url) || Url.Trim().Length == 0)
                {
                    Error = true;
                    Mensaje = "#1 - Error: Debe especificar la URL.";
                }
                else if (string.IsNullOrEmpty(From) || From.Trim().Length == 0)
                {
                    Error = true;
                    Mensaje = "#2 - Error: Debe especificar el From.";
                }
                else if (To.Count <= 0)
                {
                    Error = true;
                    Mensaje = "#3 - Error: Debe especificar el To.";
                }
                else if (To.Count > _nLimiteDeCorreosAEnviar)
                {
                    Error = true;
                    Mensaje = "#102 - Error: El numero maximo de correos permitidos es de:" + _nLimiteDeCorreosAEnviar.ToString();
                }
                //else if (_oAttachment.Count > 0 && _nTotalDeMegas > _nTotalDeMegasEnAdjuntos)
                //{
                //    Error = true;
                //    Mensaje = "#103 - Error: El tamaño maximo de archivos adjuntos es de:" + _nTotalDeMegasEnAdjuntos.ToString() + " MG";
                //}
                else if (string.IsNullOrEmpty(Subject) || Subject.Trim().Length == 0)
                {
                    Error = true;
                    Mensaje = "#4 - Error: Debe especificar el Subject.";
                }
                else if (string.IsNullOrEmpty(Body) || Body.Trim().Length == 0)
                {
                    Error = true;
                    Mensaje = "#5 - Error: Debe especificar el Body.";
                }
                else if (!From.TIsMail())
                {
                    Error = true;
                    Mensaje = "#6 - Error: El From contiene un formato de correo invalido.";
                }
                else
                {

                    string sTo = THelper.Empty;
                    short nCount = 1;

                    if (To.Count > 0 || To != null)
                    {
                        foreach (string sT in To)
                        {
                            if (sT.TIsMail())
                            {
                                if (nCount == 1)
                                {
                                    sTo = sT;
                                }
                                else
                                {
                                    sTo = sTo + ";" + sT;
                                }
                            }
                            else
                            {
                                if (nCount == 1)
                                {
                                    Error = true;
                                    Mensaje = "#7 - Error: El formato del primer correo es invalido.";
                                    break;
                                }

                            }


                            nCount++;
                        }
                    }


                    if (Error == false)
                    {
                        if (AddCurrentDate)
                        {
                            Subject += " - " + THelper.FechaHora();
                        }

                        Mail oMailSG = new Mail();
                        Personalization oPersonalization = new Personalization();

                        try
                        {
                            Email oEmailFrom = new Email();
                            oEmailFrom.Address = From;
                            oEmailFrom.Name = Titulo;

                            oMailSG.From = oEmailFrom;
                            oMailSG.Subject = Subject;
                            //oMailSG.BatchId = "001";
                            //Para pruebas
                            char[] DelimiterChars = { ';' };
                            string[] sT = sTo.ToString().Split(DelimiterChars);

                            if (sTo.Length > 1)
                            {
                                nCount = 1;
                                foreach (string sValue in sT)
                                {

                                    if (nCount == 1)
                                    {
                                        Email oEmailTo = new Email();
                                        oEmailTo.Address = sValue;
                                        oEmailTo.Name = "";
                                        oPersonalization.AddTo(oEmailTo);
                                        oEmailTo = null;

                                    }
                                    else
                                    {
                                        Email oEmailToBcc = new Email();
                                        oEmailToBcc.Address = sValue;
                                        oEmailToBcc.Name = "";
                                        oPersonalization.AddBcc(oEmailToBcc);
                                        oEmailToBcc = null;
                                    }

                                    nCount++;

                                }
                            }
                            oMailSG.AddPersonalization(oPersonalization);

                            Content oContent = new Content();
                            if (IsHtml)
                            {

                                oContent.Type = "text/html";
                            }
                            else
                            {
                                oContent.Type = "text/plain";
                            }
                            oContent.Value = Body;
                            oMailSG.AddContent(oContent);
                            oContent = null;



                            foreach (KeyValuePair<string, string> _oAttachment in _oAttachment)
                            {
                                Attachment oAttc = new Attachment();
                                try
                                {

                                    oAttc.Content = oAttc.Base64String(_oAttachment.Value);
                                    oAttc.Type = oAttc.MimeType(_oAttachment.Value);
                                    oAttc.Filename = _oAttachment.Key;
                                    oAttc.Disposition = "attachment";
                                    oAttc.ContentId = _oAttachment.Key;
                                    oMailSG.AddAttachment(oAttc);
                                }
                                catch (Exception ex)
                                {

                                }
                                finally
                                {
                                    oAttc = null;
                                }
                            }

                            Json = oMailSG.GetJson();
                        }
                        catch (Exception ex)
                        {
                            Error = true;
                            Mensaje = ex.Message.ToString();
                        }
                        finally
                        {
                            oPersonalization = null;
                            oMailSG = null;
                        }

                    }


                }

            }
            catch (Exception ex)
            {
                Error = true;
                Mensaje = ex.Message.ToString();
            }

            return Error;
        }
        #endregion

        #endregion

        #region "Metodos"
        /// <summary>
        /// 
        /// </summary>
        public TInvokeSendGrid()
        {

            
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sFrom"></param>
        /// <param name="sTo"></param>
        /// <param name="sSubject"></param>
        /// <param name="sBody"></param>
        /// <param name="sTitulo"></param>
        /// <param name="bIsHtml"></param>
        /// <param name="bIsAddCurrentDate"></param>
        public TInvokeSendGrid(string sFrom
                             , string sTo
                             , string sSubject
                             , String sBody
                             , string sTitulo
                             , bool bIsHtml = true
                             , bool bIsAddCurrentDate = false)
        {

            SendVTres(sFrom
                   ,  sTo
                   ,  sSubject
                   ,  sBody
                   ,  sTitulo
                   ,  bIsHtml 
                   ,  bIsAddCurrentDate);
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="sFrom"></param>
        /// <param name="sTo"></param>
        /// <param name="sSubject"></param>
        /// <param name="sBody"></param>
        /// <param name="sTitulo"></param>
        /// <param name="bIsHtml"></param>
        /// <param name="bIsAddCurrentDate"></param>
        public void SendVTres(string sFrom
                            , string sTo
                            , string sSubject
                            , String sBody
                            , string sTitulo
                            , bool bIsHtml = true
                            , bool bIsAddCurrentDate = false)
        {

            //Url, Link de conexion al Web Service.
            Url = @"https://api.sendgrid.com/v3/mail/send";

            From = sFrom;
            To.Add(sTo);
            Subject = sSubject;
            IsHtml = bIsHtml;
            Body = sBody;
            Titulo = sTitulo;
            AddCurrentDate = bIsAddCurrentDate;
            //Enviar y retonar valores.
            SendVTres();
        }


        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sName"></param>
        /// <param name="sFileName"></param>
        public void Attachment(string sName
                              ,string sFileName)
        {

            string sNameReal = THelper.Empty;
            long nTamanoReal = 0;
            FileInfo oFileInfo = null;
            try
            {
                oFileInfo = new FileInfo(sFileName);

                sNameReal = oFileInfo.Name;
                nTamanoReal = oFileInfo.Length;

                //Verifica si el parametro viene con valor, sino establece el nombre del archivo.
                if (string.IsNullOrEmpty(sName) && sName.Trim().Length == 0)
                {
                    sName = sNameReal;
                }

                if (!_oAttachment.ContainsKey(sName + sFileName))
                {

                    _oAttachment.Add(sName, sFileName);

                }
            }
            catch (Exception ex)
            {
                Error = true;
                Mensaje = "Ex - #1/Error: adjuntando archivo:" + ex.Message.ToString();
               
            }
            finally
            {
                _nTotalDeMegas += nTamanoReal;
                oFileInfo = null;
            }

        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        public void SendVTres()
        {
            HttpWebRequest oWsRequest = null;
            HttpWebResponse oWsResponse = null;
            Stream oRequestStream = null;
            string sCode = THelper.Empty;
            string sStatus = THelper.Empty;

            try
            {

                Error = ValidateVTres();

                if (!Error)
                {


                    try
                    {

                        Byte[] byBytes = null;

                        byBytes = Encoding.UTF8.GetBytes(Json);
                        //String sUrlCompleta = Url + "?" + sPostData;


                        oWsRequest = (HttpWebRequest)WebRequest.Create(Url);
                        oWsRequest.ContentType = ContentType + ";charset=UTF-8";
                        oWsRequest.Accept = ContentType;
                        oWsRequest.Timeout = TimeOut;
                        oWsRequest.KeepAlive = false;
                        oWsRequest.UserAgent = ".NET Framework";
                        oWsRequest.Headers.Add("Authorization: Bearer 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");

                        oWsRequest.ContentLength = byBytes.Length;
                        oWsRequest.Method = "POST";
                        oRequestStream = oWsRequest.GetRequestStream();
                        oRequestStream.Write(byBytes, 0, byBytes.Length);
                        oRequestStream.Flush();
                        oRequestStream.Close();

                        oWsResponse = (HttpWebResponse)oWsRequest.GetResponse();
                        sCode = oWsResponse.StatusCode.ToString();
                        sStatus = oWsResponse.StatusDescription.ToString().ToUpper();

                        //Verifico si la conexion y retorno esta todo OK
                        switch (oWsResponse.StatusCode)
                        {
                            case HttpStatusCode.OK:
                            case HttpStatusCode.Accepted:
                                Error = false;
                                Mensaje = "Envio realizado satisfactoriamente.";
                                break;
                            default:
                                Error = true;
                                Mensaje = "#3 - Error de envio - Estatus:" + sStatus;
                                break;
                        }

                    }
                    catch (WebException ex)
                    {

                        Error = true;

                        Mensaje = "Ex - #6/Error: Conexion fallida - Descrip:" + ex.Message.ToString() + "\r\n";
                        Mensaje += "Estatus:" + ex.Status.ToString();

                    }



                }

            }
            catch (Exception ex)
            {
                Error = true;
                Mensaje = "Ex - #5/Error: Conexion fallida - Estatus descrip:" + ex.Message.ToString();
            }
            finally
            {
                oWsResponse = null;
                oRequestStream = null;
                oWsRequest = null;
            }

        }
        #endregion

        #region "Eventos"
        #endregion

        #region "Destructor"
        /// <summary>
        /// Autor   : Larry Ramirez
        /// Fecha   : 21-05-2016
        /// Sinopsis: Variable Disposed, permite saber de manera general si ya fueron liberados los recursos no administrados a la clase (Objeto).
        /// </summary>
        private bool _Disposed = false;

        // <summary>
        /// Autor   : Larry Ramirez
        /// Fecha   : 25-05-2016
        /// Sinopsis: Metodo que permite Desechar y limpiar objetos.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        // <summary>
        /// Autor   : Larry Ramirez
        /// Fecha   : 25-05-2016
        /// Sinopsis: Logica para limpiar objetos de la memoria.
        /// </summary>
        protected virtual void Dispose(bool bIsDisposing)
        {
            //Si esta en false, significa que se tiene la oportunidad de limpiar los objetos
            if (!this._Disposed)
            {
                //Aqui se definen los objetos que se liberan
                if (bIsDisposing)
                {

                }
            }
            _Disposed = true;
        }

        // <summary>
        /// Autor   : Larry Ramirez
        /// Fecha   : 25-05-2016
        /// Sinopsis: Destructor por defecto.
        /// </summary>
        ~TInvokeSendGrid()
        {
            Dispose(false);
            GC.SuppressFinalize(this);
        }
        #endregion
    }

}
