﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace TotalApp.Package.Collections
{
    /// <summary>
    /// *Autor:Larry Ramirez
    /// *Fecha de creacion:09-01-2018
    /// *Version:1.1.20180109
    /// *Sinopsis:Clase creada de tipo arreglo
    /// </summary>
    public class TotalAppList<T> : IEnumerable<T>, IList<T>, ICollection<T>
    {
        #region "Constantes"
        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:Indica el tamaño maximo del arreglo
        /// </summary>
        public const int MAX_COUNT = 0;
        #endregion

        #region "Variables"
        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        private T[] _oArray = new T[0];
        #endregion

        #region "Propiedades"
        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="nIndex"></param>
        /// <returns></returns>
        public T this[int nIndex]
        {
            get
            {
                if (_oArray == null)
                {
                    throw new InvalidOperationException(nameof(_oArray) + " == null");
                }
                if (nIndex < 0 || _oArray.Length <= nIndex)
                {
                    throw new ArgumentException(nameof(nIndex) + " < 0 || " + nameof(_oArray) + ".Length <= " + nameof(nIndex));
                }

                return _oArray[nIndex];
            }

            set
            {
                if (_oArray == null)
                {
                    throw new InvalidOperationException(nameof(_oArray) + " == null");
                }
                if (nIndex < 0 || _oArray.Length <= nIndex)
                {
                    throw new ArgumentNullException(nameof(nIndex) + " < 0 || " + nameof(_oArray) + ".Length <= " + nameof(nIndex));
                }

                _oArray[nIndex] = value;
            }
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        public int Count
        {
            get
            {
                if (_oArray == null)
                {
                    throw new InvalidOperationException(nameof(_oArray) + " == null");
                }

                return _oArray.Length;
            }
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        public bool IsReadOnly
        {
            get
            {
                return false;
            }
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <returns></returns>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
        #endregion

        #region "Funciones"
        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="oItem"></param>
        /// <returns></returns>
        public bool Contains(T oItem)
        {
            if (_oArray == null)
            {
                throw new InvalidOperationException(nameof(_oArray) + " == null");
            }
            if (oItem == null)
            {
                throw new ArgumentNullException(nameof(oItem) + " == null");
            }

            return Array.IndexOf(_oArray, oItem) != -1;
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <returns></returns>
        public IEnumerator<T> GetEnumerator()
        {
            foreach (var oItem in _oArray)
            {
                yield return oItem;
            }
        }

        public int IndexOf(T oItem)
        {
            if (_oArray == null)
            {
                throw new ArgumentNullException(nameof(_oArray) + " == null");
            }
            return Array.IndexOf(_oArray, oItem);
        }

        
        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="oItem"></param>
        /// <returns></returns>
        public bool Remove(T oItem)
        {
            if (_oArray == null)
            {
                throw new InvalidOperationException(nameof(_oArray) + " == null");
            }
            if (oItem == null)
            {
                throw new ArgumentNullException(nameof(oItem) + " == null");
            }
            if (_oArray.Length == 0)
            {
                throw new ArgumentNullException(nameof(_oArray) + ".Length == 0");
            }
            int nIndex = Array.BinarySearch(_oArray, oItem);

            if (nIndex < 0)
            {
                return false;
            }

            RemoveAt(nIndex);

            return true;
        }
        #endregion

        #region "Metodos & constructores"
        public TotalAppList():base()
        {

        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="nCount"></param>
        public TotalAppList(int nCount)
        {
            if (MAX_COUNT > 0 && (MAX_COUNT < nCount))
            {
                throw new ArgumentException(nameof(MAX_COUNT) + " < " + nameof(nCount));
            }
            
            if (nCount <= 0)
            {
                throw new ArgumentNullException(nameof(nCount) + " < 0");
            }

            _oArray = new T[nCount];
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="oItem"></param>
        public void Add(T oItem)
        {
            if (_oArray == null)
            {
                throw new InvalidOperationException(nameof(_oArray) + " == null");
            }

            if (MAX_COUNT > 0 && (MAX_COUNT <= _oArray.Length))
            {
                throw new ArgumentException(nameof(MAX_COUNT) + " <= " + nameof(_oArray) + ".Length");
            }

            T[] _oArrayTemp = new T[_oArray.Length + 1];
            _oArray.CopyTo(_oArrayTemp, 0);
            _oArrayTemp[_oArrayTemp.Length - 1] = oItem;
            _oArray = _oArrayTemp;
        }

        /// <summary>
        /// 
        /// </summary>
        public void Clear()
        {
            _oArray = new T[0];
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="oArray"></param>
        /// <param name="_oArraynIndex"></param>
        public void CopyTo(T[] oArray, int _oArraynIndex)
        {
            if (this._oArray == null)
            {
                throw new InvalidOperationException("this." + nameof(_oArray) + " == null");
            }
            if (oArray == null)
            {
                throw new ArgumentNullException(nameof(oArray) + " == null");
            }
            if (_oArraynIndex < 0 || oArray.Length <= _oArraynIndex)
            {
                throw new ArgumentException(nameof(_oArraynIndex) + " < 0 || " + nameof(_oArray) + ".Length <= " + nameof(_oArraynIndex));
            }
            this._oArray.CopyTo(oArray, _oArraynIndex);
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="nIndex"></param>
        /// <param name="oItem"></param>
        public void Insert(int nIndex, T oItem)
        {
            if (_oArray == null)
            {
                throw new InvalidOperationException(nameof(_oArray) + " == null");
            }
            if (nIndex < 0 || _oArray.Length <= nIndex)
            {
                throw new ArgumentException(nameof(nIndex) + " < 0 || " + nameof(_oArray) + ".Length <= " + nameof(nIndex));
            }

            if (oItem == null)
            {
                throw new ArgumentNullException(nameof(oItem) + " == null");
            }

            if (MAX_COUNT > 0 && (MAX_COUNT <= _oArray.Length + 1))
            {
                throw new ArgumentException(nameof(MAX_COUNT) + " <= " + nameof(_oArray) + ".Length");
            }


            T[] _oArrayTemp = new T[_oArray.Length + 1];
            Array.Copy(_oArray, 0, _oArrayTemp, 0, nIndex);
            Array.Copy(_oArray, nIndex, _oArrayTemp, nIndex + 1, _oArray.Length - nIndex);
            _oArrayTemp[nIndex] = oItem;
            _oArray = _oArrayTemp;
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="nIndex"></param>
        public void RemoveAt(int nIndex)
        {
            if (_oArray == null)
            {
                throw new InvalidOperationException(nameof(_oArray) + " == null");
            }
            if (nIndex < 0 || _oArray.Length <= nIndex)
            {
                throw new ArgumentException(nameof(nIndex) + " < 0 || " + nameof(_oArray) + ".Length <= " + nameof(nIndex));
            }

            T[] _oArrayTemp = new T[_oArray.Length - 1];
            Array.Copy(_oArray, 0, _oArrayTemp, 0, nIndex);
            Array.Copy(_oArray, nIndex + 1, _oArrayTemp, nIndex, _oArray.Length - nIndex - 1);
            _oArray = _oArrayTemp;
        }
        #endregion

        #region "Eventos"
        #endregion

        #region "Destructor"
        ~TotalAppList()
        {

        }
        #endregion

    }
}
