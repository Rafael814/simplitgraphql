using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using TotalApp.Package.DTO;

namespace TotalApp.Package
{
    /// <summary>
    /// *Autor:Larry Ramirez
    /// *Fecha de creacion:09-01-2018
    /// *Version:1.1.20180109
    /// *Sinopsis:
    /// </summary>
    public class TEntityValidationResult
    {
        #region "Constantes"
        #endregion

        #region "Variables"
        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        public IList<ValidationResult> Errors { get; private set; }
        #endregion

        #region "Propiedades"
        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        public bool HasError
        {
            get { return Errors.Count > 0; }
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        public Int32 Count
        {
            get { return Errors.Count; }
        }
        #endregion

        #region "Funciones"
        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="oErrors"></param>
        /// <returns></returns>
        public TInfoEstatus_DTO ResultError(IList<ValidationResult> oErrors = null)
        {
            TInfoEstatus_DTO oError = new TInfoEstatus_DTO();

            Errors = oErrors ?? new List<ValidationResult>();

            if (HasError)
            {
                oError.Error = Errors.Count;
                foreach (var oResult in Errors)
                {
                    oError.Mensaje += oResult.ErrorMessage + Environment.NewLine;
                    break;
                }

                oError.StatusCode = System.Net.HttpStatusCode.InternalServerError;
            }

            return oError;

        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="oErrors"></param>
        /// <returns></returns>
        public TInfoEstatus_DTO ResultErrorAll(IList<ValidationResult> oErrors = null)
        {
            TInfoEstatus_DTO oError = new TInfoEstatus_DTO();

            Errors = oErrors ?? new List<ValidationResult>();

            if (HasError)
            {
                oError.Error = Errors.Count;
                foreach (var oResult in Errors)
                {
                    oError.Mensaje += oResult.ErrorMessage + Environment.NewLine;
                }

                oError.StatusCode = System.Net.HttpStatusCode.InternalServerError;
            }

            return oError;

        }

        #endregion

        #region "Metodos & constructores"
        public TEntityValidationResult()
        {

        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="oErrors"></param>
        public TEntityValidationResult(IList<ValidationResult> oErrors = null)
        {
            Errors = oErrors ?? new List<ValidationResult>();
        }
        #endregion

        #region "Eventos"
        #endregion

        #region "Destructor"
        ~TEntityValidationResult()
        {

        }

        
        #endregion

    }

}