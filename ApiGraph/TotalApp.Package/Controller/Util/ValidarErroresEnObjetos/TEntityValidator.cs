using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using TotalApp.Package.DTO;

namespace TotalApp.Package
{
    /// <summary>
    /// *Autor:Larry Ramirez
    /// *Fecha de creacion:09-01-2018
    /// *Version:1.1.20180109
    /// *Sinopsis:
    /// </summary>
    public class TEntityValidator<T> where T : class
    {
        #region "Constantes"
        #endregion

        #region "Variables"
        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        public List<ValidationResult> Results = new List<ValidationResult>();
        #endregion

        #region "Propiedades"
        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        public bool IsValid { get; private set; }
        #endregion

        #region "Funciones"
        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public TEntityValidationResult Validar(T obj)
        {
            Results = new List<ValidationResult>();

            var oResultContext = new ValidationContext(obj, null, null);

            IsValid = Validator.TryValidateObject(obj, oResultContext, Results, true);

            return new TEntityValidationResult(Results);
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public TInfoEstatus_DTO ValidarError(T obj)
        {
            TEntityValidationResult oEntityValidationResult = new TEntityValidationResult();
            Results = new List<ValidationResult>();

            var oResultContext = new ValidationContext(obj, null, null);

            IsValid = Validator.TryValidateObject(obj, oResultContext, Results, true);

            return oEntityValidationResult.ResultError(Results);
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public TInfoEstatus_DTO ValidarErrorAll(T obj)
        {
            TEntityValidationResult oEntityValidationResult = new TEntityValidationResult();
            Results = new List<ValidationResult>();

            var oResultContext = new ValidationContext(obj, null, null);

            IsValid = Validator.TryValidateObject(obj, oResultContext, Results, true);

            return oEntityValidationResult.ResultErrorAll(Results);
        }
        #endregion

        #region "Metodos & constructores"
        public TEntityValidator()
        {

        }
        #endregion

        #region "Eventos"
        #endregion

        #region "Destructor"
        ~TEntityValidator()
        {

        }
        #endregion
        
    }
}