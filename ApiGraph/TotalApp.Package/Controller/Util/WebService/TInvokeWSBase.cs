﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using TotalApp.Package.DTO;

namespace TotalApp.Package.UTIL
{

    /// <summary>
    /// *Autor:Larry Ramirez
    /// *Fecha de creacion:09-01-2018
    /// *Version:1.1.20180109
    /// *Sinopsis:
    /// </summary>
    public abstract class TInvokeWSBase : TInfoEstatus_DTO, IDisposable
    {
        #region "Constantes"
        #endregion

        #region "Variables"
        private TotalAppContentType _oContentType = TotalAppContentType.application_json;
        private string _sUrl = THelper.Empty;
        private int _nTimeOut = 0;
        private TypeAuthorizationWS _oTypeAuthorizationWS = TypeAuthorizationWS.N_A;
        private string _sAuthorizationWS = THelper.Empty;
        private string _sStrRequest = THelper.Empty;
        private TypeRequestDataWS _oTypeRequestDataWS = TypeRequestDataWS.N_A;
        private TypeResponseDataWS _oTypeResponseDataWS = TypeResponseDataWS.N_A;
        private string _sStrRequestOriginal = THelper.Empty;
        private string _sStrResponse = THelper.Empty;
        private HttpStatusCode _oHttpCode = HttpStatusCode.OK;
        private bool _bIsServerCertificateCustomValidation = false;
        #endregion

        #region "Propiedades"

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:Indica el tipo de contenido que se envia el Web Service
        /// </summary>
        public TotalAppContentType ContentType
        {
            get
            {
                return _oContentType;
            }
            set
            {
                _oContentType = value;
            }
        }


        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:Especifica la ruta exacta del Web Service
        /// </summary>
        public string Url
        {
            get
            {
                return _sUrl;
            }
            set
            {
                _sUrl = value;
            }
        }


        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:Indica el tiempo maximo de espera despues de la Invocacion al Web Service
        /// </summary>
        public int TimeOut
        {
            get
            {
                return _nTimeOut;
            }
            set
            {
                _nTimeOut = value;
            }
        }


        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:Especifica el tipo de autorizacion que necesita el Web Service
        /// </summary>
        public TypeAuthorizationWS TypeAuthorizationWS
        {
            get
            {
                return _oTypeAuthorizationWS;
            }
            set
            {
                _oTypeAuthorizationWS = value;
            }
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:Especifica el Key de Autorizacion
        /// </summary>
        public string AuthorizationWS
        {
            get
            {
                return _sAuthorizationWS;
            }
            set
            {
                _sAuthorizationWS = value;
            }
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:Especifica el Json que se envia, que luego sera o no convertido en Bytes, Encrypt, Compress o Base64
        /// </summary>
        public string StrRequest
        {
            get
            {
                return _sStrRequest;
            }
            set
            {
                _sStrRequest = value;
            }
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:Especifica como se debe enviar el json al Web Service
        /// </summary>
        public TypeRequestDataWS TypeRequestDataWS
        {
            get
            {
                return _oTypeRequestDataWS;
            }
            set
            {
                _oTypeRequestDataWS = value;
            }
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:Especifica el tipo de respuesta que retorna el Web Service
        /// </summary>
        public TypeResponseDataWS TypeResponseDataWS
        {
            get
            {
                return _oTypeResponseDataWS;
            }
            set
            {
                _oTypeResponseDataWS = value;
            }
        }


        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:Encapsula el jspn original, permite saber que fue lo que se envio originalmente al Web Service
        /// </summary>
        public string StrRequestOriginal
        {
            get
            {
                return _sStrRequestOriginal;
            }
            private set
            {
                _sStrRequestOriginal = value;
            }
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:Encapsula el Json de respuesta del Web Service
        /// </summary>
        public string StrResponse
        {
            get
            {
                return _sStrResponse;
            }
            set
            {
                _sStrResponse = value;
            }
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:Indica cual fue el estado o codigo del estado de respuesta del Web Service
        /// </summary>
        public HttpStatusCode HttpCode
        {
            get
            {
                return _oHttpCode;
            }
            set
            {
                _oHttpCode = value;
            }
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:Indica el tipo de contenido que se envia el Web Service
        /// </summary>
        public bool ServerCertificateCustomValidation
        {
            get
            {
                return _bIsServerCertificateCustomValidation;
            }
            set
            {
                _bIsServerCertificateCustomValidation = value;
            }
        }
        #endregion

        #region "Metodos"

        #region "Metodos generales"
        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:Metodo que valida como se enviara el Json al Web Service
        /// </summary>
        /// <param name="oWsRequest"></param>
        protected void ValidateTypeRequestDataWS()
        {
            base.OK();
            StrRequestOriginal = StrRequest;
            if (TypeRequestDataWS != TypeRequestDataWS.N_A)
            {
                switch (TypeRequestDataWS)
                {
                    case TypeRequestDataWS.Encrypt:
                        StrRequest = StrRequest.TEncrypt();
                        break;
                    case TypeRequestDataWS.Compress:
                        StrRequest = StrRequest.TCompress();
                        break;
                    case TypeRequestDataWS.Encrypt_Compress:
                        StrRequest = StrRequest.TEncrypt_Compress();
                        break;
                    case TypeRequestDataWS.Compress_Encrypt:
                        StrRequest = StrRequest.TCompress_Encrypt();
                        break;
                    case TypeRequestDataWS.Base64:
                        StrRequest = StrRequest.TBase64EncodeOrDefault();
                        break;
                }
            }
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:Metodo que permite validar antes e invicar al Web Service
        /// </summary>
        /// <returns></returns>
        protected void Validate(bool bIsPost = true)
        {
            base.OK();

            if (TimeOut == 0)
            {
                TimeOut = 600000;
            }

            if (bIsPost)
            {
                if (Url.TIsNullOrEmpty())
                {
                    base.ErrorObjEmpty("#1 - Error: Debe especificar la URL.");
                }
                else if (StrRequest.TIsNullOrEmpty())
                {
                    base.ErrorObjEmpty("#2 - Error: El Json de envio esta vacio.");
                }
            }
            else
            {
                if (Url.TIsNullOrEmpty())
                {
                    base.ErrorObjEmpty("#1 - Error: Debe especificar la URL.");
                }
            }


        }
        #endregion

        #region "Exec Accion"
        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        public abstract void Post();

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        public abstract void Put();

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        public abstract void Get();
        #endregion

        #region "Exec Accion Async"
        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <returns></returns>
        public Task PostAsync()
        {
            var oTask = new Task(() =>
            {
                Post();
            });

            oTask.Start();

            return oTask;
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <returns></returns>
        public Task PutAsync()
        {
            var oTask = new Task(() =>
            {
                Put();
            });

            oTask.Start();

            return oTask;
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <returns></returns>
        public Task GetAsync()
        {
            var oTask = new Task(() =>
            {
                Get();
            });

            oTask.Start();

            return oTask;
        }
        #endregion

        #endregion

        #region "Funciones"

        #region "Funciones generales"

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:Funcion que permite validar el retorno de un objeto del Web Service
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        protected T ValidateObjectResponse<T>() where T : class
        {

            if (TypeResponseDataWS != TypeResponseDataWS.N_A)
            {
                var oJsonHelper = new TJsonHelper_DTO();

                switch (TypeResponseDataWS)
                {
                    case TypeResponseDataWS.DesEncrypt:
                        oJsonHelper.Json = StrResponse.TDecrypting();
                        break;
                    case TypeResponseDataWS.DeCompress:
                        oJsonHelper.Json = StrResponse.TDeCompress();
                        break;
                    case TypeResponseDataWS.DesEncrypt_DeCompress:
                        oJsonHelper.Json = StrResponse.TDesEncrypt_DeCompress();
                        break;
                    case TypeResponseDataWS.DeCompress_DeEncrypt:
                        oJsonHelper.Json = StrResponse.TDeCompress_DeEncrypt();
                        break;
                    case TypeResponseDataWS.DeCodeBase64:
                        oJsonHelper.Json = StrResponse.TBase64EncodeOrDefault();
                        break;
                    default:
                        return null;
                }

                if (oJsonHelper.IsOK)
                {
                    return oJsonHelper.Json.TDeserializeObjectOrDefault<T>();
                }

                oJsonHelper.Dispose();
                oJsonHelper = null;
                return null;
            }
            else
            {
                return StrResponse.TDeserializeObjectOrDefault<T>();
            }
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:Funcion que permite validar el retorno de una lista del Web Service
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        protected List<T> ValidateListResponse<T>() where T : class
        {

            if (TypeResponseDataWS != TypeResponseDataWS.N_A)
            {
                var oJsonHelper = new TJsonHelper_DTO();

                switch (TypeResponseDataWS)
                {
                    case TypeResponseDataWS.DesEncrypt:
                        oJsonHelper.Json = StrResponse.TDecrypting();
                        break;
                    case TypeResponseDataWS.DeCompress:
                        oJsonHelper.Json = StrResponse.TDeCompress();
                        break;
                    case TypeResponseDataWS.DesEncrypt_DeCompress:
                        oJsonHelper.Json = StrResponse.TDesEncrypt_DeCompress();
                        break;
                    case TypeResponseDataWS.DeCompress_DeEncrypt:
                        oJsonHelper.Json = StrResponse.TDeCompress_DeEncrypt();
                        break;
                    case TypeResponseDataWS.DeCodeBase64:
                        oJsonHelper.Json = StrResponse.TBase64EncodeOrDefault();
                        break;
                    default:
                        return null;
                }

                if (oJsonHelper.IsOK)
                {
                    return oJsonHelper.Json.TDeserializeListObjectOrDefault<T>();
                }

                oJsonHelper.Dispose();
                oJsonHelper = null;
                return null;
            }
            else
            {
                return StrResponse.TDeserializeListObjectOrDefault<T>();
            }
        }
        #endregion

        #region "Exec Accion de objectos Genericos"
        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:Funcion que captuta y convierte los retornos de los Web Service de TotalApp en WCF o Web Api
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="U"></typeparam>
        /// <returns></returns>
        public TClassGeneric_DTO<T, U> Get<T, U>() where T : TInfoEstatus_DTO, new()
                                                   where U : class, new()
        {
            var oVal = new TClassGeneric_DTO<T, U>();

            Get();

            if (base.IsOK)
            {
                switch (HttpCode)
                {
                    case HttpStatusCode.OK:
                        oVal.Data = ValidateObjectResponse<U>();
                        break;
                    default:
                        oVal.Info.InfoEstatus(StrResponse);
                        break;
                }
            }
            return oVal;
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:Funcion que captuta y convierte los retornos de los Web Service de TotalApp en WCF o Web Api
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="U"></typeparam>
        /// <returns></returns>
        public TListGeneric_DTO<T, U> GetList<T, U>() where T : TInfoEstatus_DTO, new()
                                                                where U : class, new()
        {
            var oVal = new TListGeneric_DTO<T, U>();

            Get();

            if (base.IsOK)
            {
                switch (HttpCode)
                {
                    case HttpStatusCode.OK:
                        oVal.Data = ValidateListResponse<U>();
                        break;
                    default:
                        oVal.Info.InfoEstatus(StrResponse);
                        break;
                }
            }

            return oVal;

        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:Funcion que captuta y convierte los retornos de los Web Service de TotalApp en WCF o Web Api
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="U"></typeparam>
        /// <param name="obj"></param>
        /// <returns></returns>
        public TClassGeneric_DTO<T, U> Post<T, U>(object obj) where T : TInfoEstatus_DTO, new()
                                                              where U : class, new()
        {
            if (obj.TIsNull())
            {
                base.ErrorObjEmpty("El objeto esta vacio");
                return null;
            }

            var oVal = new TClassGeneric_DTO<T, U>();

            StrRequest = obj.TSerializeObjectOrDefault();
            ValidateTypeRequestDataWS();

            var oJsonHelper = new TJsonHelper_DTO();
            oJsonHelper.Json = StrRequest;
            StrRequest = oJsonHelper.TSerializeObjectOrDefault();
            oJsonHelper.Dispose();
            oJsonHelper = null;
            TypeRequestDataWS = TypeRequestDataWS.N_A;

            Post();

            if (base.IsOK)
            {
                switch (HttpCode)
                {
                    case HttpStatusCode.OK:
                    case HttpStatusCode.Created:
                        oVal.Data = ValidateObjectResponse<U>();
                        break;
                    default:
                        oVal.Info.InfoEstatus(StrResponse);
                        break;
                }
            }

            return oVal;
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:Funcion que captuta y convierte los retornos de los Web Service de TotalApp en WCF o Web Api
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="U"></typeparam>
        /// <param name="oJsonHelper"></param>
        /// <returns></returns>
        public TClassGeneric_DTO<T, U> Post<T, U>(TJsonHelper_DTO oJsonHelper) where T : TInfoEstatus_DTO, new()
                                                                               where U : class, new()
        {
            if (oJsonHelper.TIsNull())
            {
                base.ErrorObjEmpty("El objeto esta vacio");
                return null;
            }

            var oVal = new TClassGeneric_DTO<T, U>();

            StrRequest = oJsonHelper.TSerializeObjectOrDefault();
            TypeRequestDataWS = TypeRequestDataWS.N_A;
            ValidateTypeRequestDataWS();

            Post();

            if (base.IsOK)
            {
                switch (HttpCode)
                {
                    case HttpStatusCode.OK:
                    case HttpStatusCode.Created:
                        oVal.Data = ValidateObjectResponse<U>();
                        break;
                    default:
                        oVal.Info.InfoEstatus(StrResponse);
                        break;
                }
            }

            return oVal;
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:Funcion que captuta y convierte los retornos de los Web Service de TotalApp en WCF o Web Api
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public TInfoEstatus_DTO Post(object obj)
        {

            if (obj.TIsNull())
            {
                base.ErrorObjEmpty("El objeto esta vacio");
                return null;
            }

            StrRequest = obj.TSerializeObjectOrDefault();
            ValidateTypeRequestDataWS();

            var oJsonHelper = new TJsonHelper_DTO();
            oJsonHelper.Json = StrRequest;
            StrRequest = oJsonHelper.TSerializeObjectOrDefault();
            oJsonHelper.Dispose();
            oJsonHelper = null;
            TypeRequestDataWS = TypeRequestDataWS.N_A;

            Post();

            if (base.IsOK)
            {
                return ValidateObjectResponse<TInfoEstatus_DTO>();
            }

            return null;
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:Funcion que captuta y convierte los retornos de los Web Service de TotalApp en WCF o Web Api
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="U"></typeparam>
        /// <param name="obj"></param>
        /// <returns></returns>
        public TClassGeneric_DTO<T, U> Put<T, U>(object obj) where T : TInfoEstatus_DTO, new()
                                                             where U : class, new()
        {
            if (obj.TIsNull())
            {
                base.ErrorObjEmpty("El objeto esta vacio");
                return null;
            }

            var oVal = new TClassGeneric_DTO<T, U>();

            StrRequest = obj.TSerializeObjectOrDefault();
            ValidateTypeRequestDataWS();

            var oJsonHelper = new TJsonHelper_DTO();
            oJsonHelper.Json = StrRequest;
            StrRequest = oJsonHelper.TSerializeObjectOrDefault();
            oJsonHelper.Dispose();
            oJsonHelper = null;
            TypeRequestDataWS = TypeRequestDataWS.N_A;

            Put();

            if (base.IsOK)
            {
                switch (HttpCode)
                {
                    case HttpStatusCode.OK:
                    case HttpStatusCode.Accepted:
                        oVal.Data = ValidateObjectResponse<U>();
                        break;
                    default:
                        oVal.Info.InfoEstatus(StrResponse);
                        break;
                }
            }

            return oVal;
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:Funcion que captuta y convierte los retornos de los Web Service de TotalApp en WCF o Web Api
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="U"></typeparam>
        /// <param name="oJsonHelper"></param>
        /// <returns></returns>
        public TClassGeneric_DTO<T, U> Put<T, U>(TJsonHelper_DTO oJsonHelper) where T : TInfoEstatus_DTO, new()
                                                                              where U : class, new()
        {
            if (oJsonHelper.TIsNull())
            {
                base.ErrorObjEmpty("El objeto esta vacio");
                return null;
            }

            var oVal = new TClassGeneric_DTO<T, U>();

            StrRequest = oJsonHelper.TSerializeObjectOrDefault();
            TypeRequestDataWS = TypeRequestDataWS.N_A;
            ValidateTypeRequestDataWS();

            Put();

            if (base.IsOK)
            {
                switch (HttpCode)
                {
                    case HttpStatusCode.OK:
                    case HttpStatusCode.Accepted:
                        oVal.Data = ValidateObjectResponse<U>();
                        break;
                    default:
                        oVal.Info.InfoEstatus(StrResponse);
                        break;
                }
            }

            return oVal;
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:Funcion que captuta y convierte los retornos de los Web Service de TotalApp en WCF o Web Api
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public TInfoEstatus_DTO Put(object obj)
        {

            if (obj.TIsNull())
            {
                base.ErrorObjEmpty("El objeto esta vacio");
                return null;
            }

            StrRequest = obj.TSerializeObjectOrDefault();
            ValidateTypeRequestDataWS();

            var oJsonHelper = new TJsonHelper_DTO();
            oJsonHelper.Json = StrRequest;
            StrRequest = oJsonHelper.TSerializeObjectOrDefault();
            oJsonHelper.Dispose();
            oJsonHelper = null;
            TypeRequestDataWS = TypeRequestDataWS.N_A;

            Put();

            if (base.IsOK)
            {
                return ValidateObjectResponse<TInfoEstatus_DTO>();
            }

            return null;
        }
        #endregion

        #region "Exec Accion de objectos Genericos Async"
        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:Funcion que captuta y convierte los retornos de los Web Service de TotalApp en WCF o Web Api
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="U"></typeparam>
        /// <returns></returns>
        public Task<TClassGeneric_DTO<T, U>> GetAsync<T, U>() where T : TInfoEstatus_DTO, new()
                                                              where U : class, new()
        {
            var oTask = new Task<TClassGeneric_DTO<T, U>>(() =>
            {
                return Get<T, U>();
            });

            oTask.Start();


            return oTask;
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:Funcion que captuta y convierte los retornos de los Web Service de TotalApp en WCF o Web Api
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="U"></typeparam>
        /// <returns></returns>
        public Task<TListGeneric_DTO<T, U>> GetListAsync<T, U>() where T : TInfoEstatus_DTO, new()
                                                                 where U : class, new()
        {

            var oTask = new Task<TListGeneric_DTO<T, U>>(() =>
            {
                return GetList<T, U>();
            });

            oTask.Start();

            return oTask;
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:Funcion que captuta y convierte los retornos de los Web Service de TotalApp en WCF o Web Api
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="U"></typeparam>
        /// <param name="obj"></param>
        /// <returns></returns>
        public Task<TClassGeneric_DTO<T, U>> PostAsync<T, U>(object obj) where T : TInfoEstatus_DTO, new()
                                                                          where U : class, new()
        {
            var oTask = new Task<TClassGeneric_DTO<T, U>>(() =>
            {
                return Post<T, U>(obj);
            });

            oTask.Start();

            return oTask;
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:Funcion que captuta y convierte los retornos de los Web Service de TotalApp en WCF o Web Api
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="U"></typeparam>
        /// <param name="oJsonHelper"></param>
        /// <returns></returns>
        public Task<TClassGeneric_DTO<T, U>> PostAsync<T, U>(TJsonHelper_DTO oJsonHelper) where T : TInfoEstatus_DTO, new()
                                                                                          where U : class, new()
        {
            var oTask = new Task<TClassGeneric_DTO<T, U>>(() =>
            {
                return Post<T, U>(oJsonHelper);
            });

            oTask.Start();

            return oTask;
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:Funcion que captuta y convierte los retornos de los Web Service de TotalApp en WCF o Web Api
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public Task<TInfoEstatus_DTO> PostAsync(object obj)
        {
            var oTask = new Task<TInfoEstatus_DTO>(() =>
            {
                return Post(obj);
            });

            oTask.Start();
            return oTask;

        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:Funcion que captuta y convierte los retornos de los Web Service de TotalApp en WCF o Web Api
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="U"></typeparam>
        /// <param name="obj"></param>
        /// <returns></returns>
        public Task<TClassGeneric_DTO<T, U>> PutAsync<T, U>(object obj) where T : TInfoEstatus_DTO, new()
                                                                         where U : class, new()
        {
            var oTask = new Task<TClassGeneric_DTO<T, U>>(() =>
            {
                return Put<T, U>(obj);
            });

            oTask.Start();

            return oTask;
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:Funcion que captuta y convierte los retornos de los Web Service de TotalApp en WCF o Web Api
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="U"></typeparam>
        /// <param name="oJsonHelper"></param>
        /// <returns></returns>
        public Task<TClassGeneric_DTO<T, U>> PutAsync<T, U>(TJsonHelper_DTO oJsonHelper) where T : TInfoEstatus_DTO, new()
                                                                                         where U : class, new()
        {
            var oTask = new Task<TClassGeneric_DTO<T, U>>(() =>
            {
                return Put<T, U>(oJsonHelper);
            });

            oTask.Start();

            return oTask;
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:Funcion que captuta y convierte los retornos de los Web Service de TotalApp en WCF o Web Api
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public Task<TInfoEstatus_DTO> PutAsync(object obj)
        {
            var oTask = new Task<TInfoEstatus_DTO>(() =>
            {
                return Put(obj);
            });

            oTask.Start();

            return oTask;

        }

        #endregion

        #endregion

        #region "Eventos"
        #endregion

        #region "Destructor"
        /// <summary>
        /// Autor   : Larry Ramirez
        /// Fecha   : 21-05-2016
        /// Sinopsis: Variable Disposed, permite saber de manera general si ya fueron liberados los recursos no administrados a la clase (Objeto).
        /// </summary>
        private bool _Disposed = false;

        // <summary>
        /// Autor   : Larry Ramirez
        /// Fecha   : 25-05-2016
        /// Sinopsis: Metodo que permite Desechar y limpiar objetos.
        /// </summary>
        public new void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        // <summary>
        /// Autor   : Larry Ramirez
        /// Fecha   : 25-05-2016
        /// Sinopsis: Logica para limpiar objetos de la memoria.
        /// </summary>
        protected new void Dispose(bool bIsDisposing)
        {
            //Si esta en false, significa que se tiene la oportunidad de limpiar los objetos
            if (!this._Disposed)
            {
                //Aqui se definen los objetos que se liberan
                if (bIsDisposing)
                {

                }
            }
            _Disposed = true;
        }

        // <summary>
        /// Autor   : Larry Ramirez
        /// Fecha   : 25-05-2016
        /// Sinopsis: Destructor por defecto.
        /// </summary>
        ~TInvokeWSBase()
        {
            Dispose(false);
            GC.SuppressFinalize(this);
        }
        #endregion
    }

}
