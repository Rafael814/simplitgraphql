﻿using System;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;

namespace TotalApp.Package.UTIL
{

    /// <summary>
    /// *Autor:Larry Ramirez
    /// *Fecha de creacion:09-01-2018
    /// *Version:1.1.20180109
    /// *Sinopsis:
    /// </summary>
    public class TInvokeWS_45 : TInvokeWSBase
    {
        #region "Constantes"
        #endregion

        #region "Variables"
        #endregion

        #region "Propiedades"
        #endregion

        #region "Funciones"

        #region "Funciones generales"
        #endregion

        #endregion

        #region "Metodos & constructores"
        public TInvokeWS_45() : base()
        {

        }

        #region "Exec Accion"
        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        public override void Post()
        {
            Exec("POST");
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        public override void Put()
        {
            Exec("PUT");
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:Funcion que Ejecuta segun el tipo de metodo invocado, Ejemplo: POST, PUT... Etc
        /// </summary>
        /// <param name="sMethod"></param>
        private void Exec(string sMethod)
        {
            base.OK();
            HttpResponseMessage oResponse = new HttpResponseMessage();
            HttpClient oCliente = new HttpClient();

            try
            {
                Validate();

                if (base.IsOK)
                {
                    if (ServerCertificateCustomValidation)
                    {
                        var oHttpClientHandler = new HttpClientHandler();
                        oHttpClientHandler.ServerCertificateCustomValidationCallback = (message, cert, chain, errors) => { return true; };
                        oCliente = new HttpClient(oHttpClientHandler);
                    }


                    //oCliente.BaseAddress = new Uri(Url);
                    StringContent sRequest = new StringContent(StrRequest, System.Text.Encoding.UTF8, ContentType.TStringValueAttributeToString());

                    Authorization(oCliente);

                    if (base.IsOK)
                    {
                        if (Url.Contains(@"https://"))
                        {
                            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;
                        }

                        switch (sMethod.ToUpper())
                        {
                            case "POST":
                                oResponse = oCliente.PostAsync(new Uri(Url), sRequest).Result;
                                break;
                            case "PUT":
                                oResponse = oCliente.PutAsync(new Uri(Url), sRequest).Result;
                                break;
                        }

                        sRequest.Dispose();
                        sRequest = null;

                        try
                        {
                            HttpCode = oResponse.StatusCode;
                            StrResponse = oResponse.Content.ReadAsStringAsync().Result;
                        }
                        catch
                        {

                        }


                        if (!oResponse.IsSuccessStatusCode)
                        {
                            base.InternalServerError(1
                                                  , "Error de conexion al web service"
                                                  , TTypeError.Err_CnxWebService);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                //string sMensaje = "MakeRequest - Exception message: " + ex.Message;
                //sMensaje += "\r\n" + "MakeRequest - Exception stack trace: " + ex.StackTrace;
                //sMensaje += "\r\n" + "MakeRequest - Exception HResult: " + ex.HResult;
                //sMensaje += "\r\n" + "MakeRequest - Exception inner ex: " + ex.InnerException;

                HttpCode = System.Net.HttpStatusCode.InternalServerError;
                base.InternalServerError(-999
                                        , "Error interno de invocacion al web service"
                                        , TTypeError.Err_CnxWebService
                                        , ex.Message);
            }


            finally
            {
                oCliente.Dispose();
                oCliente = null;
                oResponse.Dispose();
                oResponse = null;
            }

        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        public override void Get()
        {
            base.OK();

            HttpResponseMessage oResponse = new HttpResponseMessage();
            HttpClient oCliente = new HttpClient();

            base.Traza("Paso 1");
            try
            {
                if (ServerCertificateCustomValidation)
                {
                    var oHttpClientHandler = new HttpClientHandler();
                    oHttpClientHandler.ServerCertificateCustomValidationCallback = (message, cert, chain, errors) => { return true; };
                    oCliente = new HttpClient(oHttpClientHandler);
                }

                Validate(false);

                if (base.IsOK)
                {
                    //oCliente.BaseAddress = new Uri(Url);
                    Authorization(oCliente);

                    if (base.IsOK)
                    {
                        if (Url.Contains(@"https://"))
                        {
                            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;
                        }


                        oResponse = oCliente.GetAsync(new Uri(Url)).Result;

                        try
                        {
                            HttpCode = oResponse.StatusCode;
                            StrResponse = oResponse.Content.ReadAsStringAsync().Result;
                        }
                        catch
                        {

                        }


                        if (!oResponse.IsSuccessStatusCode)
                        {
                            base.InternalServerError(1
                                                  , "Error de conexion al web service"
                                                  , TTypeError.Err_CnxWebService);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                //string sMensaje = "MakeRequest - Exception message: " + ex.Message;
                //sMensaje += "\r\n" + "MakeRequest - Exception stack trace: " + ex.StackTrace;
                //sMensaje += "\r\n" + "MakeRequest - Exception HResult: " + ex.HResult;
                //sMensaje += "\r\n" + "MakeRequest - Exception inner ex: " + ex.InnerException;

                HttpCode = System.Net.HttpStatusCode.InternalServerError;
                base.InternalServerError(-999
                                        , "Error interno de invocacion al web service"
                                        , TTypeError.Err_CnxWebService
                                        , ex.Message);
            }
            finally
            {
                oCliente.Dispose();
                oCliente = null;
                oResponse.Dispose();
                oResponse = null;
            }
        }
        #endregion

        #region "Metodos generales"
        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:Metodo que valida el tipo de autorizacion antes de invocar al Web Service
        /// </summary>
        /// <param name="oWsRequest"></param>
        private void Authorization(HttpClient oCliente)
        {
            base.OK();

            if (TypeAuthorizationWS != TypeAuthorizationWS.N_A)
            {
                if (!AuthorizationWS.TIsNullOrEmpty())
                {
                    switch (TypeAuthorizationWS)
                    {
                        case TypeAuthorizationWS.Basic:
                            oCliente.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", AuthorizationWS);
                            break;
                        case TypeAuthorizationWS.Bearer:
                            oCliente.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", AuthorizationWS);
                            break;
                        case TypeAuthorizationWS.Authorization:
                            oCliente.DefaultRequestHeaders.TryAddWithoutValidation("Authorization", AuthorizationWS);
                            break;
                    }
                }
                else
                {
                    base.ErrorObjEmpty("El AuthorizationWS esta vacio");
                }
            }
        }
        #endregion

        #endregion

        #region "Eventos"
        #endregion

        #region "Destructor"
        // <summary>
        /// Autor   : Larry Ramirez
        /// Fecha   : 25-05-2016
        /// Sinopsis: Destructor por defecto.
        /// </summary>
        ~TInvokeWS_45()
        {
            Dispose(false);
            GC.SuppressFinalize(this);
        }
        #endregion
    }

}
