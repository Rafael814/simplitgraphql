﻿using System;
using System.Text;
using System.IO;
using System.Net;

namespace TotalApp.Package.UTIL
{

    /// <summary>
    /// *Autor:Larry Ramirez
    /// *Fecha de creacion:09-01-2018
    /// *Version:1.1.20180109
    /// *Sinopsis:
    /// </summary>
    public class TInvokeWS_40 : TInvokeWSBase
    {
        #region "Constantes"
        #endregion

        #region "Variables"
        #endregion

        #region "Propiedades"
        #endregion

        #region "Funciones"

        #region "Funciones generales"
        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:Funcion que captura la respuesta del Web Service
        /// </summary>
        /// <param name="oWsResponse"></param>
        /// <returns></returns>
        private string WebReponse(HttpWebResponse oWsResponse)
        {
            base.OK();

            string sJson = THelper.Empty;

            try
            {
                HttpCode = oWsResponse.StatusCode;

                using (var oReader = new StreamReader(oWsResponse.GetResponseStream()))
                {
                    sJson = oReader.ReadToEnd();
                }
            }
            catch (WebException ex)
            {
                sJson = WebException(ex);
            }

            return sJson;
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:Funcion que permite capturar cualquier error de invocacion o respuesta del web service
        /// </summary>
        /// <param name="ex"></param>
        /// <returns></returns>
        private string WebException(WebException ex)
        {

            string sJson = THelper.Empty;

            if (ex.Status == WebExceptionStatus.ProtocolError)
            {
                HttpCode = ((System.Net.HttpWebResponse)ex.Response).StatusCode;
                var oResponse = ((HttpWebResponse)ex.Response);

                try
                {
                    using (var oReader = new StreamReader(oResponse.GetResponseStream()))
                    {
                        sJson = oReader.ReadToEnd();
                    }
                }
                catch (WebException ext)
                {
                    base.InternalServerError(2
                                           , "Error en la respuesta del Web Service"
                                           , TTypeError.Err_InterWebService
                                           , "Error:" + ext.Message.ToString() + "\r\n" + "Estatus:" + ext.Status.ToString());
                }
            }
            else
            {
                base.InternalServerError(1
                                        , "Error de conexion al web service"
                                        , TTypeError.Err_CnxWebService
                                        , "Error:" + ex.Message.ToString() + "\r\n" + "Estatus:" + ex.Status.ToString());
            }

            return sJson;
        }

        #endregion

        #endregion

        #region "Metodos & constructores"
        public TInvokeWS_40():base()
        {

        }


        #region "Exec Accion"
        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        public override void Post()
        {
            Exec("POST");
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        public override void Put()
        {
            Exec("PUT");
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:Funcion que Ejecuta segun el tipo de metodo invocado, Ejemplo: POST, PUT... Etc
        /// </summary>
        /// <param name="sMethod"></param>
        private void Exec(string sMethod)
        {

            base.OK();

            try
            {

                Validate();

                if (base.IsOK)
                {
                    byte[] byBytes = Encoding.UTF8.GetBytes(StrRequest);
                    HttpWebRequest oWsRequest = (HttpWebRequest)WebRequest.Create(Url);
                    oWsRequest.ContentType = ContentType.TStringValueAttributeToString() + ";charset=UTF-8";
                    oWsRequest.UserAgent = ".NET Framework";
                    oWsRequest.Method = sMethod;
                    oWsRequest.Timeout = TimeOut;
                    oWsRequest.ContentLength = byBytes.Length;
                    Authorization(oWsRequest);

                    if (base.IsOK)
                    {
                        using (var oRequestStream = oWsRequest.GetRequestStream())
                        {
                            oRequestStream.Write(byBytes, 0, byBytes.Length);
                            oRequestStream.Flush();
                            oRequestStream.Close();
                        }

                        StrResponse = WebReponse((HttpWebResponse)oWsRequest.GetResponse());
                    }

                    byBytes = null;
                    oWsRequest = null;
                }
            }
            catch (WebException ex)
            {
                StrResponse = WebException(ex);
            }
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        public override void Get()
        {
            base.OK();
            try
            {
                Validate(false);

                if (base.IsOK)
                {
                    HttpWebRequest oWsRequest = (HttpWebRequest)WebRequest.Create(Url);
                    oWsRequest.ContentType = ContentType.TStringValueAttributeToString() + ";charset=UTF-8";
                    oWsRequest.UserAgent = ".NET Framework";
                    oWsRequest.Method = "GET";
                    oWsRequest.Timeout = TimeOut;
                    Authorization(oWsRequest);

                    if (base.IsOK)
                    {
                        StrResponse = WebReponse((HttpWebResponse)oWsRequest.GetResponse());
                    }

                    oWsRequest = null;
                }
            }
            catch (WebException ex)
            {
                StrResponse = WebException(ex);
            }
        }
        #endregion

        #region "Metodos generales"
        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:Metodo que valida el tipo de autorizacion antes de invocar al Web Service
        /// </summary>
        /// <param name="oWsRequest"></param>
        private void Authorization(HttpWebRequest oWsRequest)
        {
            base.OK();

            if (TypeAuthorizationWS != TypeAuthorizationWS.N_A)
            {
                if (!AuthorizationWS.TIsNullOrEmpty())
                {
                    switch (TypeAuthorizationWS)
                    {
                        case TypeAuthorizationWS.Basic:
                            oWsRequest.Headers.Add("Authorization: Basic " + AuthorizationWS);
                            break;
                        case TypeAuthorizationWS.Bearer:
                            oWsRequest.Headers.Add("Authorization: Bearer " + AuthorizationWS);
                            break;
                        case TypeAuthorizationWS.Authorization:
                            oWsRequest.Headers.Add("Authorization" + AuthorizationWS);
                            break;
                    }
                }
                else
                {
                    base.ErrorObjEmpty("El AuthorizationWS esta vacio");
                }
            }
        }
        #endregion

        #endregion

        #region "Eventos"
        #endregion

        #region "Destructor"
        // <summary>
        /// Autor   : Larry Ramirez
        /// Fecha   : 25-05-2016
        /// Sinopsis: Destructor por defecto.
        /// </summary>
        ~TInvokeWS_40()
        {
            Dispose(false);
            GC.SuppressFinalize(this);
        }
        #endregion
    }

}
