
using System.Collections.Generic;


namespace TotalApp.Package.UTIL
{
    /// <summary>
    /// *Autor:Larry Ramirez
    /// *Fecha de creacion:09-01-2018
    /// *Version:1.1.20180109
    /// *Sinopsis:
    /// </summary>
	public static class THelperGenericListDataReader
	{
       

        #region "Variables"
        private static readonly object _oReaderLock = new object();
        #endregion

        #region "Funciones"
        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="oList"></param>
        /// <returns></returns>
        public static TGenericListDataReader<T> TGetDataReader<T>(this IEnumerable<T> oList)
        {
            lock (_oReaderLock)
            {
                return new TGenericListDataReader<T>(oList);
            }

        }
        #endregion


    }


}