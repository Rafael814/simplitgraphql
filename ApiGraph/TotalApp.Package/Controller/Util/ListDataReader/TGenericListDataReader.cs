using System;
using System.Collections.Generic;
using System.Data;
using System.Reflection;

namespace TotalApp.Package.UTIL
{
    /// <summary>
    /// *Autor:Larry Ramirez
    /// *Fecha de creacion:09-01-2018
    /// *Version:1.1.20180109
    /// *Sinopsis:
    /// </summary>
    /// <typeparam name="T"></typeparam>
	public class TGenericListDataReader<T> : IDataReader
	{
        #region "Constantes"
        #endregion

        #region "Variables"
        private IEnumerator<T> _oList = null;
        private List<PropertyInfo> oProperties = new List<PropertyInfo>();
        private Dictionary<string, int> oNameLookup = new Dictionary<string, int>();
        #endregion

        #region "Propiedades"
        #endregion


        #region "Funciones"
        #endregion

        #region "Metodos & constructores"
        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="oList"></param>
        public TGenericListDataReader(IEnumerable<T> oList)
        {
            this._oList = oList.GetEnumerator();

            oProperties.AddRange(
                typeof(T)
                .GetProperties(
                    BindingFlags.GetProperty |
                    BindingFlags.Instance |
                    BindingFlags.Public |
                    BindingFlags.DeclaredOnly
                    ));

            for (int nIndex = 0; nIndex < oProperties.Count; nIndex++)
            {
                oNameLookup[oProperties[nIndex].Name] = nIndex;
            }
        }
        #endregion

        #region "Eventos"
        #endregion

        #region "Destructor"
        #endregion

        #region IDataReader Members

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        public void Close()
		{
            _oList.Dispose();
		}

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        public int Depth
		{
			get { throw THelperInfoEstatus.NotImplemented(); }
		}

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        public DataTable GetSchemaTable()
		{
			throw THelperInfoEstatus.NotImplemented();
		}

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        public bool IsClosed
		{
			get { throw THelperInfoEstatus.NotImplemented(); }
		}

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        public bool NextResult()
		{
			throw THelperInfoEstatus.NotImplemented();
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        public bool Read()
		{
			return _oList.MoveNext();
		}

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        public int RecordsAffected
		{
			get { throw THelperInfoEstatus.NotImplemented(); }
		}

        #endregion

        #region IDisposable Members
        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        public void Dispose()
		{
			Close();
		}

        #endregion

        #region IDataRecord Members
        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        public int FieldCount
		{
			get { return oProperties.Count; }
		}

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="nValor"></param>
        /// <returns></returns>
        public bool GetBoolean(int nValor)
		{
			return (bool)GetValue(nValor);
		}

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="nValor"></param>
        /// <returns></returns>
        public byte GetByte(int nValor)
		{
			return (byte)GetValue(nValor);
		}

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="nValor"></param>
        /// <param name="fieldOffset"></param>
        /// <param name="buffer"></param>
        /// <param name="bufferoffset"></param>
        /// <param name="length"></param>
        /// <returns></returns>
        public long GetBytes(int nValor, long fieldOffset, byte[] buffer, int bufferoffset, int length)
		{
			throw THelperInfoEstatus.NotImplemented();
		}

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="nValor"></param>
        /// <returns></returns>
        public char GetChar(int nValor)
		{
			return (char)GetValue(nValor);
		}

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="nValor"></param>
        /// <param name="fieldoffset"></param>
        /// <param name="buffer"></param>
        /// <param name="bufferoffset"></param>
        /// <param name="length"></param>
        /// <returns></returns>
        public long GetChars(int nValor, long fieldoffset, char[] buffer, int bufferoffset, int length)
		{
			throw THelperInfoEstatus.NotImplemented();
		}

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="nValor"></param>
        /// <returns></returns>
        public IDataReader GetData(int nValor)
		{
			throw THelperInfoEstatus.NotImplemented();
		}

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="nValor"></param>
        /// <returns></returns>
        public string GetDataTypeName(int nValor)
		{
			throw THelperInfoEstatus.NotImplemented();
		}

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="nValor"></param>
        /// <returns></returns>
        public DateTime GetDateTime(int nValor)
		{
			return (DateTime)GetValue(nValor);
		}

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="nValor"></param>
        /// <returns></returns>
        public decimal GetDecimal(int nValor)
		{
			return (decimal)GetValue(nValor);
		}

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="nValor"></param>
        /// <returns></returns>
        public double GetDouble(int nValor)
		{
			return (double)GetValue(nValor);
		}

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="nValor"></param>
        /// <returns></returns>
        public Type GetFieldType(int nValor)
		{
			return oProperties[nValor].PropertyType;
		}

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="nValor"></param>
        /// <returns></returns>
        public float GetFloat(int nValor)
		{
			return (float)GetValue(nValor);
		}

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="nValor"></param>
        /// <returns></returns>
        public Guid GetGuid(int nValor)
		{
			return (Guid)GetValue(nValor);
		}

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="nValor"></param>
        /// <returns></returns>
        public short GetInt16(int nValor)
		{
			return (short)GetValue(nValor);
		}

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="nValor"></param>
        /// <returns></returns>
        public int GetInt32(int nValor)
		{
			return (int)GetValue(nValor);
		}

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="nValor"></param>
        /// <returns></returns>
        public long GetInt64(int nValor)
		{
			return (long)GetValue(nValor);
		}

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="nValor"></param>
        /// <returns></returns>
        public string GetName(int nValor)
		{
			return oProperties[nValor].Name;
		}

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sName"></param>
        /// <returns></returns>
        public int GetOrdinal(string sName)
		{
			if (oNameLookup.ContainsKey(sName))
			{
				return oNameLookup[sName];
			}
			else
			{
				return -1;
			}
		}

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="nValor"></param>
        /// <returns></returns>
        public string GetString(int nValor)
		{
			return (string)GetValue(nValor);
		}

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="nValor"></param>
        /// <returns></returns>
        public object GetValue(int nValor)
		{
			return oProperties[nValor].GetValue(_oList.Current, null);
		}

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="oParam"></param>
        /// <returns></returns>
        public int GetValues(object[] oParam)
		{
			int getValues = Math.Max(FieldCount, oParam.Length);

			for (int nIndex = 0; nIndex < getValues; nIndex++)
			{
                oParam[nIndex] = GetValue(nIndex);
			}

			return getValues;
		}

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="nValor"></param>
        /// <returns></returns>
        public bool IsDBNull(int nValor)
		{
			return GetValue(nValor) == null;
		}

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sName"></param>
        /// <returns></returns>
        public object this[string sName]
		{
			get
			{
				return GetValue(GetOrdinal(sName));
			}
		}

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="nValor"></param>
        /// <returns></returns>
        public object this[int nValor]
		{
			get
			{
				return GetValue(nValor);
			}
		}

		#endregion
	}
}