﻿using System;
using System.IO;
using System.Linq;
using System.Text;

namespace TotalApp.Package.UTIL
{
    /// <summary>
    /// *Autor:Larry Ramirez
    /// *Fecha de creacion:09-01-2018
    /// *Version:1.1.20180109
    /// *Sinopsis:Clase de tipo sealed, permite leer lineas en archivos
    /// </summary>
    public sealed class TFile : ITFile, IDisposable
    {
        #region "Constantes"
        #endregion

        #region "Variables"
        private string _sNombreArchivo = THelper.Empty;
        private string _sDir = THelper.Empty;
        private string sExt;
        #endregion

        #region "Propiedades"
        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:Propiedad que define el nombre del archivo, solo el nombre, no especifica la extension
        /// </summary>
        public string NombreArchivo
        {

            get { return _sNombreArchivo; }
            set { _sNombreArchivo = value; }
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:Propiedad que define el directorio donde se leera el archivo - Ejemplo: C:\Log
        /// Si no se especifica, automaticamente se le agrega la ruta del directorio raiz de la app
        /// </summary>
        public string Dir
        {
            get { return _sDir; }
            set { _sDir = value; }
        }


        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:Propiedad que define el tipo de extension del archivo - Ejemplo: .txt
        /// </summary>
        public string Ext
        {
            get { return sExt; }
            set { sExt = value; }
        }
        #endregion

        #region "Funciones"

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:Lee directamente todo el archivo
        /// </summary>
        /// <param name="sDir">Ruta directa en donde se leera el archivo - Ejemplo: C:\Log\Prueba.txt</param>
        /// <returns></returns>
        public string FileRead(string sDir)
        {
            if (sDir.TIsNullOrEmpty()) THelperInfoEstatus.ArgumentNull(nameof(sDir));

            string sRead = THelper.Empty;

            if (File.Exists(sDir))
            {
                using (FileStream oFs = new FileStream(sDir, FileMode.Open, FileAccess.Read))
                {
                    StreamReader oReader = new StreamReader(oFs, Encoding.UTF8, true);
                    sRead = oReader.ReadToEnd();
                    oReader.Close();
                    oFs.Close();
                }
            }


            return sRead;
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:Metodo que permite leer directamente en un archivo sin la necesidad de establecer previamente las propiedades de esta clase
        /// Sino que se envian como parametros e internamente el metodo establece los valores necesarios para leer el archivog
        /// </summary>
        /// <param name="sNombreArc">Parametro que define el nombre del archivo, solo el nombre, no especifica la extension</param>
        /// <param name="sDir">Propiedad que define el directorio donde se leera el archivo - Ejemplo: C:\Log,  Si no se especifica, automaticamente se le agrega la ruta del directorio raiz de la app</param>
        /// <param name="sExt">Propiedad que define el tipo de extension del archivo - Ejemplo: .txt</param>
        /// <returns></returns>
        public string FileRead(string sNombreArc
                            , string sDir
                            , string sExt)
        {

            NombreArchivo = sNombreArc;
            Dir = sDir;
            Ext = sExt;

            Validar();
            return FileRead(Dir);
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:Funcion que permite leer un archivo y encapsular todas las lineas
        /// Sino que se envian como parametros e internamente el metodo establece los valores necesarios para leer el archivog
        /// </summary>
        /// <param name="sNombreArc">Parametro que define el nombre del archivo, solo el nombre, no especifica la extension</param>
        /// <param name="sDir">Propiedad que define el directorio donde se leera el archivo - Ejemplo: C:\Log,  Si no se especifica, automaticamente se le agrega la ruta del directorio raiz de la app</param>
        /// <param name="sExt">Propiedad que define el tipo de extension del archivo - Ejemplo: .txt</param>
        /// <returns></returns>
        public string[] FileReadArray(string sNombreArc
                                    , string sDir
                                    , string sExt)
        {

            NombreArchivo = sNombreArc;
            Dir = sDir;
            Ext = sExt;

            Validar();
            return FileReadArray(Dir);
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:Funcion que permite leer un archivo y encapsular todas las lineas
        /// </summary>
        /// <param name="sDir">Ruta directa en donde se leera el archivo - Ejemplo: C:\Log\Prueba.txt</param>
        /// <returns></returns>
        public string[] FileReadArray(string sDir)
        {
            if (sDir.TIsNullOrEmpty()) THelperInfoEstatus.ArgumentNull(nameof(sDir));

            if (File.Exists(sDir))
            {
                return (from Arh in File.ReadAllLines(sDir)
                        select Arh).ToArray();
            }

            return null;
        }
        #endregion

        #region "Metodos & constructores"
        public TFile()
        {

        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:Valida previamente las propiedades que definen la ruta completa del archivo
        /// </summary>
        private void Validar()
        {
            if (NombreArchivo.TIsNullOrEmpty()) THelperInfoEstatus.ArgumentNull(nameof(NombreArchivo));

            if (Dir.TIsNullOrEmpty())
            {
                Dir = THelper.DirectorioRaiz;
            }

            Dir += @"\TotalAppFile";

            if (!Directory.Exists(Dir))
            {
                Directory.CreateDirectory(Dir);
            }

            Dir += @"\" + NombreArchivo + sExt;

        }
        #endregion


        #region "Eventos"
        #endregion

        #region "Destructor"
        /// <summary>
        /// Autor   : Larry Ramirez
        /// Fecha   : 21-05-2016
        /// Sinopsis: Variable Disposed, permite saber de manera general si ya fueron liberados los recursos no administrados a la clase (Objeto).
        /// </summary>
        private bool _Disposed = false;

        // <summary>
        /// Autor   : Larry Ramirez
        /// Fecha   : 25-05-2016
        /// Sinopsis: Metodo que permite Desechar y limpiar objetos de la memoria.
        /// </summary>
        public void Dispose()
        {

            Dispose(true);
            GC.SuppressFinalize(this);
        }

        // <summary>
        /// Autor   : Larry Ramirez
        /// Fecha   : 25-05-2016
        /// Sinopsis: Logica para limpiar objetos de la memoria.
        /// </summary>
        private void Dispose(bool bIsDisposing)
        {
            //Si esta en false, significa que se tiene la oportunidad de limpiar los objetos
            if (!this._Disposed)
            {
                //Aqui se definen los objetos que se liberan
                if (bIsDisposing)
                {

                }
            }
            _Disposed = true;
        }

        // <summary>
        /// Autor   : Larry Ramirez
        /// Fecha   : 25-05-2016
        /// Sinopsis: Destructor por defecto.
        /// </summary>
        ~TFile()
        {
            Dispose(false);
            GC.SuppressFinalize(this);

        }
        #endregion
    }
}
