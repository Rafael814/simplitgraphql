﻿
namespace TotalApp.Package.UTIL
{
    /// <summary>
    /// *Autor:Larry Ramirez
    /// *Fecha de creacion:09-01-2018
    /// *Version:1.1.20180109
    /// *Sinopsis:Clase de tipo static, permite invocar directamente los metodos de la clase ITFile, sin la necesidad de instanciar dicha clase
    /// </summary>
    public static class THelperFile
    {
        #region "Constantes"
        #endregion

        #region "Variables"
        #endregion

        #region "Propiedades"
        #endregion

        #region "Funciones"
        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:Lee directamente todo el archivo
        /// </summary>
        /// <param name="sDir">Ruta directa en donde se escribira el archivo - Ejemplo: C:\Log\Prueba.txt</param>
        /// <returns></returns>
        public static string FileRead(string sDir)
        {

            string sRead = THelper.Empty;

            using (ITFile oFile = new TFile())
            {
                sRead = oFile.FileRead(sDir);
                oFile.Dispose();
            }

            return sRead;
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:Metodo que permite leer directamente en un archivo sin la necesidad de establecer previamente las propiedades de esta clase
        /// Sino que se envian como parametros e internamente el metodo establece los valores necesarios para leer el archivog
        /// </summary>
        /// <param name="sNombreArc">Parametro que define el nombre del archivo, solo el nombre, no especifica la extension</param>
        /// <param name="sDir">Propiedad que define el directorio donde se almacenara el archivo - Ejemplo: C:\Log,  Si no se especifica, automaticamente se le agrega la ruta del directorio raiz de la app</param>
        /// <param name="sExt">Propiedad que define el tipo de extension del archivo - Ejemplo: .txt</param>
        /// <returns></returns>
        public static string FileRead(string sNombreArc
                                    , string sDir
                                    , string sExt)
        {

            string sRead = THelper.Empty;

            using (ITFile oFile = new TFile())
            {
                sRead = oFile.FileRead(sNombreArc, sDir, sExt);
                oFile.Dispose();
            }

            return sRead;
        }


        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:Funcion que permite leer un archivo y encapsular todas las lineas
        /// Sino que se envian como parametros e internamente el metodo establece los valores necesarios para leer el archivog
        /// </summary>
        /// <param name="sNombreArc">Parametro que define el nombre del archivo, solo el nombre, no especifica la extension</param>
        /// <param name="sDir">Propiedad que define el directorio donde se leera el archivo - Ejemplo: C:\Log,  Si no se especifica, automaticamente se le agrega la ruta del directorio raiz de la app</param>
        /// <param name="sExt">Propiedad que define el tipo de extension del archivo - Ejemplo: .txt</param>
        /// <returns></returns>
        public static string[] FileReadArray(string sNombreArc
                                           , string sDir
                                           , string sExt)
        {
            string[] sRead = null;

            using (ITFile oFile = new TFile())
            {
                sRead = oFile.FileReadArray(sNombreArc, sDir, sExt);
                oFile.Dispose();
            }

            return sRead;

        }


        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:Funcion que permite leer un archivo y encapsular todas las lineas
        /// </summary>
        /// <param name="sDir">Ruta directa en donde se leera el archivo - Ejemplo: C:\Log\Prueba.txt</param>
        /// <returns></returns>
        public static string[] FileReadArray(string sDir)
        {
            string[] sRead = null;

            using (ITFile oFile = new TFile())
            {
                sRead = oFile.FileReadArray(sDir);
                oFile.Dispose();
            }

            return sRead;
        }
        #endregion

        #region "Metodos & constructores"
        #endregion

        #region "Eventos"
        #endregion

        #region "Destructor"
        #endregion
    }
}
