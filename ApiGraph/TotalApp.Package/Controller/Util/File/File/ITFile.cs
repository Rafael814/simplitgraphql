﻿
using System;

namespace TotalApp.Package.UTIL
{
    /// <summary>
    /// *Autor:Larry Ramirez
    /// *Fecha de creacion:09-01-2018
    /// *Version:1.1.20180109
    /// *Sinopsis:Clase de tipo interface, permite invocar a la clase file, debido a que se considera una clase de configuracion para leer archivos
    /// </summary>
    public interface ITFile : IDisposable
    {
        #region "Constantes"
        #endregion

        #region "Variables"
        #endregion

        #region "Propiedades"
        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:Propiedad que define el nombre del archivo, solo el nombre, no especifica la extension
        /// </summary>
        string NombreArchivo { get; set; }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:Propiedad que define el directorio donde se leera el archivo - Ejemplo: C:\Log
        /// Si no se especifica, automaticamente se le agrega la ruta del directorio raiz de la app
        /// </summary>
        string Dir { get; set; }

        // <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:Propiedad que define el tipo de extension del archivo - Ejemplo: .txt
        /// </summary>
        string Ext { get; set; }
        #endregion

        #region "Funciones"
        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:Lee directamente todo el archivo
        /// </summary>
        /// <param name="sDir">Ruta directa en donde se leera el archivo - Ejemplo: C:\Log\Prueba.txt</param>
        /// <returns></returns>
        string FileRead(string sDir);

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:Metodo que permite leer directamente en un archivo sin la necesidad de establecer previamente las propiedades de esta clase
        /// Sino que se envian como parametros e internamente el metodo establece los valores necesarios para leer el archivog
        /// </summary>
        /// <param name="sNombreArc">Parametro que define el nombre del archivo, solo el nombre, no especifica la extension</param>
        /// <param name="sDir">Propiedad que define el directorio donde se leera el archivo - Ejemplo: C:\Log,  Si no se especifica, automaticamente se le agrega la ruta del directorio raiz de la app</param>
        /// <param name="sExt">Propiedad que define el tipo de extension del archivo - Ejemplo: .txt</param>
        /// <returns></returns>
        string FileRead(string sNombreArc
                      , string sDir
                      , string sExt);


        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:Funcion que permite leer un archivo y encapsular todas las lineas
        /// Sino que se envian como parametros e internamente el metodo establece los valores necesarios para leer el archivog
        /// </summary>
        /// <param name="sNombreArc">Parametro que define el nombre del archivo, solo el nombre, no especifica la extension</param>
        /// <param name="sDir">Propiedad que define el directorio donde se leera el archivo - Ejemplo: C:\Log,  Si no se especifica, automaticamente se le agrega la ruta del directorio raiz de la app</param>
        /// <param name="sExt">Propiedad que define el tipo de extension del archivo - Ejemplo: .txt</param>
        /// <returns></returns>
        string[] FileReadArray(string sNombreArc
                             , string sDir
                             , string sExt);


        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:Funcion que permite leer un archivo y encapsular todas las lineas
        /// </summary>
        /// <param name="sDir">Ruta directa en donde se leera el archivo - Ejemplo: C:\Log\Prueba.txt</param>
        /// <returns></returns>
        string[] FileReadArray(string sDir);
        #endregion

        #region "Metodos & constructores"

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:Metodo que permite Desechar y limpiar objetos de la memoria
        /// </summary>
        new void Dispose();
        #endregion

        #region "Eventos"
        #endregion

        #region "Destructor"
        #endregion
    }
}
