﻿
using System;

namespace TotalApp.Package.UTIL
{
    /// <summary>
    /// *Autor:Larry Ramirez
    /// *Fecha de creacion:09-01-2018
    /// *Version:1.1.20180109
    /// *Sinopsis:Clase de tipo interface, permite invocar a la clase log, debido a que se considera una clase de configuracion para la crear y modificacion de archivos log
    /// </summary>
    public interface ITLog: IDisposable
    {
        #region "Constantes"
        #endregion

        #region "Variables"
        #endregion

        #region "Propiedades"
        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:Propiedad que define el nombre del archivo, solo el nombre, no especifica la extension
        /// </summary>
        string NombreArchivo { get; set; }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:Propiedad que define el directorio donde se almacenara el archivo - Ejemplo: C:\Log
        /// Si no se especifica, automaticamente se le agrega la ruta del directorio raiz de la app
        /// </summary>
        string Dir { get; set; }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:Propiedad que define si se el agrega o no la fecha y hora al archivo
        /// </summary>
        bool AddFechaHoraAlNombreDelArchivo { get; set; }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:Propiedad que define el tipo de extension del archivo - Ejemplo: .txt
        /// </summary>
        string Ext { get; set; }
        #endregion

        #region "Funciones"
        #endregion

        #region "Metodos & constructores"
        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:Escribe directamente una linea en un archivo, por lo general .txt
        /// </summary>
        /// <param name="sDir">Ruta directa en donde se escribira el archivo - Ejemplo: C:\Log\Prueba.txt </param>
        /// <param name="sMensaje">Linea a escribir en el archivo</param>
        void LogWriteDirect(string sDir, string sMensaje);

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:Este metodo se ultiliza acumular trazas, para esto es necesario definir previamente las propiedades del archivo, como nombre y ruta
        /// si la ruta es vacia, automaticamente se establece una ruta por defecto que es la del directorio raiz
        /// </summary>
        /// <param name="sMensaje">Linea a escribir en el archivo</param>
        /// <param name="bIsAddFechaHora">Define si se agrega o no la fecha y la hora en el mensaje</param>
        /// <param name="bIsFinDelLog">Define si es o no fin de la traza para poder escribir los registros acumulados en el archivo</param>
        void LogWrite(string sMensaje, bool bIsAddFechaHora = false, bool bIsFinDelLog = false);

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:Metodo que permite escribir directamente en un archivo sin la necesidad de establecer previamente las propiedades de esta clase
        /// Sino que se envian como parametros e internamente el metodo establece los valores necesarios para completar, crear o actualizar el archivo de traza o log
        /// </summary>
        /// <param name="sNombreArc">Parametro que define el nombre del archivo, solo el nombre, no especifica la extension</param>
        /// <param name="sMensaje">Linea a escribir en el archivo</param>
        /// <param name="bIsAddFechaHoraAlNombreDelArchivo">Parametro que define si se el agrega o no la fecha y hora al archivo</param>
        /// <param name="bIsAddFechaHoraAlMensaje">Parametro que define si se el agrega o no la fecha y hora al mensaje</param>
        /// <param name="sDir">Propiedad que define el directorio donde se almacenara el archivo - Ejemplo: C:\Log,  Si no se especifica, automaticamente se le agrega la ruta del directorio raiz de la app</param>
        /// <param name="sExt">Propiedad que define el tipo de extension del archivo - Ejemplo: .txt</param>
        void LogWrite(string sNombreArc
                    , string sMensaje
                    , bool bIsAddFechaHoraAlNombreDelArchivo = false
                    , bool bIsAddFechaHoraAlMensaje = false
                    , string sDir = ""
                    , string sExt = ".txt");


        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:Metodo que permite escribir directamente en un archivo sin la necesidad de establecer previamente las propiedades de esta clase
        /// Aplica unicamente a objetos definidos por el programador
        /// </summary>
        ///<param name="obj">Parametro que especifica el objeto que se imprime en el archivo</param>
        /// <param name="sNombreArc">Parametro que define el nombre del archivo, solo el nombre, no especifica la extension, si esta vacio, toma automaticamente el nombre del objeto</param>
        /// <param name="sMensaje">Linea a escribir en el archivo</param>
        /// <param name="bIsAddFechaHoraAlNombreDelArchivo">Parametro que define si se el agrega o no la fecha y hora al archivo</param>
        /// <param name="bIsAddFechaHoraAlMensaje">Parametro que define si se el agrega o no la fecha y hora al mensaje</param>
        /// <param name="sDir">Propiedad que define el directorio donde se almacenara el archivo - Ejemplo: C:\Log,  Si no se especifica, automaticamente se le agrega la ruta del directorio raiz de la app</param>
        /// <param name="sExt">Propiedad que define el tipo de extension del archivo - Ejemplo: .txt</param>
        void LogWriteObject(object obj
                                , string sNombreArc
                                , string sMensaje
                                , bool bIsAddFechaHoraAlNombreDelArchivo = false
                                , bool bIsAddFechaHoraAlMensaje = false
                                , string sDir = ""
                                , string sExt = ".txt");

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:Metodo que permite Desechar y limpiar objetos de la memoria
        /// </summary>
        new void Dispose();
        #endregion

        #region "Eventos"
        #endregion

        #region "Destructor"
        #endregion
    }
}
