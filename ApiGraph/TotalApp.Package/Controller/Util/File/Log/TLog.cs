﻿using System;
using System.IO;
using System.Collections.Generic;


namespace TotalApp.Package.UTIL
{
    /// <summary>
    /// *Autor:Larry Ramirez
    /// *Fecha de creacion:09-01-2018
    /// *Version:1.1.20180109
    /// *Sinopsis:Clase de tipo sealed, permite crear y escribir lineas en archivos
    /// </summary>
    public sealed class TLog: ITLog, IDisposable
    {
        #region "Constantes"
        #endregion

        #region "Variables"
        private string _sNombreArchivo = THelper.Empty;
        private List<string> _oListMensaje = new List<string>();
        private bool _bIsAddFechaHoraAlNombreDelArchivo = false;
        private string _sDir = THelper.Empty;
        private string sExt = ".txt";
        #endregion

        #region "Propiedades"
        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:Propiedad que define el nombre del archivo, solo el nombre, no especifica la extension
        /// </summary>
        public string NombreArchivo {

            get { return _sNombreArchivo; }
            set { _sNombreArchivo = value; }
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:Propiedad que define el directorio donde se almacenara el archivo - Ejemplo: C:\Log
        /// Si no se especifica, automaticamente se le agrega la ruta del directorio raiz de la app
        /// </summary>
        public string Dir
        {
            get { return _sDir; }
            set { _sDir = value; }
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:Propiedad que define si se el agrega o no la fecha y hora al archivo
        /// </summary>
        public bool AddFechaHoraAlNombreDelArchivo
        {
            get { return _bIsAddFechaHoraAlNombreDelArchivo; }
            set { _bIsAddFechaHoraAlNombreDelArchivo = value; }
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:Propiedad que define el tipo de extension del archivo - Ejemplo: .txt
        /// </summary>
        public string Ext
        {
            get { return sExt; }
            set { sExt = value; }
        }
        #endregion

        #region "Funciones"
        #endregion

        #region "Metodos & constructores"
        public TLog()
        {

        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:Valida previamente las propiedades que definen la ruta completa del archivo
        /// </summary>
        private void Validar()
        {
            if (NombreArchivo.TIsNullOrEmpty()) THelperInfoEstatus.ArgumentNull(nameof(NombreArchivo));

            if (Dir.TIsNullOrEmpty())
            {
                Dir = THelper.DirectorioRaiz;
            }

            Dir += @"\TotalAppLog";

            if (!Directory.Exists(Dir))
            {
                Directory.CreateDirectory(Dir);
            }

            if (AddFechaHoraAlNombreDelArchivo)
            {
                NombreArchivo = NombreArchivo + DateTime.Now.ToString("yyyy_MM_dd_hh_mm_ss");
            }

            Dir += @"\" + NombreArchivo + sExt;

        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:Escribe directamente una linea en un archivo, por lo general .txt
        /// </summary>
        /// <param name="sDir">Ruta directa en donde se escribira el archivo - Ejemplo: C:\Log\Prueba.txt </param>
        /// <param name="sMensaje">Linea a escribir en el archivo</param>
        public void LogWriteDirect(string sDir, string sMensaje)
        {
            if (sDir.TIsNullOrEmpty()) THelperInfoEstatus.ArgumentNull(nameof(sDir));
            if (sMensaje.TIsNullOrEmpty()) THelperInfoEstatus.ArgumentNull(nameof(sMensaje));

            using (System.IO.FileStream oFs = new System.IO.FileStream(sDir, FileMode.OpenOrCreate | FileMode.Append, FileAccess.Write))
            {
                StreamWriter oWriter = new StreamWriter(oFs);
                oWriter.WriteLine(sMensaje);
                oWriter.Close();
                oFs.Close();
            }
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:Este metodo se ultiliza acumular trazas, para esto es necesario definir previamente las propiedades del archivo, como nombre y ruta
        /// si la ruta es vacia, automaticamente se establece una ruta por defecto que es la del directorio raiz
        /// </summary>
        /// <param name="sMensaje">Linea a escribir en el archivo</param>
        /// <param name="bIsAddFechaHora">Define si se agrega o no la fecha y la hora en el mensaje</param>
        /// <param name="bIsFinDelLog">Define si es o no fin de la traza para poder escribir los registros acumulados en el archivo</param>
        public void LogWrite(string sMensaje, bool bIsAddFechaHora = false, bool bIsFinDelLog = false)
        {
            if (bIsAddFechaHora)
            {
                sMensaje += " - " + DateTime.Now.ToString("yyyy_MM_dd_hh_mm_ss");
            }

            _oListMensaje.Add(sMensaje);

            if (bIsFinDelLog)
            {
                Validar();

                foreach (string sLinea in _oListMensaje)
                {
                    LogWriteDirect(Dir, sLinea);
                }

                _oListMensaje = new List<string>();
            }
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:Metodo que permite escribir directamente en un archivo sin la necesidad de establecer previamente las propiedades de esta clase
        /// Sino que se envian como parametros e internamente el metodo establece los valores necesarios para completar, crear o actualizar el archivo de traza o log
        /// </summary>
        /// <param name="sNombreArc">Parametro que define el nombre del archivo, solo el nombre, no especifica la extension</param>
        /// <param name="sMensaje">Linea a escribir en el archivo</param>
        /// <param name="bIsAddFechaHoraAlNombreDelArchivo">Parametro que define si se el agrega o no la fecha y hora al archivo</param>
        /// <param name="bIsAddFechaHoraAlMensaje">Parametro que define si se el agrega o no la fecha y hora al mensaje</param>
        /// <param name="sDir">Propiedad que define el directorio donde se almacenara el archivo - Ejemplo: C:\Log,  Si no se especifica, automaticamente se le agrega la ruta del directorio raiz de la app</param>
        /// <param name="sExt">Propiedad que define el tipo de extension del archivo - Ejemplo: .txt</param>
        public void LogWrite(string sNombreArc
                           , string sMensaje
                           , bool bIsAddFechaHoraAlNombreDelArchivo = false
                           , bool bIsAddFechaHoraAlMensaje = false
                           , string sDir = ""
                           , string sExt = ".txt")
        {

            NombreArchivo = sNombreArc;
            AddFechaHoraAlNombreDelArchivo = bIsAddFechaHoraAlNombreDelArchivo;
            Dir = sDir;
            Ext = sExt;

            if (bIsAddFechaHoraAlMensaje)
            {
                sMensaje += " - " + DateTime.Now.ToString("yyyy_MM_dd_hh_mm_ss");
            }

            Validar();
            LogWriteDirect(Dir, sMensaje);
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:Metodo que permite escribir directamente en un archivo sin la necesidad de establecer previamente las propiedades de esta clase
        /// Aplica unicamente a objetos definidos por el programador
        /// </summary>
        ///<param name="obj">Parametro que especifica el objeto que se imprime en el archivo</param>
        /// <param name="sNombreArc">Parametro que define el nombre del archivo, solo el nombre, no especifica la extension, si esta vacio, toma automaticamente el nombre del objeto</param>
        /// <param name="sMensaje">Linea a escribir en el archivo</param>
        /// <param name="bIsAddFechaHoraAlNombreDelArchivo">Parametro que define si se el agrega o no la fecha y hora al archivo</param>
        /// <param name="bIsAddFechaHoraAlMensaje">Parametro que define si se el agrega o no la fecha y hora al mensaje</param>
        /// <param name="sDir">Propiedad que define el directorio donde se almacenara el archivo - Ejemplo: C:\Log,  Si no se especifica, automaticamente se le agrega la ruta del directorio raiz de la app</param>
        /// <param name="sExt">Propiedad que define el tipo de extension del archivo - Ejemplo: .txt</param>
        public void LogWriteObject(object obj
                                , string sNombreArc
                                , string sMensaje
                                , bool bIsAddFechaHoraAlNombreDelArchivo = false
                                , bool bIsAddFechaHoraAlMensaje = false
                                , string sDir = ""
                                , string sExt = ".txt")
        {
            
            if (sNombreArc.TIsNullOrEmpty())
            {
                sNombreArc = "TotalApp";
            }

            LogWrite(sNombreArc
                   , THelperJson.TSerializeObjectOrDefault(obj)
                   , bIsAddFechaHoraAlNombreDelArchivo
                   , bIsAddFechaHoraAlMensaje
                   , sDir
                   , sExt);
        }
        #endregion


        #region "Eventos"
        #endregion

        #region "Destructor"
        /// <summary>
        /// Autor   : Larry Ramirez
        /// Fecha   : 21-05-2016
        /// Sinopsis: Variable Disposed, permite saber de manera general si ya fueron liberados los recursos no administrados a la clase (Objeto).
        /// </summary>
        private bool _Disposed = false;

        // <summary>
        /// Autor   : Larry Ramirez
        /// Fecha   : 25-05-2016
        /// Sinopsis: Metodo que permite Desechar y limpiar objetos de la memoria.
        /// </summary>
        public void Dispose()
        {

            Dispose(true);
            GC.SuppressFinalize(this);
        }

        // <summary>
        /// Autor   : Larry Ramirez
        /// Fecha   : 25-05-2016
        /// Sinopsis: Logica para limpiar objetos de la memoria.
        /// </summary>
        private void Dispose(bool bIsDisposing)
        {
            //Si esta en false, significa que se tiene la oportunidad de limpiar los objetos
            if (!this._Disposed)
            {
                //Aqui se definen los objetos que se liberan
                if (bIsDisposing)
                {

                }
            }
            _Disposed = true;
        }

        // <summary>
        /// Autor   : Larry Ramirez
        /// Fecha   : 25-05-2016
        /// Sinopsis: Destructor por defecto.
        /// </summary>
        ~TLog()
        {
            Dispose(false);
            GC.SuppressFinalize(this);

        }
        #endregion
    }
}
