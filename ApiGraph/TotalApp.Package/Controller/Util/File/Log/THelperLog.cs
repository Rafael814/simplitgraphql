﻿
namespace TotalApp.Package.UTIL
{
    /// <summary>
    /// *Autor:Larry Ramirez
    /// *Fecha de creacion:09-01-2018
    /// *Version:1.1.20180109
    /// *Sinopsis:Clase de tipo static, permite invocar directamente los metodos de la clase ITLog, sin la necesidad de instanciar dicha clase
    /// </summary>
    public static class THelperLog
    {
        #region "Constantes"
        #endregion

        #region "Variables"
        #endregion

        #region "Propiedades"
        #endregion

        #region "Funciones"

        #endregion

        #region "Metodos & constructores"
        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:Escribe directamente una linea en un archivo, por lo general .txt
        /// </summary>
        /// <param name="sDir">Ruta directa en donde se escribira el archivo - Ejemplo: C:\Log\Prueba.txt </param>
        /// <param name="sMensaje">Linea a escribir en el archivo</param>
        public static void LogWriteDirect(string sDir, string sMensaje)
        {
            using (ITLog oLog = new TLog())
            {
                oLog.LogWriteDirect(sDir
                                   , sMensaje);

                oLog.Dispose();
            }
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:Metodo que permite escribir directamente en un archivo sin la necesidad de establecer previamente las propiedades de esta clase
        /// Sino que se envian como parametros e internamente el metodo establece los valores necesarios para completar, crear o actualizar el archivo de traza o log
        /// </summary>
        /// <param name="sNombreArc">Parametro que define el nombre del archivo, solo el nombre, no especifica la extension</param>
        /// <param name="sMensaje">Linea a escribir en el archivo</param>
        /// <param name="bIsAddFechaHoraAlNombreDelArchivo">Parametro que define si se el agrega o no la fecha y hora al archivo</param>
        /// <param name="bIsAddFechaHoraAlMensaje">Parametro que define si se el agrega o no la fecha y hora al mensaje</param>
        /// <param name="sDir">Propiedad que define el directorio donde se almacenara el archivo - Ejemplo: C:\Log,  Si no se especifica, automaticamente se le agrega la ruta del directorio raiz de la app</param>
        /// <param name="sExt">Propiedad que define el tipo de extension del archivo - Ejemplo: .txt</param>
        public static void LogWrite(string sNombreArc
                                  , string sMensaje
                                  , bool bIsAddFechaHoraAlNombreDelArchivo = false
                                  , bool bIsAddFechaHoraAlMensaje = false
                                  , string sDir = ""
                                  , string sExt = ".txt")
        {
            using (ITLog oLog = new TLog())
            {
                oLog.LogWrite(sNombreArc
                            , sMensaje
                            , bIsAddFechaHoraAlNombreDelArchivo
                            , bIsAddFechaHoraAlMensaje
                            , sDir
                            , sExt);

                oLog.Dispose();
            }
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:Metodo que permite escribir directamente en un archivo sin la necesidad de establecer previamente las propiedades de esta clase
        /// Aplica unicamente a objetos definidos por el programador
        /// </summary>
        ///<param name="obj">Parametro que especifica el objeto que se imprime en el archivo</param>
        /// <param name="sNombreArc">Parametro que define el nombre del archivo, solo el nombre, no especifica la extension, si esta vacio, toma automaticamente el nombre del objeto</param>
        /// <param name="sMensaje">Linea a escribir en el archivo</param>
        /// <param name="bIsAddFechaHoraAlNombreDelArchivo">Parametro que define si se el agrega o no la fecha y hora al archivo</param>
        /// <param name="bIsAddFechaHoraAlMensaje">Parametro que define si se el agrega o no la fecha y hora al mensaje</param>
        /// <param name="sDir">Propiedad que define el directorio donde se almacenara el archivo - Ejemplo: C:\Log,  Si no se especifica, automaticamente se le agrega la ruta del directorio raiz de la app</param>
        /// <param name="sExt">Propiedad que define el tipo de extension del archivo - Ejemplo: .txt</param>
        public static void LogWriteObject(object obj
                                        , string sNombreArc
                                        , string sMensaje
                                        , bool bIsAddFechaHoraAlNombreDelArchivo = false
                                        , bool bIsAddFechaHoraAlMensaje = false
                                        , string sDir = ""
                                        , string sExt = ".txt")
        {

            using (ITLog oLog = new TLog())
            {
                oLog.LogWriteObject(obj
                                 , sNombreArc
                                 , sMensaje
                                 , bIsAddFechaHoraAlNombreDelArchivo
                                 , bIsAddFechaHoraAlMensaje
                                 , sDir
                                 , sExt);

                oLog.Dispose();
            }
        }
        #endregion

        #region "Eventos"
        #endregion

        #region "Destructor"
        #endregion
    }
}
