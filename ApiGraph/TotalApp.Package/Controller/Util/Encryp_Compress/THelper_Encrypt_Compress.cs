﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace TotalApp.Package.UTIL
{
    /// <summary>
    /// *Autor:Larry Ramirez
    /// *Fecha de creacion:09-01-2018
    /// *Version:1.1.20180109
    /// *Sinopsis:
    /// </summary>
    public static class THelper_Encrypt_Compress
    {
        #region "Constantes"
        #endregion

        #region "Variables"

        #region "Variables Lock"
        private static readonly object _oLock = new object();
        private static readonly object _oEncrypt = new object();
        private static readonly object _oDecrypting = new object();
        private static readonly object _oCompress = new object();
        private static readonly object _oDeCompress = new object();
        #endregion

        #endregion

        #region "Propiedades"
        #endregion

        #region "Funciones"

        #region "Encrypt"
        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sCadena"></param>
        /// <returns></returns>
        public static string TEncrypt(this string sCadena)
        {
            lock (_oEncrypt)
            {
                var oResp = THelper.Empty;
                using (var oTEncryp_Compress = new TEncrypt_Compress())
                {
                    oResp = oTEncryp_Compress.Encrypt(sCadena);
                    oTEncryp_Compress.Dispose();
                }

                return oResp;
            }
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sCadena"></param>
        /// <returns></returns>
        public static string TEncryptAsync(this string sCadena)
        {
            var oTask = new Task<string>(() => {
                return TEncrypt(sCadena);
            });
            oTask.Start();

            return oTask.Result;
        }

        

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static string TEncrypt_Object(this object obj)
        {
            lock (_oEncrypt)
            {
                var oResp = THelper.Empty;
                using (var oTEncryp_Compress = new TEncrypt_Compress())
                {
                    oResp = oTEncryp_Compress.Encrypt_Object(obj);
                    oTEncryp_Compress.Dispose();
                }

                return oResp;
            }
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static string TEncrypt_ObjectAsync(this object obj)
        {
            var oTask = new Task<string>(() => {
                return TEncrypt_Object(obj);
            });
            oTask.Start();

            return oTask.Result;
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sCadena"></param>
        /// <returns></returns>
        public static string TDecrypting(this string sCadena)
        {
            lock (_oDecrypting)
            {
                var oResp = THelper.Empty;
                using (var oTEncryp_Compress = new TEncrypt_Compress())
                {
                    oResp = oTEncryp_Compress.Decrypting(sCadena);
                    oTEncryp_Compress.Dispose();
                }

                return oResp;
            }

        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sCadena"></param>
        /// <returns></returns>
        public static string TDecryptingAsync(this string sCadena)
        {
            var oTask = new Task<string>(() => {
                return TDecrypting(sCadena);
            });
            oTask.Start();

            return oTask.Result;

        }
        #endregion

        #region "Compress"
        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sCadena"></param>
        /// <returns></returns>
        public static string TCompress(this string sCadena)
        {
            lock (_oCompress)
            {
                var oResp = THelper.Empty;
                using (var oTEncryp_Compress = new TEncrypt_Compress())
                {
                    oResp = oTEncryp_Compress.Compress(sCadena);
                    oTEncryp_Compress.Dispose();
                }

                return oResp;
            }

        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sCadena"></param>
        /// <returns></returns>
        public static string TCompressAsync(this string sCadena)
        {
            var oTask = new Task<string>(() => {
                return TCompress(sCadena);
            });
            oTask.Start();

            return oTask.Result;

        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sCadena"></param>
        /// <returns></returns>
        public static string TDeCompress(this string sCadena)
        {
            lock (_oDeCompress)
            {
                var oResp = THelper.Empty;
                using (var oTEncryp_Compress = new TEncrypt_Compress())
                {
                    oResp = oTEncryp_Compress.DeCompress(sCadena);
                    oTEncryp_Compress.Dispose();
                }
                return oResp;
            }

        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sCadena"></param>
        /// <returns></returns>
        public static string TDeCompressAsync(this string sCadena)
        {
            var oTask = new Task<string>(() => {
                return TDeCompress(sCadena);
            });
            oTask.Start();

            return oTask.Result;

        }
        #endregion

        #region "Encrypt & Compress"
        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static string TEncrypt_Compress_Object(this object obj)
        {
            lock (_oLock)
            {
                var oResp = THelper.Empty;
                using (var oTEncryp_Compress = new TEncrypt_Compress())
                {
                    oResp = oTEncryp_Compress.Encrypt_Compress_Object(obj);
                    oTEncryp_Compress.Dispose();
                }
                return oResp;
            }
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static string TEncrypt_Compress_ObjectAsync(this object obj)
        {
            var oTask = new Task<string>(() => {
                return TEncrypt_Compress_Object(obj);
            });
            oTask.Start();

            return oTask.Result;
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static string TCompress_Encrypt_Object(this object obj)
        {
            lock (_oLock)
            {
                var oResp = THelper.Empty;
                using (var oTEncryp_Compress = new TEncrypt_Compress())
                {
                    oResp = oTEncryp_Compress.Compress_Encrypt_Object(obj);
                    oTEncryp_Compress.Dispose();
                }
                return oResp;
            }
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static string TCompress_Encrypt_ObjectAsync(this object obj)
        {
            var oTask = new Task<string>(() => {
                return TCompress_Encrypt_Object(obj);
            });
            oTask.Start();

            return oTask.Result;
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sCadena"></param>
        /// <returns></returns>
        public static string TEncrypt_Compress(this string sCadena)
        {
            lock (_oLock)
            {
                var oResp = THelper.Empty;
                using (var oTEncryp_Compress = new TEncrypt_Compress())
                {
                    oResp = oTEncryp_Compress.Encrypt_Compress(sCadena);
                    oTEncryp_Compress.Dispose();
                }
                return oResp;
            }
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sCadena"></param>
        /// <returns></returns>
        public static string TEncrypt_CompressAsync(this string sCadena)
        {
            var oTask = new Task<string>(() => {
                return TEncrypt_Compress(sCadena);
            });
            oTask.Start();

            return oTask.Result;
        }


        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sCadena"></param>
        /// <returns></returns>
        public static string TCompress_Encrypt(this string sCadena)
        {
            lock (_oLock)
            {
                var oResp = THelper.Empty;
                using (var oTEncryp_Compress = new TEncrypt_Compress())
                {
                    oResp = oTEncryp_Compress.Compress_Encrypt(sCadena);
                    oTEncryp_Compress.Dispose();
                }
                return oResp;
            }
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sCadena"></param>
        /// <returns></returns>
        public static string TCompress_EncryptAsync(this string sCadena)
        {
            var oTask = new Task<string>(() => {
                return TCompress_Encrypt(sCadena);
            });
            oTask.Start();

            return oTask.Result;
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sCadena"></param>
        /// <returns></returns>
        public static string TDesEncrypt_DeCompress(this string sCadena)
        {
            lock (_oLock)
            {
                var oResp = THelper.Empty;
                using (var oTEncryp_Compress = new TEncrypt_Compress())
                {
                    oResp = oTEncryp_Compress.DesEncrypt_DeCompress(sCadena);
                    oTEncryp_Compress.Dispose();
                }
                return oResp;
            }
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sCadena"></param>
        /// <returns></returns>
        public static string TDesEncrypt_DeCompressAsync(this string sCadena)
        {
            var oTask = new Task<string>(() => {
                return TDesEncrypt_DeCompress(sCadena);
            });
            oTask.Start();

            return oTask.Result;
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sCadena"></param>
        /// <returns></returns>
        public static string TDeCompress_DeEncrypt(this string sCadena)
        {
            lock (_oLock)
            {
                var oResp = THelper.Empty;
                using (var oTEncryp_Compress = new TEncrypt_Compress())
                {
                    oResp = oTEncryp_Compress.DeCompress_DeEncrypt(sCadena);
                    oTEncryp_Compress.Dispose();
                }
                return oResp;
            }
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sCadena"></param>
        /// <returns></returns>
        public static string TDeCompress_DeEncryptAsync(this string sCadena)
        {
            var oTask = new Task<string>(() => {
                return TDeCompress_DeEncrypt(sCadena);
            });
            oTask.Start();

            return oTask.Result;
        }
        #endregion

        #region "Objectos genericos"
        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="sCadena"></param>
        /// <param name="bIsDecrypting"></param>
        /// <param name="bIsDecompressData"></param>
        /// <returns></returns>
        public static T TReturnObjectOrDefault<T>(this string sCadena
                                                , bool bIsDecrypting = true
                                                , bool bIsDecompressData = true) where T : class
        {
            lock (_oLock)
            {
                T obj = default(T);
                using (var oTEncryp_Compress = new TEncrypt_Compress())
                {
                    obj = oTEncryp_Compress.ReturnObjectOrDefault<T>(sCadena, bIsDecrypting, bIsDecompressData);
                    oTEncryp_Compress.Dispose();
                }

                return obj;
            }

        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="sCadena"></param>
        /// <param name="bIsDecrypting"></param>
        /// <param name="bIsDecompressData"></param>
        /// <returns></returns>
        public static T TReturnObjectOrDefaultAsync<T>(this string sCadena
                                                    , bool bIsDecrypting = true
                                                    , bool bIsDecompressData = true) where T : class
        {

            var oTask = new Task<T>(() => {
                return TReturnObjectOrDefault<T>(sCadena, bIsDecrypting, bIsDecompressData);
            });
            oTask.Start();

            return oTask.Result;
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="sCadena"></param>
        /// <param name="bIsDecrypting"></param>
        /// <param name="bIsDecompressData"></param>
        /// <returns></returns>
        public static List<T> TReturnListObjectOrDefault<T>(this string sCadena
                                                           , bool bIsDecrypting = true
                                                           , bool bIsDecompressData = true) where T : class
        {
            lock (_oLock)
            {
                List<T> obj = default(List<T>);
                using (var oTEncryp_Compress = new TEncrypt_Compress())
                {
                    obj = oTEncryp_Compress.ReturnListObjectOrDefault<T>(sCadena, bIsDecrypting, bIsDecompressData);
                    oTEncryp_Compress.Dispose();
                }

                return obj;
            }

        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="sCadena"></param>
        /// <param name="bIsDecrypting"></param>
        /// <param name="bIsDecompressData"></param>
        /// <returns></returns>
        public static List<T> TReturnListObjectOrDefaultAsync<T>(this string sCadena
                                                               , bool bIsDecrypting = true
                                                               , bool bIsDecompressData = true) where T : class
        {
            var oTask = new Task<List<T>>(() => {
                return TReturnListObjectOrDefault<T>(sCadena, bIsDecrypting, bIsDecompressData);
            });
            oTask.Start();

            return oTask.Result;

        }
        #endregion

        #endregion

        #region "Metodos & constructores"
        #endregion

        #region "Eventos"
        #endregion

        #region "Destructor"
        #endregion
    }
}
