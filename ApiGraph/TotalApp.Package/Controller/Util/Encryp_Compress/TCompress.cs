﻿using System;
using System.Text;
using System.IO;
using System.IO.Compression;

namespace TotalApp.Package.UTIL
{
    /// <summary>
    /// *Autor:Larry Ramirez
    /// *Fecha de creacion:09-01-2018
    /// *Version:1.1.20180109
    /// *Sinopsis:
    /// </summary>
    public partial class TEncrypt_Compress: IDisposable
    {
        #region "Compress"

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sCadena"></param>
        /// <returns></returns>
        public virtual string Compress(string sCadena)
        {
            if (sCadena.TIsNullOrEmpty()) THelperInfoEstatus.ArgumentNull(nameof(sCadena));

            MemoryStream oMs = new MemoryStream();
            Byte[] byCompressedData = null;
            try
            {
                Byte[] byData = Encoding.UTF8.GetBytes(sCadena);

                Stream stzipStream = new GZipStream(oMs, CompressionMode.Compress, true);

                stzipStream.Write(byData, 0, byData.Length);
                stzipStream.Flush();
                stzipStream.Dispose();
                stzipStream.Close();
                stzipStream = null;

                oMs.Position = 0;
                byCompressedData = new Byte[oMs.Length + 1];
                oMs.Read(byCompressedData, 0, (Int32)oMs.Length);
            }
            catch (Exception ex)
            {
                byCompressedData = null;
            }
            finally
            {
                oMs.Flush();
                oMs.Dispose();
                oMs.Close();
                oMs = null;
            }

            return Convert.ToBase64String(byCompressedData);
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sCadena"></param>
        /// <returns></returns>
        public virtual string DeCompress(string sCadena)
        {
            if (sCadena.TIsNullOrEmpty()) THelperInfoEstatus.ArgumentNull(nameof(sCadena));

            MemoryStream oMs = new MemoryStream();
            Byte[] byDcData = null;
            try
            {

                oMs = new MemoryStream(Convert.FromBase64String(sCadena));
                Stream stzipStream = new GZipStream(oMs, CompressionMode.Decompress);

                oMs = new MemoryStream();
                stzipStream.CopyTo(oMs);
                byDcData = oMs.ToArray();

                stzipStream.Flush();
                stzipStream.Dispose();
                stzipStream.Close();
                stzipStream = null;
            }
            catch (Exception ex)
            {
                byDcData = null;
            }
            finally
            {

                oMs.Flush();
                oMs.Dispose();
                oMs.Close();
                oMs = null;
            }

            return Encoding.UTF8.GetString(byDcData, 0, byDcData.Length);

        }
        #endregion


    }
}