﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace TotalApp.Package.UTIL
{
    /// <summary>
    /// *Autor:Larry Ramirez
    /// *Fecha de creacion:09-01-2018
    /// *Version:1.1.20180109
    /// *Sinopsis:Clase que permite encriptar, comprimir, desencriptar y descomprimir cadenas y objetos
    /// </summary> 
    [DataContract]
    public partial class TEncrypt_Compress: IDisposable
    {
        #region "Constantes"
        #endregion

        #region "Variables"
        private string _sCadena = THelper.Empty;
        #endregion

        #region "Propiedades"
        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        public virtual string Cadena
        {
            get { return this._sCadena; }
            set { this._sCadena = value; }
        }
        #endregion

        #region "Funciones"

        #region "Encrypt & Compress"
        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public virtual string Compress_Object(object obj) => Compress(obj.TSerializeObjectOrDefault());

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public virtual string Encrypt_Object(object obj) => Encrypt(obj.TSerializeObjectOrDefault());

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sCadena"></param>
        /// <returns></returns>
        public virtual string Encrypt_Compress(string sCadena) => Encrypt(Compress(sCadena));

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public virtual string Encrypt_Compress_Object(object obj) => Encrypt(Compress(obj.TSerializeObjectOrDefault()));

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sCadena"></param>
        /// <returns></returns>
        public virtual string Compress_Encrypt(string sCadena) => Compress(Encrypt(sCadena));

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public virtual string Compress_Encrypt_Object(object obj) => Compress(Encrypt(obj.TSerializeObjectOrDefault()));

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sCadena"></param>
        /// <returns></returns>
        public virtual string DesEncrypt_DeCompress(string sCadena) => Decrypting(DeCompress(sCadena));

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sCadena"></param>
        /// <returns></returns>
        public virtual string DeCompress_DeEncrypt(string sCadena) => DeCompress(Decrypting(sCadena));

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="bIsDecrypting"></param>
        /// <param name="bIsDecompressData"></param>
        /// <returns></returns>
        public virtual string Encrypt_Compress_Object(object obj
                                                     , bool bIsEcrypting = true
                                                     , bool bIsCompressData = true)
        {
            string sCadena = obj.TSerializeObjectOrDefault();

            if (bIsCompressData)
            {
                sCadena = Compress(sCadena);
            }

            if (bIsEcrypting)
            {
                sCadena = Encrypt(sCadena);
            }

            return sCadena;
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sCadena"></param>
        /// <param name="bIsDecrypting"></param>
        /// <param name="bIsDecompressData"></param>
        /// <returns></returns>
        public virtual string Encrypt_Compress_Object(string sCadena
                                                     , bool bIsEcrypting = true
                                                     , bool bIsCompressData = true)
        {

            if (bIsCompressData)
            {
                sCadena = Compress(sCadena);
            }

            if (bIsEcrypting)
            {
                sCadena = Encrypt(sCadena);
            }

            return sCadena;
        }

        #endregion

        #region "Objectos genericos"
        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="sCadena"></param>
        /// <param name="bIsDecrypting"></param>
        /// <param name="bIsDecompressData"></param>
        /// <returns></returns>
        public virtual T ReturnObjectOrDefault<T>(string sCadena
                                                , bool bIsDecrypting = true
                                                , bool bIsDecompressData = true) where T : class
        {

            if (bIsDecrypting)
            {
                sCadena = Decrypting(sCadena);
            }

            if (bIsDecompressData)
            {
                sCadena = DeCompress(sCadena);
            }

            return sCadena.TDeserializeObjectOrDefault<T>();

        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="sCadena"></param>
        /// <param name="bIsDecrypting"></param>
        /// <param name="bIsDecompressData"></param>
        /// <returns></returns>
        public virtual List<T> ReturnListObjectOrDefault<T>(string sCadena
                                                          , bool bIsDecrypting = true
                                                          , bool bIsDecompressData = true) where T : class
        {
            if (bIsDecrypting)
            {
                sCadena = Decrypting(sCadena);
            }

            if (bIsDecompressData)
            {
                sCadena = DeCompress(sCadena);
            }

            return sCadena.TDeserializeListObjectOrDefault<T>();

        }
        #endregion

        #endregion

        #region "Metodos"
        // <summary>
        /// Autor   : Larry Ramirez
        /// Fecha   : 25-05-2016
        /// Sinopsis: Constructor por defecto.
        /// </summary>
        public TEncrypt_Compress()
        {

        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sCadena"></param>
        /// <param name="bIsDecrypting"></param>
        /// <param name="bIsDecompressData"></param>
        public TEncrypt_Compress(string sCadena
                               , bool bIsDecrypting = true
                               , bool bIsDecompressData = true)
        {

            if (bIsDecompressData)
            {
                Cadena = DeCompress(sCadena);
            }

            if (bIsDecrypting)
            {
                Cadena = Decrypting(sCadena);
            }
        }


        #endregion

        #region "Eventos"
        #endregion

        #region "Destructor"
        /// <summary>
        /// Autor   : Larry Ramirez
        /// Fecha   : 21-05-2016
        /// Sinopsis: Variable Disposed, permite saber de manera general si ya fueron liberados los recursos no administrados a la clase (Objeto).
        /// </summary>
        private bool _Disposed = false;

        // <summary>
        /// Autor   : Larry Ramirez
        /// Fecha   : 25-05-2016
        /// Sinopsis: Metodo que permite Desechar y limpiar objetos.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
            //TAPSync.Package.Util.FreeMemory.FlushMemory();

        }

        // <summary>
        /// Autor   : Larry Ramirez
        /// Fecha   : 25-05-2016
        /// Sinopsis: Logica para limpiar objetos de la memoria.
        /// </summary>
        protected void Dispose(bool bIsDisposing)
        {
            //Si esta en false, significa que se tiene la oportunidad de limpiar los objetos
            if (!this._Disposed)
            {
                //Aqui se definen los objetos que se liberan
                if (bIsDisposing)
                {

                }
            }
            _Disposed = true;
        }

        // <summary>
        /// Autor   : Larry Ramirez
        /// Fecha   : 25-05-2016
        /// Sinopsis: Destructor por defecto.
        /// </summary>
        ~TEncrypt_Compress()
        {
            Dispose(false);
            GC.SuppressFinalize(this);

        }
        #endregion


    } 
}