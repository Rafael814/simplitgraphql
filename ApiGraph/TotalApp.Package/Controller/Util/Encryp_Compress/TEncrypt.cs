﻿using System;
using System.Text;
using System.Security.Cryptography;

namespace TotalApp.Package.UTIL
{
    /// <summary>
    /// *Autor:Larry Ramirez
    /// *Fecha de creacion:09-01-2018
    /// *Version:1.1.20180109
    /// *Sinopsis:
    /// </summary>
    public partial class TEncrypt_Compress: IDisposable
    {
        #region "Encrypt"

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sCadena"></param>
        /// <returns></returns>
        private string EncryptKey(string sCadena)
        {
            if (sCadena.TIsNullOrEmpty()) THelperInfoEstatus.ArgumentNull(nameof(sCadena));

            Random oRandom = new Random();

            //Se utilizan las clases de encriptación MD5
            MD5CryptoServiceProvider oHashmd5 = new MD5CryptoServiceProvider();

            //Algoritmo TripleDES
            TripleDESCryptoServiceProvider oTDes = new TripleDESCryptoServiceProvider();
            try
            {

                string sKey = oRandom.Next(0x10000000).ToString(); //llave para encriptar datos

                byte[] byKeyArray;

                byte[] byArray = UTF8Encoding.UTF8.GetBytes(sCadena);

                byKeyArray = oHashmd5.ComputeHash(UTF8Encoding.UTF8.GetBytes(sKey));

                oHashmd5.Clear();

                oTDes.Key = byKeyArray;
                oTDes.Mode = CipherMode.ECB;
                oTDes.Padding = PaddingMode.PKCS7;

                ICryptoTransform oCTransform = oTDes.CreateEncryptor();

                byte[] byArrayResul = oCTransform.TransformFinalBlock(byArray, 0, byArray.Length);

                oTDes.Clear();

                //se regresa el resultado en forma de una cadena
                sCadena = Convert.ToBase64String(byArrayResul, 0, byArrayResul.Length);

            }
            catch (Exception)
            {
                sCadena = THelper.Empty;
            }
            finally
            {

                oRandom = null;
                oTDes = null;
                oHashmd5 = null;
            }

            return sCadena;

        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sCadena"></param>
        /// <param name="sKey"></param>
        /// <returns></returns>
        private string DecryptingKey(string sCadena
                                   , string sKey)
        {
            if (sCadena.TIsNullOrEmpty()) THelperInfoEstatus.ArgumentNull(nameof(sCadena));
            if (sKey.TIsNullOrEmpty()) THelperInfoEstatus.ArgumentNull(nameof(sKey));

            //algoritmo MD5
            MD5CryptoServiceProvider oHashmd5 = new MD5CryptoServiceProvider();
            TripleDESCryptoServiceProvider oTDes = new TripleDESCryptoServiceProvider();
            try
            {

                byte[] byKeyArray;
                byte[] byArray = Convert.FromBase64String(sCadena);


                byKeyArray = oHashmd5.ComputeHash(UTF8Encoding.UTF8.GetBytes(sKey));

                oHashmd5.Clear();

                oTDes.Key = byKeyArray;
                oTDes.Mode = CipherMode.ECB;
                oTDes.Padding = PaddingMode.PKCS7;

                ICryptoTransform oCTransform = oTDes.CreateDecryptor();

                byte[] byArrayResul = oCTransform.TransformFinalBlock(byArray, 0, byArray.Length);

                oTDes.Clear();
                sCadena = UTF8Encoding.UTF8.GetString(byArrayResul);

            }
            catch (Exception ex)
            {

                sCadena = THelper.Empty;
            }
            finally
            {
                oTDes = null;
                oHashmd5 = null;
            }
            return sCadena;

        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sCadena"></param>
        /// <returns></returns>
        public virtual string Encrypt(string sCadena)
        {
            if (sCadena.TIsNullOrEmpty()) THelperInfoEstatus.ArgumentNull(nameof(sCadena));

            //Se utilizan las clases de encriptación MD5
            MD5CryptoServiceProvider oHashmd5 = new MD5CryptoServiceProvider();

            //Algoritmo TripleDES
            TripleDESCryptoServiceProvider oTDes = new TripleDESCryptoServiceProvider();
            try
            {

                string sKey = EncryptKey(Guid.NewGuid().ToString()); //llave para encriptar datos

                byte[] byKeyArray;

                byte[] byArray = UTF8Encoding.UTF8.GetBytes(sCadena);

                byKeyArray = oHashmd5.ComputeHash(UTF8Encoding.UTF8.GetBytes(sKey));

                oHashmd5.Clear();

                oTDes.Key = byKeyArray;
                oTDes.Mode = CipherMode.ECB;
                oTDes.Padding = PaddingMode.PKCS7;

                ICryptoTransform oCTransform = oTDes.CreateEncryptor();

                byte[] byArrayResul = oCTransform.TransformFinalBlock(byArray, 0, byArray.Length);

                oTDes.Clear();

                //se regresa el resultado en forma de una cadena
                sCadena = Convert.ToBase64String(byArrayResul, 0, byArrayResul.Length);

                sCadena = sCadena + ";" + sKey;

                sCadena = sCadena.Replace("=", "^");
                sCadena = sCadena.Replace("+", "!");
            }
            catch (Exception)
            {

                sCadena = THelper.Empty;
            }
            finally
            {
                oTDes = null;
                oHashmd5 = null;
            }
            return sCadena;
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sCadena"></param>
        /// <returns></returns>
        public virtual string Decrypting(string sCadena)
        {
            if (sCadena.TIsNullOrEmpty()) THelperInfoEstatus.ArgumentNull(nameof(sCadena));

            char[] cDelimiterChars = { ';' };
            sCadena = sCadena.Replace("^", "=");
            sCadena = sCadena.Replace("!", "+");
            string[] sKey = sCadena.Split(cDelimiterChars);
            sCadena = DecryptingKey(sKey[0], sKey[1]);

            return sCadena;

        }
        #endregion


    }
}