﻿using System;

namespace TotalApp.Package
{
    /// <summary>
    /// *Autor:Larry Ramirez
    /// *Fecha de creacion:09-01-2018
    /// *Version:1.1.20180109
    /// *Sinopsis:
    /// </summary>
    public static partial class THelperInfoEstatus
    {
        #region "Funciones"

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <returns></returns>
        public static Exception ArgumentException() => throw new ArgumentException();

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <returns></returns>
        public static Exception ArgumentNull() => throw new ArgumentNullException();

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <returns></returns>
        public static Exception ArgumentOutOfRange() => throw new ArgumentOutOfRangeException();


        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <returns></returns>
        public static Exception MoreThanOneElement() => throw new InvalidOperationException();

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <returns></returns>
        public static Exception MoreThanOneMatch() => throw new InvalidOperationException();

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <returns></returns>
        public static Exception NoElements() => throw new InvalidOperationException();

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <returns></returns>
        public static Exception NoMatch() => throw new InvalidOperationException();

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <returns></returns>
        public static Exception NotSupported() => throw new NotSupportedException();

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <returns></returns>
        public static Exception NotImplemented() => throw new NotImplementedException();

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sMensaje"></param>
        /// <returns></returns>
        public static Exception ArgumentException(string sMensaje) => throw new ArgumentException(sMensaje);

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sMensaje"></param>
        /// <returns></returns>
        public static Exception ArgumentNull(string sMensaje) => throw new ArgumentNullException(sMensaje);

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sMensaje"></param>
        /// <returns></returns>
        public static Exception ArgumentOutOfRange(string sMensaje) => throw new ArgumentOutOfRangeException(sMensaje);

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sMensaje"></param>
        /// <returns></returns>
        public static Exception MoreThanOneElement(string sMensaje) => throw new InvalidOperationException(sMensaje);

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sMensaje"></param>
        /// <returns></returns>
        public static Exception MoreThanOneMatch(string sMensaje) => throw new InvalidOperationException(sMensaje);

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sMensaje"></param>
        /// <returns></returns>
        public static Exception NoElements(string sMensaje) => throw new InvalidOperationException(sMensaje);

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sMensaje"></param>
        /// <returns></returns>
        public static Exception NoMatch(string sMensaje) => throw new InvalidOperationException(sMensaje);

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sMensaje"></param>
        /// <returns></returns>
        public static Exception NotSupported(string sMensaje) => throw new NotSupportedException(sMensaje);

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sMensaje"></param>
        /// <returns></returns>
        public static Exception NotImplemented(string sMensaje) => throw new NotImplementedException(sMensaje);

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sMensaje"></param>
        /// <returns></returns>
        public static Exception TException(string sMensaje) => throw new Exception(sMensaje);

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <returns></returns>
        public static Exception ExInternalServerError() => throw new Exception();

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sMensaje"></param>
        /// <returns></returns>
        public static Exception ExInternalServerError(string sMensaje) => throw new Exception(sMensaje);

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <returns></returns>
        public static Exception ExNotFound() => throw new Exception();

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sMensaje"></param>
        /// <returns></returns>
        public static Exception ExNotFound(string sMensaje) => throw new Exception(sMensaje);


        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static Exception ToException(this object obj)
        {
            return new Exception(obj.ToString());
        }
        #endregion
    }
}
