﻿using System;
using System.Net;
using TotalApp.Package.DTO;

namespace TotalApp.Package
{
    /// <summary>
    /// *Autor:Larry Ramirez
    /// *Fecha de creacion:09-01-2018
    /// *Version:1.1.20180109
    /// *Sinopsis:
    /// </summary>
    public static partial class THelperInfoEstatus
    {
        #region "Funciones"

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sMensaje"></param>
        /// <returns></returns>
        public static TInfoEstatus_DTO ErrorObjEmpty(string sMensaje) => new TInfoEstatus_DTO(1, sMensaje, HttpStatusCode.NotFound);

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="nError"></param>
        /// <param name="sMensaje"></param>
        /// <returns></returns>
        public static TInfoEstatus_DTO ErrorObjEmpty(int nError
                                                   , string sMensaje
                                                   , HttpStatusCode oStatusCode = HttpStatusCode.NotFound
                                                   , TTypeError oErrType = TTypeError.Err_ObjEmpty
                                                   , string sMensajeDev = THelper.Empty) => new TInfoEstatus_DTO(nError
                                                                                                               , sMensaje
                                                                                                               , oStatusCode
                                                                                                               , oErrType
                                                                                                               , sMensajeDev);

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sMensaje"></param>
        /// <param name="nError"></param>
        /// <returns></returns>
        public static TInfoEstatus_DTO ErrorObjEmpty(string sMensaje
                                                   , int nError
                                                   , HttpStatusCode oStatusCode = HttpStatusCode.NotFound
                                                   , TTypeError oErrType = TTypeError.Err_ObjEmpty
                                                   , string sMensajeDev = THelper.Empty) => new TInfoEstatus_DTO(nError
                                                                                                               , sMensaje
                                                                                                               , oStatusCode
                                                                                                               , oErrType
                                                                                                               , sMensajeDev);


        #endregion
    }
}
