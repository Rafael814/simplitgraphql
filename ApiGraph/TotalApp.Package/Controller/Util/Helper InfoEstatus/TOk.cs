﻿using System;
using System.Net;
using TotalApp.Package.DTO;

namespace TotalApp.Package
{
    /// <summary>
    /// *Autor:Larry Ramirez
    /// *Fecha de creacion:09-01-2018
    /// *Version:1.1.20180109
    /// *Sinopsis:
    /// </summary>
    public static partial class THelperInfoEstatus
    {
        #region "Funciones"
        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <returns></returns>
        public static TInfoEstatus_DTO OK() => new TInfoEstatus_DTO(0, "", HttpStatusCode.OK, TTypeError.Err_NA, THelper.Empty);

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sMensaje"></param>
        /// <returns></returns>
        public static TInfoEstatus_DTO OK(string sMensaje) => new TInfoEstatus_DTO(0, sMensaje, HttpStatusCode.OK, TTypeError.Err_NA, THelper.Empty);

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <returns></returns>
        public static TInfoEstatus_DTO Accepted() => new TInfoEstatus_DTO(0, "", HttpStatusCode.Accepted);

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <returns></returns>
        public static TInfoEstatus_DTO Created() => new TInfoEstatus_DTO(0, "", HttpStatusCode.Created);

        #endregion
    }
}
