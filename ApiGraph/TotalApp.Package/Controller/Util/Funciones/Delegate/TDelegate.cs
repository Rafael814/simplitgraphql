﻿namespace TotalApp.Package
{
    /// <summary>
    /// *Autor:Larry Ramirez
    /// *Fecha de creacion:09-01-2018
    /// *Version:1.1.20180109
    /// *Sinopsis:
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="oFunc"></param>
    /// <returns></returns>
    public delegate bool TDelFunc<T>(T oFunc);


    /// <summary>
    /// *Autor:Larry Ramirez
    /// *Fecha de creacion:09-01-2018
    /// *Version:1.1.20180109
    /// *Sinopsis:
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="oFunc"></param>
    public delegate void TDelVoid<T>(T oFunc);

}
