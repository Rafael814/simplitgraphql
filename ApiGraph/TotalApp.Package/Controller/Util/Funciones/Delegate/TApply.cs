﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace TotalApp.Package
{
    /// <summary>
    /// *Autor:Larry Ramirez
    /// *Fecha de creacion:09-01-2018
    /// *Version:1.1.20180109
    /// *Sinopsis:
    /// </summary>
    public static partial class THelper
    {
        #region "Constantes"
        #endregion

        #region "Delegados"
        
        #endregion

        #region "Variables"
        #endregion

        #region "Propiedades"
        #endregion

        #region "Funciones"
        #endregion

        #region "Metodos & constructores"
        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="oMet"></param>
        /// <param name="oParam"></param>
        public static void TMethod(Action<object[]> oMet, params object[] oParam)
        {
            oMet(oParam);
        }


        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T1"></typeparam>
        /// <typeparam name="T2"></typeparam>
        /// <param name="oMet"></param>
        /// <param name="oParam1"></param>
        /// <param name="oParam2"></param>
        public static void TMethod<T1, T2>(Action<T1, T2> oMet, T1 oParam1, T2 oParam2)
        {
            oMet(oParam1, oParam2);
        }


        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="oData"></param>
        /// <param name="oDel"></param>
        public static void TMethod<T>(this IEnumerable<T> oData
                                     , TDelVoid<T> oDel)
        {
            foreach (var oItem in oData)
            {
                oDel(oItem);
            }
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="oData"></param>
        /// <param name="oDel"></param>
        public static void TMethod<T>(this T oData
                                    , TDelVoid<T> oDel)
        {
            oDel(oData);
        }



        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="oMet"></param>
        public static void TParallelMethod(params Action[] oMet)
        {
            Parallel.Invoke(oMet);
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="oMet"></param>
        public static void TParallelMethod<T>(params Action[] oMet)
        {
            Parallel.Invoke(oMet);
        }
        #endregion

        #region "Eventos"
        #endregion

        #region "Destructor"
        #endregion
    }
}
