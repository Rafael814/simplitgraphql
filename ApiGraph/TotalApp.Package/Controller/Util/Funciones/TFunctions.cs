﻿using System;
using System.IO;

namespace TotalApp.Package
{
    /// <summary>
    /// *Autor:Larry Ramirez
    /// *Fecha de creacion:09-01-2018
    /// *Version:1.1.20180109
    /// *Sinopsis:
    /// </summary>
    public static partial class THelper
    {
        #region "Constantes"
        #endregion

        #region "Variables"

        #region "Conexion BD"
        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        public static DTO.TDatosConex_DTO DatosConex_DTO = new DTO.TDatosConex_DTO();
        #endregion


        #region "Variables Lock"
        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        private static readonly object _oObjectLock = new object();
        #endregion

        #endregion

        #region "Propiedades"
        #endregion

        #region "Funciones"
        

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sSeparador"></param>
        /// <returns></returns>
        public static string FechaHora(string sSeparador = "")
        {
            if (sSeparador.TIsNullOrEmpty())
            {
                return DateTime.Now.ToString("dd") + "-"
                     + DateTime.Now.ToString("MM") + "-"
                     + DateTime.Now.ToString("yyyy") + " "
                     + DateTime.Now.ToString("hh:mm tt");
            }
            else
            {
                return DateTime.Now.ToString("dd") + sSeparador
                     + DateTime.Now.ToString("MM") + sSeparador
                     + DateTime.Now.ToString("yyyy") + sSeparador
                     + DateTime.Now.ToString("hh:mm tt");
            }

        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="bIsNewGuid"></param>
        /// <returns></returns>
        public static string Random(bool bIsNewGuid = false)
        {
            lock (_oObjectLock)
            {
                string sRetorno = THelper.Empty;

                if (bIsNewGuid)
                {
                    sRetorno = Guid.NewGuid().ToString();
                    sRetorno = sRetorno.Replace("-", "").ToUpper().Substring(0, 15);
                }

                Random oRandom = new Random();
                sRetorno += oRandom.Next(0x19999999).ToString();

                return sRetorno;
            }

        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180220
        /// *Sinopsis:Permite leer el string de conexion de un archivo .UDL
        /// Comentario - Larry Ramirez:(Version:1.1.20180109) Se modifico el codigo para que hiciera la lectura del archivo invocando a la clase File
        /// ver el codigo: #LR-Num:1-DatosUdl
        /// Comentario - Larry Ramirez:(Version:1.1.20180110) Se altero el codigo para que pudiera tomar el directorio raiz
        /// ver el codigo: #LR-Num:2-DatosUdl
        /// </summary>
        /// <param name="sRuta">Parametro opcional, si no se envia un valor, el tomara por defecto la ruta del directorio raiz</param>
        /// <returns></returns>
        public static string[] DatosUdl(string sRuta = Empty)
        {
            string sServidor = THelper.Empty;
            string sBD = THelper.Empty;
            string sUsuario = THelper.Empty;
            string sClave = THelper.Empty;
          

            try
            {
                //#LR-Num:2-DatosUdl
                if (sRuta.TIsNullOrEmpty())
                {
                    sRuta = FilePath("Conex.udl");
                }

                string[] sText = null;

                using (UTIL.ITFile oFile = new UTIL.TFile())
                {
                    sText = oFile.FileReadArray(sRuta);
                    oFile.Dispose();
                }

                if (!sText.TIsNullOrEmpty())
                {
                    if (sText.Length > 0)
                    {
                        string[] sCadena = sText[sText.Length - 1].Split(';');
                        sServidor = sCadena[5].Split('=')[1].ToString();
                        sBD = sCadena[4].Split('=')[1].ToString();
                        sUsuario = sCadena[3].Split('=')[1].ToString();
                        sClave = sCadena[1].Split('=')[1].ToString();
                       
                    }

                }

                /*
                 *#LR-Num:1-DatosUdl
                using (FileStream oFs = new FileStream(sRuta, FileMode.Open, FileAccess.Read))
                {
                    StreamReader oSr = new StreamReader(oFs);
                    string sText = "";
                    while (oSr.Peek() > -1)
                    {
                        sText = oSr.ReadLine();
                    }

                    string[] sCadena = sText.Split(';');

                    sServidor = sCadena[5].Split('=')[1].ToString();
                    sUsuario = sCadena[3].Split('=')[1].ToString();
                    sClave = sCadena[1].Split('=')[1].ToString();
                    sBD = sCadena[4].Split('=')[1].ToString();

                    oSr.Close();
                    oFs.Close();
                }
                */
            }
            catch
            {
                sServidor = "0";
                sBD = "0";
                sUsuario = "0";
                sClave = "0";
                
            }

            return new string[] { sServidor
                                , sBD
                                , sUsuario
                                , sClave
                                };

        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:19-02-2018
        /// *Version:1.0.20180219
        /// *Sinopsis:Permite devolver el string de conexcion de un archivo .UDL
        /// </summary>
        /// <param name="sRuta"></param>
        /// <returns></returns>
        public static string ConexDatosUdl(string sRuta = Empty)
        {
            var oArray = DatosUdl(sRuta);

            return CnxStringSqlServer(oArray[0]
                                    , oArray[1]
                                    , oArray[2]
                                    , oArray[3]);

        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:20-02-2018
        /// *Version:1.0.20180219
        /// *Sinopsis:Metodo que permite setear la propiedad DTO de datos de conexion a una BD 
        /// </summary>
        /// <param name="sRuta"></param>
        public static void DatosConexUDL(string sRuta = THelper.Empty)
        {
            var oArray = DatosUdl(sRuta);

           

            if (!oArray.TIsNull())
            {
                DatosConex_DTO = new DTO.TDatosConex_DTO(oArray[0]
                                                       , oArray[1]
                                                       , oArray[2]
                                                       , oArray[3]);

               
            }
           
        }


        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="oIntervalType"></param>
        /// <param name="dtFechaDesde"></param>
        /// <param name="dtFechaHasta"></param>
        /// <returns></returns>
        public static int TDateDiff(TDateInterval oIntervalType
                                 , System.DateTime dtFechaDesde
                                 , System.DateTime dtFechaHasta = default(System.DateTime))
        {
            int nResult = 0;
            try
            {
                switch (oIntervalType)
                {
                    case TDateInterval.Day:
                    case TDateInterval.DayOfYear: //Calcular dias y dias del año
                        System.TimeSpan tsSpanForDays = dtFechaHasta - dtFechaDesde;
                        nResult = (int)tsSpanForDays.TotalDays;
                        //Tambien se puede calcular asi
                        //System.Data.Linq.SqlClient.SqlMethods.DateDiffDay;
                        break;
                    case TDateInterval.Hour: //Calcular horas
                        System.TimeSpan tsSpanForHours = dtFechaHasta - dtFechaDesde;
                        nResult = (int)tsSpanForHours.TotalHours;
                        break;
                    case TDateInterval.Minute: //Calcular minutos
                        System.TimeSpan tsSpanForMinutes = dtFechaHasta - dtFechaDesde;
                        nResult = (int)tsSpanForMinutes.TotalMinutes;
                        break;
                    case TDateInterval.Month: //calcular los meses
                        nResult = ((dtFechaHasta.Year - dtFechaDesde.Year) * 12) + (dtFechaHasta.Month - dtFechaDesde.Month);
                        break;
                    case TDateInterval.Quarter: //Calcular el o los trimestres
                        int dtFechaDesdeQuarter = (int)System.Math.Ceiling(dtFechaDesde.Month / 3.0);
                        int dtFechaHastaQuarter = (int)System.Math.Ceiling(dtFechaHasta.Month / 3.0);
                        nResult = (4 * (dtFechaHasta.Year - dtFechaDesde.Year)) + dtFechaHastaQuarter - dtFechaDesdeQuarter;
                        break;
                    case TDateInterval.Second: //Calcular los segundos
                        System.TimeSpan tsSpanForSeconds = dtFechaHasta - dtFechaDesde;
                        nResult = (int)tsSpanForSeconds.TotalSeconds;
                        break;
                    case TDateInterval.Weekday: //Calcular los dias laborables
                        System.TimeSpan tsSpanForWeekdays = dtFechaHasta - dtFechaDesde;
                        nResult = (int)(tsSpanForWeekdays.TotalDays / 7.0);
                        break;
                    case TDateInterval.WeekOfYear: // calcular las semanas del año
                        System.DateTime dtFechaDesdeModified = dtFechaDesde;
                        System.DateTime dtFechaHastaModified = dtFechaHasta;

                        while (dtFechaHastaModified.DayOfWeek != System.Globalization.DateTimeFormatInfo.CurrentInfo.FirstDayOfWeek)
                        {
                            dtFechaHastaModified = dtFechaHastaModified.AddDays(-1);
                        }

                        while (dtFechaDesdeModified.DayOfWeek != System.Globalization.DateTimeFormatInfo.CurrentInfo.FirstDayOfWeek)
                        {
                            dtFechaDesdeModified = dtFechaDesdeModified.AddDays(-1);
                        }
                        System.TimeSpan tsSpanForWeekOfYear = dtFechaHastaModified - dtFechaDesdeModified;
                        nResult = (int)(tsSpanForWeekOfYear.TotalDays / 7.0);
                        break;
                    case TDateInterval.Year: //calcular los años transcurridos
                        nResult = dtFechaHasta.Year - dtFechaDesde.Year;
                        break;
                    default:
                        nResult = 0;
                        break;
                }
            }
            catch
            {
                nResult = 0;
            }


            return nResult;
        }

        

       

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:Construye el conexion string de conexion a la bd
        /// </summary>
        /// <param name="sServidor"></param>
        /// <param name="sBd"></param>
        /// <param name="sUsuario"></param>
        /// <param name="sClave"></param>
        /// <returns></returns>
        public static string CnxStringSqlServer(string sServidor
                                               , string sBd
                                               , string sUsuario
                                               , string sClave)

        {
            if (sServidor.TIsNullOrEmpty()) THelperInfoEstatus.ArgumentNull(nameof(sServidor));
            if (sBd.TIsNullOrEmpty()) THelperInfoEstatus.ArgumentNull(nameof(sBd));
            if (sUsuario.TIsNullOrEmpty()) THelperInfoEstatus.ArgumentNull(nameof(sUsuario));
            if (sClave.TIsNullOrEmpty()) THelperInfoEstatus.ArgumentNull(nameof(sClave));

            string sConex = THelper.Empty;
            string sApp = sBd + "_" + DateTime.Now.ToString("yyyyMMddHHmmss") + "_" + THelper.Random();

            sConex = @"Data Source=@SERVIDOR;";
            sConex += @"Initial Catalog=@BD;";
            sConex += @"Integrated Security=False;";
            sConex += @"User ID=@USUARIO;";
            sConex += @"Password=@CLAVE;";

            sConex += @"Application Name=@MIAPP;";
            sConex += @"Connection Timeout=30;";
            sConex += @"pooling=false;";
            //sConex += @"MultipleActiveResultSets=True;";
            //sConex += @"Encrypt=False;";
            //sConex += @"Persist Security Info=false;";

            sConex = sConex.Replace("@SERVIDOR", sServidor);
            sConex = sConex.Replace("@BD", sBd);
            sConex = sConex.Replace("@USUARIO", sUsuario);
            sConex = sConex.Replace("@CLAVE", sClave);
            sConex = sConex.Replace("@MIAPP", sApp);

            return sConex;
        }



        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180110
        /// *Sinopsis:Funcion que retorna la ruta completa de un archivo siempre y cuando este en el directorio raiz de la app
        /// </summary>
        /// <param name="sDir">Carpeta</param>
        /// <param name="sFile">Nombre del archivo a buscar dentro de la carpeta</param>
        /// <returns></returns>
        public static string FilePath(string sDir, string sFile) => Path.Combine(Path.GetDirectoryName(THelper.DirectorioRaiz), sDir + @"\" + sFile);
        

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180110
        /// *Sinopsis:Funcion que retorna la ruta completa de un archivo siempre y cuando este en el directorio raiz de la app
        /// </summary>
        /// <param name="sFile">Nombre del archivo a buscar dentro de la carpeta</param>
        /// <returns></returns>
        public static string FilePath(string sFile) => Path.Combine(Path.GetDirectoryName(THelper.DirectorioRaiz), sFile);


        #endregion

        #region "Metodos & constructores"
        #endregion

        #region "Eventos"
        #endregion

        #region "Destructor"
        #endregion
}
}
