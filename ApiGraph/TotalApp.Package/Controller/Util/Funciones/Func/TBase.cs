﻿
namespace TotalApp.Package
{

    #region "Action"
    /// <summary>
    /// *Autor:Larry Ramirez
    /// *Fecha de creacion:14-05-2018
    /// *Version:1.1.20180514
    /// *Sinopsis:
    /// </summary>
    public delegate void TAction();

    /// <summary>
    /// *Autor:Larry Ramirez
    /// *Fecha de creacion:14-05-2018
    /// *Version:1.1.20180514
    /// *Sinopsis:
    /// </summary>
    /// <typeparam name="A"></typeparam>
    /// <typeparam name="B"></typeparam>
    /// <param name="obj1"></param>
    /// <param name="obj2"></param>
    public delegate void TAction<in A, B>(A obj1, B obj2);

    /// <summary>
    /// *Autor:Larry Ramirez
    /// *Fecha de creacion:14-05-2018
    /// *Version:1.1.20180514
    /// *Sinopsis:
    /// </summary>
    /// <typeparam name="A"></typeparam>
    /// <typeparam name="B"></typeparam>
    /// <typeparam name="C"></typeparam>
    /// <param name="obj1"></param>
    /// <param name="obj2"></param>
    /// <param name="obj3"></param>
    public delegate void TAction<in A, in B, C>(A obj1, B obj2, C obj3);

    /// <summary>
    /// *Autor:Larry Ramirez
    /// *Fecha de creacion:14-05-2018
    /// *Version:1.1.20180514
    /// *Sinopsis:
    /// </summary>
    /// <typeparam name="A"></typeparam>
    /// <typeparam name="B"></typeparam>
    /// <typeparam name="C"></typeparam>
    /// <typeparam name="D"></typeparam>
    /// <param name="obj1"></param>
    /// <param name="obj2"></param>
    /// <param name="obj3"></param>
    /// <param name="obj4"></param>
    public delegate void TAction<in A, in B, in C, D>(A obj1, B obj2, C obj3, D obj4);

    /// <summary>
    /// *Autor:Larry Ramirez
    /// *Fecha de creacion:14-05-2018
    /// *Version:1.1.20180514
    /// *Sinopsis:
    /// </summary>
    /// <typeparam name="A"></typeparam>
    /// <typeparam name="B"></typeparam>
    /// <typeparam name="C"></typeparam>
    /// <typeparam name="D"></typeparam>
    /// <typeparam name="E"></typeparam>
    /// <param name="obj1"></param>
    /// <param name="obj2"></param>
    /// <param name="obj3"></param>
    /// <param name="obj4"></param>
    /// <param name="obj5"></param>
    public delegate void TAction<in A, in B, in C, in D, E>(A obj1, B obj2, C obj3, D obj4, E obj5);

    /// <summary>
    /// *Autor:Larry Ramirez
    /// *Fecha de creacion:14-05-2018
    /// *Version:1.1.20180514
    /// *Sinopsis:
    /// </summary>
    /// <typeparam name="A"></typeparam>
    /// <typeparam name="B"></typeparam>
    /// <typeparam name="C"></typeparam>
    /// <typeparam name="D"></typeparam>
    /// <typeparam name="E"></typeparam>
    /// <typeparam name="F"></typeparam>
    /// <param name="obj1"></param>
    /// <param name="obj2"></param>
    /// <param name="obj3"></param>
    /// <param name="obj4"></param>
    /// <param name="obj5"></param>
    /// <param name="obj6"></param>
    public delegate void TAction<in A, in B, in C, in D, in E, F>(A obj1, B obj2, C obj3, D obj4, E obj5, F obj6);

    /// <summary>
    /// *Autor:Larry Ramirez
    /// *Fecha de creacion:14-05-2018
    /// *Version:1.1.20180514
    /// *Sinopsis:
    /// </summary>
    /// <typeparam name="A"></typeparam>
    /// <typeparam name="B"></typeparam>
    /// <typeparam name="C"></typeparam>
    /// <typeparam name="D"></typeparam>
    /// <typeparam name="E"></typeparam>
    /// <typeparam name="F"></typeparam>
    /// <typeparam name="G"></typeparam>
    /// <param name="obj1"></param>
    /// <param name="obj2"></param>
    /// <param name="obj3"></param>
    /// <param name="obj4"></param>
    /// <param name="obj5"></param>
    /// <param name="obj6"></param>
    /// <param name="obj7"></param>
    public delegate void TAction<in A, in B, in C, in D, in E, in F,G>(A obj1, B obj2, C obj3, D obj4, E obj5, F obj6, G obj7);

    /// <summary>
    /// *Autor:Larry Ramirez
    /// *Fecha de creacion:14-05-2018
    /// *Version:1.1.20180514
    /// *Sinopsis:
    /// </summary>
    /// <typeparam name="A"></typeparam>
    /// <typeparam name="B"></typeparam>
    /// <typeparam name="C"></typeparam>
    /// <typeparam name="D"></typeparam>
    /// <typeparam name="E"></typeparam>
    /// <typeparam name="F"></typeparam>
    /// <typeparam name="G"></typeparam>
    /// <typeparam name="H"></typeparam>
    /// <param name="obj1"></param>
    /// <param name="obj2"></param>
    /// <param name="obj3"></param>
    /// <param name="obj4"></param>
    /// <param name="obj5"></param>
    /// <param name="obj6"></param>
    /// <param name="obj7"></param>
    /// <param name="obj8"></param>
    public delegate void TAction<in A, in B, in C, in D, in E, in F, in G,H>(A obj1, B obj2, C obj3, D obj4, E obj5, F obj6, G obj7, H obj8);

    /// <summary>
    /// *Autor:Larry Ramirez
    /// *Fecha de creacion:14-05-2018
    /// *Version:1.1.20180514
    /// *Sinopsis:
    /// </summary>
    /// <typeparam name="A"></typeparam>
    /// <typeparam name="B"></typeparam>
    /// <typeparam name="C"></typeparam>
    /// <typeparam name="D"></typeparam>
    /// <typeparam name="E"></typeparam>
    /// <typeparam name="F"></typeparam>
    /// <typeparam name="G"></typeparam>
    /// <typeparam name="H"></typeparam>
    /// <typeparam name="I"></typeparam>
    /// <param name="obj1"></param>
    /// <param name="obj2"></param>
    /// <param name="obj3"></param>
    /// <param name="obj4"></param>
    /// <param name="obj5"></param>
    /// <param name="obj6"></param>
    /// <param name="obj7"></param>
    /// <param name="obj8"></param>
    /// <param name="obj9"></param>
    public delegate void TAction<in A, in B, in C, in D, in E, in F, in G, in H,I>(A obj1, B obj2, C obj3, D obj4, E obj5, F obj6, G obj7, H obj8, I obj9);

    /// <summary>
    /// *Autor:Larry Ramirez
    /// *Fecha de creacion:14-05-2018
    /// *Version:1.1.20180514
    /// *Sinopsis:
    /// </summary>
    /// <typeparam name="A"></typeparam>
    /// <typeparam name="B"></typeparam>
    /// <typeparam name="C"></typeparam>
    /// <typeparam name="D"></typeparam>
    /// <typeparam name="E"></typeparam>
    /// <typeparam name="F"></typeparam>
    /// <typeparam name="G"></typeparam>
    /// <typeparam name="H"></typeparam>
    /// <typeparam name="I"></typeparam>
    /// <typeparam name="J"></typeparam>
    /// <param name="obj1"></param>
    /// <param name="obj2"></param>
    /// <param name="obj3"></param>
    /// <param name="obj4"></param>
    /// <param name="obj5"></param>
    /// <param name="obj6"></param>
    /// <param name="obj7"></param>
    /// <param name="obj8"></param>
    /// <param name="obj9"></param>
    /// <param name="obj10"></param>
    public delegate void TAction<in A, in B, in C, in D, in E, in F, in G, in H, in I, J>(A obj1, B obj2, C obj3, D obj4, E obj5, F obj6, G obj7, H obj8, I obj9, J obj10);

    /// <summary>
    /// *Autor:Larry Ramirez
    /// *Fecha de creacion:14-05-2018
    /// *Version:1.1.20180514
    /// *Sinopsis:
    /// </summary>
    /// <typeparam name="A"></typeparam>
    /// <typeparam name="B"></typeparam>
    /// <typeparam name="C"></typeparam>
    /// <typeparam name="D"></typeparam>
    /// <typeparam name="E"></typeparam>
    /// <typeparam name="F"></typeparam>
    /// <typeparam name="G"></typeparam>
    /// <typeparam name="H"></typeparam>
    /// <typeparam name="I"></typeparam>
    /// <typeparam name="J"></typeparam>
    /// <typeparam name="K"></typeparam>
    /// <param name="obj1"></param>
    /// <param name="obj2"></param>
    /// <param name="obj3"></param>
    /// <param name="obj4"></param>
    /// <param name="obj5"></param>
    /// <param name="obj6"></param>
    /// <param name="obj7"></param>
    /// <param name="obj8"></param>
    /// <param name="obj9"></param>
    /// <param name="obj10"></param>
    /// <param name="obj11"></param>
    public delegate void TAction<in A, in B, in C, in D, in E, in F, in G, in H, in I, in J,K>(A obj1, B obj2, C obj3, D obj4, E obj5, F obj6, G obj7, H obj8, I obj9, J obj10,K obj11);

    /// <summary>
    /// *Autor:Larry Ramirez
    /// *Fecha de creacion:14-05-2018
    /// *Version:1.1.20180514
    /// *Sinopsis:
    /// </summary>
    /// <typeparam name="A"></typeparam>
    /// <typeparam name="B"></typeparam>
    /// <typeparam name="C"></typeparam>
    /// <typeparam name="D"></typeparam>
    /// <typeparam name="E"></typeparam>
    /// <typeparam name="F"></typeparam>
    /// <typeparam name="G"></typeparam>
    /// <typeparam name="H"></typeparam>
    /// <typeparam name="I"></typeparam>
    /// <typeparam name="J"></typeparam>
    /// <typeparam name="K"></typeparam>
    /// <typeparam name="L"></typeparam>
    /// <param name="obj1"></param>
    /// <param name="obj2"></param>
    /// <param name="obj3"></param>
    /// <param name="obj4"></param>
    /// <param name="obj5"></param>
    /// <param name="obj6"></param>
    /// <param name="obj7"></param>
    /// <param name="obj8"></param>
    /// <param name="obj9"></param>
    /// <param name="obj10"></param>
    /// <param name="obj11"></param>
    /// <param name="obj12"></param>
    public delegate void TAction<in A, in B, in C, in D, in E, in F, in G, in H, in I, in J, in K,L>(A obj1, B obj2, C obj3, D obj4, E obj5, F obj6, G obj7, H obj8, I obj9, J obj10, K obj11, L obj12);


    /// <summary>
    /// *Autor:Larry Ramirez
    /// *Fecha de creacion:14-05-2018
    /// *Version:1.1.20180514
    /// *Sinopsis:
    /// </summary>
    /// <typeparam name="A"></typeparam>
    /// <typeparam name="B"></typeparam>
    /// <typeparam name="C"></typeparam>
    /// <typeparam name="D"></typeparam>
    /// <typeparam name="E"></typeparam>
    /// <typeparam name="F"></typeparam>
    /// <typeparam name="G"></typeparam>
    /// <typeparam name="H"></typeparam>
    /// <typeparam name="I"></typeparam>
    /// <typeparam name="J"></typeparam>
    /// <typeparam name="K"></typeparam>
    /// <typeparam name="L"></typeparam>
    /// <typeparam name="M"></typeparam>
    /// <param name="obj1"></param>
    /// <param name="obj2"></param>
    /// <param name="obj3"></param>
    /// <param name="obj4"></param>
    /// <param name="obj5"></param>
    /// <param name="obj6"></param>
    /// <param name="obj7"></param>
    /// <param name="obj8"></param>
    /// <param name="obj9"></param>
    /// <param name="obj10"></param>
    /// <param name="obj11"></param>
    /// <param name="obj12"></param>
    /// <param name="obj13"></param>
    public delegate void TAction<in A, in B, in C, in D, in E, in F, in G, in H, in I, in J, in K, in L,M>(A obj1, B obj2, C obj3, D obj4, E obj5, F obj6, G obj7, H obj8, I obj9, J obj10, K obj11, L obj12, M obj13);


    /// <summary>
    /// *Autor:Larry Ramirez
    /// *Fecha de creacion:14-05-2018
    /// *Version:1.1.20180514
    /// *Sinopsis:
    /// </summary>
    /// <typeparam name="A"></typeparam>
    /// <typeparam name="B"></typeparam>
    /// <typeparam name="C"></typeparam>
    /// <typeparam name="D"></typeparam>
    /// <typeparam name="E"></typeparam>
    /// <typeparam name="F"></typeparam>
    /// <typeparam name="G"></typeparam>
    /// <typeparam name="H"></typeparam>
    /// <typeparam name="I"></typeparam>
    /// <typeparam name="J"></typeparam>
    /// <typeparam name="K"></typeparam>
    /// <typeparam name="L"></typeparam>
    /// <typeparam name="M"></typeparam>
    /// <typeparam name="O"></typeparam>
    /// <param name="obj1"></param>
    /// <param name="obj2"></param>
    /// <param name="obj3"></param>
    /// <param name="obj4"></param>
    /// <param name="obj5"></param>
    /// <param name="obj6"></param>
    /// <param name="obj7"></param>
    /// <param name="obj8"></param>
    /// <param name="obj9"></param>
    /// <param name="obj10"></param>
    /// <param name="obj11"></param>
    /// <param name="obj12"></param>
    /// <param name="obj13"></param>
    /// <param name="obj14"></param>
    public delegate void TAction<in A, in B, in C, in D, in E, in F, in G, in H, in I, in J, in K, in L, in M,O>(A obj1, B obj2, C obj3, D obj4, E obj5, F obj6, G obj7, H obj8, I obj9, J obj10, K obj11, L obj12, M obj13, O obj14);


    /// <summary>
    /// *Autor:Larry Ramirez
    /// *Fecha de creacion:14-05-2018
    /// *Version:1.1.20180514
    /// *Sinopsis:
    /// </summary>
    /// <typeparam name="A"></typeparam>
    /// <typeparam name="B"></typeparam>
    /// <typeparam name="C"></typeparam>
    /// <typeparam name="D"></typeparam>
    /// <typeparam name="E"></typeparam>
    /// <typeparam name="F"></typeparam>
    /// <typeparam name="G"></typeparam>
    /// <typeparam name="H"></typeparam>
    /// <typeparam name="I"></typeparam>
    /// <typeparam name="J"></typeparam>
    /// <typeparam name="K"></typeparam>
    /// <typeparam name="L"></typeparam>
    /// <typeparam name="M"></typeparam>
    /// <typeparam name="O"></typeparam>
    /// <typeparam name="P"></typeparam>
    /// <param name="obj1"></param>
    /// <param name="obj2"></param>
    /// <param name="obj3"></param>
    /// <param name="obj4"></param>
    /// <param name="obj5"></param>
    /// <param name="obj6"></param>
    /// <param name="obj7"></param>
    /// <param name="obj8"></param>
    /// <param name="obj9"></param>
    /// <param name="obj10"></param>
    /// <param name="obj11"></param>
    /// <param name="obj12"></param>
    /// <param name="obj13"></param>
    /// <param name="obj14"></param>
    /// <param name="obj15"></param>
    public delegate void TAction<in A, in B, in C, in D, in E, in F, in G, in H, in I, in J, in K, in L, in M, in O,P>(A obj1, B obj2, C obj3, D obj4, E obj5, F obj6, G obj7, H obj8, I obj9, J obj10, K obj11, L obj12, M obj13, O obj14,P obj15);

    #endregion

    #region "Func"
    /// <summary>
    /// *Autor:Larry Ramirez
    /// *Fecha de creacion:14-05-2018
    /// *Version:1.1.20180514
    /// *Sinopsis:
    /// </summary>
    /// <typeparam name="TResult"></typeparam>
    /// <returns></returns>
    public delegate TResult TFunc<out TResult>();

    /// <summary>
    /// *Autor:Larry Ramirez
    /// *Fecha de creacion:14-05-2018
    /// *Version:1.1.20180514
    /// *Sinopsis:
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="TResult"></typeparam>
    /// <param name="obj"></param>
    /// <returns></returns>
    public delegate TResult TFunc<in A, out TResult>(A obj);

    /// <summary>
    /// *Autor:Larry Ramirez
    /// *Fecha de creacion:14-05-2018
    /// *Version:1.1.20180514
    /// *Sinopsis:
    /// </summary>
    /// <typeparam name="A"></typeparam>
    /// <typeparam name="B"></typeparam>
    /// <typeparam name="TResult"></typeparam>
    /// <param name="obj1"></param>
    /// <param name="obj2"></param>
    /// <returns></returns>
    public delegate TResult TFunc<in A, in B, out TResult>(A obj1, B obj2);

    /// <summary>
    /// *Autor:Larry Ramirez
    /// *Fecha de creacion:14-05-2018
    /// *Version:1.1.20180514
    /// *Sinopsis:
    /// </summary>
    /// <typeparam name="A"></typeparam>
    /// <typeparam name="B"></typeparam>
    /// <typeparam name="C"></typeparam>
    /// <typeparam name="TResult"></typeparam>
    /// <param name="obj1"></param>
    /// <param name="obj2"></param>
    /// <param name="obj3"></param>
    /// <returns></returns>
    public delegate TResult TFunc<in A, in B, in C, out TResult>(A obj1, B obj2, C obj3);

    /// <summary>
    /// *Autor:Larry Ramirez
    /// *Fecha de creacion:14-05-2018
    /// *Version:1.1.20180514
    /// *Sinopsis:
    /// </summary>
    /// <typeparam name="A"></typeparam>
    /// <typeparam name="B"></typeparam>
    /// <typeparam name="C"></typeparam>
    /// <typeparam name="D"></typeparam>
    /// <typeparam name="TResult"></typeparam>
    /// <param name="obj1"></param>
    /// <param name="obj2"></param>
    /// <param name="obj3"></param>
    /// <param name="obj4"></param>
    /// <returns></returns>
    public delegate TResult TFunc<in A, in B, in C, in D, out TResult>(A obj1, B obj2, C obj3, D obj4);

    /// <summary>
    /// *Autor:Larry Ramirez
    /// *Fecha de creacion:14-05-2018
    /// *Version:1.1.20180514
    /// *Sinopsis:
    /// </summary>
    /// <typeparam name="A"></typeparam>
    /// <typeparam name="B"></typeparam>
    /// <typeparam name="C"></typeparam>
    /// <typeparam name="D"></typeparam>
    /// <typeparam name="E"></typeparam>
    /// <typeparam name="TResult"></typeparam>
    /// <param name="obj1"></param>
    /// <param name="obj2"></param>
    /// <param name="obj3"></param>
    /// <param name="obj4"></param>
    /// <param name="obj5"></param>
    /// <returns></returns>
    public delegate TResult TFunc<in A, in B, in C, in D, in E, out TResult>(A obj1, B obj2, C obj3, D obj4, E obj5);

    /// <summary>
    /// *Autor:Larry Ramirez
    /// *Fecha de creacion:14-05-2018
    /// *Version:1.1.20180514
    /// *Sinopsis:
    /// </summary>
    /// <typeparam name="A"></typeparam>
    /// <typeparam name="B"></typeparam>
    /// <typeparam name="C"></typeparam>
    /// <typeparam name="D"></typeparam>
    /// <typeparam name="E"></typeparam>
    /// <typeparam name="F"></typeparam>
    /// <typeparam name="TResult"></typeparam>
    /// <param name="obj1"></param>
    /// <param name="obj2"></param>
    /// <param name="obj3"></param>
    /// <param name="obj4"></param>
    /// <param name="obj5"></param>
    /// <param name="obj6"></param>
    /// <returns></returns>
    public delegate TResult TFunc<in A, in B, in C, in D, in E, in F, out TResult>(A obj1, B obj2, C obj3, D obj4, E obj5, F obj6);

    /// <summary>
    /// *Autor:Larry Ramirez
    /// *Fecha de creacion:14-05-2018
    /// *Version:1.1.20180514
    /// *Sinopsis:
    /// </summary>
    /// <typeparam name="A"></typeparam>
    /// <typeparam name="B"></typeparam>
    /// <typeparam name="C"></typeparam>
    /// <typeparam name="D"></typeparam>
    /// <typeparam name="E"></typeparam>
    /// <typeparam name="F"></typeparam>
    /// <typeparam name="G"></typeparam>
    /// <typeparam name="TResult"></typeparam>
    /// <param name="obj1"></param>
    /// <param name="obj2"></param>
    /// <param name="obj3"></param>
    /// <param name="obj4"></param>
    /// <param name="obj5"></param>
    /// <param name="obj6"></param>
    /// <param name="obj7"></param>
    /// <returns></returns>
    public delegate TResult TFunc<in A, in B, in C, in D, in E, in F, in G, out TResult>(A obj1, B obj2, C obj3, D obj4, E obj5, F obj6, G obj7);

    /// <summary>
    /// *Autor:Larry Ramirez
    /// *Fecha de creacion:14-05-2018
    /// *Version:1.1.20180514
    /// *Sinopsis:
    /// </summary>
    /// <typeparam name="A"></typeparam>
    /// <typeparam name="B"></typeparam>
    /// <typeparam name="C"></typeparam>
    /// <typeparam name="D"></typeparam>
    /// <typeparam name="E"></typeparam>
    /// <typeparam name="F"></typeparam>
    /// <typeparam name="G"></typeparam>
    /// <typeparam name="H"></typeparam>
    /// <typeparam name="TResult"></typeparam>
    /// <param name="obj1"></param>
    /// <param name="obj2"></param>
    /// <param name="obj3"></param>
    /// <param name="obj4"></param>
    /// <param name="obj5"></param>
    /// <param name="obj6"></param>
    /// <param name="obj7"></param>
    /// <param name="obj8"></param>
    /// <returns></returns>
    public delegate TResult TFunc<in A, in B, in C, in D, in E, in F, in G, in H, out TResult>(A obj1, B obj2, C obj3, D obj4, E obj5, F obj6, G obj7, H obj8);

    /// <summary>
    /// *Autor:Larry Ramirez
    /// *Fecha de creacion:14-05-2018
    /// *Version:1.1.20180514
    /// *Sinopsis:
    /// </summary>
    /// <typeparam name="A"></typeparam>
    /// <typeparam name="B"></typeparam>
    /// <typeparam name="C"></typeparam>
    /// <typeparam name="D"></typeparam>
    /// <typeparam name="E"></typeparam>
    /// <typeparam name="F"></typeparam>
    /// <typeparam name="G"></typeparam>
    /// <typeparam name="H"></typeparam>
    /// <typeparam name="I"></typeparam>
    /// <typeparam name="TResult"></typeparam>
    /// <param name="obj1"></param>
    /// <param name="obj2"></param>
    /// <param name="obj3"></param>
    /// <param name="obj4"></param>
    /// <param name="obj5"></param>
    /// <param name="obj6"></param>
    /// <param name="obj7"></param>
    /// <param name="obj8"></param>
    /// <param name="obj9"></param>
    /// <returns></returns>
    public delegate TResult TFunc<in A, in B, in C, in D, in E, in F, in G, in H, in I, out TResult>(A obj1, B obj2, C obj3, D obj4, E obj5, F obj6, G obj7, H obj8, I obj9);

    /// <summary>
    /// *Autor:Larry Ramirez
    /// *Fecha de creacion:14-05-2018
    /// *Version:1.1.20180514
    /// *Sinopsis:
    /// </summary>
    /// <typeparam name="A"></typeparam>
    /// <typeparam name="B"></typeparam>
    /// <typeparam name="C"></typeparam>
    /// <typeparam name="D"></typeparam>
    /// <typeparam name="E"></typeparam>
    /// <typeparam name="F"></typeparam>
    /// <typeparam name="G"></typeparam>
    /// <typeparam name="H"></typeparam>
    /// <typeparam name="I"></typeparam>
    /// <typeparam name="J"></typeparam>
    /// <typeparam name="TResult"></typeparam>
    /// <param name="obj1"></param>
    /// <param name="obj2"></param>
    /// <param name="obj3"></param>
    /// <param name="obj4"></param>
    /// <param name="obj5"></param>
    /// <param name="obj6"></param>
    /// <param name="obj7"></param>
    /// <param name="obj8"></param>
    /// <param name="obj9"></param>
    /// <param name="obj10"></param>
    /// <returns></returns>
    public delegate TResult TFunc<in A, in B, in C, in D, in E, in F, in G, in H, in I, in J, out TResult>(A obj1, B obj2, C obj3, D obj4, E obj5, F obj6, G obj7, H obj8, I obj9, J obj10);

    /// <summary>
    /// *Autor:Larry Ramirez
    /// *Fecha de creacion:14-05-2018
    /// *Version:1.1.20180514
    /// *Sinopsis:
    /// </summary>
    /// <typeparam name="A"></typeparam>
    /// <typeparam name="B"></typeparam>
    /// <typeparam name="C"></typeparam>
    /// <typeparam name="D"></typeparam>
    /// <typeparam name="E"></typeparam>
    /// <typeparam name="F"></typeparam>
    /// <typeparam name="G"></typeparam>
    /// <typeparam name="H"></typeparam>
    /// <typeparam name="I"></typeparam>
    /// <typeparam name="J"></typeparam>
    /// <typeparam name="K"></typeparam>
    /// <typeparam name="TResult"></typeparam>
    /// <param name="obj1"></param>
    /// <param name="obj2"></param>
    /// <param name="obj3"></param>
    /// <param name="obj4"></param>
    /// <param name="obj5"></param>
    /// <param name="obj6"></param>
    /// <param name="obj7"></param>
    /// <param name="obj8"></param>
    /// <param name="obj9"></param>
    /// <param name="obj10"></param>
    /// <param name="obj11"></param>
    /// <returns></returns>
    public delegate TResult TFunc<in A, in B, in C, in D, in E, in F, in G, in H, in I, in J, in K, out TResult>(A obj1, B obj2, C obj3, D obj4, E obj5, F obj6, G obj7, H obj8, I obj9, J obj10, K obj11);

    /// <summary>
    /// *Autor:Larry Ramirez
    /// *Fecha de creacion:14-05-2018
    /// *Version:1.1.20180514
    /// *Sinopsis:
    /// </summary>
    /// <typeparam name="A"></typeparam>
    /// <typeparam name="B"></typeparam>
    /// <typeparam name="C"></typeparam>
    /// <typeparam name="D"></typeparam>
    /// <typeparam name="E"></typeparam>
    /// <typeparam name="F"></typeparam>
    /// <typeparam name="G"></typeparam>
    /// <typeparam name="H"></typeparam>
    /// <typeparam name="I"></typeparam>
    /// <typeparam name="J"></typeparam>
    /// <typeparam name="K"></typeparam>
    /// <typeparam name="L"></typeparam>
    /// <typeparam name="TResult"></typeparam>
    /// <param name="obj1"></param>
    /// <param name="obj2"></param>
    /// <param name="obj3"></param>
    /// <param name="obj4"></param>
    /// <param name="obj5"></param>
    /// <param name="obj6"></param>
    /// <param name="obj7"></param>
    /// <param name="obj8"></param>
    /// <param name="obj9"></param>
    /// <param name="obj10"></param>
    /// <param name="obj11"></param>
    /// <param name="obj12"></param>
    /// <returns></returns>
    public delegate TResult TFunc<in A, in B, in C, in D, in E, in F, in G, in H, in I, in J, in K, in L, out TResult>(A obj1, B obj2, C obj3, D obj4, E obj5, F obj6, G obj7, H obj8, I obj9, J obj10, K obj11, L obj12);

    /// <summary>
    /// *Autor:Larry Ramirez
    /// *Fecha de creacion:14-05-2018
    /// *Version:1.1.20180514
    /// *Sinopsis:
    /// </summary>
    /// <typeparam name="A"></typeparam>
    /// <typeparam name="B"></typeparam>
    /// <typeparam name="C"></typeparam>
    /// <typeparam name="D"></typeparam>
    /// <typeparam name="E"></typeparam>
    /// <typeparam name="F"></typeparam>
    /// <typeparam name="G"></typeparam>
    /// <typeparam name="H"></typeparam>
    /// <typeparam name="I"></typeparam>
    /// <typeparam name="J"></typeparam>
    /// <typeparam name="K"></typeparam>
    /// <typeparam name="L"></typeparam>
    /// <typeparam name="M"></typeparam>
    /// <typeparam name="TResult"></typeparam>
    /// <param name="obj1"></param>
    /// <param name="obj2"></param>
    /// <param name="obj3"></param>
    /// <param name="obj4"></param>
    /// <param name="obj5"></param>
    /// <param name="obj6"></param>
    /// <param name="obj7"></param>
    /// <param name="obj8"></param>
    /// <param name="obj9"></param>
    /// <param name="obj10"></param>
    /// <param name="obj11"></param>
    /// <param name="obj12"></param>
    /// <param name="obj13"></param>
    /// <returns></returns>
    public delegate TResult TFunc<in A, in B, in C, in D, in E, in F, in G, in H, in I, in J, in K, in L, in M, out TResult>(A obj1, B obj2, C obj3, D obj4, E obj5, F obj6, G obj7, H obj8, I obj9, J obj10, K obj11, L obj12, M obj13);

    /// <summary>
    /// *Autor:Larry Ramirez
    /// *Fecha de creacion:14-05-2018
    /// *Version:1.1.20180514
    /// *Sinopsis:
    /// </summary>
    /// <typeparam name="A"></typeparam>
    /// <typeparam name="B"></typeparam>
    /// <typeparam name="C"></typeparam>
    /// <typeparam name="D"></typeparam>
    /// <typeparam name="E"></typeparam>
    /// <typeparam name="F"></typeparam>
    /// <typeparam name="G"></typeparam>
    /// <typeparam name="H"></typeparam>
    /// <typeparam name="I"></typeparam>
    /// <typeparam name="J"></typeparam>
    /// <typeparam name="K"></typeparam>
    /// <typeparam name="L"></typeparam>
    /// <typeparam name="M"></typeparam>
    /// <typeparam name="O"></typeparam>
    /// <typeparam name="TResult"></typeparam>
    /// <param name="obj1"></param>
    /// <param name="obj2"></param>
    /// <param name="obj3"></param>
    /// <param name="obj4"></param>
    /// <param name="obj5"></param>
    /// <param name="obj6"></param>
    /// <param name="obj7"></param>
    /// <param name="obj8"></param>
    /// <param name="obj9"></param>
    /// <param name="obj10"></param>
    /// <param name="obj11"></param>
    /// <param name="obj12"></param>
    /// <param name="obj13"></param>
    /// <param name="obj14"></param>
    /// <returns></returns>
    public delegate TResult TFunc<in A, in B, in C, in D, in E, in F, in G, in H, in I, in J, in K, in L, in M, in O, out TResult>(A obj1, B obj2, C obj3, D obj4, E obj5, F obj6, G obj7, H obj8, I obj9, J obj10, K obj11, L obj12, M obj13, O obj14);

    /// <summary>
    /// *Autor:Larry Ramirez
    /// *Fecha de creacion:14-05-2018
    /// *Version:1.1.20180514
    /// *Sinopsis:
    /// </summary>
    /// <typeparam name="A"></typeparam>
    /// <typeparam name="B"></typeparam>
    /// <typeparam name="C"></typeparam>
    /// <typeparam name="D"></typeparam>
    /// <typeparam name="E"></typeparam>
    /// <typeparam name="F"></typeparam>
    /// <typeparam name="G"></typeparam>
    /// <typeparam name="H"></typeparam>
    /// <typeparam name="I"></typeparam>
    /// <typeparam name="J"></typeparam>
    /// <typeparam name="K"></typeparam>
    /// <typeparam name="L"></typeparam>
    /// <typeparam name="M"></typeparam>
    /// <typeparam name="O"></typeparam>
    /// <typeparam name="P"></typeparam>
    /// <typeparam name="TResult"></typeparam>
    /// <param name="obj1"></param>
    /// <param name="obj2"></param>
    /// <param name="obj3"></param>
    /// <param name="obj4"></param>
    /// <param name="obj5"></param>
    /// <param name="obj6"></param>
    /// <param name="obj7"></param>
    /// <param name="obj8"></param>
    /// <param name="obj9"></param>
    /// <param name="obj10"></param>
    /// <param name="obj11"></param>
    /// <param name="obj12"></param>
    /// <param name="obj13"></param>
    /// <param name="obj14"></param>
    /// <param name="obj15"></param>
    /// <returns></returns>
    public delegate TResult TFunc<in A, in B, in C, in D, in E, in F, in G, in H, in I, in J, in K, in L, in M, in O, in P, out TResult>(A obj1, B obj2, C obj3, D obj4, E obj5, F obj6, G obj7, H obj8, I obj9, J obj10, K obj11, L obj12, M obj13, O obj14, P obj15);
    #endregion

    

}
