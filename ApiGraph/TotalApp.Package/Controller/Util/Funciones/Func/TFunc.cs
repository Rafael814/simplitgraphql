﻿using System;

namespace TotalApp.Package
{
    /// <summary>
    /// *Autor:Larry Ramirez
    /// *Fecha de creacion:09-01-2018
    /// *Version:1.1.20180109
    /// *Sinopsis:
    /// </summary>
    public static partial class THelper
    {
        #region "Constantes"
        #endregion
        
        #region "Variables"
        #endregion

        #region "Propiedades"
        #endregion

        #region "Expresiones Func"

        #region "Por defecto"
        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        public static Func<decimal?, decimal> FuncDecimalOrDefault = (nValue) => nValue ?? 0;
        

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        public static Func<float?, float> FuncFloatOrDefault = (nValue) => nValue ?? 0;
        

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        public static Func<int?, int> FuncIntOrDefault = (nValue) => nValue ?? 0;


        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        public static Func<long?, long> FuncLongOrDefault = (nValue) => nValue ?? 0;
        

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        public static Func<double?, double> FuncDoubleOrDefault = (nValue) => nValue ?? 0;
        #endregion

        #region "Expresiones en Predicados"
        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        public static Func<long?, bool> FuncLongIsEven = nValue => (nValue == null) ? false: (nValue % 2 == 0);

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        public static Func<int?, bool> FuncIntIsEven = nValue => (nValue == null) ? false : (nValue % 2 == 0);

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        public static Func<float?, bool> FuncFloatIsEven = nValue => (nValue == null) ? false : (nValue % 2 == 0);

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        public static Func<decimal?, bool> FuncDecimalIsEven = nValue => (nValue == null) ? false : (nValue % 2 == 0);

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        public static Func<double?, bool> FuncDoubleIsEven = nValue => (nValue == null) ? false : (nValue % 2 == 0);

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        public static Func<long?, bool> FuncLongIsPositive = nValue => (nValue ?? 0) > 0;

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        public static Func<int?, bool> FuncIntIsPositive = nValue => (nValue ?? 0) > 0;

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        public static Func<float?, bool> FuncFloatIsPositive = nValue => (nValue ?? 0) > 0;

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        public static Func<decimal?, bool> FuncDecimalIsPositive = nValue => (nValue ?? 0) > 0;

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        public static Func<double?, bool> FuncDoubleIsPositive = nValue => (nValue ?? 0) > 0;

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        public static Func<object, bool> FuncIsNull = obj => (obj == null);
        #endregion

        #endregion

        #region "Funciones"
        #endregion

        #region "Metodos & constructores"
        #endregion

        #region "Eventos"
        #endregion

        #region "Destructor"
        #endregion
    }



}
