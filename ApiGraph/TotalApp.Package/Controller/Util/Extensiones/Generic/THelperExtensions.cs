﻿using System;
using System.Text;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace TotalApp.Package
{

    /// <summary>
    /// *Autor:Larry Ramirez
    /// *Fecha de creacion:09-01-2018
    /// *Version:1.1.20180109
    /// *Sinopsis:
    /// </summary>
    public static class THelperExtensions

    {
        #region "Constantes"
        #endregion

        #region "Variables"

        #region "Variables Lock"
        private static readonly object _oObjectLock = new object();
        #endregion

        #endregion

        #region "Propiedades"
        #endregion

        #region "Funciones"

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="oData"></param>
        /// <param name="nDividirEnBloquesDe"></param>
        /// <returns></returns>
        public static int TTotalDePaginas<T>(this IEnumerable<T> oData
                                           , int nDividirEnBloquesDe) where T:class => oData.Count().TTotalDePaginas(nDividirEnBloquesDe);


        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="nTotal"></param>
        /// <param name="nDividirEnBloquesDe"></param>
        /// <returns></returns>
        public static int TTotalDePaginas(this int nTotal
                                        , int nDividirEnBloquesDe) 
        {
            lock (_oObjectLock)
            {
               
                double nTotalPagina = 0;

                try
                {
                    
                    if (nTotal > 0)
                    {
                        nTotalPagina = Convert.ToDouble(nTotal) / Convert.ToDouble(nDividirEnBloquesDe);
                        if (!(nTotalPagina % 2 == 0))
                        {
                            nTotalPagina = Math.Ceiling(nTotalPagina);

                        }

                        return Convert.ToInt32(nTotalPagina);
                    }

                }
                catch
                {

                    return 0;
                }

                return 0;

            }

        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sValor"></param>
        /// <returns></returns>
        public static Stream TGenerateStreamFromString(this string sValor)
        {
            lock (_oObjectLock)
            {
                return (sValor.TIsNullOrEmpty()) ? default(Stream): new MemoryStream(Encoding.UTF8.GetBytes(sValor));
            }
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="sPropertyName"></param>
        /// <returns></returns>
        public static bool THasProperty(this object obj, string sPropertyName) => obj.GetType().GetTypeInfo().GetDeclaredProperty(sPropertyName) != null;

       
        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="oObj"></param>
        /// <returns></returns>
        public static bool TDisposeObject(this object oObj)
        {
            bool bIsRetorno = true;
            try
            {
                if (oObj != null)
                {
                    ((IDisposable)oObj).Dispose();
                    oObj = null;
                }
            }
            catch (Exception ex)
            {
                bIsRetorno = false;
            }


            return bIsRetorno;
        }

       
        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sValue"></param>
        /// <returns></returns>
        public static bool TToBool(this string sValue)
        {
            if (sValue.TIsNullOrEmpty()) THelperInfoEstatus.ArgumentNull(nameof(sValue));

            string sVal = sValue.ToLower().Trim();
            switch (sVal)
            {
                case "false":
                    return false;
                case "f":
                    return false;
                case "true":
                    return true;
                case "t":
                    return true;
                case "yes":
                    return true;
                case "no":
                    return false;
                case "y":
                    return true;
                case "n":
                    return false;
                default:
                    throw new ArgumentException("Invalid boolean");
            }
        }
        #endregion

        #region "Metodos & constructores"
        #endregion

        #region "Eventos"
        #endregion

        #region "Destructor"
        #endregion
    }



}
