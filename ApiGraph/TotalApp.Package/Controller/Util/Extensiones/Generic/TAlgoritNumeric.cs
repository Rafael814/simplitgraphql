﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace TotalApp.Package
{
    /// <summary>
    /// *Autor:Larry Ramirez
    /// *Fecha de creacion:09-01-2018
    /// *Version:1.1.20180109
    /// *Sinopsis:
    /// </summary>
    public static partial class THelperConvert
    {
        #region "Constantes"
        #endregion

        #region "Variables"
        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        private static readonly sbyte[] TablaHex =
        {
            -1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1
           ,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1
           ,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1
           , 0, 1, 2, 3, 4, 5, 6, 7, 8, 9,-1,-1,-1,-1,-1,-1
           ,-1,10,11,12,13,14,15,-1,-1,-1,-1,-1,-1,-1,-1,-1
           ,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1
           ,-1,10,11,12,13,14,15,-1,-1,-1,-1,-1,-1,-1,-1,-1
           ,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1

        };
        #endregion

        #region "Propiedades"
        #endregion

        #region "Funciones"

        #region "Conversiones algoritmicas"

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sValor"></param>
        /// <returns></returns>
        private static string HashBytesMd5(string sValor)
        {

            MD5 oMd5 = System.Security.Cryptography.MD5.Create();

            byte[] byInputBytes = System.Text.Encoding.ASCII.GetBytes(sValor);

            byte[] byHash = oMd5.ComputeHash(byInputBytes);

            StringBuilder sRetorno = new StringBuilder();

            for (int nIndex = 0; nIndex < byHash.Length; nIndex++)
            {
                sRetorno.Append(byHash[nIndex].ToString("X2"));
            }

            return sRetorno.ToString();

        }


        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sValor"></param>
        /// <returns></returns>
        public static Int64 TToAlgoritInt64(this string sValor)
        {
            if (sValor.TIsNullOrEmpty()) THelperInfoEstatus.ArgumentNull(nameof(sValor));

            var oMd5 = HashBytesMd5(sValor);
            Int64 nNum = TablaHex[(byte)oMd5[0]];
            for (int nIndex = 1; nIndex < oMd5.Length; nIndex++)
            {
                nNum *= 16;
                nNum += TablaHex[(byte)oMd5[nIndex]];
            }
            return nNum;
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sValor"></param>
        /// <returns></returns>
        public static string TToAlgoritNumber(this string sValor) => TToAlgoritInt64(sValor).ToString();

        #endregion

        #endregion

        #region "Metodos & constructores"
        #endregion

        #region "Eventos"
        #endregion

        #region "Destructor"
        #endregion
    }
}
