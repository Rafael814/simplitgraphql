﻿using System;
using System.Collections.Generic;
using TotalApp.Package.TLINQ;

namespace TotalApp.Package
{
    /// <summary>
    /// *Autor:Larry Ramirez
    /// *Fecha de creacion:09-01-2018
    /// *Version:1.1.20180109
    /// *Sinopsis:
    /// </summary>
    public static partial class THelper
    {
        #region "Constantes"
        #endregion

        #region "Variables"
        #endregion

        #region "Propiedades"
        #endregion

        #region "Funciones"
        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="objChar"></param>
        /// <returns></returns>
        public static char TFlip(this char objChar) => Char.IsUpper(objChar) ? Char.ToLower(objChar) : Char.ToUpper(objChar);


        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sValor"></param>
        /// <param name="nVal"></param>
        /// <returns></returns>
        public static string TFlip(this string sValor, int nVal = Int32.MaxValue)
        {
            if (sValor.TIsNullOrEmpty()) return THelper.Empty;

            var oArray = new char[sValor.Length];

            for (int nCount = 0; nCount < sValor.Length; nCount++)
            {
                if (nCount < nVal)
                {
                    oArray[nCount] = sValor[nCount].TFlip();
                }
                else
                {
                    oArray[nCount] = sValor[nCount];
                }
            }

            return new String(oArray);
        }


        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sValor"></param>
        /// <param name="stringsValors"></param>
        /// <returns></returns>
        public static bool TIn(this string sValor, params string[] sParam)
        {
            foreach (string sOther in sParam)
            {
                if (string.Compare(sValor, sOther) == 0)
                {
                    return true;
                }

            }

            return false;
        }


        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="sValor"></param>
        /// <returns></returns>
        public static T TToEnum<T>(this string sValor) where T : struct
        {
            return (T)System.Enum.Parse(typeof(T), sValor, true);
        }


        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sValor"></param>
        /// <param name="nLength"></param>
        /// <returns></returns>
        public static string TRight(this string sValor, int nLength)
        {
            return sValor != null && sValor.Length > nLength ? sValor.Substring(sValor.Length - nLength) : sValor;
        }


        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sValor"></param>
        /// <param name="nLength"></param>
        /// <returns></returns>
        public static string TLeft(this string sValor, int nLength)
        {
            return sValor != null && sValor.Trim().Length > nLength ? sValor.Substring(0, nLength) : sValor;
        }


        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sValor"></param>
        /// <param name="nFirst"></param>
        /// <param name="nLength"></param>
        /// <returns></returns>
        public static string TMid(this string sValor, int nFirst, int nLength)
        {

            if (sValor.TIsNullOrEmpty()) return "";

            if (nFirst > sValor.Length) return "";

            if (nFirst + nLength > sValor.Length)
            {
                nLength = sValor.Length - nFirst;
            }

            return sValor.Substring(nFirst, nLength);
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sValor"></param>
        /// <param name="oArg"></param>
        /// <returns></returns>
        public static string TFormat(this string sValor, object oArg) => string.Format(sValor, oArg);
        

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sValor"></param>
        /// <param name="oParams"></param>
        /// <returns></returns>
        public static string TFormat(this string sValor, params object[] oParams) => string.Format(sValor, oParams);



        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sCadena"></param>
        /// <param name="sChar"></param>
        /// <returns></returns>
        public static string[] TSplit(this string sCadena, string sChar)
        {
            return (sCadena.TIsNullOrEmpty()) ? default(string[]) : sCadena.Split(new string[] { sChar }, StringSplitOptions.None);
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="sCadena"></param>
        /// <param name="sChar"></param>
        /// <returns></returns>
        public static IEnumerable<T> TSplitTo<T>(this string sCadena, params char[] sChar) where T : IConvertible
        {
            if (sCadena.TIsNullOrEmpty()) THelperInfoEstatus.ArgumentNull(nameof(sCadena));

            return sCadena.Split(sChar, StringSplitOptions.None).TSelect(x => (T)Convert.ChangeType(x, typeof(T)));
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sCadena"></param>
        /// <returns></returns>
        public static string TFirstCharacterOrDefault(this string sCadena)
        {
            return (!sCadena.TIsNullOrEmpty())
                    ? sCadena.Substring(0, 1)
                    : THelper.Empty;
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sCadena"></param>
        /// <returns></returns>
        public static string TLastCharacterOrDefault(this string sCadena)
        {
            return (!sCadena.TIsNullOrEmpty())
                    ? sCadena.Substring(sCadena.Trim().Length - 1, 1)
                    : THelper.Empty;
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sValor"></param>
        /// <param name="sCaracter"></param>
        /// <param name="nHasta"></param>
        /// <param name="bIsRellenarALaIzq"></param>
        /// <returns></returns>
        public static string TRellenarOrDefault(this string sValor
                                              , string sCaracter
                                              , int nHasta
                                              , bool bIsRellenarALaIzq = false)
        {

            if (sValor.TIsNullOrEmpty())
            {
                return sValor;
            }

            if (sValor.Length >= nHasta)
            {
                return sValor;
            }

            int nLen = nHasta - sValor.Length;
            for (int nIndex = 1; nIndex < nLen; nIndex++)
            {
                if (bIsRellenarALaIzq)
                {
                    sValor = sCaracter + sValor;
                }
                else
                {
                    sValor = sValor + sCaracter;
                }

                if (sValor.Length == nHasta)
                {
                    break;
                }
            }

            return sValor;
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sValor"></param>
        /// <param name="sDefault"></param>
        /// <returns></returns>
        public static string TStringDefaultIfEmpty(this string sValor, string sDefault)
        {
            return sValor.TIsNullOrEmpty() ? sDefault : sValor;
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sValor"></param>
        /// <param name="objValues"></param>
        /// <returns></returns>
        public static string TJoin(string sSeparador, params object[] objValues)
        {

            if (objValues.TIsNullOrEmpty()) THelperInfoEstatus.ArgumentNull(nameof(objValues));

            var oResult =  objValues.TAggregate(new System.Text.StringBuilder(), (x, y) => {

                if (x.Length > 0)
                {
                    x.Append(sSeparador);
                }
                   
                x.Append(y);
                return x;
            }).ToString();

            return oResult;
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="objValues"></param>
        /// <param name="sSeparador"></param>
        /// <returns></returns>
        public static string TJoin(this object[] objValues, string sSeparador) => TJoin(sSeparador, objValues);
        
        
        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sValor"></param>
        /// <returns></returns>
        public static string TConcatNewLine(this string[] sValor) => sValor.TJoin(Environment.NewLine);

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sValor"></param>
        /// <returns></returns>
        public static string TConcatNewLine(params object[] objValues) => objValues.TJoin(Environment.NewLine);

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sValor"></param>
        /// <param name="nValor"></param>
        /// <returns></returns>
        public static string TRepeat(this string sValor, int nValor) => string.Concat(System.Linq.Enumerable.Repeat(sValor, nValor));
        #endregion

        #region "Metodos & constructores"
        #endregion

        #region "Eventos"
        #endregion

        #region "Destructor"
        #endregion
    }
}
