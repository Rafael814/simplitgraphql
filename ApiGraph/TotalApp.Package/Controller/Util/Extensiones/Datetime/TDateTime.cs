﻿using System;


namespace TotalApp.Package
{
    /// <summary>
    /// *Autor:Larry Ramirez
    /// *Fecha de creacion:09-01-2018
    /// *Version:1.1.20180109
    /// *Sinopsis:
    /// </summary>
    public static class THelperDateTime
    {
        #region "Constantes"
        #endregion

        #region "Variables"

        #endregion

        #region "Propiedades"
        #endregion

        #region "Funciones"
        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        public static DateTime TNow => DateTime.Now;

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="dtFH"></param>
        /// <returns></returns>
        public static DateTime TTomorrow(this DateTime dtFH) => dtFH.AddDays(1);

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        public static DateTime TTesterDay() => DateTime.Today.AddDays(1);



        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="dtFH"></param>
        /// <returns></returns>
        public static DateTime TYesterDay(this DateTime dtFH) => dtFH.AddDays(-1);

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        public static DateTime TYesterDay() => DateTime.Today.AddDays(-1);


        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="dtFecha"></param>
        /// <returns></returns>
        public static DateTime TFirstDayOfMonthFromDateTime(this DateTime dtFecha)
        {
            if (dtFecha == null)
            {
                return new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
            }

            return new DateTime(dtFecha.Year, dtFecha.Month, 1);

        }


        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="dtFecha"></param>
        /// <returns></returns>
        public static DateTime TLastDayOfMonthFromDateTime(this DateTime dtFecha)
        {
            if (dtFecha == null)
            {
                return new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1).AddMonths(1).AddDays(-1);
            }

            DateTime dtFirstDayOfTheMonth = new DateTime(dtFecha.Year, dtFecha.Month, 1);
            return dtFirstDayOfTheMonth.AddMonths(1).AddDays(-1);
        }
        #endregion

        #region "Metodos & constructores"
        #endregion

        #region "Eventos"
        #endregion

        #region "Destructor"
        #endregion
    }
}
