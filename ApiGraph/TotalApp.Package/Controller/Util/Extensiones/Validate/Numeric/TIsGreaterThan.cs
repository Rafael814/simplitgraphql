﻿
namespace TotalApp.Package
{

    /// <summary>
    /// *Autor:Larry Ramirez
    /// *Fecha de creacion:09-01-2018
    /// *Version:1.1.20180109
    /// *Sinopsis:
    /// </summary>
    public static partial class THelperValidate
    {
        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="nValue"></param>
        /// <param name="nHigher"></param>
        /// <returns></returns>
        public static bool TIsGreaterThan(this decimal nValue, decimal nHigher) => nValue > nHigher;

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="nValue"></param>
        /// <param name="nHigher"></param>
        /// <returns></returns>
        public static bool TIsGreaterThan(this float nValue, float nHigher) => nValue > nHigher;

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="nValue"></param>
        /// <param name="nHigher"></param>
        /// <returns></returns>
        public static bool TIsGreaterThan(this int nValue, float nHigher) => nValue > nHigher;

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="nValue"></param>
        /// <param name="nHigher"></param>
        /// <returns></returns>
        public static bool TIsGreaterThan(this long nValue, float nHigher) => nValue > nHigher;

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="nValue"></param>
        /// <param name="nHigher"></param>
        /// <returns></returns>
        public static bool TIsGreaterThan(this double nValue, float nHigher) => nValue > nHigher;

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="nValue"></param>
        /// <param name="nHigher"></param>
        /// <returns></returns>
        public static bool TIsGreaterThan(this decimal? nValue, decimal nHigher) => (nValue == null) ? false : nValue > nHigher;

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="nValue"></param>
        /// <param name="nHigher"></param>
        /// <returns></returns>
        public static bool TIsGreaterThan(this float? nValue, float nHigher) => (nValue == null) ? false : nValue > nHigher;

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="nValue"></param>
        /// <param name="nHigher"></param>
        /// <returns></returns>
        public static bool TIsGreaterThan(this int? nValue, float nHigher) => (nValue == null) ? false : nValue > nHigher;

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="nValue"></param>
        /// <param name="nHigher"></param>
        /// <returns></returns>
        public static bool TIsGreaterThan(this long? nValue, float nHigher) => (nValue == null) ? false : nValue > nHigher;

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="nValue"></param>
        /// <param name="nHigher"></param>
        /// <returns></returns>
        public static bool TIsGreaterThan(this double? nValue, float nHigher) => (nValue == null) ? false : nValue > nHigher;

      
        
    }

}
