﻿
namespace TotalApp.Package
{

    /// <summary>
    /// *Autor:Larry Ramirez
    /// *Fecha de creacion:09-01-2018
    /// *Version:1.1.20180109
    /// *Sinopsis:
    /// </summary>
    public static partial class THelperValidate
    {
        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj"></param>
        /// <param name="oDesde"></param>
        /// <param name="oHasta"></param>
        /// <returns></returns>
        public static bool TBetweenComp<T>(this T obj, T oDesde, T oHasta) where T : System.IComparable<T>
        {
            return obj.CompareTo(oDesde) >= 0 && obj.CompareTo(oHasta) <= 0;
        }


        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="nNum"></param>
        /// <param name="nNumIni"></param>
        /// <param name="nNumFin"></param>
        /// <returns></returns>
        public static bool TIsBetween(this short nNum
                                    , short nNumIni
                                    , short nNumFin) => (nNum >= nNumIni && nNum <= nNumFin);


        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="nNum"></param>
        /// <param name="nNumIni"></param>
        /// <param name="nNumFin"></param>
        /// <returns></returns>
        public static bool TIsBetween(this int nNum
                                    , int nNumIni
                                    , int nNumFin) => (nNum >= nNumIni && nNum <= nNumFin);


        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="nNum"></param>
        /// <param name="nNumIni"></param>
        /// <param name="nNumFin"></param>
        /// <returns></returns>
        public static bool TIsBetween(this float nNum
                                    , float nNumIni
                                    , float nNumFin) => (nNum >= nNumIni && nNum <= nNumFin);


        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="nNum"></param>
        /// <param name="nNumIni"></param>
        /// <param name="nNumFin"></param>
        /// <returns></returns>
        public static bool TIsBetween(this decimal nNum
                                    , decimal nNumIni
                                    , decimal nNumFin) => (nNum >= nNumIni && nNum <= nNumFin);


        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="nNum"></param>
        /// <param name="nNumIni"></param>
        /// <param name="nNumFin"></param>
        /// <returns></returns>
        public static bool TIsBetween(this double nNum
                                    , double nNumIni
                                    , double nNumFin) => (nNum >= nNumIni && nNum <= nNumFin);
    }

}
