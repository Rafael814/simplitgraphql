﻿
namespace TotalApp.Package
{

    /// <summary>
    /// *Autor:Larry Ramirez
    /// *Fecha de creacion:09-01-2018
    /// *Version:1.1.20180109
    /// *Sinopsis:
    /// </summary>
    public static partial class THelperValidate
    {
        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="nValue"></param>
        /// <returns></returns>
        public static decimal? TDecimalOrDefault(this decimal? nValue) => THelper.FuncDecimalOrDefault(nValue);

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="nValue"></param>
        /// <returns></returns>
        public static int? TIntOrDefault(this int? nValue) => THelper.FuncIntOrDefault(nValue);

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="nValue"></param>
        /// <returns></returns>
        public static long? TLongOrDefault(this long? nValue) => THelper.FuncLongOrDefault(nValue);

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="nValue"></param>
        /// <returns></returns>
        public static float? TFloatOrDefault(this float? nValue) => THelper.FuncFloatOrDefault(nValue);

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="nValue"></param>
        /// <returns></returns>
        public static double? TFloatOrDefault(this double? nValue) => THelper.FuncDoubleOrDefault(nValue);
    }

}
