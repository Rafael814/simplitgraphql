﻿
namespace TotalApp.Package
{

    /// <summary>
    /// *Autor:Larry Ramirez
    /// *Fecha de creacion:09-01-2018
    /// *Version:1.1.20180109
    /// *Sinopsis:
    /// </summary>
    public static partial class THelperValidate
    {
        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="nValue"></param>
        /// <returns></returns>
        public static bool TGreaterThan(this short nValue, short nGreaterThan) => nValue > nGreaterThan;

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="nValue"></param>
        /// <returns></returns>
        public static bool TGreaterThan(this int nValue, int nGreaterThan) => nValue > nGreaterThan;

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="nValue"></param>
        /// <returns></returns>
        public static bool TGreaterThan(this long nValue, long nGreaterThan) => nValue > nGreaterThan;

        

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="nValue"></param>
        /// <returns></returns>
        public static bool TGreaterThan(this decimal nValue, decimal nGreaterThan) => nValue > nGreaterThan;


        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="nValue"></param>
        /// <returns></returns>
        public static bool TGreaterThan(this float nValue, float nGreaterThan) => nValue > nGreaterThan;

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="nValue"></param>
        /// <returns></returns>
        public static bool TGreaterThan(this double nValue, double nGreaterThan) => nValue > nGreaterThan;


        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="nValue"></param>
        /// <returns></returns>
        public static bool TGreaterThan(this short? nValue, short nGreaterThan) => nValue == null ? false : nValue > nGreaterThan;


        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="nValue"></param>
        /// <returns></returns>
        public static bool TGreaterThan(this int? nValue, int nGreaterThan) => nValue == null ? false : nValue > nGreaterThan;

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="nValue"></param>
        /// <returns></returns>
        public static bool TGreaterThan(this long? nValue, long nGreaterThan) => nValue == null ? false : nValue  > nGreaterThan;



        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="nValue"></param>
        /// <returns></returns>
        public static bool TGreaterThan(this decimal? nValue, decimal nGreaterThan) => nValue == null ? false : nValue > nGreaterThan;


        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="nValue"></param>
        /// <returns></returns>
        public static bool TGreaterThan(this float? nValue, float nGreaterThan) => nValue == null ? false : nValue > nGreaterThan;

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="nValue"></param>
        /// <returns></returns>
        public static bool TGreaterThan(this double? nValue, double nGreaterThan) => nValue == null ? false : nValue > nGreaterThan;
    }

}
