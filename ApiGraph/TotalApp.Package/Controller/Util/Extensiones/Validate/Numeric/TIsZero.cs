﻿
namespace TotalApp.Package
{

    /// <summary>
    /// *Autor:Larry Ramirez
    /// *Fecha de creacion:09-01-2018
    /// *Version:1.1.20180109
    /// *Sinopsis:
    /// </summary>
    public static partial class THelperValidate
    {
        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sValue"></param>
        /// <returns></returns>
        public static bool TIsZero(this short nValue) => (nValue == 0);

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sValue"></param>
        /// <returns></returns>
        public static bool TIsZero(this int nValue) => (nValue == 0);

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sValue"></param>
        /// <returns></returns>
        public static bool TIsZero(this long nValue) => (nValue == 0);

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sValue"></param>
        /// <returns></returns>
        public static bool TIsZero(this decimal nValue) => (nValue == 0);

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sValue"></param>
        /// <returns></returns>
        public static bool TIsZero(this float nValue) => (nValue == 0);
    }

}
