﻿
namespace TotalApp.Package
{

    /// <summary>
    /// *Autor:Larry Ramirez
    /// *Fecha de creacion:09-01-2018
    /// *Version:1.1.20180109
    /// *Sinopsis:
    /// </summary>
    public static partial class THelperValidate
    {
        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="nValue"></param>
        /// <returns></returns>
        public static bool TIsEven(this int nValue) => (nValue % 2 == 0);

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="nValue"></param>
        /// <returns></returns>
        public static bool TIsEven(this long nValue) => (nValue % 2 == 0);

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="nValue"></param>
        /// <returns></returns>
        public static bool TIsEven(this float nValue) => (nValue % 2 == 0);

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="nValue"></param>
        /// <returns></returns>
        public static bool TIsEven(this decimal nValue) => (nValue % 2 == 0);

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="nValue"></param>
        /// <returns></returns>
        public static bool TIsEven(this int? nValue) => (nValue == null) ? false : (nValue % 2 == 0);

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="nValue"></param>
        /// <returns></returns>
        public static bool TIsEven(this long? nValue) => (nValue == null) ? false : (nValue % 2 == 0);

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="nValue"></param>
        /// <returns></returns>
        public static bool TIsEven(this float? nValue) => (nValue == null) ? false : (nValue % 2 == 0);

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="nValue"></param>
        /// <returns></returns>
        public static bool TIsEven(this decimal? nValue) => (nValue == null) ? false : (nValue % 2 == 0);


    }

}
