﻿
using System;
using System.Globalization;

namespace TotalApp.Package
{

    /// <summary>
    /// *Autor:Larry Ramirez
    /// *Fecha de creacion:09-01-2018
    /// *Version:1.1.20180109
    /// *Sinopsis:
    /// </summary>
    public static partial class THelperValidate
    {
        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sValue"></param>
        /// <param name="sFormat"></param>
        /// <returns></returns>
        public static bool TIsDateTime(this string sValue, string sFormat)
        {
            if (sValue.TIsNullOrEmpty())
            {
                return false;
            }

            if (sFormat.TIsNullOrEmpty())
            {
                return false;
            }

            DateTime dtVal = default(DateTime);
            return DateTime.TryParseExact(sValue, sFormat, CultureInfo.InvariantCulture, DateTimeStyles.None, out dtVal);
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sValue"></param>
        /// <returns></returns>
        public static bool TIsNull(this DateTime? nValue) => (nValue == null);


        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="dtFecha"></param>
        /// <param name="dtFechaIni"></param>
        /// <param name="dtFechaFin"></param>
        /// <returns></returns>
        public static bool TIsBetween(this DateTime dtFecha
                                    , DateTime dtFechaIni
                                    , DateTime dtFechaFin) => (dtFecha >= dtFechaIni && dtFecha <= dtFechaFin);
    }

}
