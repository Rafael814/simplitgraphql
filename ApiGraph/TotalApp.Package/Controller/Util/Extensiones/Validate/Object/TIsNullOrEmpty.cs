﻿
using System.Collections.Generic;
using System.IO;
using TotalApp.Package.TLINQ;

namespace TotalApp.Package
{

    /// <summary>
    /// *Autor:Larry Ramirez
    /// *Fecha de creacion:09-01-2018
    /// *Version:1.1.20180109
    /// *Sinopsis:
    /// </summary>
    public static partial class THelperValidate
    {

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static bool TIsNull(this object obj) => (obj == null);

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sValue"></param>
        /// <returns></returns>
        public static bool TIsEmpty(this string sValue) => sValue.Trim().Length == 0;



        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="oStValue"></param>
        /// <returns></returns>
        public static bool TValidarStream(this Stream oStValue)
        {

                if(oStValue == null)
                {
                    return false;
                }

                if (oStValue.CanRead == false)
                {
                    return false;
                }

            return true;
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="oStValue"></param>
        /// <returns></returns>
        public static bool TCloseStream(this Stream oStValue)
        {
            try
            {
                if (oStValue != null)
                {
                    //oStValue.Flush();
                    oStValue.Dispose();
                    oStValue.Close();
                    oStValue = null;
                }

            }
            catch
            {

            }

            return true;
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="oSReader"></param>
        /// <returns></returns>
        public static bool TCloseReader(this StreamReader oSReader)
        {
            
            try
            {
                if (oSReader != null)
                {
                    //oSReader.BaseStream.Flush();
                    oSReader.Dispose();
                    oSReader.Close();
                    oSReader = null;
                }

            }
            catch
            {

            }

            return true;
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="oStValue"></param>
        /// <param name="oSReader"></param>
        /// <returns></returns>
        public static bool TCloseStreamMix(this Stream oStValue
                                         , StreamReader oSReader)
        {
            oStValue.TCloseStream();
            oSReader.TCloseReader();

            return true;
        }


        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sValue"></param>
        /// <returns></returns>
        public static bool TIsNullOrEmpty(this string sValue) => (sValue == null || sValue.Trim().Length == 0);

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="oData"></param>
        /// <returns></returns>
        public static bool TIsNullOrEmpty<T>(this IEnumerable<T> oData) => (oData == null || !oData.TAny());

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="oData"></param>
        /// <returns></returns>
        public static bool THasValue<T>(this IEnumerable<T> oData) => (oData != null && oData.TAny());

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="oData"></param>
        /// <param name="bIsValidarPrimeratabla"></param>
        /// <returns></returns>
        public static bool TIsNullOrEmptyDataSet(this System.Data.DataSet oData, bool bIsValidarPrimeratabla = false)
        {
            if (oData == null)
            {
                return true;
            }
            else if (oData.Tables.Count == 0)
            {
                return true;
            }
            else
            {
                if (bIsValidarPrimeratabla)
                {
                    return TIsNullOrEmptyTable(oData.Tables[0]);
                }
            }

            return false;
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="oData"></param>
        /// <returns></returns>
        public static bool TIsNullOrEmptyTable(this System.Data.DataTable oData) => (oData == null || oData.Rows.Count == 0);

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj"></param>
        /// <param name="oResult"></param>
        /// <returns></returns>
        public static bool TTryCast<T>(this object obj, out T oResult)
        {
            oResult = default(T);
            if (obj is T)
            {
                oResult = (T)obj;
                return true;
            }

            // If it's null, we can't get the type.
            if (obj != null)
            {
                var oConverter = System.ComponentModel.TypeDescriptor.GetConverter(typeof(T));
                if (oConverter.CanConvertFrom(obj.GetType()))
                {
                    oResult = (T)oConverter.ConvertFrom(obj);
                }
                else
                {
                    return false;
                }


                return true;
            }

            return !typeof(T).IsValueType;
        }

    }

}
