﻿using System.Text.RegularExpressions;

namespace TotalApp.Package
{

    /// <summary>
    /// *Autor:Larry Ramirez
    /// *Fecha de creacion:09-01-2018
    /// *Version:1.1.20180109
    /// *Sinopsis:
    /// </summary>
    public static partial class THelperValidate
    {

        #region "Variables Lock"
        private static readonly object _oObjectLockValid = new object();
        #endregion

      
        #region "Funciones"

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sValue"></param>
        /// <param name="sExp"></param>
        /// <returns></returns>
        public static bool TIsValid(this string sValue, string sExp)
        {
            lock (_oObjectLockValid)
            {
                if (sValue.TIsNullOrEmpty())
                {
                    return false;
                }

                if (sExp.TIsNullOrEmpty())
                {
                    return false;
                }

                sExp = sExp.Replace(@"\\", @"\");
                sExp = sExp.Replace(@"//", @"/");

                if (Regex.IsMatch(sValue, sExp))
                {
                    if (Regex.Replace(sValue, sExp, THelper.Empty).Length == 0)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }

                return false;
            }
        }


        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sValue"></param>
        /// <param name="sExp"></param>
        /// <returns></returns>
        public static bool TIsCedula(this string sValue, string sExp = THelper.ExpRCedula) => TIsValid(sValue.ToUpper(), sExp);


        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sValue"></param>
        /// <param name="sExp"></param>
        /// <returns></returns>
        public static bool TIsRif(this string sValue, string sExp = THelper.ExpRRif) => TIsValid(sValue.ToUpper(), sExp);

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sValue"></param>
        /// <param name="sExp"></param>
        /// <returns></returns>
        public static bool TIsUrl(this string sValue, string sExp = THelper.ExpRUrl) => TIsValid(sValue.ToUpper(), sExp);

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sValue"></param>
        /// <param name="sExp"></param>
        /// <returns></returns>
        public static bool TIsDirIP(this string sValue, string sExp = THelper.ExpRDirIp) => TIsValid(sValue.ToUpper(), sExp);

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sValue"></param>
        /// <param name="sExp"></param>
        /// <returns></returns>
        public static bool TIsCodPostal(this string sValue, string sExp = THelper.ExpRCodPostal) => TIsValid(sValue.ToUpper(), sExp);

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sValue"></param>
        /// <param name="sExp"></param>
        /// <returns></returns>
        public static bool TIsMail(this string sValue, string sExp = THelper.ExpRMail) => TIsValid(sValue.ToUpper(), sExp);

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sValue"></param>
        /// <param name="sExp"></param>
        /// <returns></returns>
        public static bool TIsNumeric(this string sValue, string sExp = THelper.ExpRNumerico) => TIsValid(sValue.ToUpper(), sExp);

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sValue"></param>
        /// <param name="sExp"></param>
        /// <returns></returns>
        public static bool TIsTelephoneNumber(this string sValue, string sExp = THelper.ExpRTelefonico) => TIsValid(sValue.ToUpper(), sExp);

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sValue"></param>
        /// <param name="sExp"></param>
        /// <returns></returns>
        public static bool TIsAlpha(this string sValue, string sExp = THelper.ExpRLetras) => TIsValid(sValue.ToUpper(), sExp);

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sValue"></param>
        /// <param name="sExp"></param>
        /// <returns></returns>
        public static bool TIsAlphaNumeric(this string sValue, string sExp = THelper.ExpRAlfaNumerico) => TIsValid(sValue.ToUpper(), sExp);

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sValue"></param>
        /// <param name="sExp"></param>
        /// <returns></returns>
        public static bool TIsPositive(this string sValue, string sExp = THelper.ExpRNumericoPositivo) => TIsValid(sValue.ToUpper(), sExp);

        #endregion

       
    }



}
