﻿using System;
using System.Collections.Generic;
using System.Data;

namespace TotalApp.Package
{
    /// <summary>
    /// *Autor:Larry Ramirez
    /// *Fecha de creacion:09-01-2018
    /// *Version:1.1.20180109
    /// *Sinopsis:
    /// </summary>
    public static partial class THelperConvert
    {
        #region "Constantes"
        #endregion

        #region "Propiedades"
        #endregion

        #region "Funciones"

        #region "Conversiones numericas"

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sValue"></param>
        /// <param name="nDefault"></param>
        /// <returns></returns>
        public static Int16 TToShortOrDefault(this string sValue, Int16 nDefault = 0)
        {
            if (sValue.TIsNullOrEmpty())
            {
                return 0;
            }

            Int16 nNumber;
            Int16.TryParse(sValue
                          , out nNumber);

            return (nNumber = nNumber == 0 ? nDefault : nNumber);
        }


        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sValue"></param>
        /// <param name="nDefault"></param>
        /// <returns></returns>
        public static Int32 TToIntOrDefault(this string sValue, Int32 nDefault = 0)
        {
            if (sValue.TIsNullOrEmpty())
            {
                return 0;
            }

            int nNumber;
            Int32.TryParse(sValue
                          , out nNumber);

            return (nNumber = nNumber == 0 ? nDefault : nNumber);
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sValue"></param>
        /// <returns></returns>
        public static Int64 TToLongOrDefault(this string sValue, Int64 nDefault = 0)
        {

            if (sValue.TIsNullOrEmpty())
            {
                return 0;
            }

            Int64 nNumber;
            Int64.TryParse(sValue
                          , out nNumber);

            return (nNumber = nNumber == 0 ? nDefault : nNumber);
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sValue"></param>
        /// <returns></returns>
        public static decimal TToDecimalOrDefault(this string sValue, Decimal nDefault = 0)
        {
            if (sValue.TIsNullOrEmpty())
            {
                return 0;
            }

            decimal nNumber;
            decimal.TryParse(sValue
                          , out nNumber);

            return (nNumber = nNumber == 0 ? nDefault : nNumber);

        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sValue"></param>
        /// <returns></returns>
        public static double TToDoubleOrDefault(this string sValue, double nDefault = 0)
        {
            if (sValue.TIsNullOrEmpty())
            {
                return 0;
            }

            double nNumber;
            double.TryParse(sValue
                          , out nNumber);

            return (nNumber = nNumber == 0 ? nDefault : nNumber);
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sValue"></param>
        /// <returns></returns>
        public static float TToFloatOrDefault(this string sValue, float nDefault = 0)
        {
            if (sValue.TIsNullOrEmpty())
            {
                return 0;
            }

            float nNumber;
            float.TryParse(sValue
                          , out nNumber);

            return (nNumber = nNumber == 0 ? nDefault : nNumber);
        }


        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sValue"></param>
        /// <param name="dtDefault"></param>
        /// <returns></returns>
        public static DateTime TToDatetimeOrDefault(this string sValue, DateTime dtDefault = default(DateTime))
        {
            if (sValue.TIsNullOrEmpty()) THelperInfoEstatus.ArgumentNull(nameof(sValue));

            DateTime dtFH;
            DateTime.TryParse(sValue, out dtFH);

            return (dtFH == null ? dtDefault : dtFH);
        }
        #endregion

        #region "Conversiones a objetos"
        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static T TConvertTo<T>(this object obj) => (T)Convert.ChangeType(obj, typeof(T));

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="oTabla"></param>
        /// <returns></returns>
        public static T TDataTableToObjectOrDefault<T>(this DataTable oTabla) where T : class => (oTabla.TIsNullOrEmptyTable()) ? default(T) : oTabla.TDeserializeObjectOrDefault<T>();


        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="oData"></param>
        /// <param name="nIndexTable"></param>
        /// <returns></returns>
        public static T TDataTableToObjectOrDefault<T>(this DataSet oData, short nIndexTable = 0) where T : class => (oData.Tables[nIndexTable].TIsNullOrEmptyTable()) ? default(T) : oData.Tables[nIndexTable].TDeserializeObjectOrDefault<T>();


        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="oTabla"></param>
        /// <returns></returns>
        public static List<T> TDataTableToListOrDefault<T>(this DataTable oTabla) where T : class => (oTabla.TIsNullOrEmptyTable()) ? default(List<T>) : oTabla.TDeserializeListObjectOrDefault<T>();


        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="oData"></param>
        /// <param name="nIndexTable"></param>
        /// <returns></returns>
        public static List<T> TDataTableToListOrDefault<T>(this DataSet oData, short nIndexTable = 0) where T : class => (oData.Tables[nIndexTable].TIsNullOrEmptyTable()) ? default(List<T>) : oData.Tables[nIndexTable].TDeserializeListObjectOrDefault<T>();


        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="oData"></param>
        /// <returns></returns>
        public static DataTable TListToDataTableOrDefault<T>(this IEnumerable<T> oData) where T : class => (oData.TIsNullOrEmpty()) ? default(DataTable) : oData.TDeserializeObjectOrDefault<DataTable>();

        #endregion

        #region "Medidores"

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="oBytes"></param>
        /// <returns></returns>
        public static double TBytesToMegabytes(long oBytes) => (oBytes / 1024f) / 1024f;


        // <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="oMegaBytes"></param>
        /// <returns></returns>
        public static double TMegabytesToGigabytes(double oMegaBytes) => oMegaBytes / 1024.0;


        // <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="oMegaBytes"></param>
        /// <returns></returns>
        public static double TMegabytesToTerabytes(double oMegaBytes) => oMegaBytes / (1024.0 * 1024.0);


        // <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="oGigaBytes"></param>
        /// <returns></returns>
        public static double TGigabytesToMegabytes(double oGigaBytes) => oGigaBytes * 1024.0;


        // <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="oGigaBytes"></param>
        /// <returns></returns>
        public static double TGigabytesToTerabytes(double oGigaBytes) => oGigaBytes / 1024.0;


        // <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="oTeraBytes"></param>
        /// <returns></returns>
        public static double TTerabytesToMegabytes(double oTeraBytes) => oTeraBytes * (1024.0 * 1024.0);


        // <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="oTeraBytes"></param>
        /// <returns></returns>
        public static double TTerabytesToGigabytes(double oTeraBytes) => oTeraBytes * 1024.0;

        #endregion
        #endregion

        #region "Metodos & constructores"
        #endregion

        #region "Eventos"
        #endregion

        #region "Destructor"
        #endregion
    }
}
