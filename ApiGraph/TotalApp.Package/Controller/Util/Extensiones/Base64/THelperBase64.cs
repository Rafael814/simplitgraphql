﻿using System;
using System.Text;
using System.IO;
using System.Threading.Tasks;
using System.Drawing;

namespace TotalApp.Package
{
    /// <summary>
    /// *Autor:Larry Ramirez
    /// *Fecha de creacion:09-01-2018
    /// *Version:1.1.20180109
    /// *Sinopsis:
    /// </summary>
    public static class THelperBase64
    {
        #region "Constantes"
        #endregion

        #region "Variables"
        #endregion

        #region "Propiedades"
        #endregion

        #region "Funciones"

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sB64"></param>
        /// <param name="sFilePath"></param>
        /// <returns></returns>
        public static bool TBase64ToSaveImage(string sB64, string sFilePath)
        {

            try
            {

                byte[] oImgBytes = Convert.FromBase64String(sB64);

                using (var oImgFile = new FileStream(sFilePath, FileMode.Create))
                {
                    oImgFile.Write(oImgBytes, 0, oImgBytes.Length);
                    oImgFile.Flush();
                }

                return true;
            }
            catch (Exception e)
            {
                return false;
            }

        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sRuta"></param>
        /// <returns></returns>
        public static string TBase64StringFile(this string sRuta) => sRuta.TIsNullOrEmpty() 
                                                                  ? THelper.Empty 
                                                                  : Convert.ToBase64String(File.ReadAllBytes(sRuta));



        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sRuta"></param>
        /// <returns></returns>
        public static string TBase64StringFileAsync(this string sRuta)
        {
            var oTask = new Task<string>(() => {
                return TBase64StringFile(sRuta);
            });
            oTask.Start();

            return oTask.Result;
        }


        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sCadena"></param>
        /// <returns></returns>
        public static string TBase64EncodeOrDefault(this string sCadena)
        {
            if (sCadena.TIsNullOrEmpty())
            {
                return THelper.Empty;
            }

            byte[] byCadenaByte = new byte[sCadena.Length];
            byCadenaByte = Encoding.UTF8.GetBytes(sCadena);
            return Convert.ToBase64String(byCadenaByte);

        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sCadena"></param>
        /// <returns></returns>
        public static string TBase64EncodeOrDefaultAsync(this string sCadena)
        {

            var oTask = new Task<string>(() => {
                return TBase64EncodeOrDefaultAsync(sCadena);
            });
            oTask.Start();

            return oTask.Result;
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sCadena"></param>
        /// <returns></returns>
        public static string TBase64DecodeOrDefault(this string sCadena)
        {
            if (sCadena.TIsNullOrEmpty())
            {
                return THelper.Empty;
            }

            var oEncoder = new System.Text.UTF8Encoding();
            var oUtfDecode = oEncoder.GetDecoder();

            byte[] byCadenaByte = Convert.FromBase64String(sCadena);
            int ncharCount = oUtfDecode.GetCharCount(byCadenaByte, 0, byCadenaByte.Length);
            char[] cDecodedChar = new char[ncharCount];
            oUtfDecode.GetChars(byCadenaByte, 0, byCadenaByte.Length, cDecodedChar, 0);
            return new string(cDecodedChar);
        }


        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sCadena"></param>
        /// <returns></returns>
        public static string TBase64DecodeOrDefaultAsync(this string sCadena)
        {
            var oTask = new Task<string>(() => {
                return TBase64DecodeOrDefault(sCadena);
            });
            oTask.Start();

            return oTask.Result;
        }
        #endregion

        #region "Metodos & constructores"
        #endregion

        #region "Eventos"
        #endregion

        #region "Destructor"
        #endregion
    }
}
