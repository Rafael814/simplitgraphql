﻿using System;
using System.Collections.Generic;
using System.Xml;
using Newtonsoft.Json;

namespace TotalApp.Package
{
    public static class THelperXml
    {
        #region "Constantes"
        #endregion

        #region "Variables"

        #region "Variables Lock"
        private static readonly object _oDeserializeXmlObject = new object();
        private static readonly object _oDeserializeXmlListObject = new object();
        private static readonly object _oSerializeXmlObject = new object();
        #endregion

        #endregion

        #region "Propiedades"
        #endregion

        #region "Funciones"

        #region "Serializar"
        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sXml"></param>
        /// <returns></returns>
        public static String TSerializeXmlToJsonOrDefault(this String sXml)
        {
            lock (_oSerializeXmlObject)
            {
                if (!sXml.TIsNullOrEmpty())
                {
                    XmlDocument oObj = new XmlDocument();
                    oObj.LoadXml(sXml);
                    return JsonConvert.SerializeXmlNode(oObj, Newtonsoft.Json.Formatting.None, true);
                }
                else
                {
                    return THelper.Empty;
                }

            }
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="oObj"></param>
        /// <returns></returns>
        public static String TSerializeXmlToJsonOrDefault(this XmlDocument oObj)
        {
            lock (_oSerializeXmlObject)
            {
                if (!oObj.TIsNull())
                {
                    return JsonConvert.SerializeXmlNode(oObj, Newtonsoft.Json.Formatting.None, true);
                }
                else
                {
                    return THelper.Empty;
                }
            }
        }
        #endregion

        #region "Deserializar"
        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="oObj"></param>
        /// <returns></returns>
        public static T TDeserializeObjectXmlToJsonOrDefault<T>(this XmlDocument oObj) where T : class
        {
            lock (_oDeserializeXmlObject)
            {
                return !oObj.TIsNull() ? JsonConvert.DeserializeObject<T>(oObj.TSerializeXmlToJsonOrDefault()) : default(T);
            }
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="sXml"></param>
        /// <returns></returns>
        public static T TDeserializeObjectXmlToJsonOrDefault<T>(this string sXml) where T : class
        {
            lock (_oDeserializeXmlObject)
            {
                return !sXml.TIsNullOrEmpty() ? JsonConvert.DeserializeObject<T>(sXml.TSerializeXmlToJsonOrDefault()) : default(T);
            }
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="oObj"></param>
        /// <returns></returns>
        public static List<T> TDeserializeListObjectXmlToJsonOrDefault<T>(this XmlDocument oObj) where T : class
        {
            lock (_oDeserializeXmlObject)
            {
                return !oObj.TIsNull() ? JsonConvert.DeserializeObject<List<T>>(oObj.TSerializeXmlToJsonOrDefault()) : default(List<T>);
            }
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="sXml"></param>
        /// <returns></returns>
        public static List<T> TDeserializeListObjectXmlToJsonOrDefault<T>(this string sXml) where T : class
        {
            lock (_oDeserializeXmlObject)
            {
                return !sXml.TIsNullOrEmpty() ? JsonConvert.DeserializeObject<List<T>>(sXml.TSerializeXmlToJsonOrDefault()) : default(List<T>);
            }
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sJson"></param>
        /// <param name="sRoot"></param>
        /// <returns></returns>
        public static XmlDocument TDeserializeObjectJsonToXmlOrDefault(this String sJson, string sRoot = THelper.Empty)
        {
            lock (_oDeserializeXmlObject)
            {
                if (!sJson.TIsNullOrEmpty())
                {
                    if (!sRoot.TIsNullOrEmpty())
                    {
                        return JsonConvert.DeserializeXmlNode(sJson, sRoot);
                    }
                    else
                    {
                        return JsonConvert.DeserializeXmlNode(sJson);
                    }
                }
                else
                {
                    return default(XmlDocument);
                }
            }

        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="oObj"></param>
        /// <param name="sRoot"></param>
        /// <returns></returns>
        public static XmlDocument TDeserializeObjectJsonToXmlOrDefault(this object oObj, string sRoot = THelper.Empty)
        {
            lock (_oDeserializeXmlObject)
            {
                if (!oObj.TIsNull())
                {
                    if (!sRoot.TIsNullOrEmpty())
                    {
                        return JsonConvert.DeserializeXmlNode(oObj.TSerializeObjectOrDefault(), sRoot);
                    }
                    else
                    {
                        return JsonConvert.DeserializeXmlNode(oObj.TSerializeObjectOrDefault());
                    }
                }
                else
                {
                    return default(XmlDocument);
                }
            }

        }
        #endregion

        #endregion

        #region "Metodos & constructores"
        #endregion

        #region "Eventos"
        #endregion

        #region "Destructor"
        #endregion
    }
}
