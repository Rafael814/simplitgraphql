﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace TotalApp.Package
{
    /// <summary>
    /// *Autor:Larry Ramirez
    /// *Fecha de creacion:09-01-2018
    /// *Version:1.1.20180109
    /// *Sinopsis:
    /// </summary>
    public static class THelperJson
    {
        #region "Constantes"
        #endregion

        #region "Variables"

        #region "Variables Lock"
        private static readonly object _oDeserializeJsonObject = new object();
        private static readonly object _oDeserializeJsonListObject = new object();
        private static readonly object _oSerializeJsonObject = new object();
        #endregion

        #endregion

        #region "Propiedades"
        #endregion

        #region "Funciones"

        #region "Serializar"
        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="oObj"></param>
        /// <param name="sFormatDate"></param>
        /// <returns></returns>
        public static String TSerializeObjectOrDefault(this object oObj, string sFormatDate = "yyyy-MM-dd HH:mm:ss")
        {
            lock (_oSerializeJsonObject)
            {
                //return JsonConvert.SerializeObject(oObj,
                //                         Newtonsoft.Json.Formatting.None,
                //                         new JsonSerializerSettings { NullValueHandling = NullValueHandling.Include, DefaultValueHandling = DefaultValueHandling.Include, StringEscapeHandling = StringEscapeHandling.EscapeHtml });

                //return JsonConvert.SerializeObject(oObj
                //                                   , Newtonsoft.Json.Formatting.None
                //                                   , new IsoDateTimeConverter() { DateTimeFormat = "yyyy-MM-dd HH:mm:ss" }
                //     
                
                return !oObj.TIsNull() ? JsonConvert.SerializeObject(oObj
                                                                   , Newtonsoft.Json.Formatting.None
                                                                   , new JsonSerializerSettings
                                                                   {
                                                                      NullValueHandling = NullValueHandling.Include
                                                                                              ,
                                                                      DateFormatString = sFormatDate
                                                                                              ,
                                                                      DefaultValueHandling = DefaultValueHandling.Include
                                                                                              ,
                                                                      StringEscapeHandling = StringEscapeHandling.EscapeHtml
                                                                   }
                                                                   )
                                                                   : default(string);

            }
        }
        #endregion

        #region "Deserializar"
        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="sJson"></param>
        /// <returns></returns>
        public static T TDeserializeObjectOrDefault<T>(this String sJson) where T : class
        {
            lock (_oDeserializeJsonObject)
            {
                return !sJson.TIsNullOrEmpty() ? JsonConvert.DeserializeObject<T>(sJson) : default(T);
            }
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="sJson"></param>
        /// <returns></returns>
        public static T TDeserializeObjectAnonymousOrDefault<T>(this string sJson,T objAnonymous) 
        {
            lock (_oDeserializeJsonObject)
            {
                return !sJson.TIsNullOrEmpty() ? JsonConvert.DeserializeAnonymousType(sJson, objAnonymous) : default(T);
            }
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="oObj"></param>
        /// <param name="bIsUnicoReg"></param>
        /// <returns></returns>
        public static T TDeserializeObjectAnonymousOrDefault<T>(this object oObj, T objAnonymous, bool bIsUnicoReg = false) 
        {
            lock (_oDeserializeJsonObject)
            {
                if (!bIsUnicoReg)
                {
                    return !oObj.TIsNull() ? JsonConvert.DeserializeAnonymousType(oObj.TSerializeObjectOrDefault(), objAnonymous) : default(T);
                }
                else
                {
                    return !oObj.TIsNull() ? JsonConvert.DeserializeAnonymousType(oObj.TSerializeObjectOrDefault().Replace("[", "").Replace("]", ""), objAnonymous) : default(T);
                }
            }
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="oObj"></param>
        /// <param name="bIsUnicoReg"></param>
        /// <returns></returns>
        public static T TDeserializeObjectOrDefault<T>(this object oObj, bool bIsUnicoReg = false) where T : class
        {
            lock (_oDeserializeJsonObject)
            {
                if (!bIsUnicoReg)
                {
                    return !oObj.TIsNull() ? JsonConvert.DeserializeObject<T>(oObj.TSerializeObjectOrDefault()) : default(T);
                }
                else
                {
                    return !oObj.TIsNull() ? JsonConvert.DeserializeObject<T>(oObj.TSerializeObjectOrDefault().Replace("[", "").Replace("]", "")) : default(T);
                }
            }
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="sJson"></param>
        /// <returns></returns>
        public static List<T> TDeserializeListObjectOrDefault<T>(this String sJson) where T : class
        {
            lock (_oDeserializeJsonListObject)
            {
                return !sJson.TIsNullOrEmpty() ? JsonConvert.DeserializeObject<List<T>>(sJson) : default(List<T>);
            }
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="oObj"></param>
        /// <returns></returns>
        public static List<T> TDeserializeListObjectOrDefault<T>(this object oObj) where T : class
        {
            lock (_oDeserializeJsonListObject)
            {
                return !oObj.TIsNull() ? JsonConvert.DeserializeObject<List<T>>(oObj.TSerializeObjectOrDefault()) : default(List<T>);
            }
        }

        #endregion

        #endregion

        #region "Metodos & constructores"
        #endregion

        #region "Eventos"
        #endregion

        #region "Destructor"
        #endregion
    }
}
