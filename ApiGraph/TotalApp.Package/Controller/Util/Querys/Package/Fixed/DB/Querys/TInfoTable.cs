﻿


namespace TotalApp.Package
{
    /// <summary>
    /// *Autor:Larry Ramirez
    /// *Fecha de creacion:09-01-2018
    /// *Version:1.1.20180109
    /// *Sinopsis:Clase de tipo static, querys genericos que se utilizan o podrian ser utilizados en el portafolio de TotalAplicaciones
    /// </summary>
    public static partial class THelperQuerys
    {
        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:Query que permite extraer las informacion de las tablas de una base de datos
        /// </summary>
        public static string GetListInfoTable => @"
                         SELECT [Tb].[object_id]   AS [Id]
						       ,[Tb].[name]        AS [Table]
						    FROM sys.tables AS [Tb]
						  

        ";



        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:Query que permite extraer las informacion de las tablas de una base de datos
        /// </summary>
        public static string GetInfoTable => @"

                       SELECT [Tb].[object_id]   AS [Id]
						     ,[Tb].[name]        AS [Table]
						 FROM sys.tables AS [Tb]
                        WHERE [Tb].[name] = '@Table'
        ";
    }
}
