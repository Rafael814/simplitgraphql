﻿
namespace TotalApp.Package
{
    /// <summary>
    /// *Autor:Larry Ramirez
    /// *Fecha de creacion:09-01-2018
    /// *Version:1.1.20180109
    /// *Sinopsis:Clase de tipo static, Esta clase permite invovar a las variables o funciones que contienen querys genericos
    /// </summary>
    public static partial class THelperQuerys
    {
        #region "Funciones"

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:Funcion de tipo (Logica de negocio), permite validar el nombre de la Bd y reemplazarlo en el query 
        /// para tomar la infomacion d euna base de datos en especifico
        /// </summary>
        /// <param name="sBd"></param>
        /// <returns></returns>
        public static string InfoBD(string sBd)
        {
            if (sBd.TIsNullOrEmpty()) THelperInfoEstatus.ArgumentNull(nameof(sBd));

            return GetInfoBD.Replace("@BD", sBd);
        }

        #endregion
    }
}
