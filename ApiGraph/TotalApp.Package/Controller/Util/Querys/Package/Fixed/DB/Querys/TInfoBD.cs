﻿


namespace TotalApp.Package
{
    /// <summary>
    /// *Autor:Larry Ramirez
    /// *Fecha de creacion:09-01-2018
    /// *Version:1.1.20180109
    /// *Sinopsis:Clase de tipo static, querys genericos que se utilizan o podrian ser utilizados en el portafolio de TotalAplicaciones
    /// </summary>
    public static partial class THelperQuerys
    {
        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:Query para extraer la info basica de las Bd que existen en la instancia del servidor
        /// </summary>
        public static string GetListBasicInfoBD => @"
                         SELECT [Bd].[database_id] AS [Id]
						       ,[Bd].[name]        AS [Bd]
						    FROM sys.databases AS [Bd]
						   WHERE name  NOT IN('master','tempdb','model','msdb','')
						     AND [Bd].[state] = 0

        ";

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:Query para extraer la info de las Bd que existen en la instancia del servidor
        /// </summary>
        public static string GetListInfoBD => @"
                          SELECT [Bd].[database_id] AS [Id]
						        ,[Bd].[name]        AS [Bd]
                                ,[Est].[database_guid]  AS [GuId]
						    FROM sys.databases AS [Bd]
					  INNER JOIN sys.database_recovery_status AS [Est] ON [Bd].[database_id] = [Est].[database_id]
						   WHERE name  NOT IN('master','tempdb','model','msdb','')
						     AND [Bd].[state] = 0

        ";

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:Query para extraer la info de una Bd
        /// </summary>
        public static string GetInfoBD => @"
                          SELECT [Bd].[database_id] AS [Id]
						        ,[Bd].[name]        AS [Bd]
                                ,[Est].[database_guid]  AS [GuId]
						    FROM sys.databases AS [Bd]
					  INNER JOIN sys.database_recovery_status AS [Est] ON [Bd].[database_id] = [Est].[database_id]
						   WHERE name  NOT IN('master','tempdb','model','msdb','')
						     AND [Bd].[state] = 0
                             AND [Bd].[name] = '@BD'
        ";
    }
}
