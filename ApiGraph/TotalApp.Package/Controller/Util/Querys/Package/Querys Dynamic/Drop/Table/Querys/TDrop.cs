﻿


namespace TotalApp.Package
{
    /// <summary>
    /// *Autor:Larry Ramirez
    /// *Fecha de creacion:09-01-2018
    /// *Version:1.1.20180109
    /// *Sinopsis:
    /// </summary>
    public static partial class THelperQuerys
    {
        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        public static string TranDropTable => @"        
            DROP TABLE @Table
        ";

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        public static string TranDropTableIfExist => @"        
             IF OBJECT_ID(N'@Table',N'U') IS NOT NULL
	         BEGIN
		         DROP TABLE @Table
		     END

        ";
    }
}
