﻿
namespace TotalApp.Package
{
    /// <summary>
    /// *Autor:Larry Ramirez
    /// *Fecha de creacion:09-01-2018
    /// *Version:1.1.20180109
    /// *Sinopsis:
    /// </summary>
    public static partial class THelperQuerys
    {
        #region "Funciones"
        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sTable"></param>
        /// <returns></returns>
        public static string Tran_DropTable(string sTable)
        {
            if (sTable.TIsNullOrEmpty()) THelperInfoEstatus.ArgumentNull(nameof(sTable));

            return TranDropTable.Replace("@Table", sTable);
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sTables"></param>
        /// <returns></returns>
        public static string Tran_DropTable(params string[] sTables)
        {
            if (sTables.TIsNullOrEmpty()) THelperInfoEstatus.ArgumentNull(nameof(sTables));

            var sScript = THelper.Empty;

            foreach (var sTable in sTables)
            {
                sScript += TranDropTable.Replace("@Table", sTable) + "\r\n";
            }

            return sScript;
        }


        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sTable"></param>
        /// <returns></returns>
        public static string Tran_DropTableIfExist(string sTable)
        {
            if (sTable.TIsNullOrEmpty()) THelperInfoEstatus.ArgumentNull(nameof(sTable));

            return TranDropTableIfExist.Replace("@Table", sTable);
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sTables"></param>
        /// <returns></returns>
        public static string Tran_DropTableIfExist(params string[] sTables)
        {
            if (sTables.TIsNullOrEmpty()) THelperInfoEstatus.ArgumentNull(nameof(sTables));

            var sScript = THelper.Empty;

            foreach (var sTable in sTables)
            {
                sScript += TranDropTableIfExist.Replace("@Table", sTable) + "\r\n";
            }

            return sScript;
        }
        #endregion
    }
}
