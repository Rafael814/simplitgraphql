﻿
namespace TotalApp.Package
{
    /// <summary>
    /// *Autor:Larry Ramirez
    /// *Fecha de creacion:09-01-2018
    /// *Version:1.1.20180109
    /// *Sinopsis:
    /// </summary>
    public static partial class THelperQuerys
    {
        #region "Funciones"
        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sFunction"></param>
        /// <returns></returns>
        public static string Tran_DropFunction(string sFunction)
        {
            if (sFunction.TIsNullOrEmpty()) THelperInfoEstatus.ArgumentNull(nameof(sFunction));

            return TranDropFunction.Replace("@Function", sFunction);
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sFunctions"></param>
        /// <returns></returns>
        public static string Tran_DropFunction(params string[] sFunctions)
        {
            if (sFunctions.TIsNullOrEmpty()) THelperInfoEstatus.ArgumentNull(nameof(sFunctions));

            var sScript = THelper.Empty;

            foreach (var sFunction in sFunctions)
            {
                sScript += TranDropFunction.Replace("@Function", sFunction) + "\r\n";
            }

            return sScript;
        }


        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sFunction"></param>
        /// <returns></returns>
        public static string Tran_DropFunctionScalarIfExist(string sFunction)
        {
            if (sFunction.TIsNullOrEmpty()) THelperInfoEstatus.ArgumentNull(nameof(sFunction));

            return TranDropFunctionScalarIfExist.Replace("@Function", sFunction);
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sFunctions"></param>
        /// <returns></returns>
        public static string Tran_DropFunctionScalarIfExist(params string[] sFunctions)
        {
            if (sFunctions.TIsNullOrEmpty()) THelperInfoEstatus.ArgumentNull(nameof(sFunctions));

            var sScript = THelper.Empty;

            foreach (var sFunction in sFunctions)
            {
                sScript += TranDropFunctionScalarIfExist.Replace("@Function", sFunction) + "\r\n";
            }

            return sScript;
        }
        #endregion
    }
}
