﻿
namespace TotalApp.Package
{
    /// <summary>
    /// *Autor:Larry Ramirez
    /// *Fecha de creacion:09-01-2018
    /// *Version:1.1.20180109
    /// *Sinopsis:
    /// </summary>
    public static partial class THelperQuerys
    {
        #region "Funciones"
        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sProcedure"></param>
        /// <returns></returns>
        public static string Tran_DropProcedure(string sProcedure)
        {
            if (sProcedure.TIsNullOrEmpty()) THelperInfoEstatus.ArgumentNull(nameof(sProcedure));

            return TranDropProcedure.Replace("@Procedure", sProcedure);
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sProcedures"></param>
        /// <returns></returns>
        public static string Tran_DropProcedure(params string[] sProcedures)
        {
            if (sProcedures.TIsNullOrEmpty()) THelperInfoEstatus.ArgumentNull(nameof(sProcedures));

            var sScript = THelper.Empty;

            foreach (var sProcedure in sProcedures)
            {
                sScript += TranDropProcedure.Replace("@Procedure", sProcedure) + "\r\n";
            }

            return sScript;
        }


        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sProcedure"></param>
        /// <returns></returns>
        public static string Tran_DropProcedureIfExist(string sProcedure)
        {
            if (sProcedure.TIsNullOrEmpty()) THelperInfoEstatus.ArgumentNull(nameof(sProcedure));

            return TranDropProcedureIfExist.Replace("@Procedure", sProcedure);
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sProcedures"></param>
        /// <returns></returns>
        public static string Tran_DropProcedureIfExist(params string[] sProcedures)
        {
            if (sProcedures.TIsNullOrEmpty()) THelperInfoEstatus.ArgumentNull(nameof(sProcedures));

            var sScript = THelper.Empty;

            foreach (var sProcedure in sProcedures)
            {
                sScript += TranDropProcedureIfExist.Replace("@Procedure", sProcedure) + "\r\n";
            }

            return sScript;
        }
        #endregion
    }
}
