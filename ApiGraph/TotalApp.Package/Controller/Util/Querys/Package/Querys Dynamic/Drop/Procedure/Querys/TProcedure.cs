﻿


namespace TotalApp.Package
{
    /// <summary>
    /// *Autor:Larry Ramirez
    /// *Fecha de creacion:09-01-2018
    /// *Version:1.1.20180109
    /// *Sinopsis:
    /// </summary>
    public static partial class THelperQuerys
    {
        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        public static string TranDropProcedure => @"        
            DROP PROCEDURE @Procedure
        ";

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        public static string TranDropProcedureIfExist => @"        
             IF OBJECT_ID(N'@Procedure',N'P') IS NOT NULL
	         BEGIN
		         DROP PROCEDURE @Procedure
		     END

        ";
    }
}
