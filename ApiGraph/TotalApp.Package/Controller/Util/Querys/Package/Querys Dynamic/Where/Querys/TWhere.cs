﻿


namespace TotalApp.Package
{
    /// <summary>
    /// *Autor:Larry Ramirez
    /// *Fecha de creacion:09-01-2018
    /// *Version:1.1.20180109
    /// *Sinopsis:
    /// </summary>
    public static partial class THelperQuerys
    {
        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        public static string GetSelectAllWhere => @"        
            SELECT * FROM @Tabla WHERE @Where
        ";

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        public static string GetSelectTopAllWhere => @"        
            SELECT TOP @Top * FROM @Tabla WHERE @Where
        ";

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        public static string GetSelectDynamicWhere => @"        
            SELECT @Campos FROM @Tabla WHERE @Where
        ";


        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        public static string GetSelectTopDynamicWhere => @"        
            SELECT TOP @Top @Campos FROM @Tabla WHERE @Where
        ";

    }
}
