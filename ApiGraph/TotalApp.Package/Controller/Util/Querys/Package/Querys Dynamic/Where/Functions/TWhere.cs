﻿
namespace TotalApp.Package
{
    /// <summary>
    /// *Autor:Larry Ramirez
    /// *Fecha de creacion:09-01-2018
    /// *Version:1.1.20180109
    /// *Sinopsis:
    /// </summary>
    public static partial class THelperQuerys
    {
        #region "Funciones"

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sTable"></param>
        /// <param name="sWhere"></param>
        /// <returns></returns>
        public static string SelectAllWhere(string sTable, string sWhere)
        {
            if (sTable.TIsNullOrEmpty()) THelperInfoEstatus.ArgumentNull(nameof(sTable));
            if (sWhere.TIsNullOrEmpty()) THelperInfoEstatus.ArgumentNull(nameof(sWhere));

            string sSql = GetSelectAllWhere;

            sSql = sSql.Replace("@Tabla", sTable);
            sSql = sSql.Replace("@Where", sWhere);

            return sSql;
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sTable"></param>
        /// <param name="nTop"></param>
        /// <param name="sWhere"></param>
        /// <returns></returns>
        public static string SelectTopAllWhere(string sTable, int nTop, string sWhere)
        {
            if (sTable.TIsNullOrEmpty()) THelperInfoEstatus.ArgumentNull(nameof(sTable));
            if (sWhere.TIsNullOrEmpty()) THelperInfoEstatus.ArgumentNull(nameof(sWhere));

            string sSql = GetSelectTopAllWhere;

            sSql = sSql.Replace("@Tabla", sTable);
            sSql = sSql.Replace("@Top", nTop.ToString());
            sSql = sSql.Replace("@Where", sWhere);

            return sSql;
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sTable"></param>
        /// <param name="sWhere"></param>
        /// <param name="sCampos"></param>
        /// <returns></returns>
        public static string SelectDynamicWhere(string sTable, string sWhere, params string[] sCampos)
        {
            if (sTable.TIsNullOrEmpty()) THelperInfoEstatus.ArgumentNull(nameof(sTable));
            if (sCampos.TIsNullOrEmpty()) THelperInfoEstatus.ArgumentNull(nameof(sCampos));

            string sSql = GetSelectDynamicWhere;

            sSql = sSql.Replace("@Tabla", sTable);
            sSql = sSql.Replace("@Campos", THelper.TJoin(",", sCampos));
            sSql = sSql.Replace("@Where", sWhere);

            return sSql;
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sTable"></param>
        /// <param name="nTop"></param>
        /// <param name="sWhere"></param>
        /// <param name="sCampos"></param>
        /// <returns></returns>
        public static string SelectDynamicWhere(string sTable,int nTop, string sWhere, params string[] sCampos)
        {
            if (sTable.TIsNullOrEmpty()) THelperInfoEstatus.ArgumentNull(nameof(sTable));
            if (sCampos.TIsNullOrEmpty()) THelperInfoEstatus.ArgumentNull(nameof(sCampos));

            string sSql = GetSelectTopDynamicWhere;

            sSql = sSql.Replace("@Tabla", sTable);
            sSql = sSql.Replace("@Top", nTop.ToString());
            sSql = sSql.Replace("@Campos", THelper.TJoin(",", sCampos));
            sSql = sSql.Replace("@Where", sWhere);

            return sSql;
        }
        #endregion
    }
}
