﻿
namespace TotalApp.Package
{
    /// <summary>
    /// *Autor:Larry Ramirez
    /// *Fecha de creacion:09-01-2018
    /// *Version:1.1.20180109
    /// *Sinopsis:
    /// </summary>
    public static partial class THelperQuerys
    {
        #region "Funciones"
        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sTable"></param>
        /// <returns></returns>
        public static string Tran_Truncate(string sTable)
        {
            if (sTable.TIsNullOrEmpty()) THelperInfoEstatus.ArgumentNull(nameof(sTable));

            return TranTruncate.Replace("@Table", sTable);
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sTables"></param>
        /// <returns></returns>
        public static string Tran_Truncate(params string[] sTables)
        {
            if (sTables.TIsNullOrEmpty()) THelperInfoEstatus.ArgumentNull(nameof(sTables));

            var sScript = THelper.Empty;

            foreach (var sTable in sTables)
            {
                sScript += TranTruncate.Replace("@Table", sTable) + "\r\n";
            }

            return sScript;
        }
        #endregion
    }
}
