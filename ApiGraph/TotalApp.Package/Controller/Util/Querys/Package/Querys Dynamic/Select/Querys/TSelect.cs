﻿


namespace TotalApp.Package
{
    /// <summary>
    /// *Autor:Larry Ramirez
    /// *Fecha de creacion:09-01-2018
    /// *Version:1.1.20180109
    /// *Sinopsis:
    /// </summary>
    public static partial class THelperQuerys
    {
        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        public static string GetSelectAll => @"        
            SELECT * FROM @Tabla
        ";

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        public static string GetSelectTopAll => @"        
            SELECT TOP @Top * FROM @Tabla
        ";

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        public static string GetSelectDynamic => @"        
            SELECT @Campos FROM @Tabla
        ";


        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        public static string GetSelectTopDynamic => @"        
            SELECT TOP @Top @Campos FROM @Tabla
        ";

    }
}
