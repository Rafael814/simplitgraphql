﻿
namespace TotalApp.Package
{
    /// <summary>
    /// *Autor:Larry Ramirez
    /// *Fecha de creacion:09-01-2018
    /// *Version:1.1.20180109
    /// *Sinopsis:Clase de tipo static, Esta clase permite invovar a las variables o funciones que contienen querys genericos
    /// </summary>
    public static partial class THelperQuerys
    {
        #region "Funciones"
        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sTable"></param>
        /// <returns></returns>
        public static string SelectAll(string sTable)
        {
            if (sTable.TIsNullOrEmpty()) THelperInfoEstatus.ArgumentNull(nameof(sTable));

            return GetSelectAll.Replace("@Tabla", sTable);
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sTable"></param>
        /// <param name="nTop"></param>
        /// <returns></returns>
        /// 
        public static string TopSelectAll(string sTable, int nTop = 1)
        {
            if (sTable.TIsNullOrEmpty()) THelperInfoEstatus.ArgumentNull(nameof(sTable));

            return GetSelectTopAll.Replace("@Tabla", sTable).Replace("@Top", nTop.ToString());
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sTable"></param>
        /// <param name="sCampos"></param>
        /// <returns></returns>
        public static string SelectDynamic(string sTable, params string[] sCampos)
        {
            if (sTable.TIsNullOrEmpty()) THelperInfoEstatus.ArgumentNull(nameof(sTable));
            if (sCampos.TIsNullOrEmpty()) THelperInfoEstatus.ArgumentNull(nameof(sCampos));

            string sSql = GetSelectDynamic;

            sSql = sSql.Replace("@Tabla", sTable);
            sSql = sSql.Replace("@Campos", THelper.TJoin(",", sCampos));

            return sSql;
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sTable"></param>
        /// <param name="nTop"></param>
        /// <param name="sCampos"></param>
        /// <returns></returns>
        public static string TopSelectDynamic(string sTable, int nTop, params string[] sCampos)
        {
            if (sTable.TIsNullOrEmpty()) THelperInfoEstatus.ArgumentNull(nameof(sTable));
            if (sCampos.TIsNullOrEmpty()) THelperInfoEstatus.ArgumentNull(nameof(sCampos));

            string sSql = GetSelectTopDynamic;

            sSql = sSql.Replace("@Tabla", sTable);
            sSql = sSql.Replace("@Top", nTop.ToString());
            sSql = sSql.Replace("@Campos", THelper.TJoin(",", sCampos));

            return sSql;
        }
        #endregion
    }
}
