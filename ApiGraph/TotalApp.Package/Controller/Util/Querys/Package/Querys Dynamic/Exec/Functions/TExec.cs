﻿
namespace TotalApp.Package
{
    /// <summary>
    /// *Autor:Larry Ramirez
    /// *Fecha de creacion:09-01-2018
    /// *Version:1.1.20180109
    /// *Sinopsis:
    /// </summary>
    public static partial class THelperQuerys
    {
        #region "Funciones"
        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sSql"></param>
        /// <returns></returns>
        public static string Tran_ExecSQL(string sSql)
        {
            if (sSql.TIsNullOrEmpty()) THelperInfoEstatus.ArgumentNull(nameof(sSql));

            return TranExecSQL.Replace("@Sql", sSql);
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sSql"></param>
        /// <returns></returns>
        public static string Tran_ExecuteSQL(string sSql)
        {
            if (sSql.TIsNullOrEmpty()) THelperInfoEstatus.ArgumentNull(nameof(sSql));

            return TranExecuteSQL.Replace("@Sql", sSql);
        }
        #endregion
    }
}
