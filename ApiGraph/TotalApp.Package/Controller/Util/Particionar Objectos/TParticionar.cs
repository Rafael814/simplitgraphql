﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using TotalApp.Package.TLINQ;

namespace TotalApp.Package.UTIL
{
    /// <summary>
    /// *Autor:Larry Ramirez
    /// *Fecha de creacion:09-01-2018
    /// *Version:1.1.20180109
    /// *Sinopsis:
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class TParticionar<T> :IDisposable where T : class
    {
        #region "Constantes"
        #endregion

        #region "Variables"
        private int _nTotalDeReg = 0;
        #endregion

        #region "Propiedades"
        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        public int TotalDeReg
        {
            get
            {
                return _nTotalDeReg;
            }
            set
            {
                _nTotalDeReg = value;
            }
        }
        #endregion

        #region "Funciones"

        #region "Generico"
        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="oData"></param>
        /// <param name="nPagina"></param>
        /// <param name="nDividirEnBloquesDe"></param>
        /// <param name="nTotalDePag"></param>
        /// <param name="oPred"></param>
        /// <returns></returns>
        public TListData<T> Paginar(IEnumerable<T> oData
                                 , int nPagina
                                 , int nDividirEnBloquesDe
                                 , int nTotalDePag = 0
                                 , Func<T, bool> oPred = null)
        {

            var oResult = new TListData<T>();

            oResult.TotalPorPagina = nDividirEnBloquesDe;
            oResult.Pag = nPagina + 1;

            oResult.TotalDePag = (nTotalDePag == 0) ? oData.TTotalDePaginas(nDividirEnBloquesDe) : nTotalDePag;

            if (oResult.TotalDePag > 0)
            {
                if (oPred == null)
                {
                    /*
                    oResult.Data = oData
                                  .Skip(nPagina * nDividirEnBloquesDe)
                                  .Take(nDividirEnBloquesDe)
                                  .ToList();
                                  */

                    //var oR = oData.TSkip(10);
                    oResult.Data = oData.TPaginateToList(nPagina * nDividirEnBloquesDe
                                                        , nDividirEnBloquesDe);
                                      
                }
                else
                {
                    /*
                  oResult.Data = oData
                                .Where(oPred)
                                .Skip(nPagina * nDividirEnBloquesDe)
                                .Take(nDividirEnBloquesDe)
                                .ToList();
                                */

                    oResult.Data = oData.TWhereIf(oPred)
                                        .TPaginateToList(nPagina * nDividirEnBloquesDe
                                                      , nDividirEnBloquesDe);

                  
                }

                oResult.TotalEnEstaPag = oResult.Data.Count();
            }

            return oResult;

        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="oData"></param>
        /// <param name="nDividirEn"></param>
        /// <param name="oPred"></param>
        /// <returns></returns>
        public IEnumerable<TListData<T>> ListaDeParticiones(IEnumerable<T> oData
                                                          , int nDividirEn
                                                          , Func<T, bool> oPred = null)
        {
            if (!oData.TIsNullOrEmpty())
            {
                TotalDeReg = oData.Count();
                var nTotalDePaginas = oData.TTotalDePaginas(nDividirEn);

                for (int nPag = 0; nPag < nTotalDePaginas; nPag++)
                {
                    yield return Paginar(oData
                                        , nPag
                                        , nDividirEn
                                        , nTotalDePaginas
                                        , oPred);
                }
            }
        }

        #endregion

        #region "DataSet - Table - DataRow"
        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="oData"></param>
        /// <param name="nPagina"></param>
        /// <param name="nDividirEnBloquesDe"></param>
        /// <param name="nTotalDePag"></param>
        /// <param name="oPred"></param>
        /// <returns></returns>
        public TListData<DataTable> Paginar(DataTable oData
                                        , int nPagina
                                        , int nDividirEnBloquesDe
                                        , int nTotalDePag = 0
                                        , Func<DataRow, bool> oPred = null)
        {

            var oResult = new TListData<DataTable>();

            oResult.TotalPorPagina = nDividirEnBloquesDe;
            oResult.Pag = nPagina + 1;

            if (nTotalDePag == 0)
            {
                oResult.TotalDePag = oData.AsEnumerable().TTotalDePaginas(nDividirEnBloquesDe);
            }
            else
            {
                oResult.TotalDePag = nTotalDePag;
            }

            if (oResult.TotalDePag > 0)
            {
                if (oPred == null)
                {
                    oResult.Data.Add(oData
                                    .AsEnumerable()
                                    .Skip(nPagina * nDividirEnBloquesDe)
                                    .Take(nDividirEnBloquesDe)
                                    .ToList().CopyToDataTable());
                }
                else
                {
                    oResult.Data.Add(oData
                                    .AsEnumerable()
                                    .Where(oPred)
                                    .Skip(nPagina * nDividirEnBloquesDe)
                                    .Take(nDividirEnBloquesDe)
                                    .ToList().CopyToDataTable());
                }

                oResult.TotalEnEstaPag = oResult.Data.Count();
            }

            return oResult;

        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="oData"></param>
        /// <param name="nDividirEn"></param>
        /// <param name="oPred"></param>
        /// <returns></returns>
        public IEnumerable<TListData<DataTable>> ListaDeParticiones(DataTable oData
                                                                 , int nDividirEn
                                                                 , Func<DataRow, bool> oPred = null)
        {
            var oRetorno = new List<TListData<DataTable>>();

            if (oData != null)
            {
                if (oData.AsEnumerable().Any())
                {
                    TotalDeReg = oData.AsEnumerable().Count();
                    var nTotalDePaginas = oData.AsEnumerable().TTotalDePaginas(nDividirEn);

                    for (int nPag = 0; nPag < nTotalDePaginas; nPag++)
                    {
                        var oPagina = Paginar(oData
                                            , nPag
                                            , nDividirEn
                                            , nTotalDePaginas
                                            , oPred);


                        oRetorno.Add(oPagina);
                    }
                }

            }

            return oRetorno;
        }
        #endregion


        #endregion

        #region "Metodos & constructores"
        public TParticionar():base()
        {

        }
        #endregion

        #region "Eventos"
        #endregion

        #region "Destructor"
        /// <summary>
        /// Autor   : Larry Ramirez
        /// Fecha   : 21-05-2016
        /// Sinopsis: Variable Disposed, permite saber de manera general si ya fueron liberados los recursos no administrados a la clase (Objeto).
        /// </summary>
        private bool _Disposed = false;

        // <summary>
        /// Autor   : Larry Ramirez
        /// Fecha   : 25-05-2016
        /// Sinopsis: Metodo que permite Desechar y limpiar objetos.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        // <summary>
        /// Autor   : Larry Ramirez
        /// Fecha   : 25-05-2016
        /// Sinopsis: Logica para limpiar objetos de la memoria.
        /// </summary>
        protected void Dispose(bool bIsDisposing)
        {
            //Si esta en false, significa que se tiene la oportunidad de limpiar los objetos
            if (!this._Disposed)
            {
                //Aqui se definen los objetos que se liberan
                if (bIsDisposing)
                {

                }
            }
            _Disposed = true;
        }

        // <summary>
        /// Autor   : Larry Ramirez
        /// Fecha   : 25-05-2016
        /// Sinopsis: Destructor por defecto.
        /// </summary>
        ~TParticionar()
        {
            Dispose(false);
            GC.SuppressFinalize(this);

        }
        #endregion
    }
}
