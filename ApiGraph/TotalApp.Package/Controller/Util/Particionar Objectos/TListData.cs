﻿using System.Collections.Generic;

namespace TotalApp.Package.UTIL
{
    /// <summary>
    /// *Autor:Larry Ramirez
    /// *Fecha de creacion:09-01-2018
    /// *Version:1.1.20180109
    /// *Sinopsis:
    /// </summary>
    public class TListData<T>: TConfPag where T: class
    {
        #region "Constantes"
        #endregion

        #region "Variables"
        private List<T> _oList = new List<T>();
        #endregion

        #region "Propiedades"
        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        public List<T> Data
        {
            get
            {
                return _oList;
            }
            set
            {
                _oList = value;
            }
        }
        #endregion

        #region "Funciones"
        #endregion

        #region "Metodos & constructores"
        public TListData():base()
        {
          
        }
        #endregion

        #region "Eventos"
        #endregion

        #region "Destructor"
        ~TListData()
        {

        }
        #endregion
    }
}
