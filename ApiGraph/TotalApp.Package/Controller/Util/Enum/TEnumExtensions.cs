﻿using System;
using System.Linq;
using System.Reflection;

namespace TotalApp.Package
{
    /// <summary>
    /// *Autor:Larry Ramirez
    /// *Fecha de creacion:09-01-2018
    /// *Version:1.1.20180109
    /// *Sinopsis:
    /// </summary>
    public static class TEnumExtensions
    {
        #region "Constantes"
        #endregion

        #region "Variables"
        #endregion

        #region "Propiedades"
        #endregion

        #region "Funciones"

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="oValue"></param>
        /// <returns></returns>
        public static string TStringValueAttributeToString(this Enum oValue)
        {
            Type oType = oValue.GetType();
            FieldInfo oFinfo = oType.GetField(oValue.ToString());
            return (oFinfo.GetCustomAttributes(typeof(TStringValueAttribute), false).FirstOrDefault() as TStringValueAttribute).Value;

        }
        #endregion

        #region "Metodos & constructores"
        #endregion

        #region "Eventos"
        #endregion

        #region "Destructor"
        #endregion
    }
}
