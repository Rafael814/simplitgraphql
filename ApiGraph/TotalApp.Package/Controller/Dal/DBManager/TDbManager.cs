﻿using System;
using System.Data;
using System.Collections.Generic;

namespace TotalApp.Package.DAL
{
    /// <summary>
    /// *Autor:Larry Ramirez
    /// *Fecha de creacion:09-01-2018
    /// *Version:1.1.20180109
    /// *Sinopsis:
    /// </summary>
    public sealed class TDbManager : ITDbManager, IDisposable
    {
        #region "Variables"
        private IDbConnection _oIdbConnection;
        private IDataReader _oIDataReader;
        private IDbCommand _oIDbCommand;
        private DataProvider _oProviderType;
        private IDbTransaction _oIDbTransaction = null;
        private IDbDataParameter[] _oIDbParameters = null;
        private string _sConnectionString = string.Empty;
        private int _nTimeOut = 0;
        #endregion

        #region "Propiedades"
        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        public IDbConnection Connection
        {
            get
            {
                return _oIdbConnection;
            }
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        public IDataReader DataReader
        {
            get
            {
                return _oIDataReader;
            }
            set
            {
                _oIDataReader = value;
            }
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        public DataProvider ProviderType
        {
            get
            {
                return _oProviderType;

            }
            set
            {
                _oProviderType = value;

            }
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        public string ConnectionString
        {
            get
            {
                return _sConnectionString;

            }
            set
            {
                _sConnectionString = value;
            }
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        public IDbCommand Command
        {
            get
            {
                return _oIDbCommand;
            }
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        public IDbTransaction Transaction
        {
            get
            {
                return _oIDbTransaction;
            }
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        public IDbDataParameter[] Parameters
        {
            get
            {
                return _oIDbParameters;
            }
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        public string Exception { get; private set; }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        public int Error { get; private set; }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        public int TimeOut
        {
            get
            {
                return _nTimeOut;

            }
            set
            {
                _nTimeOut = value;
            }
        }

        #endregion

        #region "Eventos"
        #endregion

        #region "Funciones"

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sCommandText"></param>
        /// <param name="sCommandType"></param>
        /// <returns></returns>
        public IDataReader ExecuteReader(string sCommandText
                                        , CommandType sCommandType)
        {
            try
            {
                this._oIDbCommand = TDbManagerFactory.GetCommand(this.ProviderType);
                _oIDbCommand.Connection = this.Connection;
                PrepareCommand(_oIDbCommand,
                               this.Connection,
                               this.Transaction,
                               sCommandType,
                               sCommandText,
                               this.Parameters);
                this.DataReader = _oIDbCommand.ExecuteReader();
                _oIDbCommand.Parameters.Clear();

            }
            catch (Exception ex)
            {
                Error = 1;
                Exception = ex.Message;
            }

            return this.DataReader;
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sCommandText"></param>
        /// <param name="oCommandType"></param>
        /// <returns></returns>
        public int ExecuteNonQuery(CommandType oCommandType
                                  , string sCommandText)
        {
            int nReturnValue = -1;
            try
            {
                this._oIDbCommand = TDbManagerFactory.GetCommand(this.ProviderType);
              
                PrepareCommand(_oIDbCommand
                             , this.Connection
                             , this.Transaction
                             , oCommandType
                             , sCommandText
                             , this.Parameters);

                nReturnValue = _oIDbCommand.ExecuteNonQuery();
                _oIDbCommand.Parameters.Clear();
            }
            catch (Exception ex)
            {
                Error = 1;
                Exception = ex.Message;
            }

            return nReturnValue;
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="oCommandType"></param>
        /// <param name="sCommandText"></param>
        /// <returns></returns>
        public object ExecuteScalar(CommandType oCommandType
                                   , string sCommandText)
        {
            object oReturnValue = -1;
            try
            {
                this._oIDbCommand = TDbManagerFactory.GetCommand(this.ProviderType);

                PrepareCommand(_oIDbCommand,
                               this.Connection,
                               this.Transaction,
                               oCommandType,
                               sCommandText,
                               this.Parameters);
                oReturnValue = _oIDbCommand.ExecuteScalar();
                _oIDbCommand.Parameters.Clear();
            }
            catch (Exception ex)
            {
                Error = 1;
                Exception = ex.Message;
            }

            return oReturnValue;
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="oCommandType"></param>
        /// <param name="sCommandText"></param>
        /// <returns></returns>
        public DataSet ExecuteDataSet(CommandType oCommandType
                                     , string sCommandText)
        {
            DataSet oData = new DataSet();
            try
            {
                this._oIDbCommand = TDbManagerFactory.GetCommand(this.ProviderType);

                PrepareCommand(_oIDbCommand,
                               this.Connection,
                               this.Transaction,
                               oCommandType,
                               sCommandText,
                               this.Parameters);

                IDbDataAdapter oDataAdapter = TDbManagerFactory.GetDataAdapter(this.ProviderType);
                oDataAdapter.SelectCommand = _oIDbCommand;

                oDataAdapter.Fill(oData);
                _oIDbCommand.Parameters.Clear();
            }
            catch (Exception ex)
            {
                Error = 1;
                Exception = ex.Message;
            }
            return oData;
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="oCommandType"></param>
        /// <param name="sCommandText"></param>
        /// <returns></returns>
        public List<T> ExecuteList<T>(CommandType oCommandType
                                     , string sCommandText) where T : class
        {
            try
            {
                var oData = ExecuteDataSet(oCommandType, sCommandText);

                if (Error == 0)
                {
                    return TotalApp.Package.THelperJson.TDeserializeListObjectOrDefault<T>(oData.Tables[0]);
                }
            }
            catch (Exception ex)
            {
                Error = 1;
                Exception = ex.Message;
            }

            return default(List<T>);
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="oCommandType"></param>
        /// <param name="sCommandText"></param>
        /// <returns></returns>
        public T ExecuteObject<T>(CommandType oCommandType
                                , string sCommandText) where T : class
        {
            try
            {
                var oData = ExecuteDataSet(oCommandType, sCommandText);

                if (Error == 0)
                {
                    return oData.Tables[0].TDeserializeObjectOrDefault<T>(true);
                    //return THelperJson.TDeserializeObjectOrDefault<T>(oData.Tables[0], true);
                }
            }
            catch (Exception ex)
            {
                Error = 1;
                Exception = ex.Message;
            }

            return default(T);
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="oCommandType"></param>
        /// <param name="sCommandText"></param>
        /// <param name="objAnonymous"></param>
        /// <param name="bIsUnicoReg"></param>
        /// <returns></returns>
        public T ExecuteObjectAnonymous<T>(CommandType oCommandType
                                         , string sCommandText
                                         , T objAnonymous
                                         , bool bIsUnicoReg = false) 
        {
            try
            {
                var oData = ExecuteDataSet(oCommandType, sCommandText);

                if (Error == 0)
                {
                    return TotalApp.Package.THelperJson.TDeserializeObjectAnonymousOrDefault(oData.Tables[0], objAnonymous, bIsUnicoReg);
                }
            }
            catch (Exception ex)
            {
                Error = 1;
                Exception = ex.Message;
            }

            return default(T);
        }
        #endregion

        #region "Metodos y constructores"

        public TDbManager()
        {
            //
            // TODO: Agregar aquí la lógica del constructor
            //
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="oProviderType"></param>
        public TDbManager(DataProvider oProviderType)
        {
            this.ProviderType = oProviderType;
        }


        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="oProviderType"></param>
        /// <param name="sConnectionString"></param>
        public TDbManager(DataProvider oProviderType
                        , string sConnectionString)
        {
            this.ProviderType = oProviderType;
            this.ConnectionString = sConnectionString;
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180117
        /// *Sinopsis:
        /// *Comentario: Version(1.1.20180109), se agrego la propiedad nNumIntentosDeConex
        /// </summary> 
        /// <param name="nNumIntentosDeConex">Parametro que permite definir el numero de intentos para conectarse a una base de datos</param>
        public void Open(short nNumIntentosDeConex = 1)
        {
            try
            {
                _oIdbConnection = TDbManagerFactory.GetConnection(this.ProviderType);
                _oIdbConnection.ConnectionString = this.ConnectionString;

                short nCount = 0;
                while (_oIdbConnection.State == ConnectionState.Closed)
                {
                    if (_oIdbConnection.State == ConnectionState.Closed)
                    {
                        if (nCount == nNumIntentosDeConex)
                        {
                            if (_oIdbConnection.State == ConnectionState.Closed)
                            {
                                Error = -1000;
                                Exception += Environment.NewLine + " - Se agoto el numero de intentos para conectar a la BD";
                            }
                            break;
                        }
                        else
                        {
                            try
                            {
                                _oIdbConnection.Open();
                            }
                            catch (Exception ex)
                            {
                                Error = -1000;
                                Exception = ex.Message;
                            }

                        }

                        nCount++;

                    }
                }

            }
            catch (Exception ex)
            {
                Error = -1000;
                Exception = ex.Message;
            }

            if (Error == 0)
            {
                this._oIDbCommand = TDbManagerFactory.GetCommand(this.ProviderType);
            }

        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        public void Close()
        {
            if (_oIdbConnection != null && _oIdbConnection.State != ConnectionState.Closed)
            {
                _oIdbConnection.Close();
                this._oIdbConnection = null;
            }
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        public void Dispose()
        {
            GC.SuppressFinalize(this);
            this.Close();
            this._oIDbCommand = null;
            this._oIDbTransaction = null;
            
        }


        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        public void CloseReader()
        {
            if (this.DataReader != null)
            {
                this.DataReader.Dispose();
                this.DataReader.Close();
            }
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="nParamsCount"></param>
        public void CreateParameters(int nParamsCount)
        {
            try
            {
                _oIDbParameters = new IDbDataParameter[nParamsCount];
                _oIDbParameters = TDbManagerFactory.GetParameters(this.ProviderType
                                                                 ,nParamsCount);
            }
            catch (Exception ex)
            {
                Error = 1;
                Exception = ex.Message;
            }
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="nIndex"></param>
        /// <param name="sParamName"></param>
        /// <param name="objValue"></param>
        public void AddParameters(int nIndex
                                , string sParamName
                                , object objValue)
        {
            try
            {
                if (nIndex < _oIDbParameters.Length)
                {
                    _oIDbParameters[nIndex].ParameterName = sParamName;
                    _oIDbParameters[nIndex].Value = objValue;
                }
            }
            catch (Exception ex)
            {
                Error = 1;
                Exception = ex.Message;
            }
        }


        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="nIndex"></param>
        /// <param name="sParamName"></param>
        /// <param name="objValue"></param>
        /// <param name="oDirection"></param>
        /// <param name="nSize"></param>
        public void AddParameters(int nIndex
                                 , string sParamName
                                 , object objValue
                                 , ParameterDirection oDirection
                                 , int nSize)
        {
            try
            {
                if (nIndex < _oIDbParameters.Length)
                {
                    _oIDbParameters[nIndex].ParameterName = sParamName;
                    _oIDbParameters[nIndex].Value = objValue;
                    _oIDbParameters[nIndex].Direction = oDirection;
                    _oIDbParameters[nIndex].Size = nSize;
                }
            }
            catch (Exception ex)
            {
                Error = 1;
                Exception = ex.Message;

            }

        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="nIndex"></param>
        /// <param name="sParamName"></param>
        /// <param name="objValue"></param>
        /// <param name="oDirection"></param>
        /// <param name="nSize"></param>
        /// <param name="oDbType"></param>
        /// <param name="byPresion"></param>
        public void AddParameters(int nIndex
                                , string sParamName
                                , object objValue
                                , ParameterDirection oDirection
                                , int nSize
                                , DbType oDbType
                                , byte byPresion)
        {
            try
            {

                if (nIndex < _oIDbParameters.Length)
                {
                    _oIDbParameters[nIndex].ParameterName = sParamName;
                    _oIDbParameters[nIndex].Value = objValue;
                    _oIDbParameters[nIndex].Direction = oDirection;
                    _oIDbParameters[nIndex].Size = nSize;
                    _oIDbParameters[nIndex].DbType = oDbType;
                    _oIDbParameters[nIndex].Precision = byPresion;
                }
            }
            catch (Exception ex)
            {
                Error = 1;
                Exception = ex.Message;
            }


        }



        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="nIndex"></param>
        /// <param name="sParamName"></param>
        /// <param name="objValue"></param>
        /// <param name="oDirection"></param>
        /// <param name="nSize"></param>
        /// <param name="oDbType"></param>
        /// <param name="byPresion"></param>
        /// <param name="byScale"></param>
        public void AddParameters(int nIndex
                                , string sParamName
                                , object objValue
                                , ParameterDirection oDirection
                                , int nSize
                                , DbType oDbType
                                , byte byPresion
                                , byte byScale)
        {
            try
            {
                if (nIndex < _oIDbParameters.Length)
                {
                    _oIDbParameters[nIndex].ParameterName = sParamName;
                    _oIDbParameters[nIndex].Value = objValue;
                    _oIDbParameters[nIndex].Direction = oDirection;
                    _oIDbParameters[nIndex].Size = nSize;
                    _oIDbParameters[nIndex].DbType = oDbType;
                    _oIDbParameters[nIndex].Precision = byPresion;
                    _oIDbParameters[nIndex].Scale = byScale;
                }
            }
            catch (Exception ex)
            {
                Error = 1;
                Exception = ex.Message;

            }
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        public void BeginTransaction()
        {
            try
            {
                if (this._oIDbTransaction == null)
                {
                    _oIDbTransaction = TDbManagerFactory.GetTransaction(this.ProviderType);
                }

                this._oIDbCommand.Transaction = _oIDbTransaction;
            }
            catch (Exception ex)
            {
                Error = 1;
                Exception = ex.Message;
            }
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        public void CommitTransaction()
        {
            try
            {
                if (this._oIDbTransaction != null)
                {
                    this._oIDbTransaction.Commit();
                }
            }
            catch (Exception ex)
            {
                Error = 1;
                Exception = ex.Message;
            }

        }


        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="oCommand"></param>
        /// <param name="oCommandParameters"></param>
        private void AttachParameters(IDbCommand oCommand
                                     , IDbDataParameter[] oCommandParameters)
        {
            try
            {
                foreach (IDbDataParameter oIdbParameter in oCommandParameters)
                {
                    if ((oIdbParameter.Direction == ParameterDirection.InputOutput) &&
                        (oIdbParameter.Value == null))
                    {
                        oIdbParameter.Value = DBNull.Value;
                    }
                    oCommand.Parameters.Add(oIdbParameter);
                }
            }
            catch (Exception ex)
            {
                Error = 1;
                Exception = ex.Message;
            }


        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="oCommand"></param>
        /// <param name="oConnection"></param>
        /// <param name="oTransaction"></param>
        /// <param name="oCommandType"></param>
        /// <param name="sCommandText"></param>
        /// <param name="oCommandParameters"></param>
        private void PrepareCommand(IDbCommand oCommand
                                   , IDbConnection oConnection
                                   , IDbTransaction oTransaction
                                   , CommandType oCommandType
                                   , string sCommandText
                                   , IDbDataParameter[] oCommandParameters)
        {
            try
            {
                oCommand.Connection = oConnection;
                oCommand.CommandText = sCommandText;
                oCommand.CommandType = oCommandType;

                if (TimeOut == 0)
                {
                    TimeOut = 900; //15 minutos
                }

                oCommand.CommandTimeout = TimeOut;


                if (oTransaction != null)
                {
                    oCommand.Transaction = oTransaction;
                }

                if (oCommandParameters != null)
                {
                    AttachParameters(oCommand, oCommandParameters);
                }
            }
            catch (Exception ex)
            {
                Error = 1;
                Exception = ex.Message;
            }


        }
        #endregion

        #region "Destructor"
        ~TDbManager()
        {

        }
        #endregion
    }
}