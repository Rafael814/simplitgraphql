﻿using System;
using System.Data;
using System.Collections.Generic;

namespace TotalApp.Package.DAL
{
    /// <summary>
    /// *Autor:Larry Ramirez
    /// *Fecha de creacion:09-01-2018
    /// *Version:1.1.20180109
    /// *Sinopsis:
    /// </summary>
    public interface ITDbManager
    {
        #region "Variables"
        #endregion

        #region "Propiedades"
        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:Priedad que define el tipo de proveedor de conexion a una BD, Ejemplo: SqlServer, Oracle
        /// </summary>
        DataProvider ProviderType
        {
            get;
            set;
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:Propiedad que encapsula el string de conexion a una BD
        /// </summary>
        string ConnectionString
        {
            get;
            set;
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:Propiedad solo lectura, permite encapsular la conexion existente entre la app y la BD
        /// </summary>
        IDbConnection Connection
        {
            get;
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:Propiedad solo lectura, permite encapsular la Transaccion existente entre la app y la BD
        /// </summary>
        IDbTransaction Transaction
        {
            get;
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:Propiedad solo lectura, permite encapsular el Reader(data) de la BD
        /// </summary>
        IDataReader DataReader
        {
            get;
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:Propiedad solo lectura, permite encapsular la declaracion sql que se esta ejecutando
        /// </summary>
        IDbCommand Command
        {
            get;
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:Propiedad solo lectura, permite encapsular el arreglo de parametros de ejecucion conforme a la declaracion sql
        /// </summary>
        IDbDataParameter[] Parameters
        {
            get;
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:Propiedad de solo lecturta, encapsula el mensaje de error, en caso de que ocurra
        /// </summary>
        string Exception { get; }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:Propiedad de solo lecturta, encapsula el error, en caso de que ocurra
        /// </summary>
        int Error { get; }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:Propiedad que permite establecer un tiempo limite de ejecucion
        /// </summary>
        int TimeOut { get; set; }
        #endregion

        #region "Eventos"
        #endregion

        #region "Funciones"
        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sCommandText"></param>
        /// <param name="oCommandType"></param>
        /// <returns></returns>
        IDataReader ExecuteReader(string sCommandText
                                 ,CommandType oCommandType);

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="oCommandType"></param>
        /// <param name="sCommandText"></param>
        /// <returns></returns>
        DataSet ExecuteDataSet(CommandType oCommandType
                              ,string sCommandText);

        
        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="oCommandType"></param>
        /// <param name="sCommandText"></param>
        /// <returns></returns>
        List<T> ExecuteList<T>(CommandType oCommandType
                              , string sCommandText) where T : class;

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="oCommandType"></param>
        /// <param name="sCommandText"></param>
        /// <returns></returns>
        T ExecuteObject<T>(CommandType oCommandType
                          , string sCommandText) where T : class;


        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="oCommandType"></param>
        /// <param name="sCommandText"></param>
        /// <param name="objAnonymous"></param>
        /// <param name="bIsUnicoReg"></param>
        /// <returns></returns>
        T ExecuteObjectAnonymous<T>(CommandType oCommandType
                                  , string sCommandText
                                  , T objAnonymous
                                  , bool bIsUnicoReg = false);


        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="oCommandType"></param>
        /// <param name="sCommandText"></param>
        /// <returns></returns>
        object ExecuteScalar(CommandType oCommandType
                            ,string sCommandText);

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="oCommandType"></param>
        /// <param name="sCommandText"></param>
        /// <returns></returns>
        int ExecuteNonQuery(CommandType oCommandType
                           ,string sCommandText);
        #endregion

        #region "Metodos y constructores"

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180117
        /// *Sinopsis:
        /// *Comentario: Version(1.1.20180109), se agrego la propiedad nNumIntentosDeConex
        /// </summary> 
        /// <param name="nNumIntentosDeConex">Parametro que permite definir el numero de intentos para conectarse a una base de datos</param>
        void Open(short nNumIntentosDeConex = 1);

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>   
        void BeginTransaction();

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>    
        void CommitTransaction();

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary> 
        /// <param name="nParamsCount"></param>
        void CreateParameters(int nParamsCount);


        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary> 
        /// <param name="nIndex"></param>
        /// <param name="sParamName"></param>
        /// <param name="objValue"></param>
        void AddParameters(int nIndex
                          , string sParamName
                          , object objValue);




        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary> 
        /// <param name="nIndex"></param>
        /// <param name="sParamName"></param>
        /// <param name="objValue"></param>
        /// <param name="oDirection"></param>
        /// <param name="nSize"></param>
        void AddParameters(int nIndex
                         , string sParamName
                         , object objValue
                         , ParameterDirection oDirection
                         , int nSize);



        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary> 
        /// <param name="nIndex"></param>
        /// <param name="sParamName"></param>
        /// <param name="objValue"></param>
        /// <param name="oDirection"></param>
        /// <param name="nSize"></param>
        /// <param name="oDbType"></param>
        /// <param name="byPresion"></param>
        void AddParameters(int nIndex
                         , string sParamName
                         , object objValue
                         , ParameterDirection oDirection
                         , int nSize
                         , DbType oDbType
                         , byte byPresion);



        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary> 
        /// <param name="nIndex"></param>
        /// <param name="sParamName"></param>
        /// <param name="objValue"></param>
        /// <param name="oDirection"></param>
        /// <param name="nSize"></param>
        /// <param name="oDbType"></param>
        /// <param name="byPresion"></param>
        /// <param name="byScale"></param>
        void AddParameters(int nIndex
                         , string sParamName
                         , object objValue
                         , ParameterDirection oDirection
                         , int nSize
                         , DbType oDbType
                         , byte byPresion
                         , byte byScale);


        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>    
        void CloseReader();

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>   
        void Close();

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>    
        void Dispose();

        #endregion
    }

}