﻿using System;
using System.Data;
using System.Data.Odbc;
using System.Data.SqlClient;

namespace TotalApp.Package.DAL
{
    /// <summary>
    /// *Autor:Larry Ramirez
    /// *Fecha de creacion:09-01-2018
    /// *Version:1.1.20180109
    /// *Sinopsis:
    /// </summary>
    public sealed class TDbManagerFactory
    {
        #region "Variables"
        #endregion

        #region "Propiedades"
        #endregion

        #region "Eventos"
        #endregion

        #region "Funciones"

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="providerType"></param>
        /// <returns></returns>
        public static IDbConnection GetConnection(DataProvider providerType)
        {

            switch (providerType)
            {
                case DataProvider.SqlServer:
                    return new SqlConnection();
                default:
                    return null;
            }
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="providerType"></param>
        /// <returns></returns>
        public static IDbCommand GetCommand(DataProvider providerType)
        {
            switch (providerType)
            {
                case DataProvider.SqlServer:
                    return new SqlCommand();
                default:
                    return null;
            }

        }


        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="providerType"></param>
        /// <returns></returns>
        public static IDbDataAdapter GetDataAdapter(DataProvider providerType)
        {
            switch (providerType)
            {
                case DataProvider.SqlServer:
                    return new SqlDataAdapter();
                default:
                    return null;
            }

        }


        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="providerType"></param>
        /// <returns></returns>
        public static IDbTransaction GetTransaction(DataProvider providerType)
        {
            return GetConnection(providerType).BeginTransaction();
        }


        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="providerType"></param>
        /// <returns></returns>
        public static IDbDataParameter GetParameter(DataProvider providerType)
        {
            switch (providerType)
            {
                case DataProvider.SqlServer:
                    return new SqlParameter();
                default:
                    return null;
            }
        }


        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="providerType"></param>
        /// <param name="paramsCount"></param>
        /// <returns></returns>
        public static IDbDataParameter[] GetParameters(DataProvider providerType,
                                                       int paramsCount)
        {
            IDbDataParameter[] iDbParameters = new IDbDataParameter[paramsCount];

            for (int intPos = 0; intPos < paramsCount; ++intPos)
            {
                iDbParameters[intPos] = new SqlParameter();
            }

            return iDbParameters;
        }
        #endregion

        #region "Metodos y constructores"

        /// <summary>
        /// 
        /// </summary>
        public TDbManagerFactory()
        {
            //
            // TODO: Agregar aquí la lógica del constructor
            //
        }
        
        #endregion

        #region "Destructor"
        ~TDbManagerFactory()
        {

        }
        #endregion
    }
}