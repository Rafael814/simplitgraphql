﻿
using System;
using System.Data;

namespace TotalApp.Package.DAL
{
    /// <summary>
    /// *Autor:Larry Ramirez
    /// *Fecha de creacion:09-01-2018
    /// *Version:1.1.20180109
    /// *Sinopsis:
    /// </summary>
    public partial class TBase_ExecScript_DAL
    {
        #region "Get"
        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sCnx"></param>
        /// <param name="sSql"></param>
        /// <param name="bIsValidarPrimeraTabla"></param>
        /// <returns></returns>
        public virtual DataSet Get_DataSet_SqlServer(string sCnx
                                                   , string sSql
                                                   , bool bIsValidarPrimeraTabla = true)
        {

            if (sCnx.TIsNullOrEmpty()) THelperInfoEstatus.ArgumentNull(nameof(sCnx));
            if (sSql.TIsNullOrEmpty()) THelperInfoEstatus.ArgumentNull(nameof(sSql));

            base.OK();

            DataSet oData = new DataSet();
            ITDbManager oTDbManager = new TDbManager(DataProvider.SqlServer);
            try
            {

                oTDbManager.ConnectionString = sCnx;
                oTDbManager.Open(NumIntentosDeConex);

                if (oTDbManager.Error == 0)
                {
                    oTDbManager.CreateParameters(0);
                    oData = oTDbManager.ExecuteDataSet(CommandType.Text, sSql);

                    if (oTDbManager.Error != 0)
                    {
                        base.InternalServerError(oTDbManager.Error
                                                , "Error de ejecucion en la BD"
                                                , TTypeError.Err_Bd
                                                , oTDbManager.Exception);
                    }
                    else
                    {
                        if (oData.TIsNullOrEmptyDataSet())
                        {
                            base.InternalServerError("El objecto esta vacio", TTypeError.Err_ObjEmpty);
                        }
                        else
                        {
                            if (bIsValidarPrimeraTabla)
                            {
                                if (oData.Tables[0].TIsNullOrEmptyTable())
                                {
                                    base.NotFound("No existen registros que mostrar", TTypeError.Err_ObjEmpty);
                                }
                            }

                        }
                    }
                }
                else
                {
                    base.InternalServerError(oTDbManager.Error
                                           , "Error de conexion a la BD"
                                           , TTypeError.Err_CnxBD
                                           , oTDbManager.Exception);

                }

            }
            catch (Exception ex)
            {
                base.InternalServerError("Error Excepcion", TTypeError.Err_CT_DAL, ex.Message);
            }
            finally
            {
                oTDbManager.Dispose();
                oTDbManager = null;
            }

            return oData;
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sBd"></param>
        /// <param name="sCnx"></param>
        /// <param name="sSql"></param>
        /// <param name="bIsUsarPlantilla"></param>
        /// <param name="bIsValidarPrimeraTabla"></param>
        /// <returns></returns>
        public virtual DataSet Get_DataSet_SqlServer(string sBd
                                                   , string sCnx
                                                   , string sSql
                                                   , bool bIsUsarPlantilla = true
                                                   , bool bIsValidarPrimeraTabla = true)
        {
            if (sBd.TIsNullOrEmpty()) THelperInfoEstatus.ArgumentNull(nameof(sBd));


            if (bIsUsarPlantilla)
            {
                sSql = Templade_Get_ForSqlServer(sBd, sSql);
            }


            return Get_DataSet_SqlServer(sCnx, sSql, bIsValidarPrimeraTabla);
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sServidor"></param>
        /// <param name="sBd"></param>
        /// <param name="sUsuario"></param>
        /// <param name="sClave"></param>
        /// <param name="sSql"></param>
        /// <param name="bIsUsarPlantilla"></param>
        /// <param name="bIsValidarPrimeraTabla"></param>
        /// <returns></returns>
        public virtual DataSet Get_DataSet_SqlServer(string sServidor
                                                    , string sBd
                                                    , string sUsuario
                                                    , string sClave
                                                    , string sSql
                                                    , bool bIsUsarPlantilla = true
                                                    , bool bIsValidarPrimeraTabla = true) => Get_DataSet_SqlServer(sBd
                                                                                                                  , CnxStringSqlServer(sServidor, sBd, sUsuario, sClave)
                                                                                                                  , sSql
                                                                                                                  , bIsUsarPlantilla
                                                                                                                  , bIsValidarPrimeraTabla);



        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="oDatosConex_DTO"></param>
        /// <param name="sSql"></param>
        /// <param name="bIsUsarPlantilla"></param>
        /// <param name="bIsValidarPrimeraTabla"></param>
        /// <returns></returns>
        public virtual DataSet Get_DataSet_SqlServer(DTO.TDatosConex_DTO oDatosConex_DTO
                                                    , string sSql
                                                    , bool bIsUsarPlantilla = true
                                                    , bool bIsValidarPrimeraTabla = true) => Get_DataSet_SqlServer(oDatosConex_DTO.DB
                                                                                                                 , CnxStringSqlServer(oDatosConex_DTO.Server, oDatosConex_DTO.DB, oDatosConex_DTO.User, oDatosConex_DTO.Pass)
                                                                                                                 , sSql
                                                                                                                 , bIsUsarPlantilla
                                                                                                                 , bIsValidarPrimeraTabla);

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sSql"></param>
        /// <param name="bIsUsarPlantilla"></param>
        /// <param name="bIsValidarPrimeraTabla"></param>
        /// <returns></returns>
        public virtual DataSet Get_DataSet_SqlServerByDC(string sSql
                                                       , bool bIsUsarPlantilla = true
                                                       , bool bIsValidarPrimeraTabla = true) => Get_DataSet_SqlServer(THelper.DatosConex_DTO
                                                                                                                     , sSql
                                                                                                                     , bIsUsarPlantilla);

        #endregion
    }
}
