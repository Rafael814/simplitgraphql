﻿
using System;
using System.Collections.Generic;
using System.Data;

namespace TotalApp.Package.DAL
{
    /// <summary>
    /// *Autor:Larry Ramirez
    /// *Fecha de creacion:09-01-2018
    /// *Version:1.1.20180109
    /// *Sinopsis:
    /// </summary>
    public partial class TBase_ExecScript_DAL
    {
        #region "Get"
        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="sCnx"></param>
        /// <param name="sSql"></param>
        /// <param name="bIsValidarObject"></param>
        /// <returns></returns>
        public virtual List<T> Get_List_SqlServer<T>(string sCnx
                                                   , string sSql
                                                   , bool bIsValidarObject = true) where T : class
        {
            if (sCnx.TIsNullOrEmpty()) THelperInfoEstatus.ArgumentNull(nameof(sCnx));
            if (sSql.TIsNullOrEmpty()) THelperInfoEstatus.ArgumentNull(nameof(sSql));

            base.OK();

            List<T> oList = new List<T>();
            ITDbManager oTDbManager = new TDbManager(DataProvider.SqlServer);
            try
            {

                oTDbManager.ConnectionString = sCnx;
                oTDbManager.Open(NumIntentosDeConex);

                if (oTDbManager.Error == 0)
                {
                    oTDbManager.CreateParameters(0);

                    oList = oTDbManager.ExecuteList<T>(CommandType.Text, sSql);

                    if (oTDbManager.Error != 0)
                    {
                        base.InternalServerError(oTDbManager.Error
                                                , "Error de ejecucion en la BD"
                                                , TTypeError.Err_Bd
                                                , oTDbManager.Exception);
                    }
                    else
                    {
                        if (bIsValidarObject)
                        {
                            if (oList.TIsNullOrEmpty())
                            {
                                base.NotFound("No existen registros que mostrar", TTypeError.Err_ObjEmpty);
                            }
                        }
                    }
                }
                else
                {
                    base.InternalServerError(oTDbManager.Error
                                             , "Error de conexion a la BD"
                                             , TTypeError.Err_CnxBD
                                             , oTDbManager.Exception);
                }

            }
            catch (Exception ex)
            {
                base.InternalServerError("Error Excepcion", TTypeError.Err_CT_DAL, ex.Message);
            }
            finally
            {
                oTDbManager.Dispose();
                oTDbManager = null;
            }

            return oList;
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="sBd"></param>
        /// <param name="sCnx"></param>
        /// <param name="sSql"></param>
        /// <param name="bIsUsarPlantilla"></param>
        /// <param name="bIsValidarObject"></param>
        /// <returns></returns>
        public virtual List<T> Get_List_SqlServer<T>(string sBd
                                                   , string sCnx
                                                   , string sSql
                                                   , bool bIsUsarPlantilla = true
                                                   , bool bIsValidarObject = true) where T : class
        {
            if (sBd.TIsNullOrEmpty()) THelperInfoEstatus.ArgumentNull(nameof(sBd));

            if (bIsUsarPlantilla)
            {
                sSql = Templade_Get_ForSqlServer(sBd, sSql);
            }

            return Get_List_SqlServer<T>(sCnx, sSql, bIsValidarObject);
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="sServidor"></param>
        /// <param name="sBd"></param>
        /// <param name="sUsuario"></param>
        /// <param name="sClave"></param>
        /// <param name="sSql"></param>
        /// <param name="bIsUsarPlantilla"></param>
        /// <param name="bIsValidarObject"></param>
        /// <returns></returns>
        public virtual List<T> Get_List_SqlServer<T>(string sServidor
                                                   , string sBd
                                                   , string sUsuario
                                                   , string sClave
                                                   , string sSql
                                                   , bool bIsUsarPlantilla = true
                                                   , bool bIsValidarObject = true) where T : class => Get_List_SqlServer<T>(sBd
                                                                                                                          , CnxStringSqlServer(sServidor, sBd, sUsuario, sClave)
                                                                                                                          , sSql
                                                                                                                          , bIsUsarPlantilla
                                                                                                                          , bIsValidarObject);





        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="oDatosConex_DTO"></param>
        /// <param name="sSql"></param>
        /// <param name="bIsUsarPlantilla"></param>
        /// <param name="bIsValidarObject"></param>
        /// <returns></returns>
        public virtual List<T> Get_List_SqlServer<T>(DTO.TDatosConex_DTO oDatosConex_DTO
                                                   , string sSql
                                                   , bool bIsUsarPlantilla = true
                                                   , bool bIsValidarObject = true) where T : class => Get_List_SqlServer<T>(oDatosConex_DTO.DB
                                                                                                                           , CnxStringSqlServer(oDatosConex_DTO.Server, oDatosConex_DTO.DB, oDatosConex_DTO.User, oDatosConex_DTO.Pass)
                                                                                                                           , sSql
                                                                                                                           , bIsUsarPlantilla
                                                                                                                           , bIsValidarObject);


        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="sSql"></param>
        /// <param name="bIsUsarPlantilla"></param>
        /// <param name="bIsValidarObject"></param>
        /// <returns></returns>
        public virtual List<T> Get_List_SqlServerByDC<T>(string sSql
                                                       , bool bIsUsarPlantilla = true
                                                       , bool bIsValidarObject = true) where T : class => Get_List_SqlServer<T>(THelper.DatosConex_DTO
                                                                                                                               , sSql
                                                                                                                               , bIsUsarPlantilla
                                                                                                                               , bIsValidarObject);

        #endregion
    }
}
