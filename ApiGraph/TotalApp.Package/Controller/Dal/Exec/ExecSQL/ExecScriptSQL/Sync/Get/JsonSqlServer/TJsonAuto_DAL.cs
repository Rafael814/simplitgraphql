﻿
using System;
using System.Data;

namespace TotalApp.Package.DAL
{
    /// <summary>
    /// *Autor:Larry Ramirez
    /// *Fecha de creacion:09-01-2018
    /// *Version:1.1.20180109
    /// *Sinopsis:
    /// </summary>
    public partial class TBase_ExecScript_DAL
    {
        #region "Get"
        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sCnx"></param>
        /// <param name="sSql"></param>
        /// <returns></returns>
        public virtual String Get_JsonAuto_SqlServer(string sCnx
                                                   , string sSql
                                                   , bool bIsUsarPlantillaJsonAuto = true)
        {
            if (sCnx.TIsNullOrEmpty()) THelperInfoEstatus.ArgumentNull(nameof(sCnx));
            if (sSql.TIsNullOrEmpty()) THelperInfoEstatus.ArgumentNull(nameof(sSql));

            if (bIsUsarPlantillaJsonAuto)
            {
                sSql = Templade_Get_StringJsonAuto_ForSql(sSql);
            }
           

            base.OK();

            String sData = THelper.Empty;
            ITDbManager oTDbManager = new TDbManager(DataProvider.SqlServer);
            try
            {
                oTDbManager.ConnectionString = sCnx;
                oTDbManager.Open(NumIntentosDeConex);

                if (oTDbManager.Error == 0)
                {
                    oTDbManager.CreateParameters(0);

                    try
                    {
                        sData = (String)oTDbManager.ExecuteScalar(CommandType.Text, sSql);

                        if (oTDbManager.Error != 0)
                        {
                            base.InternalServerError(oTDbManager.Error
                                                    , "Error de ejecucion en la BD"
                                                    , TTypeError.Err_Bd
                                                    , oTDbManager.Exception);
                        }

                    }
                    catch (Exception ex)
                    {
                        base.NotFound(2, "No existe registros que mostrar", TTypeError.Err_ObjEmpty);

                    }
                }
                else
                {
                    base.InternalServerError(oTDbManager.Error
                                            , "Error de conexion a la BD"
                                            , TTypeError.Err_CnxBD
                                            , oTDbManager.Exception);
                }
            }
            catch (Exception ex)
            {
                base.InternalServerError("Error Excepcion", TTypeError.Err_CT_DAL, ex.Message);
            }
            finally
            {
                oTDbManager.Dispose();
                oTDbManager = null;
            }

            return sData;
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sBd"></param>
        /// <param name="sCnx"></param>
        /// <param name="sSql"></param>
        /// <param name="bIsUsarPlantilla"></param>
        /// <returns></returns>
        public virtual String Get_JsonAuto_SqlServer(string sBd
                                                   , string sCnx
                                                   , string sSql
                                                   , bool bIsUsarPlantilla = true)
        {
            if (sBd.TIsNullOrEmpty()) THelperInfoEstatus.ArgumentNull(nameof(sBd));
            sSql = Templade_Get_StringJsonAuto_ForSql(sSql);

            if (bIsUsarPlantilla)
            {
                sSql = Templade_Get_ForSqlServer(sBd, sSql);
            }

            return Get_JsonAuto_SqlServer(sCnx, sSql,false);
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sServidor"></param>
        /// <param name="sBd"></param>
        /// <param name="sUsuario"></param>
        /// <param name="sClave"></param>
        /// <param name="sSql"></param>
        /// <param name="bIsUsarPlantilla"></param>
        /// <returns></returns>
        public virtual String Get_JsonAuto_SqlServer(string sServidor
                                                    , string sBd
                                                    , string sUsuario
                                                    , string sClave
                                                    , string sSql
                                                    , bool bIsUsarPlantilla = true) => Get_JsonAuto_SqlServer(sBd
                                                                                                            , CnxStringSqlServer(sServidor, sBd, sUsuario, sClave)
                                                                                                            , sSql
                                                                                                            , bIsUsarPlantilla);
                                                      



        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="oDatosConex_DTO"></param>
        /// <param name="sSql"></param>
        /// <param name="bIsUsarPlantilla"></param>
        /// <returns></returns>
        public virtual String Get_JsonAuto_SqlServer(DTO.TDatosConex_DTO oDatosConex_DTO
                                                    , string sSql
                                                    , bool bIsUsarPlantilla = true) => Get_JsonAuto_SqlServer(oDatosConex_DTO.DB
                                                                                                            , CnxStringSqlServer(oDatosConex_DTO.Server, oDatosConex_DTO.DB, oDatosConex_DTO.User, oDatosConex_DTO.Pass)
                                                                                                            , sSql
                                                                                                            , bIsUsarPlantilla);


        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sSql"></param>
        /// <param name="bIsUsarPlantilla"></param>
        /// <returns></returns>
        public virtual String Get_JsonAuto_SqlServerByDC(string sSql
                                                       , bool bIsUsarPlantilla = true) => Get_JsonAuto_SqlServer(THelper.DatosConex_DTO
                                                                                                               , sSql
                                                                                                               , bIsUsarPlantilla);

        #endregion
    }
}
