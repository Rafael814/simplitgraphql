﻿namespace TotalApp.Package.DAL
{
    /// <summary>
    /// *Autor:Larry Ramirez
    /// *Fecha de creacion:09-01-2018
    /// *Version:1.1.20180109
    /// *Sinopsis:
    /// </summary>
    public partial class TBase_ExecScript_DAL
    {
        #region "Get"
        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sCnx"></param>
        /// <param name="sSql"></param>
        /// <param name="bIsValidarPrimeraTabla"></param>
        /// <param name="bIsDecrypting"></param>
        /// <param name="bIsDecompressData"></param>
        /// <returns></returns>
        public virtual string Get_DataSet_Encrypt_Compress_SqlServer(string sCnx
                                                                   , string sSql
                                                                   , bool bIsValidarPrimeraTabla = true
                                                                   , bool bIsDecrypting = true
                                                                   , bool bIsDecompressData = true)
        {


            var oData = Get_DataSet_SqlServer(sCnx, sSql, bIsValidarPrimeraTabla);

            return base.IsOK ? base.Encrypt_Compress_Object(oData, bIsDecrypting, bIsDecompressData) : "";

        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sBd"></param>
        /// <param name="sCnx"></param>
        /// <param name="sSql"></param>
        /// <param name="bIsUsarPlantilla"></param>
        /// <param name="bIsValidarPrimeraTabla"></param>
        /// <param name="bIsDecrypting"></param>
        /// <param name="bIsDecompressData"></param>
        /// <returns></returns>
        public virtual string Get_DataSet_Encrypt_Compress_SqlServer(string sBd
                                                                   , string sCnx
                                                                   , string sSql
                                                                   , bool bIsUsarPlantilla = true
                                                                   , bool bIsValidarPrimeraTabla = true
                                                                   , bool bIsDecrypting = true
                                                                   , bool bIsDecompressData = true)
        {

            var oData = Get_DataSet_SqlServer(sBd, sCnx, sSql, bIsUsarPlantilla, bIsValidarPrimeraTabla);

            return base.IsOK ? base.Encrypt_Compress_Object(oData, bIsDecrypting, bIsDecompressData) : "";
        }


        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sServidor"></param>
        /// <param name="sBd"></param>
        /// <param name="sUsuario"></param>
        /// <param name="sClave"></param>
        /// <param name="sSql"></param>
        /// <param name="bIsUsarPlantilla"></param>
        /// <param name="bIsValidarPrimeraTabla"></param>
        /// <param name="bIsDecrypting"></param>
        /// <param name="bIsDecompressData"></param>
        /// <returns></returns>
        public virtual string Get_DataSet_Encrypt_Compress_SqlServer(string sServidor
                                                                   , string sBd
                                                                   , string sUsuario
                                                                   , string sClave
                                                                   , string sSql
                                                                   , bool bIsUsarPlantilla = true
                                                                   , bool bIsValidarPrimeraTabla = true
                                                                   , bool bIsDecrypting = true
                                                                   , bool bIsDecompressData = true) => Get_DataSet_Encrypt_Compress_SqlServer(sBd
                                                                                                                                            , CnxStringSqlServer(sServidor, sBd, sUsuario, sClave)
                                                                                                                                            , sSql
                                                                                                                                            , bIsUsarPlantilla
                                                                                                                                            , bIsValidarPrimeraTabla
                                                                                                                                            , bIsDecrypting
                                                                                                                                            , bIsDecompressData);




        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="oDatosConex_DTO"></param>
        /// <param name="sSql"></param>
        /// <param name="bIsUsarPlantilla"></param>
        /// <param name="bIsValidarPrimeraTabla"></param>
        /// <param name="bIsDecrypting"></param>
        /// <param name="bIsDecompressData"></param>
        /// <returns></returns>
        public virtual string Get_DataSet_Encrypt_Compress_SqlServer(DTO.TDatosConex_DTO oDatosConex_DTO
                                                                    , string sSql
                                                                    , bool bIsUsarPlantilla = true
                                                                    , bool bIsValidarPrimeraTabla = true
                                                                    , bool bIsDecrypting = true
                                                                    , bool bIsDecompressData = true) => Get_DataSet_Encrypt_Compress_SqlServer(oDatosConex_DTO.DB
                                                                                                                                            , CnxStringSqlServer(oDatosConex_DTO.Server, oDatosConex_DTO.DB, oDatosConex_DTO.User, oDatosConex_DTO.Pass)
                                                                                                                                            , sSql
                                                                                                                                            , bIsUsarPlantilla
                                                                                                                                            , bIsValidarPrimeraTabla
                                                                                                                                            , bIsDecrypting
                                                                                                                                            , bIsDecompressData);


        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sSql"></param>
        /// <param name="bIsUsarPlantilla"></param>
        /// <param name="bIsValidarPrimeraTabla"></param>
        /// <param name="bIsDecrypting"></param>
        /// <param name="bIsDecompressData"></param>
        /// <returns></returns>
        public virtual string Get_DataSet_Encrypt_Compress_SqlServerByDC(string sSql
                                                                       , bool bIsUsarPlantilla = true
                                                                       , bool bIsValidarPrimeraTabla = true
                                                                       , bool bIsDecrypting = true
                                                                       , bool bIsDecompressData = true) => Get_DataSet_Encrypt_Compress_SqlServer(THelper.DatosConex_DTO
                                                                                                                                                , sSql
                                                                                                                                                , bIsUsarPlantilla
                                                                                                                                                , bIsValidarPrimeraTabla
                                                                                                                                                , bIsDecrypting
                                                                                                                                                , bIsDecompressData);

        #endregion
    }
}
