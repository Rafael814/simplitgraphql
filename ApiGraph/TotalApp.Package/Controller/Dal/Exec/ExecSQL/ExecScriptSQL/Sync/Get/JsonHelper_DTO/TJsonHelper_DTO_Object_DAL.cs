﻿using TotalApp.Package.DTO;

namespace TotalApp.Package.DAL
{
    /// <summary>
    /// *Autor:Larry Ramirez
    /// *Fecha de creacion:09-01-2018
    /// *Version:1.1.20180109
    /// *Sinopsis:
    /// </summary>
    public partial class TBase_ExecScript_DAL
    {
        #region "Get"
        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="sCnx"></param>
        /// <param name="sSql"></param>
        /// <param name="bIsValidarObject"></param>
        /// <param name="bIsDecrypting"></param>
        /// <param name="bIsDecompressData"></param>
        /// <returns></returns>
        public virtual TJsonHelper_DTO Get_JsonHelper_Object_Encrypt_Compress_SqlServer<T>(string sCnx
                                                                                       , string sSql
                                                                                       , bool bIsValidarObject = true
                                                                                       , bool bIsDecrypting = true
                                                                                       , bool bIsDecompressData = true) where T : class
        {

            var oData = Get_Object_SqlServer<T>(sCnx, sSql, bIsValidarObject);

            return new TJsonHelper_DTO(base.IsOK ? base.Encrypt_Compress_Object(oData, bIsDecrypting, bIsDecompressData) : "");
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="sBd"></param>
        /// <param name="sCnx"></param>
        /// <param name="sSql"></param>
        /// <param name="bIsUsarPlantilla"></param>
        /// <param name="bIsValidarObject"></param>
        /// <param name="bIsDecrypting"></param>
        /// <param name="bIsDecompressData"></param>
        /// <returns></returns>
        public virtual TJsonHelper_DTO Get_JsonHelper_Object_Encrypt_Compress_SqlServer<T>(string sBd
                                                                                        , string sCnx
                                                                                        , string sSql
                                                                                        , bool bIsUsarPlantilla = true
                                                                                        , bool bIsValidarObject = true
                                                                                        , bool bIsDecrypting = true
                                                                                        , bool bIsDecompressData = true) where T : class
        {

            var oData = Get_Object_SqlServer<T>(sBd, sCnx, sSql, bIsValidarObject);

            return new TJsonHelper_DTO(base.IsOK ? base.Encrypt_Compress_Object(oData, bIsDecrypting, bIsDecompressData) : "");
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="sServidor"></param>
        /// <param name="sBd"></param>
        /// <param name="sUsuario"></param>
        /// <param name="sClave"></param>
        /// <param name="sSql"></param>
        /// <param name="bIsUsarPlantilla"></param>
        /// <param name="bIsValidarObject"></param>
        /// <param name="bIsDecrypting"></param>
        /// <param name="bIsDecompressData"></param>
        /// <returns></returns>
        public virtual TJsonHelper_DTO Get_JsonHelper_Object_Encrypt_Compress_SqlServer<T>(string sServidor
                                                                                        , string sBd
                                                                                        , string sUsuario
                                                                                        , string sClave
                                                                                        , string sSql
                                                                                        , bool bIsUsarPlantilla = true
                                                                                        , bool bIsValidarObject = true
                                                                                        , bool bIsDecrypting = true
                                                                                        , bool bIsDecompressData = true) where T : class => Get_JsonHelper_Object_Encrypt_Compress_SqlServer<T>(sBd
                                                                                                                                                                                            , CnxStringSqlServer(sServidor, sBd, sUsuario, sClave)
                                                                                                                                                                                            , sSql
                                                                                                                                                                                            , bIsUsarPlantilla
                                                                                                                                                                                            , bIsValidarObject
                                                                                                                                                                                            , bIsDecrypting
                                                                                                                                                                                            , bIsDecompressData);



        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="oDatosConex_DTO"></param>
        /// <param name="sSql"></param>
        /// <param name="bIsUsarPlantilla"></param>
        /// <param name="bIsValidarObject"></param>
        /// <param name="bIsDecrypting"></param>
        /// <param name="bIsDecompressData"></param>
        /// <returns></returns>
        public virtual TJsonHelper_DTO Get_JsonHelper_Object_Encrypt_Compress_SqlServer<T>(DTO.TDatosConex_DTO oDatosConex_DTO
                                                                                          , string sSql
                                                                                          , bool bIsUsarPlantilla = true
                                                                                          , bool bIsValidarObject = true
                                                                                          , bool bIsDecrypting = true
                                                                                          , bool bIsDecompressData = true) where T : class => Get_JsonHelper_Object_Encrypt_Compress_SqlServer<T>(oDatosConex_DTO.DB
                                                                                                                                                                                               , CnxStringSqlServer(oDatosConex_DTO.Server, oDatosConex_DTO.DB, oDatosConex_DTO.User, oDatosConex_DTO.Pass)
                                                                                                                                                                                               , sSql
                                                                                                                                                                                               , bIsUsarPlantilla
                                                                                                                                                                                               , bIsValidarObject
                                                                                                                                                                                               , bIsDecrypting
                                                                                                                                                                                               , bIsDecompressData);


        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="sSql"></param>
        /// <param name="bIsUsarPlantilla"></param>
        /// <param name="bIsValidarObject"></param>
        /// <param name="bIsDecrypting"></param>
        /// <param name="bIsDecompressData"></param>
        /// <returns></returns>
        public virtual TJsonHelper_DTO Get_JsonHelper_Object_Encrypt_Compress_SqlServerByDC<T>(string sSql
                                                                                           , bool bIsUsarPlantilla = true
                                                                                           , bool bIsValidarObject = true
                                                                                           , bool bIsDecrypting = true
                                                                                           , bool bIsDecompressData = true) where T : class => Get_JsonHelper_Object_Encrypt_Compress_SqlServer<T>(THelper.DatosConex_DTO
                                                                                                                                                                                               , sSql
                                                                                                                                                                                               , bIsUsarPlantilla
                                                                                                                                                                                               , bIsValidarObject
                                                                                                                                                                                               , bIsDecrypting
                                                                                                                                                                                               , bIsDecompressData);

        #endregion
    }
}
