﻿
using System;
using System.Data;

namespace TotalApp.Package.DAL
{
    /// <summary>
    /// *Autor:Larry Ramirez
    /// *Fecha de creacion:09-01-2018
    /// *Version:1.1.20180109
    /// *Sinopsis:
    /// </summary>
    public partial class TBase_ExecScript_DAL
    {
        #region "Tran"
        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sCnx"></param>
        /// <param name="sSql"></param>
        /// <returns></returns>
        public virtual int Tran_Exec_SqlServer(string sCnx
                                             , string sSql)
        {
            if (sCnx.TIsNullOrEmpty()) THelperInfoEstatus.ArgumentNull(nameof(sCnx));
            if (sSql.TIsNullOrEmpty()) THelperInfoEstatus.ArgumentNull(nameof(sSql));

            base.OK();

            int nRetorno = 0;
            ITDbManager oTDbManager = new TDbManager(DataProvider.SqlServer);
            try
            {

                oTDbManager.ConnectionString = sCnx;
                oTDbManager.Open(NumIntentosDeConex);

                if (oTDbManager.Error == 0)
                {
                    oTDbManager.CreateParameters(0);

                    try
                    {
                        nRetorno = oTDbManager.ExecuteNonQuery(CommandType.Text, sSql);

                        if (oTDbManager.Error != 0)
                        {
                            base.InternalServerError(oTDbManager.Error
                                                    , "Error de ejecucion en la BD"
                                                    , TTypeError.Err_Bd
                                                    , oTDbManager.Exception);
                        }
                    }
                    catch (Exception ex)
                    {
                        base.InternalServerError(2, "Error Excepcion", TTypeError.Err_CT_DAL, ex.Message);
                    }
                }
                else
                {
                    base.InternalServerError(oTDbManager.Error
                                            , "Error de conexion a la BD"
                                            , TTypeError.Err_CnxBD
                                            , oTDbManager.Exception);
                }

            }
            catch (Exception ex)
            {
                base.InternalServerError("Error Excepcion", TTypeError.Err_CT_DAL, ex.Message);
            }
            finally
            {
                oTDbManager.Dispose();
                oTDbManager = null;
            }

            return nRetorno;
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sBd"></param>
        /// <param name="sCnx"></param>
        /// <param name="sSql"></param>
        /// <param name="bIsUsarPlantilla"></param>
        /// <returns></returns>
        public virtual int Tran_Exec_SqlServer(string sBd
                                             , string sCnx
                                             , string sSql
                                             , bool bIsUsarPlantilla = true)
        {
            if (sBd.TIsNullOrEmpty()) THelperInfoEstatus.ArgumentNull(nameof(sBd));

            if (bIsUsarPlantilla)
            {
                sSql = Templade_Tran_Sql(sBd, sSql);
            }

            return Tran_Exec_SqlServer(sCnx, sSql);
        }


        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sServidor"></param>
        /// <param name="sBd"></param>
        /// <param name="sUsuario"></param>
        /// <param name="sClave"></param>
        /// <param name="sSql"></param>
        /// <param name="bIsUsarPlantilla"></param>
        /// <returns></returns>
        public virtual int Tran_Exec_SqlServer(string sServidor
                                             , string sBd
                                             , string sUsuario
                                             , string sClave
                                             , string sSql
                                             , bool bIsUsarPlantilla = true) => Tran_Exec_SqlServer(sBd
                                                                                                   , CnxStringSqlServer(sServidor, sBd, sUsuario, sClave)
                                                                                                   , sSql
                                                                                                   , bIsUsarPlantilla);
       

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="oDatosConex_DTO"></param>
        /// <param name="sSql"></param>
        /// <param name="bIsUsarPlantilla"></param>
        /// <returns></returns>
        public virtual int Tran_Exec_SqlServer(DTO.TDatosConex_DTO oDatosConex_DTO
                                            , string sSql
                                            , bool bIsUsarPlantilla = true) => Tran_Exec_SqlServer(oDatosConex_DTO.DB
                                                                                                  , CnxStringSqlServer(oDatosConex_DTO.Server, oDatosConex_DTO.DB, oDatosConex_DTO.User, oDatosConex_DTO.Pass)
                                                                                                  , sSql
                                                                                                  , bIsUsarPlantilla);


        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sSql"></param>
        /// <param name="bIsUsarPlantilla"></param>
        /// <returns></returns>
        public virtual int Tran_Exec_SqlServerByDC(string sSql
                                                 , bool bIsUsarPlantilla = true) => Tran_Exec_SqlServer(THelper.DatosConex_DTO
                                                                                                      , sSql
                                                                                                      , bIsUsarPlantilla);

        #endregion
    }
}
