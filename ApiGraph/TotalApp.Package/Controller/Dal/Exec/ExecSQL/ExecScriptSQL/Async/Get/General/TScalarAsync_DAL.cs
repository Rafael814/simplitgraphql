﻿
using System;
using System.Threading.Tasks;

namespace TotalApp.Package.DAL
{
    /// <summary>
    /// *Autor:Larry Ramirez
    /// *Fecha de creacion:09-01-2018
    /// *Version:1.1.20180109
    /// *Sinopsis:
    /// </summary>
    public partial class TBase_ExecScript_DAL
    {
        #region "Get"
        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sCnx"></param>
        /// <param name="sSql"></param>
        /// <returns></returns>
        public virtual Task<object> Get_Scalar_SqlServerAsync(string sCnx
                                                            , string sSql)
        {
            var oTask = new Task<object>(() =>
            {
                return Get_Scalar_SqlServer(sCnx, sSql);
            });

            oTask.Start();

            return oTask;
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sBd"></param>
        /// <param name="sCnx"></param>
        /// <param name="sSql"></param>
        /// <param name="bIsUsarPlantilla"></param>
        /// <returns></returns>
        public virtual Task<object> Get_Scalar_SqlServerAsync(string sBd
                                                            , string sCnx
                                                            , string sSql
                                                            , bool bIsUsarPlantilla = true)
        {
            var oTask = new Task<object>(() =>
            {
                return Get_Scalar_SqlServer(sBd, sCnx, sSql, bIsUsarPlantilla);
            });

            oTask.Start();

            return oTask;
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sServidor"></param>
        /// <param name="sBd"></param>
        /// <param name="sUsuario"></param>
        /// <param name="sClave"></param>
        /// <param name="sSql"></param>
        /// <param name="bIsUsarPlantilla"></param>
        /// <returns></returns>
        public virtual Task<object> Get_Scalar_SqlServerAsync(string sServidor
                                                            , string sBd
                                                            , string sUsuario
                                                            , string sClave
                                                            , string sSql
                                                            , bool bIsUsarPlantilla = true)
        {

            var oTask = new Task<object>(() =>
            {
                return Get_Scalar_SqlServer(sServidor, sBd, sUsuario, sClave, sSql, bIsUsarPlantilla);
            });

            oTask.Start();

            return oTask;

        }


        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="oDatosConex_DTO"></param>
        /// <param name="sSql"></param>
        /// <param name="bIsUsarPlantilla"></param>
        /// <returns></returns>
        public virtual Task<object> Get_Scalar_SqlServerAsync(DTO.TDatosConex_DTO oDatosConex_DTO
                                                            , string sSql
                                                            , bool bIsUsarPlantilla = true)
        {
            var oTask = new Task<object>(() =>
            {
                return Get_Scalar_SqlServer(oDatosConex_DTO, sSql, bIsUsarPlantilla);
            });

            oTask.Start();

            return oTask;
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sSql"></param>
        /// <param name="bIsUsarPlantilla"></param>
        /// <returns></returns>
        public virtual Task<object> Get_Scalar_SqlServerByDCAsync(string sSql
                                                                , bool bIsUsarPlantilla = true)
        {
            var oTask = new Task<object>(() =>
            {
                return Get_Scalar_SqlServerByDC(sSql, bIsUsarPlantilla);
            });

            oTask.Start();

            return oTask;
        }
        #endregion
    }
}
