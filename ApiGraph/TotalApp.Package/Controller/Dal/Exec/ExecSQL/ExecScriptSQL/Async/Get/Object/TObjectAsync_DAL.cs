﻿using System.Threading.Tasks;

namespace TotalApp.Package.DAL
{
    /// <summary>
    /// *Autor:Larry Ramirez
    /// *Fecha de creacion:09-01-2018
    /// *Version:1.1.20180109
    /// *Sinopsis:
    /// </summary>
    public partial class TBase_ExecScript_DAL
    {
        #region "Get"
        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="sCnx"></param>
        /// <param name="sSql"></param>
        /// <param name="bIsValidarObject"></param>
        /// <returns></returns>
        public virtual Task<T> Get_Object_SqlServerAsync<T>(string sCnx
                                                          , string sSql
                                                          , bool bIsValidarObject = true) where T : class
        {
            var oTask = new Task<T>(() =>
            {
                return Get_Object_SqlServer<T>(sCnx, sSql, bIsValidarObject);
            });

            oTask.Start();

            return oTask;

        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="sBd"></param>
        /// <param name="sCnx"></param>
        /// <param name="sSql"></param>
        /// <param name="bIsUsarPlantilla"></param>
        /// <param name="bIsValidarObject"></param>
        /// <returns></returns>
        public virtual Task<T> Get_Object_SqlServerAsync<T>(string sBd
                                                           , string sCnx
                                                           , string sSql
                                                           , bool bIsUsarPlantilla = true
                                                           , bool bIsValidarObject = true) where T : class
        {
            var oTask = new Task<T>(() =>
            {
                return Get_Object_SqlServer<T>(sBd, sCnx, sSql, bIsUsarPlantilla, bIsValidarObject);
            });

            oTask.Start();

            return oTask;

        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="sServidor"></param>
        /// <param name="sBd"></param>
        /// <param name="sUsuario"></param>
        /// <param name="sClave"></param>
        /// <param name="sSql"></param>
        /// <param name="bIsUsarPlantilla"></param>
        /// <param name="bIsValidarObject"></param>
        /// <returns></returns>
        public virtual Task<T> Get_Object_SqlServerAsync<T>(string sServidor
                                                           , string sBd
                                                           , string sUsuario
                                                           , string sClave
                                                           , string sSql
                                                           , bool bIsUsarPlantilla = true
                                                           , bool bIsValidarObject = true) where T : class
        {

            var oTask = new Task<T>(() =>
            {
                return Get_Object_SqlServer<T>(sServidor, sBd, sUsuario, sClave, sSql, bIsUsarPlantilla, bIsValidarObject);
            });

            oTask.Start();

            return oTask;
        }


        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="oDatosConex_DTO"></param>
        /// <param name="sSql"></param>
        /// <param name="bIsUsarPlantilla"></param>
        /// <param name="bIsValidarObject"></param>
        /// <returns></returns>
        public virtual Task<T> Get_Object_SqlServerAsync<T>(DTO.TDatosConex_DTO oDatosConex_DTO
                                                           , string sSql
                                                           , bool bIsUsarPlantilla = true
                                                           , bool bIsValidarObject = true) where T : class
        {

            var oTask = new Task<T>(() =>
            {
                return Get_Object_SqlServer<T>(oDatosConex_DTO, sSql, bIsUsarPlantilla, bIsValidarObject);
            });

            oTask.Start();

            return oTask;
        }


        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="sSql"></param>
        /// <param name="bIsUsarPlantilla"></param>
        /// <param name="bIsValidarObject"></param>
        /// <returns></returns>
        public virtual Task<T> Get_Object_SqlServerByDCAsync<T>(string sSql
                                                              , bool bIsUsarPlantilla = true
                                                              , bool bIsValidarObject = true) where T : class
        {

            var oTask = new Task<T>(() =>
            {
                return Get_Object_SqlServerByDC<T>(sSql, bIsUsarPlantilla, bIsValidarObject);
            });

            oTask.Start();

            return oTask;
        }
        #endregion
    }
}
