﻿using System.Collections.Generic;
using System.Threading.Tasks;
using TotalApp.Package.TLINQ;
using TotalApp.Package.UTIL;


namespace TotalApp.Package.DAL
{
    /// <summary>
    /// *Autor:Larry Ramirez
    /// *Fecha de creacion:09-01-2018
    /// *Version:1.1.20180109
    /// *Sinopsis:
    /// </summary>
    public partial class TBase_ExecScript_DAL
    {
        #region "Get"
        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="sCnx"></param>
        /// <param name="sSql"></param>
        /// <param name="nDividirEn"></param>
        /// <param name="bIsValidarObject"></param>
        /// <returns></returns>
        public virtual Task<List<TListData<T>>> Get_Part_List_SqlServerAsync<T>(string sCnx
                                                                              , string sSql
                                                                              , int nDividirEn
                                                                              , bool bIsValidarObject = true) where T : class
        {
            var oTask = new Task<List<TListData<T>>>(() =>
            {
                return Get_Part_List_SqlServer<T>(sCnx, sSql, nDividirEn, bIsValidarObject);
            });

            oTask.Start();

            return oTask;
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="sBd"></param>
        /// <param name="sCnx"></param>
        /// <param name="sSql"></param>
        /// <param name="nDividirEn"></param>
        /// <param name="bIsUsarPlantilla"></param>
        /// <param name="bIsValidarObject"></param>
        /// <returns></returns>
        public virtual Task<List<TListData<T>>> Get_Part_List_SqlServerAsync<T>(string sBd
                                                                              , string sCnx
                                                                              , string sSql
                                                                              , int nDividirEn
                                                                              , bool bIsUsarPlantilla = true
                                                                              , bool bIsValidarObject = true) where T : class
        {
            var oTask = new Task<List<TListData<T>>>(() =>
            {
                return Get_Part_List_SqlServer<T>(sBd, sCnx, sSql, nDividirEn, bIsUsarPlantilla , bIsValidarObject);
            });

            oTask.Start();

            return oTask;
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="sServidor"></param>
        /// <param name="sBd"></param>
        /// <param name="sUsuario"></param>
        /// <param name="sClave"></param>
        /// <param name="sSql"></param>
        /// <param name="nDividirEn"></param>
        /// <param name="bIsUsarPlantilla"></param>
        /// <param name="bIsValidarObject"></param>
        /// <returns></returns>
        public virtual Task<List<TListData<T>>> Get_Part_List_SqlServerAsync<T>(string sServidor
                                                                              , string sBd
                                                                              , string sUsuario
                                                                              , string sClave
                                                                              , string sSql
                                                                              , int nDividirEn
                                                                              , bool bIsUsarPlantilla = true
                                                                              , bool bIsValidarObject = true) where T : class
        {
            var oTask = new Task<List<TListData<T>>>(() =>
            {
                return Get_Part_List_SqlServer<T>(sServidor, sBd, sUsuario, sClave, sSql, nDividirEn, bIsUsarPlantilla, bIsValidarObject);
            });

            oTask.Start();

            return oTask;
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="oDatosConex_DTO"></param>
        /// <param name="sSql"></param>
        /// <param name="bIsUsarPlantilla"></param>
        /// <param name="bIsValidarPrimeraTabla"></param>
        /// <returns></returns>
        public virtual Task<List<TListData<T>>> Get_Part_List_SqlServerAsync<T>(DTO.TDatosConex_DTO oDatosConex_DTO
                                                                              , string sSql
                                                                              , int nDividirEn
                                                                              , bool bIsUsarPlantilla = true
                                                                              , bool bIsValidarObject = true) where T : class
        {

            var oTask = new Task<List<TListData<T>>>(() =>
            {
                return Get_Part_List_SqlServer<T>(oDatosConex_DTO, sSql, nDividirEn, bIsUsarPlantilla, bIsValidarObject);
            });

            oTask.Start();

            return oTask;
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="sSql"></param>
        /// <param name="nDividirEn"></param>
        /// <param name="bIsUsarPlantilla"></param>
        /// <param name="bIsValidarObject"></param>
        /// <returns></returns>
        public virtual Task<List<TListData<T>>> Get_Part_List_SqlServerByDCAsync<T>(string sSql
                                                                                  , int nDividirEn
                                                                                  , bool bIsUsarPlantilla = true
                                                                                  , bool bIsValidarObject = true) where T : class
        {

            var oTask = new Task<List<TListData<T>>>(() =>
            {
                return Get_Part_List_SqlServerByDC<T>(sSql, nDividirEn, bIsUsarPlantilla, bIsValidarObject);
            });

            oTask.Start();

            return oTask;
        }
        #endregion
    }
}
