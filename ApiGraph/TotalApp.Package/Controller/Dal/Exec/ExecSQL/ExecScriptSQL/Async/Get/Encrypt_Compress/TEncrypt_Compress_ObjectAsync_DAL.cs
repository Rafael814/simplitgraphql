﻿using System.Threading.Tasks;

namespace TotalApp.Package.DAL
{
    /// <summary>
    /// *Autor:Larry Ramirez
    /// *Fecha de creacion:09-01-2018
    /// *Version:1.1.20180109
    /// *Sinopsis:
    /// </summary>
    public partial class TBase_ExecScript_DAL
    {
        #region "Get"
        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="sCnx"></param>
        /// <param name="sSql"></param>
        /// <param name="bIsValidarObject"></param>
        /// <param name="bIsDecrypting"></param>
        /// <param name="bIsDecompressData"></param>
        /// <returns></returns>
        public virtual Task<string> Get_Object_Encrypt_Compress_SqlServerAsync<T>(string sCnx
                                                                                 , string sSql
                                                                                 , bool bIsValidarObject = true
                                                                                 , bool bIsDecrypting = true
                                                                                 , bool bIsDecompressData = true) where T : class
        {

            var oTask = new Task<string>(() =>
            {
                return Get_Object_Encrypt_Compress_SqlServer<T>(sCnx, sSql, bIsValidarObject, bIsDecrypting, bIsDecompressData);
            });

            oTask.Start();

            return oTask;
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="sBd"></param>
        /// <param name="sCnx"></param>
        /// <param name="sSql"></param>
        /// <param name="bIsUsarPlantilla"></param>
        /// <param name="bIsValidarObject"></param>
        /// <param name="bIsDecrypting"></param>
        /// <param name="bIsDecompressData"></param>
        /// <returns></returns>
        public virtual Task<string> Get_Object_Encrypt_Compress_SqlServerAsync<T>(string sBd
                                                                                , string sCnx
                                                                                , string sSql
                                                                                , bool bIsUsarPlantilla = true
                                                                                , bool bIsValidarObject = true
                                                                                , bool bIsDecrypting = true
                                                                                , bool bIsDecompressData = true) where T : class
        {

            var oTask = new Task<string>(() =>
            {
                return Get_Object_Encrypt_Compress_SqlServer<T>(sBd, sCnx, sSql, bIsUsarPlantilla, bIsValidarObject, bIsDecrypting, bIsDecompressData);
            });

            oTask.Start();

            return oTask;
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="sServidor"></param>
        /// <param name="sBd"></param>
        /// <param name="sUsuario"></param>
        /// <param name="sClave"></param>
        /// <param name="sSql"></param>
        /// <param name="bIsUsarPlantilla"></param>
        /// <param name="bIsValidarObject"></param>
        /// <param name="bIsDecrypting"></param>
        /// <param name="bIsDecompressData"></param>
        /// <returns></returns>
        public virtual Task<string> Get_Object_Encrypt_Compress_SqlServerAsync<T>(string sServidor
                                                                                , string sBd
                                                                                , string sUsuario
                                                                                , string sClave
                                                                                , string sSql
                                                                                , bool bIsUsarPlantilla = true
                                                                                , bool bIsValidarObject = true
                                                                                , bool bIsDecrypting = true
                                                                                , bool bIsDecompressData = true) where T : class
        {

            var oTask = new Task<string>(() =>
            {
                return Get_Object_Encrypt_Compress_SqlServer<T>(sServidor, sBd, sUsuario, sClave, sSql, bIsUsarPlantilla, bIsValidarObject, bIsDecrypting, bIsDecompressData);
            });

            oTask.Start();

            return oTask;

        }


        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="oDatosConex_DTO"></param>
        /// <param name="sSql"></param>
        /// <param name="bIsUsarPlantilla"></param>
        /// <param name="bIsValidarObject"></param>
        /// <param name="bIsDecrypting"></param>
        /// <param name="bIsDecompressData"></param>
        /// <returns></returns>
        public virtual Task<string> Get_Object_Encrypt_Compress_SqlServerAsync<T>(DTO.TDatosConex_DTO oDatosConex_DTO
                                                                                , string sSql
                                                                                , bool bIsUsarPlantilla = true
                                                                                , bool bIsValidarObject = true
                                                                                , bool bIsDecrypting = true
                                                                                , bool bIsDecompressData = true) where T : class
        {

            var oTask = new Task<string>(() =>
            {
                return Get_Object_Encrypt_Compress_SqlServer<T>(oDatosConex_DTO, sSql, bIsUsarPlantilla, bIsValidarObject, bIsDecrypting, bIsDecompressData);
            });

            oTask.Start();

            return oTask;
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="sSql"></param>
        /// <param name="bIsUsarPlantilla"></param>
        /// <param name="bIsValidarObject"></param>
        /// <param name="bIsDecrypting"></param>
        /// <param name="bIsDecompressData"></param>
        /// <returns></returns>
        public virtual Task<string> Get_Object_Encrypt_Compress_SqlServerByDCAsync<T>(string sSql
                                                                                    , bool bIsUsarPlantilla = true
                                                                                    , bool bIsValidarObject = true
                                                                                    , bool bIsDecrypting = true
                                                                                    , bool bIsDecompressData = true) where T : class
        {

            var oTask = new Task<string>(() =>
            {
                return Get_Object_Encrypt_Compress_SqlServerByDC<T>(sSql, bIsUsarPlantilla, bIsValidarObject, bIsDecrypting, bIsDecompressData);
            });

            oTask.Start();

            return oTask;
        }
        #endregion
    }
}
