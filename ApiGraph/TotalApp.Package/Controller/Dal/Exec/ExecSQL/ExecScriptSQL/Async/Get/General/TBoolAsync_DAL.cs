﻿using System.Threading.Tasks;

namespace TotalApp.Package.DAL
{
    /// <summary>
    /// *Autor:Larry Ramirez
    /// *Fecha de creacion:09-01-2018
    /// *Version:1.1.20180109
    /// *Sinopsis:
    /// </summary>
    public partial class TBase_ExecScript_DAL
    {
        #region "Get"
        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sCnx"></param>
        /// <param name="sSql"></param>
        /// <param name="bIsValidarPrimeraTabla"></param>
        /// <returns></returns>
        public virtual Task<bool> Get_Bool_SqlServerAsync(string sCnx
                                                        , string sSql
                                                        , bool bIsValidarPrimeraTabla = true)
        {


            var oTask = new Task<bool>(() =>
            {
                return Get_Bool_SqlServer(sCnx, sSql, bIsValidarPrimeraTabla);
            });

            oTask.Start();

            return oTask;
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sBd"></param>
        /// <param name="sCnx"></param>
        /// <param name="sSql"></param>
        /// <param name="bIsUsarPlantilla"></param>
        /// <param name="bIsValidarPrimeraTabla"></param>
        /// <returns></returns>
        public virtual Task<bool> Get_Bool_SqlServerAsync(string sBd
                                                        , string sCnx
                                                        , string sSql
                                                        , bool bIsUsarPlantilla = true
                                                        , bool bIsValidarPrimeraTabla = true)
        {

            var oTask = new Task<bool>(() =>
            {
                return Get_Bool_SqlServer(sBd, sCnx, sSql, bIsUsarPlantilla, bIsValidarPrimeraTabla);
            });

            oTask.Start();

            return oTask;
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sServidor"></param>
        /// <param name="sBd"></param>
        /// <param name="sUsuario"></param>
        /// <param name="sClave"></param>
        /// <param name="sSql"></param>
        /// <param name="bIsUsarPlantilla"></param>
        /// <param name="bIsValidarPrimeraTabla"></param>
        /// <returns></returns>
        public virtual Task<bool> Get_Bool_SqlServerAsync(string sServidor
                                                        , string sBd
                                                        , string sUsuario
                                                        , string sClave
                                                        , string sSql
                                                        , bool bIsUsarPlantilla = true
                                                        , bool bIsValidarPrimeraTabla = true)
        {

            var oTask = new Task<bool>(() =>
            {
                return Get_Bool_SqlServer(sServidor, sBd, sUsuario, sClave, sSql, bIsUsarPlantilla, bIsValidarPrimeraTabla);
            });

            oTask.Start();

            return oTask;
        }


        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="oDatosConex_DTO"></param>
        /// <param name="sSql"></param>
        /// <param name="bIsUsarPlantilla"></param>
        /// <param name="bIsValidarPrimeraTabla"></param>
        /// <returns></returns>
        public virtual Task<bool> Get_Bool_SqlServerAsync(DTO.TDatosConex_DTO oDatosConex_DTO
                                                        , string sSql
                                                        , bool bIsUsarPlantilla = true
                                                        , bool bIsValidarPrimeraTabla = true)
        {

            var oTask = new Task<bool>(() =>
            {
                return Get_Bool_SqlServer(oDatosConex_DTO, sSql, bIsUsarPlantilla, bIsValidarPrimeraTabla);
            });

            oTask.Start();

            return oTask;
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sSql"></param>
        /// <param name="bIsUsarPlantilla"></param>
        /// <param name="bIsValidarPrimeraTabla"></param>
        /// <returns></returns>
        public virtual Task<bool> Get_Bool_SqlServerByDCAsync(string sSql
                                                            , bool bIsUsarPlantilla = true
                                                            , bool bIsValidarPrimeraTabla = true)
        {

            var oTask = new Task<bool>(() =>
            {
                return Get_Bool_SqlServerByDC(sSql, bIsUsarPlantilla, bIsValidarPrimeraTabla);
            });

            oTask.Start();

            return oTask;
        }
        #endregion
    }
}
