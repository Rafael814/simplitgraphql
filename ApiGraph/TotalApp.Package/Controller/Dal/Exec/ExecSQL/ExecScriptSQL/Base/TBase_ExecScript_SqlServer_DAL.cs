﻿namespace TotalApp.Package.DAL
{
    /// <summary>
    /// *Autor:Larry Ramirez
    /// *Fecha de creacion:09-01-2018
    /// *Version:1.1.20180117
    /// *Sinopsis:Clase generica que permite ejecutar script en cualquier BD SQL SERVER desde 2000 o superior
    /// los elementos de esta clase son virtual, ya que la clase se puede usar como una clase base, asi modificar los elementos
    /// publicos de ser necesario
    /// </summary>
    public partial class TBase_ExecScript_DAL: TBase_Exec_DAL
    {
        #region "Constantes"
        #endregion

        #region "Variables"
        #endregion

        #region "Propiedades"
        #endregion

        #region "Funciones"
        #endregion

        #region "Metodos & constructores"
        public TBase_ExecScript_DAL():base()
        {

        }
        #endregion

        #region "Eventos"
        #endregion

        #region "Destructor"
       
        // <summary>
        /// Autor   : Larry Ramirez
        /// Fecha   : 25-05-2016
        /// Sinopsis: Destructor por defecto.
        /// </summary>
        ~TBase_ExecScript_DAL()
        {
          
        }
        #endregion
    }
}
