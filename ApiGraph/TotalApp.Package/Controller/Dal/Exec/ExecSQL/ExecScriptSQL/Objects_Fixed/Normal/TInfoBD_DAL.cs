﻿using System.Collections.Generic;
using TotalApp.Package.DTO;

namespace TotalApp.Package.DAL
{
    /// <summary>
    /// *Autor:Larry Ramirez
    /// *Fecha de creacion:09-01-2018
    /// *Version:1.1.20180117
    /// *Sinopsis:Clase que permite retornar objetos ya establecidos de la libreria TotalApp en su agrupacion DTO
    /// para este caso aplica solo a la Informacion general de la base de datos
    /// </summary>
    public class TInfoBD_DAL : TBase_ExecScript_DAL
    {
        #region "Constantes"
        #endregion

        #region "Variables"
        #endregion

        #region "Propiedades"
        #endregion

        #region "Funciones"


        #region "Get"

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sServidor"></param>
        /// <param name="sBd"></param>
        /// <param name="sUsuario"></param>
        /// <param name="sClave"></param>
        /// <returns></returns>
        public TInfoBd_DTO Get_InfoBd(string sServidor
                                    , string sBd
                                    , string sUsuario
                                    , string sClave) => base.Get_Object_SqlServer<TInfoBd_DTO>(sServidor
                                                                                             , sBd
                                                                                             , sUsuario
                                                                                             , sClave
                                                                                             , THelperQuerys.InfoBD(sBd));


        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="oDatosConex_DTO"></param>
        /// <returns></returns>
        public TInfoBd_DTO Get_InfoBd(TDatosConex_DTO oDatosConex_DTO) => base.Get_Object_SqlServer<TInfoBd_DTO>(oDatosConex_DTO.Server
                                                                                                                , "master"
                                                                                                                , oDatosConex_DTO.User
                                                                                                                , oDatosConex_DTO.Pass
                                                                                                                , THelperQuerys.InfoBD(oDatosConex_DTO.DB));

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sCnx"></param>
        /// <returns></returns>
        public List<TInfoBd_DTO> Get_ListBasicInfoBD(string sCnx) => base.Get_List_SqlServer<TInfoBd_DTO>(sCnx
                                                                                                        , THelperQuerys.GetListBasicInfoBD);

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sServidor"></param>
        /// <param name="sBd"></param>
        /// <param name="sUsuario"></param>
        /// <param name="sClave"></param>
        /// <returns></returns>
        public List<TInfoBd_DTO> Get_ListBasicInfoBD(string sServidor
                                                   , string sBd
                                                   , string sUsuario
                                                   , string sClave) => base.Get_List_SqlServer<TInfoBd_DTO>(sServidor
                                                                                                          , sBd
                                                                                                          , sUsuario
                                                                                                          , sClave
                                                                                                          , THelperQuerys.GetListBasicInfoBD);


        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="oDatosConex_DTO"></param>
        /// <returns></returns>
        public List<TInfoBd_DTO> Get_ListBasicInfoBD(TDatosConex_DTO oDatosConex_DTO) => base.Get_List_SqlServer<TInfoBd_DTO>(oDatosConex_DTO.Server
                                                                                                                            , "master"
                                                                                                                            , oDatosConex_DTO.User
                                                                                                                            , oDatosConex_DTO.Pass
                                                                                                                            , THelperQuerys.GetListBasicInfoBD);

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sCnx"></param>
        /// <returns></returns>
        public List<TInfoBd_DTO> Get_ListInfoBD(string sCnx) => base.Get_List_SqlServer<TInfoBd_DTO>(sCnx
                                                                                                   , THelperQuerys.GetListInfoBD);

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sServidor"></param>
        /// <param name="sBd"></param>
        /// <param name="sUsuario"></param>
        /// <param name="sClave"></param>
        /// <returns></returns>
        public List<TInfoBd_DTO> Get_ListInfoBD(string sServidor
                                              , string sBd
                                              , string sUsuario
                                              , string sClave) => base.Get_List_SqlServer<TInfoBd_DTO>(sServidor
                                                                                                     , sBd
                                                                                                     , sUsuario
                                                                                                     , sClave
                                                                                                     , THelperQuerys.GetListInfoBD);


        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="oDatosConex_DTO"></param>
        /// <returns></returns>
        public List<TInfoBd_DTO> Get_ListInfoBD(TDatosConex_DTO oDatosConex_DTO) => base.Get_List_SqlServer<TInfoBd_DTO>(oDatosConex_DTO.Server
                                                                                                                        , "master"
                                                                                                                        , oDatosConex_DTO.User
                                                                                                                        , oDatosConex_DTO.Pass
                                                                                                                        , THelperQuerys.GetListInfoBD);

        #endregion


        #endregion

        #region "Metodos & constructores"
        public TInfoBD_DAL():base()
        {

        }
        #endregion

        #region "Eventos"
        #endregion

        #region "Destructor"
      
       
        // <summary>
        /// Autor   : Larry Ramirez
        /// Fecha   : 25-05-2016
        /// Sinopsis: Destructor por defecto.
        /// </summary>
        ~TInfoBD_DAL()
        {
            base.Dispose();
        }
        #endregion
    }
}
