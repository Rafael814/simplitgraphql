﻿using System.Collections.Generic;
using TotalApp.Package.DTO;

namespace TotalApp.Package.DAL
{
    /// <summary>
    /// *Autor:Larry Ramirez
    /// *Fecha de creacion:09-01-2018
    /// *Version:1.1.20180117
    /// *Sinopsis:Clase que permite retornar objetos ya establecidos de la libreria TotalApp en su agrupacion DTO
    /// para este caso aplica solo a la Informacion general de las tablas
    /// </summary>
    public class TInfoTable_DAL : TBase_ExecScript_DAL
    {
        #region "Constantes"
        #endregion

        #region "Variables"
        #endregion

        #region "Propiedades"
        #endregion

        #region "Funciones"


        #region "Get"

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sCnx"></param>
        /// <param name="sTable"></param>
        /// <returns></returns>
        public TInfoTable_DTO Get_InfoBd(string sCnx
                                       , string sTable) => base.Get_Object_SqlServer<TInfoTable_DTO>(sCnx
                                                                                                   , THelperQuerys.InfoTable(sTable));


        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sServidor"></param>
        /// <param name="sBd"></param>
        /// <param name="sUsuario"></param>
        /// <param name="sClave"></param>
        /// <param name="sTable"></param>
        /// <returns></returns>
        public TInfoTable_DTO Get_InfoBd(string sServidor
                                       , string sBd
                                       , string sUsuario
                                       , string sClave
                                       , string sTable) => base.Get_Object_SqlServer<TInfoTable_DTO>(sServidor
                                                                                                  , sBd
                                                                                                  , sUsuario
                                                                                                  , sClave
                                                                                                  , THelperQuerys.InfoTable(sTable));



        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="oDatosConex_DTO"></param>
        /// <param name="sTable"></param>
        /// <returns></returns>
        public TInfoTable_DTO Get_InfoBd(TDatosConex_DTO oDatosConex_DTO
                                       , string sTable) => base.Get_Object_SqlServer<TInfoTable_DTO>(oDatosConex_DTO
                                                                                                   , THelperQuerys.InfoTable(sTable));



        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sCnx"></param>
        /// <param name="sTable"></param>
        /// <returns></returns>
        public List<TInfoTable_DTO> Get_ListInfoTable(string sCnx
                                                    , string sTable) => base.Get_List_SqlServer<TInfoTable_DTO>(sCnx
                                                                                                        , THelperQuerys.GetListInfoTable);


        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sServidor"></param>
        /// <param name="sBd"></param>
        /// <param name="sUsuario"></param>
        /// <param name="sClave"></param>
        /// <param name="sTable"></param>
        /// <returns></returns>
        public List<TInfoTable_DTO> GetListInfoTable(string sServidor
                                       , string sBd
                                       , string sUsuario
                                       , string sClave
                                       , string sTable) => base.Get_List_SqlServer<TInfoTable_DTO>(sServidor
                                                                                                  , sBd
                                                                                                  , sUsuario
                                                                                                  , sClave
                                                                                                  , THelperQuerys.GetListInfoTable);



        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="oDatosConex_DTO"></param>
        /// <param name="sTable"></param>
        /// <returns></returns>
        public List<TInfoTable_DTO> GetListInfoTable(TDatosConex_DTO oDatosConex_DTO
                                                   , string sTable) => base.Get_List_SqlServer<TInfoTable_DTO>(oDatosConex_DTO
                                                                                                             , THelperQuerys.GetListInfoTable);



        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sCnx"></param>
        /// <param name="sTable"></param>
        /// <returns></returns>
        public bool Get_ExistsTable(string sCnx
                                  , string sTable) => base.Get_Bool_SqlServer(sCnx
                                                                             , THelperQuerys.InfoTable(sTable));


        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sServidor"></param>
        /// <param name="sBd"></param>
        /// <param name="sUsuario"></param>
        /// <param name="sClave"></param>
        /// <param name="sTable"></param>
        /// <returns></returns>
        /// <summary>
        public bool Get_ExistsTable(string sServidor
                                  , string sBd
                                  , string sUsuario
                                  , string sClave
                                  , string sTable) => base.Get_Bool_SqlServer(sServidor
                                                                            , sBd
                                                                            , sUsuario
                                                                            , sClave
                                                                            , THelperQuerys.InfoTable(sTable));


        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sServidor"></param>
        /// <param name="sBd"></param>
        /// <param name="sUsuario"></param>
        /// <param name="sClave"></param>
        /// <param name="sTable"></param>
        /// <returns></returns>
        public bool Get_ExistsTable(TDatosConex_DTO oDatosConex_DTO
                                  , string sTable) => base.Get_Bool_SqlServer(oDatosConex_DTO
                                                                            , THelperQuerys.InfoTable(sTable));
        #endregion


        #endregion

        #region "Metodos & constructores"
        public TInfoTable_DAL():base()
        {

        }
        #endregion

        #region "Eventos"
        #endregion

        #region "Destructor"
      
       
        // <summary>
        /// Autor   : Larry Ramirez
        /// Fecha   : 25-05-2016
        /// Sinopsis: Destructor por defecto.
        /// </summary>
        ~TInfoTable_DAL()
        {
            base.Dispose();
        }
        #endregion
    }
}
