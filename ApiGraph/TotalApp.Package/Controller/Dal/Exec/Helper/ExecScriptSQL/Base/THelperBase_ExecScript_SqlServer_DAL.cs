﻿using System.Net;

namespace TotalApp.Package.DAL
{
    /// <summary>
    /// *Autor:Larry Ramirez
    /// *Fecha de creacion:09-01-2018
    /// *Version:1.1.20180117
    /// *Sinopsis:
    /// </summary>
    public static partial class THelperBase_ExecScript_DAL
    {
        #region "Constantes"
        #endregion

        #region "Variables"
        private static int _nError = 0;
        private static string _sMensaje = THelper.Empty;
        private static HttpStatusCode _oStatusCode = HttpStatusCode.OK;
        private static string _sMensajeInterno = THelper.Empty;
        #endregion

        #region "Propiedades"
        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        public static int Error
        {
            get { return _nError; }
            set
            {
                _nError = value;
            }

        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        public static string Mensaje
        {
            get => _sMensaje; 
            set => _sMensaje = value;
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        public static HttpStatusCode StatusCode
        {
            get { return _oStatusCode; }
            set
            {
                _oStatusCode = value;
            }
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:08-02-2018
        /// *Version:1.1.20180208
        /// *Sinopsis:
        /// </summary>
        public static string MensajeDev
        {
            get => _sMensajeInterno; 
            set => _sMensajeInterno = value;
        }


        #endregion

        #region "Funciones"
        #endregion

        #region "Metodos & constructores"
        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:08-02-2018
        /// *Version:1.1.20180208
        /// *Sinopsis:
        /// </summary>
        private static void OK()
        {
            Error = 0;
            Mensaje = THelper.Empty;
            StatusCode = HttpStatusCode.OK;
            MensajeDev = THelper.Empty;
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:08-02-2018
        /// *Version:1.1.20180208
        /// *Sinopsis:
        /// </summary>
        /// <param name="nError"></param>
        /// <param name="sMensaje"></param>
        /// <param name="oStatusCode"></param>
        /// <param name="sMensajeDev"></param>
        private static void SetError(int nError
                                   , string sMensaje
                                   , HttpStatusCode oStatusCode
                                   , string sMensajeDev)
        {
            Error = nError;
            Mensaje = sMensaje;
            StatusCode = oStatusCode;
            MensajeDev = sMensajeDev;
        }
        #endregion

        #region "Eventos"
        #endregion

        #region "Destructor"
        #endregion
    }
}
