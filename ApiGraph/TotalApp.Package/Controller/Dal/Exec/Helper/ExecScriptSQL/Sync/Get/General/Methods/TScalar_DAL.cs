﻿
namespace TotalApp.Package.DAL
{
    /// <summary>
    /// *Autor:Larry Ramirez
    /// *Fecha de creacion:09-01-2018
    /// *Version:1.1.20180109
    /// *Sinopsis:
    /// </summary>
    public static partial class THelperBase_ExecScript_DAL
    {
        #region "Get"

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="sCnx"></param>
        /// <param name="sSql"></param>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static void Get_Scalar_SqlServer<T>(string sCnx
                                               , string sSql
                                               , ref T obj)
        {
            OK();
            obj = default(T);
            using (var oDal = new TBase_ExecScript_DAL())
            {
                var oResp = oDal.Get_Scalar_SqlServer(sCnx
                                                    , sSql);

                if (!oDal.IsOK)
                {
                    obj = oResp.TConvertTo<T>();
                }
                else
                {
                    SetError(oDal.Error
                           , oDal.Mensaje
                           , oDal.StatusCode
                           , oDal.MensajeDev);
                }
               

                oDal.Dispose();
            }

        }


        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="sBd"></param>
        /// <param name="sCnx"></param>
        /// <param name="sSql"></param>
        /// <param name="obj"></param>
        /// <param name="bIsUsarPlantilla"></param>
        public static void Get_Scalar_SqlServer<T>(string sBd
                                                 ,  string sCnx
                                                 ,  string sSql
                                                 ,  ref T obj
                                                 ,  bool bIsUsarPlantilla = true)
        {

            OK();
            obj = default(T);
            using (var oDal = new TBase_ExecScript_DAL())
            {
                var oResp = oDal.Get_Scalar_SqlServer(sBd
                                                    , sCnx
                                                    , sSql);

                if (!oDal.IsOK)
                {
                    obj = oResp.TConvertTo<T>();
                }
                else
                {
                    SetError(oDal.Error
                           , oDal.Mensaje
                           , oDal.StatusCode
                           , oDal.MensajeDev);
                }


                oDal.Dispose();
            }

        }


        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="sServidor"></param>
        /// <param name="sBd"></param>
        /// <param name="sUsuario"></param>
        /// <param name="sClave"></param>
        /// <param name="sSql"></param>
        /// <param name="obj"></param>
        /// <param name="bIsUsarPlantilla"></param>
        /// <returns></returns>
        public static void  Get_Scalar_SqlServer<T>(string sServidor
                                                  , string sBd
                                                  , string sUsuario
                                                  , string sClave
                                                  , string sSql
                                                  , ref T obj
                                                  , bool bIsUsarPlantilla = true) => Get_Scalar_SqlServer(sBd
                                                                                                        , THelper.CnxStringSqlServer(sServidor, sBd, sUsuario, sClave)
                                                                                                        , sSql
                                                                                                        , ref obj
                                                                                                        , bIsUsarPlantilla);



        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name=""></param>
        public static void Get_Scalar_SqlServer<T>(this DTO.TDatosConex_DTO oDatosConex_DTO
                                                  , string sSql
                                                  , ref T obj
                                                  , bool bIsUsarPlantilla = true) => Get_Scalar_SqlServer(oDatosConex_DTO.DB
                                                                                                        , THelper.CnxStringSqlServer(oDatosConex_DTO.Server, oDatosConex_DTO.DB, oDatosConex_DTO.User, oDatosConex_DTO.Pass)
                                                                                                        , sSql
                                                                                                        , ref obj
                                                                                                        , bIsUsarPlantilla);



        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="sSql"></param>
        /// <param name="obj"></param>
        /// <param name="bIsUsarPlantilla"></param>
        public static void Get_Scalar_SqlServerByDC<T>(string sSql
                                                      , ref T obj
                                                      , bool bIsUsarPlantilla = true) => Get_Scalar_SqlServer(THelper.DatosConex_DTO
                                                                                                           , sSql
                                                                                                           , ref obj
                                                                                                           , bIsUsarPlantilla);

        #endregion
    }
}
