﻿using System.Collections.Generic;
using TotalApp.Package.UTIL;


namespace TotalApp.Package.DAL
{
    /// <summary>
    /// *Autor:Larry Ramirez
    /// *Fecha de creacion:09-01-2018
    /// *Version:1.1.20180109
    /// *Sinopsis:
    /// </summary>
    public static partial class THelperBase_ExecScript_DAL
    {
        #region "Get"
        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="sCnx"></param>
        /// <param name="sSql"></param>
        /// <param name="nDividirEn"></param>
        /// <param name="bIsValidarObject"></param>
        /// <returns></returns>
        public static List<TListData<T>> Get_Part_List_SqlServer<T>(string sCnx
                                                                   , string sSql
                                                                   , int nDividirEn
                                                                   , bool bIsValidarObject = true) where T : class
        {


            OK();
            var oResp = default(List<TListData<T>>);
            using (var oDal = new TBase_ExecScript_DAL())
            {
                oResp = oDal.Get_Part_List_SqlServer<T>(sCnx
                                                      , sSql
                                                      , nDividirEn
                                                      , bIsValidarObject);

                SetError(oDal.Error
                       , oDal.Mensaje
                       , oDal.StatusCode
                       , oDal.MensajeDev);

                oDal.Dispose();
            }

            return oResp;
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="sBd"></param>
        /// <param name="sCnx"></param>
        /// <param name="sSql"></param>
        /// <param name="nDividirEn"></param>
        /// <param name="bIsUsarPlantilla"></param>
        /// <param name="bIsValidarObject"></param>
        /// <returns></returns>
        public static List<TListData<T>> Get_Part_List_SqlServer<T>(string sBd
                                                                    , string sCnx
                                                                    , string sSql
                                                                    , int nDividirEn
                                                                    , bool bIsUsarPlantilla = true
                                                                    , bool bIsValidarObject = true) where T : class
        {
            OK();
            var oResp = default(List<TListData<T>>);
            using (var oDal = new TBase_ExecScript_DAL())
            {
                oResp = oDal.Get_Part_List_SqlServer<T>(sBd
                                                      , sCnx
                                                      , sSql
                                                      , nDividirEn
                                                      , bIsUsarPlantilla
                                                      , bIsValidarObject);

                SetError(oDal.Error
                       , oDal.Mensaje
                       , oDal.StatusCode
                       , oDal.MensajeDev);

                oDal.Dispose();
            }

            return oResp;
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="sServidor"></param>
        /// <param name="sBd"></param>
        /// <param name="sUsuario"></param>
        /// <param name="sClave"></param>
        /// <param name="sSql"></param>
        /// <param name="nDividirEn"></param>
        /// <param name="bIsUsarPlantilla"></param>
        /// <param name="bIsValidarObject"></param>
        /// <returns></returns>
        public static List<TListData<T>> Get_Part_List_SqlServer<T>(string sServidor
                                                                   , string sBd
                                                                   , string sUsuario
                                                                   , string sClave
                                                                   , string sSql
                                                                   , int nDividirEn
                                                                   , bool bIsUsarPlantilla = true
                                                                   , bool bIsValidarObject = true) where T : class => Get_Part_List_SqlServer<T>(sBd
                                                                                                                                              , THelper.CnxStringSqlServer(sServidor, sBd, sUsuario, sClave)
                                                                                                                                              , sSql
                                                                                                                                              , nDividirEn
                                                                                                                                              , bIsUsarPlantilla
                                                                                                                                              , bIsValidarObject);



        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="oDatosConex_DTO"></param>
        /// <param name="sSql"></param>
        /// <param name="bIsUsarPlantilla"></param>
        /// <param name="bIsValidarPrimeraTabla"></param>
        /// <returns></returns>
        public static List<TListData<T>> Get_Part_List_SqlServer<T>(this DTO.TDatosConex_DTO oDatosConex_DTO
                                                                    , string sSql
                                                                    , int nDividirEn
                                                                    , bool bIsUsarPlantilla = true
                                                                    , bool bIsValidarObject = true) where T : class => Get_Part_List_SqlServer<T>(oDatosConex_DTO.DB
                                                                                                                                              , THelper.CnxStringSqlServer(oDatosConex_DTO.Server, oDatosConex_DTO.DB, oDatosConex_DTO.User, oDatosConex_DTO.Pass)
                                                                                                                                              , sSql
                                                                                                                                              , nDividirEn
                                                                                                                                              , bIsUsarPlantilla
                                                                                                                                              , bIsValidarObject);

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="sSql"></param>
        /// <param name="nDividirEn"></param>
        /// <param name="bIsUsarPlantilla"></param>
        /// <param name="bIsValidarObject"></param>
        /// <returns></returns>
        public static List<TListData<T>> Get_Part_List_SqlServerByDC<T>(string sSql
                                                                       , int nDividirEn
                                                                       , bool bIsUsarPlantilla = true
                                                                       , bool bIsValidarObject = true) where T : class => Get_Part_List_SqlServer<T>(THelper.DatosConex_DTO
                                                                                                                                                   , sSql
                                                                                                                                                   , nDividirEn
                                                                                                                                                   , bIsUsarPlantilla
                                                                                                                                                   , bIsValidarObject);
        #endregion
    }
}
