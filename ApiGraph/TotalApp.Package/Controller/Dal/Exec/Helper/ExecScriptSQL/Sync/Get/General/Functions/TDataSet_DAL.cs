﻿using System.Data;

namespace TotalApp.Package.DAL
{
    /// <summary>
    /// *Autor:Larry Ramirez
    /// *Fecha de creacion:09-01-2018
    /// *Version:1.1.20180109
    /// *Sinopsis:
    /// </summary>
    public static partial class THelperBase_ExecScript_DAL
    {
        #region "Get"
        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sCnx"></param>
        /// <param name="sSql"></param>
        /// <param name="bIsValidarPrimeraTabla"></param>
        /// <returns></returns>
        public static DataSet Get_DataSet_SqlServer(string sCnx
                                                   , string sSql
                                                   , bool bIsValidarPrimeraTabla = true)
        {
            OK();
            var oResp = new DataSet();
            using (var oDal = new TBase_ExecScript_DAL())
            {
                oResp = oDal.Get_DataSet_SqlServer(sCnx
                                                 , sSql
                                                 , bIsValidarPrimeraTabla);

                SetError(oDal.Error
                       , oDal.Mensaje
                       , oDal.StatusCode
                       , oDal.MensajeDev);

                oDal.Dispose();
            }

            return oResp;
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sBd"></param>
        /// <param name="sCnx"></param>
        /// <param name="sSql"></param>
        /// <param name="bIsUsarPlantilla"></param>
        /// <param name="bIsValidarPrimeraTabla"></param>
        /// <returns></returns>
        public static DataSet Get_DataSet_SqlServer(string sBd
                                                  , string sCnx
                                                  , string sSql
                                                  , bool bIsUsarPlantilla = true
                                                  , bool bIsValidarPrimeraTabla = true)
        {

            OK();
            var oResp = new DataSet();
            using (var oDal = new TBase_ExecScript_DAL())
            {
                oResp = oDal.Get_DataSet_SqlServer(sBd
                                                 , sCnx
                                                 , sSql
                                                 , bIsUsarPlantilla
                                                 , bIsValidarPrimeraTabla);

                SetError(oDal.Error
                       , oDal.Mensaje
                       , oDal.StatusCode
                       , oDal.MensajeDev);

                oDal.Dispose();
            }

            return oResp;
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sServidor"></param>
        /// <param name="sBd"></param>
        /// <param name="sUsuario"></param>
        /// <param name="sClave"></param>
        /// <param name="sSql"></param>
        /// <param name="bIsUsarPlantilla"></param>
        /// <param name="bIsValidarPrimeraTabla"></param>
        /// <returns></returns>
        public static DataSet Get_DataSet_SqlServer(string sServidor
                                                  , string sBd
                                                  , string sUsuario
                                                  , string sClave
                                                  , string sSql
                                                  , bool bIsUsarPlantilla = true
                                                  , bool bIsValidarPrimeraTabla = true)  => Get_DataSet_SqlServer(sBd
                                                                                                          , THelper.CnxStringSqlServer(sServidor, sBd, sUsuario, sClave)
                                                                                                          , sSql
                                                                                                          , bIsUsarPlantilla
                                                                                                          , bIsValidarPrimeraTabla);




        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="oDatosConex_DTO"></param>
        /// <param name="sSql"></param>
        /// <param name="bIsUsarPlantilla"></param>
        /// <param name="bIsValidarPrimeraTabla"></param>
        /// <returns></returns>
        public static DataSet Get_DataSet_SqlServer(this DTO.TDatosConex_DTO oDatosConex_DTO
                                                    , string sSql
                                                    , bool bIsUsarPlantilla = true
                                                    , bool bIsValidarPrimeraTabla = true) => Get_DataSet_SqlServer(oDatosConex_DTO.DB
                                                                                                                  , THelper.CnxStringSqlServer(oDatosConex_DTO.Server, oDatosConex_DTO.DB, oDatosConex_DTO.User, oDatosConex_DTO.Pass)
                                                                                                                  , sSql
                                                                                                                  , bIsUsarPlantilla
                                                                                                                  , bIsValidarPrimeraTabla);


        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sSql"></param>
        /// <param name="bIsUsarPlantilla"></param>
        /// <param name="bIsValidarPrimeraTabla"></param>
        /// <returns></returns>
        public static DataSet Get_DataSet_SqlServerByDC(string sSql
                                                       , bool bIsUsarPlantilla = true
                                                       , bool bIsValidarPrimeraTabla = true) => Get_DataSet_SqlServer(THelper.DatosConex_DTO
                                                                                                                   , sSql
                                                                                                                   , bIsUsarPlantilla
                                                                                                                   , bIsValidarPrimeraTabla);

        #endregion
    }
}
