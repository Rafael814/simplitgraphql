﻿using System.Collections.Generic;

namespace TotalApp.Package.DAL
{
    /// <summary>
    /// *Autor:Larry Ramirez
    /// *Fecha de creacion:09-01-2018
    /// *Version:1.1.20180109
    /// *Sinopsis:
    /// </summary>
    public static partial class THelperBase_ExecScript_DAL
    {
        #region "Get"
        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="sCnx"></param>
        /// <param name="sSql"></param>
        /// <param name="bIsValidarObject"></param>
        /// <returns></returns>
        public static List<T> Get_List_SqlServer<T>(string sCnx
                                                   , string sSql
                                                   , bool bIsValidarObject = true) where T : class
        {
            OK();
            var oResp = default(List<T>);
            using (var oDal = new TBase_ExecScript_DAL())
            {
                oResp = oDal.Get_List_SqlServer<T>(sCnx
                                                , sSql
                                                , bIsValidarObject);

                SetError(oDal.Error
                       , oDal.Mensaje
                       , oDal.StatusCode
                       , oDal.MensajeDev);

                oDal.Dispose();
            }

            return oResp;
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="sBd"></param>
        /// <param name="sCnx"></param>
        /// <param name="sSql"></param>
        /// <param name="bIsUsarPlantilla"></param>
        /// <param name="bIsValidarObject"></param>
        /// <returns></returns>
        public static List<T> Get_List_SqlServer<T>(string sBd
                                                   , string sCnx
                                                   , string sSql
                                                   , bool bIsUsarPlantilla = true
                                                   , bool bIsValidarObject = true) where T : class
        {
            OK();
            var oResp = default(List<T>);
            using (var oDal = new TBase_ExecScript_DAL())
            {
                oResp = oDal.Get_List_SqlServer<T>(sBd
                                                 , sCnx
                                                 , sSql
                                                 , bIsUsarPlantilla
                                                 , bIsValidarObject);

                SetError(oDal.Error
                       , oDal.Mensaje
                       , oDal.StatusCode
                       , oDal.MensajeDev);

                oDal.Dispose();
            }

            return oResp;
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sServidor"></param>
        /// <param name="sBd"></param>
        /// <param name="sUsuario"></param>
        /// <param name="sClave"></param>
        /// <param name="sSql"></param>
        /// <param name="bIsUsarPlantilla"></param>
        /// <param name="bIsValidarObject"></param>
        /// <returns></returns>
        public static List<T> Get_List_SqlServer<T>(string sServidor
                                                  , string sBd
                                                  , string sUsuario
                                                  , string sClave
                                                  , string sSql
                                                  , bool bIsUsarPlantilla = true
                                                  , bool bIsValidarObject = true) where T:class => Get_List_SqlServer<T>(sBd
                                                                                                                      , THelper.CnxStringSqlServer(sServidor, sBd, sUsuario, sClave)
                                                                                                                      , sSql
                                                                                                                      , bIsUsarPlantilla
                                                                                                                      , bIsValidarObject);




        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="oDatosConex_DTO"></param>
        /// <param name="sSql"></param>
        /// <param name="bIsUsarPlantilla"></param>
        /// <param name="bIsValidarObject"></param>
        /// <returns></returns>
        public static List<T> Get_List_SqlServer<T>(this DTO.TDatosConex_DTO oDatosConex_DTO
                                                  , string sSql
                                                  , bool bIsUsarPlantilla = true
                                                  , bool bIsValidarObject = true) where T : class  => Get_List_SqlServer<T>(oDatosConex_DTO.DB
                                                                                                                          , THelper.CnxStringSqlServer(oDatosConex_DTO.Server, oDatosConex_DTO.DB, oDatosConex_DTO.User, oDatosConex_DTO.Pass)
                                                                                                                          , sSql
                                                                                                                          , bIsUsarPlantilla
                                                                                                                          , bIsValidarObject);


        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sSql"></param>
        /// <param name="bIsUsarPlantilla"></param>
        /// <param name="bIsValidarObject"></param>
        /// <returns></returns>
        public static List<T> Get_List_SqlServerByDC<T>(string sSql
                                                       , bool bIsUsarPlantilla = true
                                                       , bool bIsValidarObject = true) where T : class  => Get_List_SqlServer<T>(THelper.DatosConex_DTO
                                                                                                                               , sSql
                                                                                                                               , bIsUsarPlantilla
                                                                                                                               , bIsValidarObject);
        #endregion
    }
}
