﻿namespace TotalApp.Package.DAL
{
    /// <summary>
    /// *Autor:Larry Ramirez
    /// *Fecha de creacion:09-01-2018
    /// *Version:1.1.20180109
    /// *Sinopsis:
    /// </summary>
    public static partial class THelperBase_ExecScript_DAL
    {
        #region "Get"

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="sCnx"></param>
        /// <param name="sSql"></param>
        /// <param name="bIsValidarObject"></param>
        /// <param name="bIsDecrypting"></param>
        /// <param name="bIsDecompressData"></param>
        /// <returns></returns>
        public static string Get_Object_Encrypt_Compress_SqlServer<T>(string sCnx
                                                                  , string sSql
                                                                  , bool bIsValidarObject = true
                                                                  , bool bIsDecrypting = true
                                                                  , bool bIsDecompressData = true) where T : class
        {
            OK();
            var oResp = THelper.Empty;
            using (var oDal = new TBase_ExecScript_DAL())
            {
                oResp = oDal.Get_Object_Encrypt_Compress_SqlServer<T>(sCnx
                                                                  , sSql
                                                                  , bIsValidarObject
                                                                  , bIsDecrypting
                                                                  , bIsDecompressData);

                SetError(oDal.Error
                       , oDal.Mensaje
                       , oDal.StatusCode
                       , oDal.MensajeDev);

                oDal.Dispose();
            }

            return oResp;
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="sBd"></param>
        /// <param name="sCnx"></param>
        /// <param name="sSql"></param>
        /// <param name="bIsUsarPlantilla"></param>
        /// <param name="bIsValidarObject"></param>
        /// <param name="bIsDecrypting"></param>
        /// <param name="bIsDecompressData"></param>
        /// <returns></returns>
        public static string Get_Object_Encrypt_Compress_SqlServer<T>(string sBd
                                                                   , string sCnx
                                                                   , string sSql
                                                                   , bool bIsUsarPlantilla = true
                                                                   , bool bIsValidarObject = true
                                                                   , bool bIsDecrypting = true
                                                                   , bool bIsDecompressData = true) where T : class
        {
            OK();
            var oResp = THelper.Empty;
            using (var oDal = new TBase_ExecScript_DAL())
            {
                oResp = oDal.Get_Object_Encrypt_Compress_SqlServer<T>(sBd
                                                                   , sCnx
                                                                   , sSql
                                                                   , bIsUsarPlantilla
                                                                   , bIsValidarObject
                                                                   , bIsDecrypting
                                                                   , bIsDecompressData);

                SetError(oDal.Error
                       , oDal.Mensaje
                       , oDal.StatusCode
                       , oDal.MensajeDev);

                oDal.Dispose();
            }

            return oResp;
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="sServidor"></param>
        /// <param name="sBd"></param>
        /// <param name="sUsuario"></param>
        /// <param name="sClave"></param>
        /// <param name="sSql"></param>
        /// <param name="bIsUsarPlantilla"></param>
        /// <param name="bIsValidarObject"></param>
        /// <param name="bIsDecrypting"></param>
        /// <param name="bIsDecompressData"></param>
        /// <returns></returns>
        public static string Get_Object_Encrypt_Compress_SqlServer<T>(string sServidor
                                                                    , string sBd
                                                                    , string sUsuario
                                                                    , string sClave
                                                                    , string sSql
                                                                    , bool bIsUsarPlantilla = true
                                                                    , bool bIsValidarObject = true
                                                                    , bool bIsDecrypting = true
                                                                    , bool bIsDecompressData = true) where T : class => Get_Object_Encrypt_Compress_SqlServer<T>(sBd
                                                                                                                                                            , THelper.CnxStringSqlServer(sServidor, sBd, sUsuario, sClave)
                                                                                                                                                            , sSql
                                                                                                                                                            , bIsUsarPlantilla
                                                                                                                                                            , bIsValidarObject
                                                                                                                                                            , bIsDecrypting
                                                                                                                                                            , bIsDecompressData);


        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="oDatosConex_DTO"></param>
        /// <param name="sSql"></param>
        /// <param name="bIsUsarPlantilla"></param>
        /// <param name="bIsValidarObject"></param>
        /// <param name="bIsDecrypting"></param>
        /// <param name="bIsDecompressData"></param>
        /// <returns></returns>
        public static string Get_Object_Encrypt_Compress_SqlServer<T>(this DTO.TDatosConex_DTO oDatosConex_DTO
                                                                    , string sSql
                                                                    , bool bIsUsarPlantilla = true
                                                                    , bool bIsValidarObject = true
                                                                    , bool bIsDecrypting = true
                                                                    , bool bIsDecompressData = true) where T : class => Get_Object_Encrypt_Compress_SqlServer<T>(oDatosConex_DTO.DB
                                                                                                                                                               , THelper.CnxStringSqlServer(oDatosConex_DTO.Server, oDatosConex_DTO.DB, oDatosConex_DTO.User, oDatosConex_DTO.Pass)
                                                                                                                                                               , sSql
                                                                                                                                                               , bIsUsarPlantilla
                                                                                                                                                               , bIsValidarObject
                                                                                                                                                               , bIsDecrypting
                                                                                                                                                               , bIsDecompressData);


        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="sSql"></param>
        /// <param name="bIsUsarPlantilla"></param>
        /// <param name="bIsValidarObject"></param>
        /// <param name="bIsDecrypting"></param>
        /// <param name="bIsDecompressData"></param>
        /// <returns></returns>
        public static string Get_Object_Encrypt_Compress_SqlServerByDC<T>(string sSql
                                                                       , bool bIsUsarPlantilla = true
                                                                       , bool bIsValidarObject = true
                                                                       , bool bIsDecrypting = true
                                                                       , bool bIsDecompressData = true) where T : class => Get_Object_Encrypt_Compress_SqlServer<T>(THelper.DatosConex_DTO
                                                                                                                                                                , sSql
                                                                                                                                                                , bIsUsarPlantilla
                                                                                                                                                                , bIsValidarObject
                                                                                                                                                                , bIsDecrypting
                                                                                                                                                                , bIsDecompressData);
        #endregion
    }
}
