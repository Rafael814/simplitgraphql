﻿
namespace TotalApp.Package.DAL
{
    /// <summary>
    /// *Autor:Larry Ramirez
    /// *Fecha de creacion:09-01-2018
    /// *Version:1.1.20180109
    /// *Sinopsis:
    /// </summary>
    public static partial class THelperBase_ExecScript_DAL
    {
        #region "Tran"
        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sCnx"></param>
        /// <param name="sSql"></param>
        /// <returns></returns>
        public static int Tran_Exec_SqlServer(string sCnx
                                            , string sSql)
        {
            OK();
            var oResp = 0;
            using (var oDal = new TBase_ExecScript_DAL())
            {
                oResp = oDal.Tran_Exec_SqlServer(sCnx
                                               , sSql);

                SetError(oDal.Error
                       , oDal.Mensaje
                       , oDal.StatusCode
                       , oDal.MensajeDev);

                oDal.Dispose();
            }

            return oResp;
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sBd"></param>
        /// <param name="sCnx"></param>
        /// <param name="sSql"></param>
        /// <param name="bIsUsarPlantilla"></param>
        /// <returns></returns>
        public static int Tran_Exec_SqlServer(string sBd
                                             , string sCnx
                                             , string sSql
                                             , bool bIsUsarPlantilla = true)
        {
            OK();
            var oResp = 0;
            using (var oDal = new TBase_ExecScript_DAL())
            {
                oResp = oDal.Tran_Exec_SqlServer(sBd
                                               , sCnx
                                               , sSql
                                               , bIsUsarPlantilla);

                SetError(oDal.Error
                       , oDal.Mensaje
                       , oDal.StatusCode
                       , oDal.MensajeDev);

                oDal.Dispose();
            }

            return oResp;
        }


        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sServidor"></param>
        /// <param name="sBd"></param>
        /// <param name="sUsuario"></param>
        /// <param name="sClave"></param>
        /// <param name="sSql"></param>
        /// <param name="bIsUsarPlantilla"></param>
        /// <returns></returns>
        public static int Tran_Exec_SqlServer(string sServidor
                                            , string sBd
                                            , string sUsuario
                                            , string sClave
                                            , string sSql
                                            , bool bIsUsarPlantilla = true) => Tran_Exec_SqlServer(sBd
                                                                                                , THelper.CnxStringSqlServer(sServidor, sBd, sUsuario, sClave)
                                                                                                , sSql
                                                                                                , bIsUsarPlantilla);



        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="oDatosConex_DTO"></param>
        /// <param name="sSql"></param>
        /// <param name="bIsUsarPlantilla"></param>
        /// <returns></returns>
        public static int Tran_Exec_SqlServer(this DTO.TDatosConex_DTO oDatosConex_DTO
                                            , string sSql
                                            , bool bIsUsarPlantilla = true) => Tran_Exec_SqlServer(oDatosConex_DTO.DB
                                                                                                , THelper.CnxStringSqlServer(oDatosConex_DTO.Server, oDatosConex_DTO.DB, oDatosConex_DTO.User, oDatosConex_DTO.Pass)
                                                                                                , sSql
                                                                                                , bIsUsarPlantilla);


        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sSql"></param>
        /// <param name="bIsUsarPlantilla"></param>
        /// <returns></returns>
        public static int Tran_Exec_SqlServerByDC(string sSql
                                                 , bool bIsUsarPlantilla = true) => THelper.DatosConex_DTO.Tran_Exec_SqlServer(sSql, bIsUsarPlantilla);
                                                                                                     

        #endregion
    }
}
