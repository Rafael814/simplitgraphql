﻿using System;
using TotalApp.Package.DTO;

namespace TotalApp.Package.DAL
{
    /// <summary>
    /// *Autor:Larry Ramirez
    /// *Fecha de creacion:09-01-2018
    /// *Version:1.1.20180117
    /// *Sinopsis:Clase generica que permite invocar objetos de BD SQL 
    /// </summary>
    public abstract class TBase_Exec_DAL : TInfoEstatus_DTO, IDisposable
    {
        #region "Constantes"
        #endregion

        #region "Variables"
        #endregion

        #region "Propiedades"
        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180117
        /// *Sinopsis:Propiedad que permite definir el numero de intentos para conectarse a una base de datos
        /// </summary>
        public short NumIntentosDeConex { get; set; } = 1;
       
        #endregion

        #region "Funciones"

        #region "Conexiones"
        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:Construye el conexion string de conexion a la bd
        /// </summary>
        /// <param name="sServidor"></param>
        /// <param name="sBd"></param>
        /// <param name="sUsuario"></param>
        /// <param name="sClave"></param>
        /// <returns></returns>
        public virtual string CnxStringSqlServer(string sServidor
                                                , string sBd
                                                , string sUsuario
                                                , string sClave)

        {
            if (sServidor.TIsNullOrEmpty())  THelperInfoEstatus.ArgumentNull(nameof(sServidor));
            if (sBd.TIsNullOrEmpty())  THelperInfoEstatus.ArgumentNull(nameof(sBd));
            if (sUsuario.TIsNullOrEmpty())  THelperInfoEstatus.ArgumentNull(nameof(sUsuario));
            if (sClave.TIsNullOrEmpty())  THelperInfoEstatus.ArgumentNull(nameof(sClave));

            string sConex = THelper.Empty;
            string sApp = sBd + "_" + DateTime.Now.ToString("yyyyMMddHHmmss") + "_" + THelper.Random();

            sConex = @"Data Source=@SERVIDOR;";
            sConex += @"Initial Catalog=@BD;";
            sConex += @"Integrated Security=False;";
            sConex += @"User ID=@USUARIO;";
            sConex += @"Password=@CLAVE;";

            sConex += @"Application Name=@MIAPP;";
            sConex += @"Connection Timeout=30;";
            sConex += @"pooling=false;";
            //sConex += @"MultipleActiveResultSets=True;";
            //sConex += @"Encrypt=False;";
            //sConex += @"Persist Security Info=false;";

            sConex = sConex.Replace("@SERVIDOR", sServidor);
            sConex = sConex.Replace("@BD", sBd);
            sConex = sConex.Replace("@USUARIO", sUsuario);
            sConex = sConex.Replace("@CLAVE", sClave);
            sConex = sConex.Replace("@MIAPP", sApp);

            return sConex;
        }
        #endregion

        #region "Templade Sql"
        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sBd"></param>
        /// <param name="sql"></param>
        /// <returns></returns>
        public virtual string Templade_Get_ForSqlServer(string sBd
                                                      , string sql)
        {

            string sRetorno = @"USE [@Bd]
                                 SET ANSI_NULLS ON
                                 SET QUOTED_IDENTIFIER ON
                                 BEGIN TRY
                                     BEGIN
                                         SET NOCOUNT ON
		                                 SET DATEFORMAT YMD
		                               
		                                 @Query

                                     END
                                 END TRY
                                 BEGIN CATCH
                                     DECLARE @ErrorMessage   AS NVARCHAR(4000) = ''
					                        ,@ErrorSeverity  AS INT            = 0
					                        ,@ErrorState     AS INT            = 0

					                 SELECT @ErrorMessage  = ERROR_MESSAGE()
						                   ,@ErrorSeverity = ERROR_SEVERITY()
						                   ,@ErrorState    = ERROR_STATE();

                                     RAISERROR(@ErrorMessage
                                              ,@ErrorSeverity
                                              ,@ErrorState);

                                 END CATCH
                                 SET NOCOUNT ON
                                 SET ANSI_NULLS OFF
                                 SET QUOTED_IDENTIFIER OFF";

            sRetorno = sRetorno.Replace("@Bd", sBd);
            sRetorno = sRetorno.Replace("@Query", sql);

            return sRetorno;


        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sql"></param>
        /// <returns></returns>
        public virtual string Templade_Get_StringJsonAuto_ForSql(string sql)
        {
            string sRetorno = @"
                                  DECLARE @Json    AS VARCHAR(MAX) = ''

                                    SET @Json = (
                                            @Query
                                       FOR JSON AUTO) 
                        
                                      SELECT @Json AS [Json]


                                ";


            sRetorno = sRetorno.Replace("@Query", sql);

            return sRetorno;


        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sql"></param>
        /// <returns></returns>
        public virtual string Templade_Get_StringJsonPath_ForSql(string sql)
        {
            string sRetorno = @"
                                  DECLARE @Json    AS VARCHAR(MAX) = ''

                                    SET @Json = (
                                            @Query
                                       FOR JSON PATH, INCLUDE_NULL_VALUES) 
                        
                                      SELECT @Json AS [Json]


                                ";


            sRetorno = sRetorno.Replace("@Query", sql);

            return sRetorno;


        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sBd"></param>
        /// <param name="sql"></param>
        /// <returns></returns>
        public virtual string Templade_Tran_Sql(string sBd
                                              , string sql)
        {
            string sTran = THelper.Random(true);

            string sRetorno = @"USE [@Bd]
                                 SET ANSI_NULLS ON
                                 SET QUOTED_IDENTIFIER ON
                                 BEGIN TRY
                                     BEGIN
                                         SET NOCOUNT ON
		                                 SET DATEFORMAT YMD
		
		                                 BEGIN TRAN @Transaccion

                                         BEGIN
                                             @Query

                                         END
                                         COMMIT TRAN @Transaccion

                                     END
                                 END TRY
                                 BEGIN CATCH
                                     DECLARE @ErrorMessage   AS NVARCHAR(4000) = ''
					                        ,@ErrorSeverity  AS INT            = 0
					                        ,@ErrorState     AS INT            = 0

					                 SELECT @ErrorMessage  = ERROR_MESSAGE()
						                   ,@ErrorSeverity = ERROR_SEVERITY()
						                   ,@ErrorState    = ERROR_STATE();

					                 IF @@TRANCOUNT > 0
                                     BEGIN
                                         ROLLBACK TRAN @Transaccion
                                     END

                                     RAISERROR(@ErrorMessage
                                              ,@ErrorSeverity
                                              ,@ErrorState);

                                 END CATCH
                                 SET NOCOUNT ON
                                 SET ANSI_NULLS OFF
                                 SET QUOTED_IDENTIFIER OFF";

            sRetorno = sRetorno.Replace("@Bd", sBd);
            sRetorno = sRetorno.Replace("@Transaccion", '\u0022' + sTran + '\u0022');
            sRetorno = sRetorno.Replace("@Query", sql);

            return sRetorno;



        }

        #endregion

        #region "Exec"

        #region "Verificar conexion"
        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sCnx"></param>
        /// <returns></returns>
        public virtual bool VerificarConexionSqlServer(string sCnx)
        {

            if (sCnx.TIsNullOrEmpty()) THelperInfoEstatus.ArgumentNull(nameof(sCnx));
            
            base.OK();
            bool bIsRetorno = true;
            ITDbManager oTDbManager = new TDbManager(DataProvider.SqlServer);
            try
            {

                oTDbManager.ConnectionString = sCnx;
                oTDbManager.Open(NumIntentosDeConex);

                if (oTDbManager.Error != 0)
                {
                    bIsRetorno = false;
                    base.InternalServerError(oTDbManager.Error
                                            , "Error de conexion a la BD"
                                            , TTypeError.Err_CnxBD
                                            , oTDbManager.Exception);
                }

            }
            catch (Exception ex)
            {
                bIsRetorno = false;

                base.InternalServerError("Error Excepcion", TTypeError.Err_CT_DAL, ex.Message);
            }
            finally
            {
                oTDbManager.Dispose();
                oTDbManager = null;
            }

           
            return bIsRetorno;
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sServidor"></param>
        /// <param name="sBd"></param>
        /// <param name="sUsuario"></param>
        /// <param name="sClave"></param>
        /// <returns></returns>
        public virtual bool VerificarConexionSqlServer(string sServidor
                                                      , string sBd
                                                      , string sUsuario
                                                      , string sClave) => VerificarConexionSqlServer(CnxStringSqlServer(sServidor, sBd, sUsuario, sClave));

        #endregion

        #endregion


        #endregion

        #region "Metodos & constructores"
        public TBase_Exec_DAL():base()
        {
          
        }
        #endregion

        #region "Eventos"
        #endregion

        #region "Destructor"
        /// <summary>
        /// Autor   : Larry Ramirez
        /// Fecha   : 21-05-2016
        /// Sinopsis: Variable Disposed, permite saber de manera general si ya fueron liberados los recursos no administrados a la clase (Objeto).
        /// </summary>
        private bool _Disposed = false;

        // <summary>
        /// Autor   : Larry Ramirez
        /// Fecha   : 25-05-2016
        /// Sinopsis: Metodo que permite Desechar y limpiar objetos de la memoria.
        /// </summary>
        public new void Dispose()
        {

            Dispose(true);
            GC.SuppressFinalize(this);
        }

        // <summary>
        /// Autor   : Larry Ramirez
        /// Fecha   : 25-05-2016
        /// Sinopsis: Logica para limpiar objetos de la memoria.
        /// </summary>
        private new void Dispose(bool bIsDisposing)
        {
            //Si esta en false, significa que se tiene la oportunidad de limpiar los objetos
            if (!this._Disposed)
            {
                //Aqui se definen los objetos que se liberan
                if (bIsDisposing)
                {

                }
            }
            _Disposed = true;
        }

        // <summary>
        /// Autor   : Larry Ramirez
        /// Fecha   : 25-05-2016
        /// Sinopsis: Destructor por defecto.
        /// </summary>
        ~TBase_Exec_DAL()
        {
            Dispose(false);
            GC.SuppressFinalize(this);

        }
        #endregion
    }
}
