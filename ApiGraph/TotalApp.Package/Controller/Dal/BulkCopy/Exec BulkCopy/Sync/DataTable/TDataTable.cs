﻿using System;
using System.Data;
using System.Data.SqlClient;

namespace TotalApp.Package.DAL
{
    /// <summary>
    /// *Autor:Larry Ramirez
    /// *Fecha de creacion:09-01-2018
    /// *Version:1.1.20180109
    /// *Sinopsis:
    /// </summary> 
    public partial class TBulkCopy_DAL
    {
        #region "DataTable"
        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:Metodo que permite realizar el BulkCopy de un DataTable
        /// </summary>
        /// <param name="oData">DataTable</param>
        /// <param name="sCnx">Conexion string de la BD</param>
        /// <param name="sTabla">Tabla donde se realizara el BulkCopy</param>
        /// <param name="sCampos">Campos a ser mapeados durante el Insert, por defecto es null, significa que tomara por defecto los campos de la tabla de derecha a izquierda, en el orden 
        /// en el que esten ordenados en la lista</param>
        public virtual void SqlBulkCopy(DataTable oData
                                      , string sCnx
                                      , string sTabla
                                      , string[] sCampos = null)
        {


            base.OK();

            if (oData.TIsNullOrEmptyTable())
            {
                base.InternalServerError(1, "El objeto esta vacio",TTypeError.Err_ObjEmpty);
            }
            else
            {
                try
                {
                    using (SqlConnection oSqlCn = new SqlConnection(sCnx))
                    {
                        oSqlCn.Open();

                        string sTran = THelper.Random(true);
                        using (SqlTransaction oSqlTransaction = oSqlCn.BeginTransaction(IsolationLevel.ReadUncommitted, sTran))
                        {

                            using (SqlBulkCopy oSqlBulkCopy = new SqlBulkCopy(oSqlCn, SqlBulkCopyOptions.Default, oSqlTransaction))
                            {
                                short nCommit = 0;
                                try
                                {
                                    if (sCampos != null)
                                    {
                                        oSqlBulkCopy.ColumnMappings.Clear();
                                        foreach (string sCampo in sCampos)
                                        {
                                            oSqlBulkCopy.ColumnMappings.Add(sCampo, sCampo);
                                        }
                                    }

                                    oSqlBulkCopy.DestinationTableName = sTabla;
                                    oSqlBulkCopy.BulkCopyTimeout = 3600;
                                    oSqlBulkCopy.BatchSize = 20000;
                                    oSqlBulkCopy.WriteToServer(oData);

                                    nCommit = 1;
                                    oSqlTransaction.Commit();

                                }
                                catch (SqlException ex)
                                {

                                    TInfoError(sTabla, oSqlBulkCopy, ex.Message);

                                    if (nCommit == 1)
                                    {
                                        oSqlTransaction.Rollback(sTran);
                                    }
                                }
                                finally
                                {
                                    if (oSqlBulkCopy != null)
                                    {
                                        ((IDisposable)oSqlBulkCopy).Dispose();
                                        oSqlBulkCopy.Close();
                                    }
                                }


                            }

                            if (oSqlTransaction != null)
                            {
                                oSqlTransaction.Dispose();
                            }

                        }

                        oSqlCn.Dispose();
                        oSqlCn.Close();
                    }

                }
                catch (Exception ex)
                {
                    TInfoError(sTabla, null, ex.Message);
                }

            }


        }


        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:Metodo que permite realizar el BulkCopy de un DataTable
        /// </summary>
        /// <param name="oData">DataTable</param>
        /// <param name="sServidor"></param>
        /// <param name="sBd"></param>
        /// <param name="sUsuario"></param>
        /// <param name="sClave"></param>
        /// <param name="sTabla">Tabla donde se realizara el BulkCopy</param>
        /// <param name="sCampos">Campos a ser mapeados durante el Insert, por defecto es null, significa que tomara por defecto los campos de la tabla de derecha a izquierda, en el orden 
        /// en el que esten ordenados en la lista</param>
        public virtual void SqlBulkCopy(DataTable oData
                                      , string sServidor
                                      , string sBd
                                      , string sUsuario
                                      , string sClave
                                      , string sTabla
                                      , string[] sCampos = null) => SqlBulkCopy(oData
                                                                              , CnxStringSqlServer(sServidor, sBd, sUsuario, sClave)
                                                                              , sTabla
                                                                              , sCampos);



        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="oData"></param>
        /// <param name="oDTO"></param>
        /// <param name="sTabla"></param>
        /// <param name="sCampos"></param>
        public virtual void SqlBulkCopy(DataTable oData
                                      , DTO.TDatosConex_DTO oDTO
                                      , string sTabla
                                      , string[] sCampos = null) => SqlBulkCopy(oData
                                                                              , CnxStringSqlServer(oDTO.Server, oDTO.DB, oDTO.User, oDTO.Pass)
                                                                              , sTabla
                                                                              , sCampos);


        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="oData"></param>
        /// <param name="sTabla"></param>
        /// <param name="sCampos"></param>
        public virtual void SqlBulkCopyByDC(DataTable oData
                                          , string sTabla
                                          , string[] sCampos = null) => SqlBulkCopy(oData
                                                                                  , THelper.DatosConex_DTO
                                                                                  , sTabla
                                                                                  , sCampos);
        #endregion


    }
}
