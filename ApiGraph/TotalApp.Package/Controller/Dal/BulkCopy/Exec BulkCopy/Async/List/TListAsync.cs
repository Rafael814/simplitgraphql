﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace TotalApp.Package.DAL
{
    /// <summary>
    /// *Autor:Larry Ramirez
    /// *Fecha de creacion:09-01-2018
    /// *Version:1.1.20180109
    /// *Sinopsis:
    /// </summary> 
    public partial class TBulkCopy_DAL
    {

        #region "Lista"

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:Metodo que permite realizar el BulkCopy de una Lista, de un determinado objeto
        /// </summary>
        /// <typeparam name="T">(T) objeto generico</typeparam>
        /// <param name="oData">List</param>
        /// <param name="sCnx">Conexion string de la BD</param>
        /// <param name="sTabla">Tabla donde se realizara el BulkCopy</param>
        /// <param name="sCampos">Campos a ser mapeados durante el Insert, por defecto es null, significa que tomara por defecto los campos de la tabla de derecha a izquierda, en el orden 
        /// en el que esten ordenados en la lista</param>
        public virtual Task SqlBulkCopyAsync<T>(List<T> oData
                                         , string sCnx
                                         , string sTabla
                                         , string[] sCampos = null) where T : class
        {


            var oTask = new Task(() =>
            {
                SqlBulkCopy(oData, sCnx, sTabla, sCampos);
            });

            oTask.Start();

            return oTask;


        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:Metodo que permite realizar el BulkCopy de una Lista, de un determinado objeto
        /// esta funcion invoca la funcion  SqlBulkCopy<T> de tipo generica, debido a que sus parametros son especificos y construyen
        /// el string de conexion
        /// </summary>
        /// <typeparam name="T">(T) objeto generico</typeparam>
        /// <param name="oData"></param>
        /// <param name="sServidor"></param>
        /// <param name="sBd"></param>
        /// <param name="sUsuario"></param>
        /// <param name="sClave"></param>
        /// <param name="sTabla">Tabla donde se realizara el BulkCopy</param>
        /// <param name="sCampos">Campos a ser mapeados durante el Insert, por defecto es null, significa que tomara por defecto los campos de la tabla de derecha a izquierda, en el orden 
        /// en el que esten ordenados en la lista</param>
        public virtual Task SqlBulkCopyAsync<T>(List<T> oData
                                               , string sServidor
                                               , string sBd
                                               , string sUsuario
                                               , string sClave
                                               , string sTabla
                                               , string[] sCampos = null) where T : class
        {
            var oTask = new Task(() =>
            {
                SqlBulkCopy(oData, sServidor, sBd, sUsuario, sClave, sTabla, sCampos);
            });

            oTask.Start();

            return oTask;

        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="oData"></param>
        /// <param name="oDTO"></param>
        /// <param name="sTabla"></param>
        /// <param name="sCampos"></param>
        public virtual Task SqlBulkCopyAsync<T>(List<T> oData
                                              , DTO.TDatosConex_DTO oDTO
                                              , string sTabla
                                              , string[] sCampos = null) where T : class
        {

            var oTask = new Task(() =>
            {
                SqlBulkCopy(oData, oDTO, sTabla, sCampos);
            });

            oTask.Start();

            return oTask;
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="oData"></param>
        /// <param name="sTabla"></param>
        /// <param name="sCampos"></param>
        public virtual Task SqlBulkCopyByDCAsync<T>(List<T> oData
                                                  , string sTabla
                                                  , string[] sCampos = null) where T : class
        {
            var oTask = new Task(() =>
            {
                SqlBulkCopyByDC(oData, sTabla, sCampos);
            });

            oTask.Start();

            return oTask;

        }
        #endregion


    }
}
