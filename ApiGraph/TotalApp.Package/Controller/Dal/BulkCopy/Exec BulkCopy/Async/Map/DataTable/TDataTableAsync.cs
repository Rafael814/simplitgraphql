﻿using System.Data;
using System.Threading.Tasks;

namespace TotalApp.Package.DAL
{
    /// <summary>
    /// *Autor:Larry Ramirez
    /// *Fecha de creacion:09-01-2018
    /// *Version:1.1.20180109
    /// *Sinopsis:
    /// </summary> 
    public partial class TBulkCopy_DAL
    {

        #region "Map"
        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:Metodo que permite realizar el BulkCopy de un DataTable, mapeando automaticamente los campos del objeto
        /// de izquierda a derecha e el orden establecido por el objeto
        /// </summary>
        /// <param name="oData">DataTable</param>
        /// <param name="sCnx">Conexion string de la BD</param>
        /// <param name="sTabla">Tabla donde se realizara el BulkCopy</param>
        public virtual Task SqlBulkCopyMapAsync(DataTable oData
                                              , string sCnx
                                              , string sTabla)
        {

            var oTask = new Task(() =>
            {
                SqlBulkCopyMap(oData, sCnx, sTabla);
            });

            oTask.Start();

            return oTask;

        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:Metodo que permite realizar el BulkCopy de un DataTable, mapeando automaticamente los campos del objeto
        /// de izquierda a derecha e el orden establecido por el objeto
        ///  esta funcion invoca la funcion  SqlBulkCopyMapAsync de un DataTable, debido a que sus parametros son especificos y construyen
        /// el string de conexion
        /// </summary>
        /// <param name="oData">DataTable</param>
        /// <param name="sServidor"></param>
        /// <param name="sBd"></param>
        /// <param name="sUsuario"></param>
        /// <param name="sClave"></param>
        /// <param name="sTabla">Tabla donde se realizara el BulkCopy</param>
        public virtual Task SqlBulkCopyMapAsync(DataTable oData
                                              , string sServidor
                                              , string sBd
                                              , string sUsuario
                                              , string sClave
                                              , string sTabla)
        {
            var oTask = new Task(() =>
            {
                SqlBulkCopyMap(oData, sServidor, sBd, sUsuario, sClave, sTabla);
            });

            oTask.Start();

            return oTask;

        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="oData"></param>
        /// <param name="oDTO"></param>
        /// <param name="sTabla"></param>
        public virtual Task SqlBulkCopyMapAsync(DataTable oData
                                              , DTO.TDatosConex_DTO oDTO
                                              , string sTabla)
        {

            var oTask = new Task(() =>
            {
                SqlBulkCopyMap(oData, oDTO, sTabla);
            });

            oTask.Start();

            return oTask;
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="oData"></param>
        /// <param name="sTabla"></param>
        public virtual Task SqlBulkCopyMapByDCAsync(DataTable oData
                                                  , string sTabla)
        {

            var oTask = new Task(() =>
            {
                SqlBulkCopyMapByDC(oData, sTabla);
            });

            oTask.Start();

            return oTask;
        }
        #endregion


    }
}
