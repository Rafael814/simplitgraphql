﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace TotalApp.Package.DAL
{
    /// <summary>
    /// *Autor:Larry Ramirez
    /// *Fecha de creacion:09-01-2018
    /// *Version:1.1.20180109
    /// *Sinopsis:
    /// </summary> 
    public partial class TBulkCopy_DAL
    {

        #region "Map"
       

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:Metodo que permite realizar el BulkCopy de una Lista, de un determinado objeto
        /// esta funcion invoca la funcion  SqlBulkCopyMap(T) de tipo DataTable, debido a que sus parametros son especificos y construyen
        /// el string de conexion
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="oData"></param>
        /// <param name="sCnx"></param>
        /// <param name="sTabla"></param>
        public virtual Task SqlBulkCopyMapAsync<T>(List<T> oData
                                                 , string sCnx
                                                 , string sTabla) where T : class
        {

            var oTask = new Task(() =>
            {
                SqlBulkCopyMap(oData, sCnx, sTabla);
            });

            oTask.Start();

            return oTask;
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:Metodo que permite realizar el BulkCopy de una Lista, de un determinado objeto
        /// esta funcion invoca la funcion  SqlBulkCopyMap(T) de tipo DataTable, debido a que sus parametros son especificos y construyen
        /// el string de conexion
        /// </summary>
        /// <typeparam name="T">(T) objeto generico</typeparam>
        /// <param name="oData"></param>
        /// <param name="sServidor"></param>
        /// <param name="sBd"></param>
        /// <param name="sUsuario"></param>
        /// <param name="sClave"></param>
        /// <param name="sTabla">Tabla donde se realizara el BulkCopy</param>
        public virtual Task SqlBulkCopyMapAsync<T>(List<T> oData
                                                 , string sServidor
                                                 , string sBd
                                                 , string sUsuario
                                                 , string sClave
                                                 , string sTabla) where T : class
        {

            var oTask = new Task(() =>
            {
                SqlBulkCopyMap(oData, sServidor, sBd, sUsuario, sClave, sTabla);
            });

            oTask.Start();

            return oTask;
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="oData"></param>
        /// <param name="oDTO"></param>
        /// <param name="sTabla"></param>
        public virtual Task SqlBulkCopyMapAsync<T>(List<T> oData
                                                 , DTO.TDatosConex_DTO oDTO
                                                 , string sTabla) where T : class
        {

            var oTask = new Task(() =>
            {
                SqlBulkCopyMap(oData, oDTO, sTabla);
            });

            oTask.Start();

            return oTask;
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="oData"></param>
        /// <param name="sTabla"></param>
        /// <param name="sCampos"></param>
        public virtual Task SqlBulkCopyMapByDCAsync<T>(List<T> oData
                                                     , string sTabla) where T : class
        {

            var oTask = new Task(() =>
            {
                SqlBulkCopyMapByDC(oData, sTabla);
            });

            oTask.Start();

            return oTask;
        }
        #endregion


    }
}
