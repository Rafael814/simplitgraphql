﻿using System;
using System.Data.SqlClient;
using System.Reflection;
using System.Text.RegularExpressions;
using TotalApp.Package.DTO;

namespace TotalApp.Package.DAL
{
    /// <summary>
    /// *Autor:Larry Ramirez
    /// *Fecha de creacion:09-01-2018
    /// *Version:1.1.20180109
    /// *Sinopsis:Clase que permite realizar TBulkCopy a las BD
    /// </summary> 
    public partial class TBulkCopy_DAL : TInfoEstatus_DTO, IDisposable
    {
        #region "Constantes"
        #endregion

        #region "Variables"
        #endregion

        #region "Propiedades"
        #endregion

        #region "Funciones"

        #region "Conexiones" 

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:Construye el conexion string de conexion a la bd
        /// </summary>
        /// <param name="sServidor"></param>
        /// <param name="sBd"></param>
        /// <param name="sUsuario"></param>
        /// <param name="sClave"></param>
        /// <returns></returns>
        public virtual string CnxStringSqlServer(string sServidor
                                                , string sBd
                                                , string sUsuario
                                                , string sClave)
        {
            if (sServidor.TIsNullOrEmpty()) throw THelperInfoEstatus.ArgumentNull(nameof(sServidor));
            if (sBd.TIsNullOrEmpty()) throw THelperInfoEstatus.ArgumentNull(nameof(sBd));
            if (sUsuario.TIsNullOrEmpty()) throw THelperInfoEstatus.ArgumentNull(nameof(sUsuario));
            if (sClave.TIsNullOrEmpty()) throw THelperInfoEstatus.ArgumentNull(nameof(sClave));

            string sConex = THelper.Empty;
            string sApp = sBd + "_" + DateTime.Now.ToString("yyyyMMddHHmmss") + "_" + THelper.Random();

            sConex = @"Data Source=@SERVIDOR;";
            sConex += @"Initial Catalog=@BD;";
            sConex += @"Integrated Security=False;";
            sConex += @"User ID=@USUARIO;";
            sConex += @"Password=@CLAVE;";

            sConex += @"Application Name=@MIAPP;";
            sConex += @"Connection Timeout=30;";
            sConex += @"pooling=false;";
            //sConex += @"MultipleActiveResultSets=True;";
            //sConex += @"Encrypt=False;";
            //sConex += @"Persist Security Info=false;";

            sConex = sConex.Replace("@SERVIDOR", sServidor);
            sConex = sConex.Replace("@BD", sBd);
            sConex = sConex.Replace("@USUARIO", sUsuario);
            sConex = sConex.Replace("@CLAVE", sClave);
            sConex = sConex.Replace("@MIAPP", sApp);

            return sConex;
        }
        #endregion

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:Metodo (logica de negocio) para la captura de errores
        /// </summary>
        /// <param name="sTabla">Nombre de la tabla</param>
        /// <param name="oSqlBulkCopy">Objeto que contiene la info del Bulk</param>
        /// <param name="ex">Sinopsis del error</param>
        private void TInfoError(string sTabla, SqlBulkCopy oSqlBulkCopy, string sEx)
        {

            Error = 2;
            Mensaje = "Error en BulCopy";
            MensajeDev = THelper.Empty;
            MensajeDev += "#1ex - Error en la funcion SqlBulkCopy";
            MensajeDev += "Tabla:" + sTabla;
            MensajeDev += "\r\n";
            MensajeDev += sEx;
            MensajeDev += "\r\n";
            TypeErr = TTypeError.Err_CT_BkC;
            StatusCode = System.Net.HttpStatusCode.InternalServerError;

            /*
            if (sEx.Contains("Received an invalid column length from the bcp client for colid"))
            {
                string sPattern = @"\d+";
                Match oMatch = Regex.Match(sEx.ToString(), sPattern);
                var nIndex = System.Convert.ToInt32(oMatch.Value) - 1;

                FieldInfo oFi = typeof(SqlBulkCopy).GetField("_sortedColumnMappings", BindingFlags.NonPublic | BindingFlags.Instance);
                var oSortedColumns = oFi.GetValue(oSqlBulkCopy);
                var oItems = (Object[])oSortedColumns.GetType().GetField("_items", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(oSortedColumns);

                FieldInfo oFitemdata = oItems[nIndex].GetType().GetField("_metadata", BindingFlags.NonPublic | BindingFlags.Instance);
                var oMetadata = oFitemdata.GetValue(oItems[nIndex]);

                var oColumn = oMetadata.GetType().GetField("column", BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance).GetValue(oMetadata);
                var oLength = oMetadata.GetType().GetField("length", BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance).GetValue(oMetadata);

                MensajeDev += String.Format("Column: {0} contains data with a length greater than: {1}", oColumn, oLength);

                oFi = null;
                oFitemdata = null;
            }
            */

           
        }

     

        #endregion

        #region "Metodos & constructores"
        public TBulkCopy_DAL(): base()
        {

        }
        #endregion

        #region "Eventos"
        #endregion

        #region "Destructor"
        /// <summary>
        /// Autor   : Larry Ramirez
        /// Fecha   : 21-05-2016
        /// Sinopsis: Variable Disposed, permite saber de manera general si ya fueron liberados los recursos no administrados a la clase (Objeto).
        /// </summary>
        private bool _Disposed = false;

        // <summary>
        /// Autor   : Larry Ramirez
        /// Fecha   : 25-05-2016
        /// Sinopsis: Metodo que permite Desechar y limpiar objetos de la memoria.
        /// </summary>
        public new void Dispose()
        {

            Dispose(true);
            GC.SuppressFinalize(this);
        }

        // <summary>
        /// Autor   : Larry Ramirez
        /// Fecha   : 25-05-2016
        /// Sinopsis: Logica para limpiar objetos de la memoria.
        /// </summary>
        private new void Dispose(bool bIsDisposing)
        {
            //Si esta en false, significa que se tiene la oportunidad de limpiar los objetos
            if (!this._Disposed)
            {
                //Aqui se definen los objetos que se liberan
                if (bIsDisposing)
                {

                }
            }
            _Disposed = true;
        }

        // <summary>
        /// Autor   : Larry Ramirez
        /// Fecha   : 25-05-2016
        /// Sinopsis: Destructor por defecto.
        /// </summary>
        ~TBulkCopy_DAL()
        {
            Dispose(false);
            GC.SuppressFinalize(this);
        }
        #endregion
    }
}
