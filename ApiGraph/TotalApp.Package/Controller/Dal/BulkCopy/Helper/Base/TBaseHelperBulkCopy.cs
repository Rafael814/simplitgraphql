﻿
namespace TotalApp.Package.DAL
{
    /// <summary>
    /// *Autor:Larry Ramirez
    /// *Fecha de creacion:09-01-2018
    /// *Version:1.1.20180109
    /// *Sinopsis:Clase de tipo static, permite invocar directamente a la clase TBulkCopy_DAL sin la necesidad de instanciarla
    /// </summary>
    public static partial class THelperBulkCopy
    {
        #region "Constantes"
        #endregion

        #region "Variables"
        #endregion

        #region "Propiedades"
        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        public static int Error { get; set; }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        public static string Mensaje { get; set; }

        #endregion

        #region "Funciones"
        #endregion

        #region "Metodos & constructores"
        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        private static void Ok()
        {
            Error = 0;
            Mensaje = THelper.Empty;
        }
        #endregion

        #region "Eventos"
        #endregion

        #region "Destructor"

        #endregion
    }
}
