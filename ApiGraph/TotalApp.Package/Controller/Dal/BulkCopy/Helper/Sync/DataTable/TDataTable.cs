﻿using System.Data;

namespace TotalApp.Package.DAL
{
    /// <summary>
    /// *Autor:Larry Ramirez
    /// *Fecha de creacion:09-01-2018
    /// *Version:1.1.20180109
    /// *Sinopsis:
    /// </summary> 
    public partial class THelperBulkCopy
    {
        #region "DataTable"
        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:Metodo que permite realizar el BulkCopy de un DataTable
        /// </summary>
        /// <param name="oData">DataTable</param>
        /// <param name="sCnx">Conexion string de la BD</param>
        /// <param name="sTabla">Tabla donde se realizara el BulkCopy</param>
        /// <param name="sCampos">Campos a ser mapeados durante el Insert, por defecto es null, significa que tomara por defecto los campos de la tabla de derecha a izquierda, en el orden 
        /// en el que esten ordenados en la lista</param>
        public static void SqlBulkCopy(DataTable oData
                                     , string sCnx
                                     , string sTabla
                                     , string[] sCampos = null)
        {


            Ok();
            using (TBulkCopy_DAL oDAL = new TBulkCopy_DAL())
            {
                oDAL.SqlBulkCopy(oData, sCnx, sTabla, sCampos);

                Error = oDAL.Error;
                Mensaje = oDAL.Mensaje;

                oDAL.Dispose();
            }
        }


        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:Metodo que permite realizar el BulkCopy de un DataTable
        /// </summary>
        /// <param name="oData">DataTable</param>
        /// <param name="sServidor"></param>
        /// <param name="sBd"></param>
        /// <param name="sUsuario"></param>
        /// <param name="sClave"></param>
        /// <param name="sTabla">Tabla donde se realizara el BulkCopy</param>
        /// <param name="sCampos">Campos a ser mapeados durante el Insert, por defecto es null, significa que tomara por defecto los campos de la tabla de derecha a izquierda, en el orden 
        /// en el que esten ordenados en la lista</param>
        public static void SqlBulkCopy(DataTable oData
                                      , string sServidor
                                      , string sBd
                                      , string sUsuario
                                      , string sClave
                                      , string sTabla
                                      , string[] sCampos = null) => SqlBulkCopy(oData
                                                                              , THelper.CnxStringSqlServer(sServidor, sBd, sUsuario, sClave)
                                                                              , sTabla
                                                                              , sCampos);



        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="oData"></param>
        /// <param name="oDTO"></param>
        /// <param name="sTabla"></param>
        /// <param name="sCampos"></param>
        public static void SqlBulkCopy(DataTable oData
                                      , DTO.TDatosConex_DTO oDTO
                                      , string sTabla
                                      , string[] sCampos = null) => SqlBulkCopy(oData
                                                                              , THelper.CnxStringSqlServer(oDTO.Server, oDTO.DB, oDTO.User, oDTO.Pass)
                                                                              , sTabla
                                                                              , sCampos);


        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="oData"></param>
        /// <param name="sTabla"></param>
        /// <param name="sCampos"></param>
        public static void SqlBulkCopyByDC(DataTable oData
                                          , string sTabla
                                          , string[] sCampos = null) => SqlBulkCopy(oData
                                                                                  , THelper.DatosConex_DTO
                                                                                  , sTabla
                                                                                  , sCampos);
        #endregion


    }
}
