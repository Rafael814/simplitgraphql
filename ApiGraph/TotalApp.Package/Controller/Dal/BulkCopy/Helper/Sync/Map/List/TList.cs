﻿using System.Collections.Generic;

namespace TotalApp.Package.DAL
{
    /// <summary>
    /// *Autor:Larry Ramirez
    /// *Fecha de creacion:09-01-2018
    /// *Version:1.1.20180109
    /// *Sinopsis:
    /// </summary> 
    public partial class THelperBulkCopy
    {

        #region "Map"


        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:Metodo que permite realizar el BulkCopy de una Lista, de un determinado objeto
        /// esta funcion invoca la funcion  SqlBulkCopyMap(T) de tipo DataTable, debido a que sus parametros son especificos y construyen
        /// el string de conexion
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="oData"></param>
        /// <param name="sCnx"></param>
        /// <param name="sTabla"></param>
        public static void SqlBulkCopyMap<T>(List<T> oData
                                            , string sCnx
                                            , string sTabla) where T : class => SqlBulkCopyMap(oData.TListToDataTableOrDefault()
                                                                                             , sCnx
                                                                                             , sTabla);


        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:Metodo que permite realizar el BulkCopy de una Lista, de un determinado objeto
        /// esta funcion invoca la funcion  SqlBulkCopyMap(T) de tipo DataTable, debido a que sus parametros son especificos y construyen
        /// el string de conexion
        /// </summary>
        /// <typeparam name="T">(T) objeto generico</typeparam>
        /// <param name="oData"></param>
        /// <param name="sServidor"></param>
        /// <param name="sBd"></param>
        /// <param name="sUsuario"></param>
        /// <param name="sClave"></param>
        /// <param name="sTabla">Tabla donde se realizara el BulkCopy</param>
        public static void SqlBulkCopyMap<T>(List<T> oData
                                            , string sServidor
                                            , string sBd
                                            , string sUsuario
                                            , string sClave
                                            , string sTabla) where T : class => SqlBulkCopyMap(oData.TListToDataTableOrDefault()
                                                                                             , THelper.CnxStringSqlServer(sServidor, sBd, sUsuario, sClave)
                                                                                             , sTabla);


        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="oData"></param>
        /// <param name="oDTO"></param>
        /// <param name="sTabla"></param>
        public static void SqlBulkCopyMap<T>(List<T> oData
                                           , DTO.TDatosConex_DTO oDTO
                                           , string sTabla) where T : class => SqlBulkCopyMap(oData
                                                                                             , THelper.CnxStringSqlServer(oDTO.Server, oDTO.DB, oDTO.User, oDTO.Pass)
                                                                                             , sTabla);


        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="oData"></param>
        /// <param name="sTabla"></param>
        /// <param name="sCampos"></param>
        public static void SqlBulkCopyMapByDC<T>(List<T> oData
                                                , string sTabla) where T : class => SqlBulkCopyMap(oData
                                                                                                 , THelper.DatosConex_DTO
                                                                                                 , sTabla);

        #endregion


    }
}
