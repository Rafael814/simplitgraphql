﻿
namespace TotalApp
{
    /// <summary>
    /// *Autor:Larry Ramirez
    /// *Fecha de creacion:09-01-2018
    /// *Version:1.1.20180109
    /// *Sinopsis:
    /// </summary>
    public class Plantilla
    {
        #region "Constantes"
        #endregion

        #region "Variables"
        #endregion

        #region "Propiedades"
        #endregion

        #region "Funciones"
        #endregion

        #region "Metodos & constructores"
        public Plantilla()
        {

        }
        #endregion

        #region "Eventos"
        #endregion

        #region "Destructor"
        ~Plantilla()
        {

        }
        #endregion
    }
}
