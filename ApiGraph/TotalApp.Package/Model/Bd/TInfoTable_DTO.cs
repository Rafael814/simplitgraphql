﻿using System;
using System.Runtime.Serialization;

namespace TotalApp.Package.DTO
{
    /// <summary>
    /// *Autor:Larry Ramirez
    /// *Fecha de creacion:09-01-2018
    /// *Version:1.1.20180109
    /// *Sinopsis:Clase que permite encapsular la informacion de las tablas de una BD
    /// </summary>
    [DataContract]
    public class TInfoTable_DTO
    {
        #region "Variables"
        #endregion

        #region "Propiedades"
        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:Id de la base de datos, el Id varia segun la creacion o adjunto de la bd en el server
        /// </summary>
        [DataMember(IsRequired = false)]
        public Int32 Id
        {
            get; set;
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:Nombre d ela base de datos
        /// </summary>
        [DataMember(IsRequired = true)]
        public string Table
        {
            get; set;
        }
        #endregion

        #region "Funciones"
        #endregion

        #region "Metodos & constructores"
        #endregion

        #region "Eventos"
        #endregion

        #region "Destructor"
        ~TInfoTable_DTO()
        {

        }
        #endregion
    }
}
