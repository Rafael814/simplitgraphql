﻿using System;
using System.Runtime.Serialization;

namespace TotalApp.Package.DTO
{
    /// <summary>
    /// *Autor:Larry Ramirez
    /// *Fecha de creacion:09-01-2018
    /// *Version:1.1.20180109
    /// *Sinopsis:Clase que permite encapsular la informacion de una Bd
    /// </summary>
    [DataContract]
    public class TInfoBd_DTO
    {
        #region "Variables"
        #endregion

        #region "Propiedades"
        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:Id de la base de datos, el Id varia segun la creacion o adjunto de la bd en el server
        /// </summary>
        [DataMember(IsRequired = false)]
        public Int32 Id
        {
            get; set;
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:Nombre d ela base de datos
        /// </summary>
        [DataMember(IsRequired = true)]
        public string Bd
        {
            get; set;
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:Propiedad GuId, unico en la base de datos, si la BD es creada o adjuntada en otro servidor, el GuId cambia segun el server
        /// </summary>
        [DataMember(IsRequired = false)]
        public string GuId
        {
            get; set;
        }
        #endregion

        #region "Funciones"
        #endregion

        #region "Metodos & constructores"
        #endregion

        #region "Eventos"
        #endregion

        #region "Destructor"
        ~TInfoBd_DTO()
        {

        }
        #endregion
    }
}
