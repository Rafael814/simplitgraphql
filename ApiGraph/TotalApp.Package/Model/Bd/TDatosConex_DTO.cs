﻿
using System;

namespace TotalApp.Package.DTO
{
    /// <summary>
    /// *Autor:Larry Ramirez
    /// *Fecha de creacion:09-01-2018
    /// *Version:1.1.20180109
    /// *Sinopsis:Clase que permite encapsular los datos de conexion a una BD
    /// </summary>
    public class TDatosConex_DTO

    {
        #region "Constantes"
        #endregion

        #region "Variables"
        private string _sServer = THelper.Empty;
        private string _sDB = THelper.Empty;
        private string _sUser = THelper.Empty;
        private string _sPass = THelper.Empty;
        private string _sStringConex = THelper.Empty;
        #endregion

        #region "Propiedades"
        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        public string Server
        {
            get => this._sServer;
            set
            {
                if (!value.TIsNullOrEmpty() && this._sServer != value)
                {
                    this._sServer = value;
                    SetDatosConex(_sServer, DB, User, Pass);
                }
            }
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        public string DB
        {
            get => this._sDB;
            set
            {
                if (!value.TIsNullOrEmpty() && this._sDB != value)
                {
                    this._sDB = value;
                    SetDatosConex(Server, _sDB, User, Pass);
                }
            }
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        public string User
        {
            get => this._sUser;
            set
            {

                if (!value.TIsNullOrEmpty() && this._sUser != value)
                {
                    this._sUser = value;
                    SetDatosConex(Server, DB, _sUser, Pass);
                }
            }
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        public string Pass
        {
            get => this._sPass;
            set
            {
                if (!value.TIsNullOrEmpty() && this._sPass != value)
                {
                    this._sPass = value;
                    SetDatosConex(Server, DB, User, _sPass);
                }

            }
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        public string StringConex
        {
            get => this._sStringConex;
            set
            {
                this._sStringConex = value;

            }
        }

        public DateTime FechaReg { get; set; }
        #endregion

        #region "Funciones"


        #endregion

        #region "Metodos & constructores"
        public TDatosConex_DTO()
        {
            FechaReg = DateTime.Now;
        }

        public TDatosConex_DTO(string sServer
                             , string sDB
                             , string sUser
                             , string sPass
                             , DataProvider oDataProvider = DataProvider.SqlServer)
        {
            Server = sServer;
            DB = sDB;
            User = sUser;
            Pass = sPass;
            FechaReg = DateTime.Now;
        }


        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sServer"></param>
        /// <param name="sDB"></param>
        /// <param name="sUser"></param>
        /// <param name="sPass"></param>
        /// <param name="oDataProvider"></param>
        public void SetDatosConex(string sServer
                                , string sDB
                                , string sUser
                                , string sPass
                                , DataProvider oDataProvider = DataProvider.SqlServer)
        {

            if (!sServer.TIsNullOrEmpty()
             && !sDB.TIsNullOrEmpty()
             && !sUser.TIsNullOrEmpty()
             && !sPass.TIsNullOrEmpty())
            {
                FechaReg = DateTime.Now;
                switch (oDataProvider)
                {
                    case DataProvider.SqlServer:
                        StringConex = THelper.CnxStringSqlServer(sServer, sDB, sUser, sPass);
                        break;
                    default:
                        THelperInfoEstatus.NotImplemented("Elemento no implementado");
                        break;
                }
            }

        }
        #endregion

        #region "Eventos"
        #endregion

        #region "Destructor"

        ~TDatosConex_DTO()
        {

        }
        #endregion
    }
}
