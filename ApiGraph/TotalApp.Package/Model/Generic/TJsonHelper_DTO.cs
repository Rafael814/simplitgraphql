﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;


namespace TotalApp.Package.DTO
{
    //// <summary>
    /// *Autor:Larry Ramirez
    /// *Fecha de creacion:09-01-2018
    /// *Version:1.1.20180109
    /// *Sinopsis:
    /// </summary>
    [DataContract]
    public class TJsonHelper_DTO: TInfoEstatus_DTO, IDisposable
    {
        #region "Constantes"
        #endregion

        #region "Variables"
        private string _sJson = THelper.Empty;
        #endregion

        #region "Propiedades"
        //// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        [DataMember(IsRequired = true)]
        public String Json
        {
            get { return this._sJson; }
            set {

                this._sJson = value;

                if (value.TIsNullOrEmpty())
                {
                    base.ErrorObjEmpty("El Json esta vacio"
                                      , 1
                                      , System.Net.HttpStatusCode.InternalServerError);
                }
            }
        }
        #endregion

        #region "Funciones"

        #region "override"
        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <returns></returns>
        public override string ToString() => THelperJson.TSerializeObjectOrDefault(this);
        #endregion

        #region "Objectos genericos"
        //// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <returns></returns>
        public new string ThisSerializeObjectOrDefault() => THelperJson.TSerializeObjectOrDefault(this);

        //// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="sJson"></param>
        /// <param name="bIsDecrypting"></param>
        /// <param name="bIsDecompressData"></param>
        /// <returns></returns>
        public T ThisReturnObjectOrDefault<T>(string sJson
                                            , bool bIsDecrypting = true
                                            , bool bIsDecompressData = true) where T : class
        {
            var oResp = THelperJson.TDeserializeObjectOrDefault<TJsonHelper_DTO>(sJson);

            return base.ReturnObjectOrDefault<T>(oResp.Json, bIsDecrypting, bIsDecompressData);
        }

        //// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="sJson"></param>
        /// <param name="bIsDecrypting"></param>
        /// <param name="bIsDecompressData"></param>
        /// <returns></returns>
        public List<T> ThisReturnListObjectOrDefault<T>(string sJson
                                                       , bool bIsDecrypting = true
                                                       , bool bIsDecompressData = true) where T : class
        {
            var oResp = THelperJson.TDeserializeObjectOrDefault<TJsonHelper_DTO>(sJson);

            return base.ReturnListObjectOrDefault<T>(oResp.Json, bIsDecrypting, bIsDecompressData);
        }

        //// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="bIsDecrypting"></param>
        /// <param name="bIsDecompressData"></param>
        /// <returns></returns>
        public T ThisReturnObjectOrDefault<T>(bool bIsDecrypting = true
                                            , bool bIsDecompressData = true) where T : class => base.ReturnObjectOrDefault<T>(Json, bIsDecrypting, bIsDecompressData);


        //// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="bIsDecrypting"></param>
        /// <param name="bIsDecompressData"></param>
        /// <returns></returns>
        public List<T> ThisReturnListObjectOrDefault<T>(bool bIsDecrypting = true
                                                      , bool bIsDecompressData = true) where T : class => base.ReturnListObjectOrDefault<T>(Json, bIsDecrypting, bIsDecompressData);

        #endregion

        #endregion

        #region "Metodos & constructores"

        #region "Por defecto"
        public TJsonHelper_DTO() : base()
        {

        }

        //// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sJson"></param>
        public TJsonHelper_DTO(string sJson):base()
        {
            Json = sJson.TIsNullOrEmpty() ? "" : sJson;
        }

        //// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sJson"></param>
        /// <param name="bIsDecrypting"></param>
        /// <param name="bIsDecompressData"></param>
        public TJsonHelper_DTO(string sJson
                             , bool bIsDecrypting = true
                             , bool bIsDecompressData = true):base()
        {
            Json = sJson.TIsNullOrEmpty() ? "": base.Encrypt_Compress_Object(sJson, bIsDecrypting, bIsDecompressData);
        }

        //// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="bIsDecrypting"></param>
        /// <param name="bIsDecompressData"></param>
        public TJsonHelper_DTO(object obj
                             , bool bIsEcrypting = true
                             , bool bIsCompressData = true):base()
        {
            Json = obj.TIsNull() ? "": base.Encrypt_Compress_Object(obj, bIsEcrypting, bIsCompressData);
        }
        #endregion

        #region "Encrypt"
        //// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        public void ThisEncrypt() => Json = base.Encrypt(Json);

        //// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        public void ThisDecrypting() => Json = base.Decrypting(Json);
        #endregion

        #region "Compress"
        //// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        public void ThisCompress() => Json = base.Encrypt(Json);

        //// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        public void ThisDeCompress() => Json = base.Decrypting(Json);
        #endregion

        #region "Encrypt & Compress"
        //// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        public void ThisEncrypt_Compress() => Json = base.Encrypt_Compress(Json);

        //// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        public void ThisEncrypt_Compress_Object(object obj) => Json = base.Encrypt_Compress_Object(obj);

        //// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        public void ThisCompress_Encrypt() => Json = base.Compress_Encrypt(Json);

        //// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        public void ThisCompress_Encrypt_Object(object obj) => Json = base.Compress_Encrypt_Object(obj);

        //// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        public void ThisDesEncrypt_DeCompress() => Json = base.DesEncrypt_DeCompress(Json);

        //// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        public void ThisDeCompress_DeEncrypt() => Json = base.DeCompress_DeEncrypt(Json);
        #endregion

        #endregion

        #region "Eventos"
        #endregion

        #region "Destructor"
        /// <summary>
        /// Autor   : Larry Ramirez
        /// Fecha   : 21-05-2016
        /// Sinopsis: Variable Disposed, permite saber de manera general si ya fueron liberados los recursos no administrados a la clase (Objeto).
        /// </summary>
        private bool _Disposed = false;

        // <summary>
        /// Autor   : Larry Ramirez
        /// Fecha   : 25-05-2016
        /// Sinopsis: Metodo que permite Desechar y limpiar objetos de la memoria.
        /// </summary>
        public new void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        // <summary>
        /// Autor   : Larry Ramirez
        /// Fecha   : 25-05-2016
        /// Sinopsis: Logica para limpiar objetos de la memoria.
        /// </summary>
        private new void Dispose(bool bIsDisposing)
        {
            //Si esta en false, significa que se tiene la oportunidad de limpiar los objetos
            if (!this._Disposed)
            {
                //Aqui se definen los objetos que se liberan
                if (bIsDisposing)
                {

                }
            }
            _Disposed = true;
        }

        // <summary>
        /// Autor   : Larry Ramirez
        /// Fecha   : 25-05-2016
        /// Sinopsis: Destructor por defecto.
        /// </summary>
        ~TJsonHelper_DTO()
        {
            Dispose(false);
            GC.SuppressFinalize(this);

        }
        #endregion
    }
}
