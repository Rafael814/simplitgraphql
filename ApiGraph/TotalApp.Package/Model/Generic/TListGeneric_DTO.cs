﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;


namespace TotalApp.Package.DTO
{
    //// <summary>
    /// *Autor:Larry Ramirez
    /// *Fecha de creacion:09-01-2018
    /// *Version:1.1.20180109
    /// *Sinopsis:
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="U"></typeparam>
    [DataContract]
    public class TListGeneric_DTO<T, U>: TInfoEstatus_DTO, IDisposable
                                        where T : class, new()
                                        where U : class, new()
    {
        #region "Constantes"
        #endregion

        #region "Variables"
        private T _oInfo = new T();
        private List<U> _oObjList = new List<U>();
        #endregion

        #region "Propiedades"
        //// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        [DataMember(IsRequired = true)]
        public T Info
        {
            get { return this._oInfo; }
            set { this._oInfo = value; }
        }

        //// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        [DataMember(IsRequired = false)]
        public List<U> Data
        {
            get { return this._oObjList; }
            set { this._oObjList = value; }
        }
        #endregion

        #region "Funciones"

        #region "override"
        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <returns></returns>
        public override string ToString() => THelperJson.TSerializeObjectOrDefault(this);
        #endregion

        #region "Json"
        //// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <returns></returns>
        public new string ThisSerializeObjectOrDefault() => THelperJson.TSerializeObjectOrDefault(this);

        //// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sJson"></param>
        /// <returns></returns>
        public new T ThisDeserializeObjectOrDefault(string sJson) => THelperJson.TDeserializeObjectOrDefault<T>(sJson);
        #endregion

        #region "Encrypt"
        //// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <returns></returns>
        public new string ThisEncrypt_Object() => base.Encrypt_Object(this);
        #endregion

        #region "Compress"
        //// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <returns></returns>
        public new string ThisCompress_Object() => base.Compress_Object(this);
        #endregion

        #region "Encrypt & Compress"
        //// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <returns></returns>
        public new string ThisEncrypt_Compress_Object() => base.Encrypt_Compress_Object(this);


        //// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <returns></returns>
        public new string ThisCompress_Encrypt_Object() => base.Compress_Encrypt_Object(this);
        #endregion

        #endregion

        #region "Metodos & constructores"
        public TListGeneric_DTO(): base()
        {
          
        }

        //// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="oInfo"></param>
        /// <param name="oData"></param>
        public TListGeneric_DTO(T oInfo, List<U> oData):base()
        {
            Info = oInfo;
            Data = oData;
        }
        #endregion

        #region "Eventos"
        #endregion

        #region "Destructor"
        /// <summary>
        /// Autor   : Larry Ramirez
        /// Fecha   : 21-05-2016
        /// Sinopsis: Variable Disposed, permite saber de manera general si ya fueron liberados los recursos no administrados a la clase (Objeto).
        /// </summary>
        private bool _Disposed = false;

        // <summary>
        /// Autor   : Larry Ramirez
        /// Fecha   : 25-05-2016
        /// Sinopsis: Metodo que permite Desechar y limpiar objetos de la memoria.
        /// </summary>
        public new void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        // <summary>
        /// Autor   : Larry Ramirez
        /// Fecha   : 25-05-2016
        /// Sinopsis: Logica para limpiar objetos de la memoria.
        /// </summary>
        private new void Dispose(bool bIsDisposing)
        {
            //Si esta en false, significa que se tiene la oportunidad de limpiar los objetos
            if (!this._Disposed)
            {
                //Aqui se definen los objetos que se liberan
                if (bIsDisposing)
                {

                }
            }
            _Disposed = true;
        }

        // <summary>
        /// Autor   : Larry Ramirez
        /// Fecha   : 25-05-2016
        /// Sinopsis: Destructor por defecto.
        /// </summary>
        ~TListGeneric_DTO()
        {
            Dispose(false);
            GC.SuppressFinalize(this);

        }
        #endregion
    }
}
