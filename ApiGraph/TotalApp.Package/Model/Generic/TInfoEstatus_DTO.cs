﻿using System;
using System.Net;
using System.Runtime.Serialization;
using TotalApp.Package.UTIL;

namespace TotalApp.Package.DTO
{
    /// <summary>
    /// *Autor:Larry Ramirez
    /// *Fecha de creacion:09-01-2018
    /// *Version:1.1.20180109
    /// *Sinopsis:
    /// </summary>
    [DataContract]
    public class TInfoEstatus_DTO : TEncrypt_Compress, IDisposable
    {
        #region "Variables"
        private int _nError = 0;
        private string _sMensaje = THelper.Empty;
        private HttpStatusCode _oStatusCode = HttpStatusCode.OK;
        private bool _bIsOK = true;
        private TTypeError _oErrType = TTypeError.Err_NA;
        private string _sMensajeInterno = THelper.Empty;
        private DateTime _dtFechaHora = DateTime.Now;
        private string _sTraza = THelper.Empty;
        #endregion

        #region "Propiedades"
        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        [DataMember(IsRequired = false)]
        public virtual int Error
        {
            get { return this._nError; }
            set
            {
                this._nError = value;
                FechaHora = DateTime.Now;
            }

        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        [DataMember(IsRequired = false)]
        public virtual string Mensaje
        {
            get => this._sMensaje; // { return this._sMensaje; }
            set => this._sMensaje = value; //{ this._sMensaje = value; }
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        [DataMember(IsRequired = false)]
        public virtual HttpStatusCode StatusCode
        {
            get { return this._oStatusCode; }
            set
            {
                this._oStatusCode = value;
                IsOK = (value == HttpStatusCode.OK);

            }
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        [DataMember(IsRequired = false)]
        public virtual bool IsOK
        {
            get { return this._bIsOK; }
            private set { this._bIsOK = value; }
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:08-02-2018
        /// *Version:1.1.20180208
        /// *Sinopsis:
        /// </summary>
        [DataMember(IsRequired = false)]
        public virtual TTypeError TypeErr
        {
            get { return this._oErrType; }
            set
            {
                this._oErrType = value;
            }
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:08-02-2018
        /// *Version:1.1.20180208
        /// *Sinopsis:
        /// </summary>
        [DataMember(IsRequired = false)]
        public virtual string MensajeDev
        {
            get => this._sMensajeInterno; // { return this._sMensaje; }
            set => this._sMensajeInterno = value; //{ this._sMensaje = value; }
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:08-02-2018
        /// *Version:1.1.20180208
        /// *Sinopsis:
        /// </summary>
        [DataMember(IsRequired = false)]
        public DateTime FechaHora
        {
            get => this._dtFechaHora; 
            private set => this._dtFechaHora = value; 
        }

       
        #endregion

        #region "Funciones"

        #region "override"
        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <returns></returns>
        public override string ToString() => THelperJson.TSerializeObjectOrDefault(this);
        #endregion

        #region "Json"
        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <returns></returns>
        public string ThisSerializeObjectOrDefault() => THelperJson.TSerializeObjectOrDefault(this);

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sJson"></param>
        /// <returns></returns>
        public TInfoEstatus_DTO ThisDeserializeObjectOrDefault(string sJson) => THelperJson.TDeserializeObjectOrDefault<TInfoEstatus_DTO>(sJson);
        #endregion

        #region "Encrypt"
        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <returns></returns>
        public string ThisEncrypt_Object() => base.Encrypt_Object(this);
        #endregion

        #region "Compress"
        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <returns></returns>
        public string ThisCompress_Object() => base.Compress_Object(this);
        #endregion

        #region "Encrypt & Compress"
        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <returns></returns>
        public string ThisEncrypt_Compress_Object() => base.Encrypt_Compress_Object(this);


        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <returns></returns>
        public string ThisCompress_Encrypt_Object() => base.Compress_Encrypt_Object(this);
        #endregion

        #endregion

        #region "Metodos & constructores"

        #region "Traza"
        //// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sMensaje"></param>
        public virtual void Traza(string sMensaje, bool bIsAddFechaHoraAlMensaje = true)
        {
            try
            {
                if (bIsAddFechaHoraAlMensaje)
                {
                    this._sTraza += sMensaje + " - " + DateTime.Now.ToString("yyyy/MM/dd hh:mm:ss") + "\r\n";
                }
                else
                {
                    this._sTraza += sMensaje + "\r\n";
                }
            }
            catch
            {

            }
            

        }


        //// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sNombreArc"></param>
        /// <param name="bIsAddFechaHoraAlNombreDelArchivo"></param>
        /// <param name="bIsAddFechaHoraAlMensaje"></param>
        /// <param name="sDir"></param>
        /// <param name="sExt"></param>
        public virtual void Imprimir_Traza(string sNombreArc = "TotalApp"
                                         , bool bIsAddFechaHoraAlNombreDelArchivo = false
                                         , bool bIsAddFechaHoraAlMensaje = false
                                         , string sDir = ""
                                         , string sExt = ".txt") {

            try
            {
                if (this._sTraza.Length >= 1)
                {
                    THelperLog.LogWrite(sNombreArc
                                      , _sTraza
                                      , bIsAddFechaHoraAlNombreDelArchivo
                                      , bIsAddFechaHoraAlMensaje
                                      , sDir
                                      , sExt);

                    _sTraza = THelper.Empty;
                    OK();
                }
            }
            catch
            {

            }
        }

        //// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sNombreArc"></param>
        /// <param name="bIsAddFechaHoraAlNombreDelArchivo"></param>
        public virtual void Imprimir_Traza_InternalServerError(string sNombreArc = "TotalApp"
                                                             , bool bIsAddFechaHoraAlNombreDelArchivo = true)
        {
            try
            {
                if (this._sTraza.Length >= 1)
                {
                    if (StatusCode == HttpStatusCode.InternalServerError)
                    {

                        THelperLog.LogWrite(sNombreArc
                                          , _sTraza
                                          , bIsAddFechaHoraAlNombreDelArchivo);

                        _sTraza = THelper.Empty;
                        OK();
                    }
                }
            }
            catch
            {

            }

           
        }


        //// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <returns></returns>
        public string Leer_Traza() => _sTraza;
        #endregion

        //// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="oErrType"></param>
        /// <param name="sMensajeDev"></param>
        private void SetTypeErrorMensajeDev(TTypeError oErrType = TTypeError.Err_NA
                                      , string sMensajeDev = THelper.Empty)
        {
            TypeErr = oErrType;

            if (sMensajeDev.TIsNullOrEmpty())
            {
                MensajeDev = TypeErr.TStringValueAttributeToString();
            }
            else
            {
                MensajeDev = sMensajeDev;
            }
           
        }

        #region "Por defecto"

        public TInfoEstatus_DTO()
        {
            Error = 0;
            Mensaje = THelper.Empty;
            StatusCode = HttpStatusCode.OK;
            TypeErr = TTypeError.Err_NA;
            MensajeDev = THelper.Empty;
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sJson"></param>
        public TInfoEstatus_DTO(string sJson)
        {
            var oResp = sJson.TDeserializeObjectOrDefault<TInfoEstatus_DTO>();

            if (!oResp.TIsNull())
            {
                Error = oResp.Error;
                Mensaje = oResp.Mensaje;
                StatusCode = oResp.StatusCode;
                TypeErr = oResp.TypeErr;
                MensajeDev = oResp.MensajeDev;
            }
        }

        //// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="nError"></param>
        /// <param name="sMensaje"></param>
        /// <param name="oStatusCode"></param>
        /// <param name="oErrType"></param>
        /// <param name="sMensajeDev"></param>
        public TInfoEstatus_DTO(int nError
                              , string sMensaje
                              , HttpStatusCode oStatusCode = HttpStatusCode.OK
                              , TTypeError oErrType = TTypeError.Err_NA
                              , string sMensajeDev = THelper.Empty)
        {
            Error = nError;
            Mensaje = sMensaje;
            StatusCode = oStatusCode;
            SetTypeErrorMensajeDev(oErrType, sMensajeDev);
        }

        //// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sMensaje"></param>
        /// <param name="nError"></param>
        /// <param name="oStatusCode"></param>
        /// <param name="oErrType"></param>
        /// <param name="sMensajeDev"></param>
        public TInfoEstatus_DTO(string sMensaje
                               , int nError
                               , HttpStatusCode oStatusCode = HttpStatusCode.OK
                               , TTypeError oErrType = TTypeError.Err_NA
                               , string sMensajeDev = THelper.Empty)
        {
            Error = nError;
            Mensaje = sMensaje;
            StatusCode = oStatusCode;
            SetTypeErrorMensajeDev(oErrType, sMensajeDev);
        }
        #endregion


        #region "Info estatus"

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sJson"></param>
        public virtual void InfoEstatus(string sJson)
        {
            var oResp = sJson.TDeserializeObjectOrDefault<TInfoEstatus_DTO>();

            if (!oResp.TIsNull())
            {
                Error = oResp.Error;
                Mensaje = oResp.Mensaje;
                StatusCode = oResp.StatusCode;
                TypeErr = oResp.TypeErr;
                MensajeDev = oResp.MensajeDev;
            }
        }

        //// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="nError"></param>
        /// <param name="sMensaje"></param>
        /// <param name="oStatusCode"></param>
        /// <param name="oErrType"></param>
        /// <param name="sMensajeDev"></param>
        public virtual void InfoEstatus(int nError
                                      , string sMensaje
                                      , HttpStatusCode oStatusCode = HttpStatusCode.OK
                                      , TTypeError oErrType = TTypeError.Err_NA
                                      , string sMensajeDev = THelper.Empty)
        {
            Error = nError;
            Mensaje = sMensaje;
            StatusCode = oStatusCode;
            SetTypeErrorMensajeDev(oErrType, sMensajeDev);
        }

        //// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sMensaje"></param>
        /// <param name="nError"></param>
        /// <param name="oStatusCode"></param>
        /// <param name="oErrType"></param>
        /// <param name="sMensajeDev"></param>
        public virtual void InfoEstatus(string sMensaje
                                      , int nError
                                      , HttpStatusCode oStatusCode = HttpStatusCode.OK
                                      , TTypeError oErrType = TTypeError.Err_NA
                                      , string sMensajeDev = THelper.Empty)
        {
            Error = nError;
            Mensaje = sMensaje;
            StatusCode = oStatusCode;
            SetTypeErrorMensajeDev(oErrType, sMensajeDev);
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="objInfoEstatus"></param>
        public virtual void InfoEstatus(TInfoEstatus_DTO objInfoEstatus)
        {
            Error = objInfoEstatus.Error;
            Mensaje = objInfoEstatus.Mensaje;
            StatusCode = objInfoEstatus.StatusCode;
            TypeErr = objInfoEstatus.TypeErr;
            MensajeDev = objInfoEstatus.MensajeDev;
        }
        #endregion


        #region "OK"
        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        public virtual void OK()
        {
            Error = 0;
            Mensaje = THelper.Empty;
            StatusCode = HttpStatusCode.OK;
            SetTypeErrorMensajeDev();
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="nError"></param>
        /// <param name="sMensaje"></param>
        public virtual void OK(int nError, string sMensaje)
        {
            Error = nError;
            Mensaje = sMensaje;
            StatusCode = HttpStatusCode.OK;
            SetTypeErrorMensajeDev();
        }

        /// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sMensaje"></param>
        /// <param name="nError"></param>
        public virtual void OK(string sMensaje, int nError = 0)
        {
            Error = nError;
            Mensaje = sMensaje;
            StatusCode = HttpStatusCode.OK;
            SetTypeErrorMensajeDev();
        }

        //// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sMensaje"></param>
        public virtual void OK(string sMensaje)
        {
            Error = 0;
            Mensaje = sMensaje;
            StatusCode = HttpStatusCode.OK;
            SetTypeErrorMensajeDev();
        }
        #endregion

        #region "NotFound"
        //// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="nError"></param>
        /// <param name="sMensaje"></param>
        /// <param name="oErrType"></param>
        /// <param name="sMensajeDev"></param>
        public virtual void NotFound(int nError
                                   , string sMensaje
                                   , TTypeError oErrType = TTypeError.Err_NA
                                   , string sMensajeDev = THelper.Empty)
        {
            Error = nError;
            Mensaje = sMensaje;
            StatusCode = HttpStatusCode.NotFound;
            SetTypeErrorMensajeDev(oErrType, sMensajeDev);
        }

        //// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sMensaje"></param>
        /// <param name="nError"></param>
        /// <param name="oErrType"></param>
        /// <param name="sMensajeDev"></param>
        public virtual void NotFound(string sMensaje
                                   , int nError
                                   , TTypeError oErrType = TTypeError.Err_NA
                                   , string sMensajeDev = THelper.Empty)
        {
            Error = nError;
            Mensaje = sMensaje;
            StatusCode = HttpStatusCode.NotFound;
            SetTypeErrorMensajeDev(oErrType, sMensajeDev);
        }

        //// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sMensaje"></param>
        /// <param name="oErrType"></param>
        /// <param name="sMensajeDev"></param>
        public virtual void NotFound(string sMensaje
                                   , TTypeError oErrType = TTypeError.Err_NA
                                   , string sMensajeDev = THelper.Empty)
        {
            Error = 1;
            Mensaje = sMensaje;
            StatusCode = HttpStatusCode.NotFound;
            SetTypeErrorMensajeDev(oErrType, sMensajeDev);
        }
        #endregion

        #region "InternalServerError"

        //// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="nError"></param>
        /// <param name="sMensaje"></param>
        /// <param name="oErrType"></param>
        /// <param name="sMensajeDev"></param>
        public virtual void InternalServerError(int nError
                                              , string sMensaje
                                              , TTypeError oErrType = TTypeError.Err_NA
                                              , string sMensajeDev = THelper.Empty)
        {
            Error = nError;
            Mensaje = sMensaje;
            StatusCode = HttpStatusCode.InternalServerError;
            SetTypeErrorMensajeDev(oErrType, sMensajeDev);
        }

        //// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sMensaje"></param>
        /// <param name="nError"></param>
        /// <param name="oErrType"></param>
        /// <param name="sMensajeDev"></param>
        public virtual void InternalServerError(string sMensaje
                                              , int nError 
                                              , TTypeError oErrType = TTypeError.Err_NA
                                              , string sMensajeDev = THelper.Empty)
        {
            Error = nError;
            Mensaje = sMensaje;
            StatusCode = HttpStatusCode.InternalServerError;
            SetTypeErrorMensajeDev(oErrType, sMensajeDev);
        }

        //// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sMensaje"></param>
        /// <param name="oErrType"></param>
        /// <param name="sMensajeDev"></param>
        public virtual void InternalServerError(string sMensaje
                                              , TTypeError oErrType = TTypeError.Err_NA
                                              , string sMensajeDev = THelper.Empty)
        {
            Error = 1;
            Mensaje = sMensaje;
            StatusCode = HttpStatusCode.InternalServerError;
            SetTypeErrorMensajeDev(oErrType, sMensajeDev);
        }


        //// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="ex"></param>
        /// <param name="nError"></param>
        /// <param name="sMensajeDev"></param>
        public virtual void InternalServerError(Exception ex
                                              , int nError = 1
                                              , string sMensajeDev = THelper.Empty)
        {
            Error = 1;
            Mensaje = ex.Message;
            StatusCode = HttpStatusCode.InternalServerError;
            SetTypeErrorMensajeDev(TTypeError.Err_NCT, sMensajeDev);
        }
        #endregion

        #region "Object Empty"
        //// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="nError"></param>
        /// <param name="sMensaje"></param>
        /// <param name="oStatusCode"></param>
        public virtual void ErrorObjEmpty(int nError
                                        , string sMensaje
                                        , HttpStatusCode oStatusCode = HttpStatusCode.NotFound)
        {
            Error = nError;
            Mensaje = sMensaje;
            StatusCode = HttpStatusCode.NotFound;
            SetTypeErrorMensajeDev(TTypeError.Err_ObjEmpty, THelper.Empty);
        }

        //// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sMensaje"></param>
        /// <param name="nError"></param>
        /// <param name="oStatusCode"></param>
        public virtual void ErrorObjEmpty(string sMensaje
                                        , int nError
                                        , HttpStatusCode oStatusCode = HttpStatusCode.NotFound)
        {
            Error = nError;
            Mensaje = sMensaje;
            StatusCode = HttpStatusCode.NotFound;
            SetTypeErrorMensajeDev(TTypeError.Err_ObjEmpty, THelper.Empty);
        }

        //// <summary>
        /// *Autor:Larry Ramirez
        /// *Fecha de creacion:09-01-2018
        /// *Version:1.1.20180109
        /// *Sinopsis:
        /// </summary>
        /// <param name="sMensaje"></param>
        /// <param name="oStatusCode"></param>
        public virtual void ErrorObjEmpty(string sMensaje
                                        , HttpStatusCode oStatusCode = HttpStatusCode.NotFound)
        {
            Error = 1;
            Mensaje = sMensaje;
            StatusCode = HttpStatusCode.NotFound;
            SetTypeErrorMensajeDev(TTypeError.Err_ObjEmpty, THelper.Empty);
        }
        #endregion

        #region Created
        /// <summary>
        /// Autor: Rafael Sanchez
        /// *Fecha de creacion:18-07-2018
        /// *Version:1.1.20180718
        /// *Sinopsis: Metodo que establece mensaje on status created
        /// </summary>
        /// <param name="sMensaje"></param>
        public virtual void Created(string sMensaje)
        {
            Error = 0;
            Mensaje = sMensaje;
            StatusCode = HttpStatusCode.Created;
            SetTypeErrorMensajeDev();
        }
        #endregion

        #endregion

        #region "Eventos"
        #endregion

        #region "Destructor"
        /// <summary>
        /// Autor   : Larry Ramirez
        /// Fecha   : 21-05-2016
        /// Sinopsis: Variable Disposed, permite saber de manera general si ya fueron liberados los recursos no administrados a la clase (Objeto).
        /// </summary>
        private bool _Disposed = false;

        // <summary>
        /// Autor   : Larry Ramirez
        /// Fecha   : 25-05-2016
        /// Sinopsis: Metodo que permite Desechar y limpiar objetos de la memoria.
        /// </summary>
        public new void Dispose()
        {

            Dispose(true);
            GC.SuppressFinalize(this);
        }

        // <summary>
        /// Autor   : Larry Ramirez
        /// Fecha   : 25-05-2016
        /// Sinopsis: Logica para limpiar objetos de la memoria.
        /// </summary>
        private new void Dispose(bool bIsDisposing)
        {
            //Si esta en false, significa que se tiene la oportunidad de limpiar los objetos
            if (!this._Disposed)
            {
                //Aqui se definen los objetos que se liberan
                if (bIsDisposing)
                {

                }
            }
            _Disposed = true;
        }

        // <summary>
        /// Autor   : Larry Ramirez
        /// Fecha   : 25-05-2016
        /// Sinopsis: Destructor por defecto.
        /// </summary>
        ~TInfoEstatus_DTO()
        {
            Dispose(false);
            GC.SuppressFinalize(this);

        }
        #endregion
    }
}
