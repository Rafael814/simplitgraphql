﻿using GraphQL.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiGraph.Model.Type
{
    public class ProductType : ObjectGraphType<Product>
    {
        public ProductType()
        {
            Name = "Product";

            Field(x => x.id, type: typeof(IdGraphType)).Description("The ID of the product.");
            Field(x => x.codProd).Description("The id of the product");
            Field(x => x.name).Description("The name of the product");
            Field(x => x.category).Description("The category of the product");
            Field(x => x.price).Description("The price of the product");
            Field(x => x.offer, type: typeof(ListGraphType<OfferType>)).Description("Offers");
        }
    }
}
