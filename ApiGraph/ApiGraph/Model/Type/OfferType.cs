﻿using GraphQL.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiGraph.Model.Type
{
    public class OfferType : ObjectGraphType<Offer>
    {
        public OfferType()
        {
            Name = "Offer";

            Field(x => x.codOffer, type: typeof(IdGraphType)).Description("The ID of the product.");
            Field(x => x.tipoOffer).Description("The type of the offer");
            Field(x => x.descrip).Description("The name of the offer");
            Field(x => x.value).Description("The value of the offer");
        }
    }
}
