﻿using ApiGraph.ApiGraph.DAL;
using ApiGraph.Model.Type;
using GraphQL.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiGraph.Query
{
    public class ProductQuery: ObjectGraphType
    {
        public ProductQuery()
        {
            var oDAL = new ProductDAL();
            Field<ProductType>(
                "product",
                arguments: new QueryArguments(
                    new QueryArgument<IdGraphType> { Name = "codprod", Description = "The ID of the product." }),
                resolve: context =>
                {
                    var codprod = /*context.GetArgument<Guid?>("codprod");*/"";
                    var product = oDAL.GetProducts().FirstOrDefault(i => i.codProd == codprod.ToString()); ;
                    return product;
                });

            Field<ListGraphType<ProductType>>(
                "products",
                resolve: context =>
                {
                    var products = oDAL.GetProducts();
                    return products;
                });
        }
    }
    
}
