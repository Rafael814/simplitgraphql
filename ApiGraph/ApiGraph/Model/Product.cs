﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiGraph.Model
{
    public class Product
    {
        public string id { get; set; }
        public string codProd { get; set; }
        public string name { get; set; }
        public int category { get; set; }
        public decimal price { get; set; }
        public List<Offer> offer { get; set; }
    }
}
