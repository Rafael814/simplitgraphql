﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiGraph.Model
{
    public class Offer
    {
        public string  codOffer{ get; set; }
        public string descrip { get; set; }
        public int tipoOffer { get; set; }
        public decimal value { get; set; }
    }
}
