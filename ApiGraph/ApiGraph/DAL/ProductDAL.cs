﻿using ApiGraph.Model;
using ApiGraph.Util;
using System;
using System.Collections.Generic;
using TotalApp.Package;
using TotalApp.Package.DAL;

namespace ApiGraph.ApiGraph.DAL
{
    public class ProductDAL :  TBase_ExecScript_DAL
    {
        public List<Product> GetProducts()
        {
            var oResp = new List<Product>();
            try
            {
                string sSql = @"SELECT [CodProd] 
                                        ,[CodProd] AS id
                                        ,[Descrip] AS name
                                        ,[CodInst] AS category
                                        ,[Precio1] AS price
                                        FROM[dbo].[SAPROD] WITH(NOLOCK)";

                oResp = base.Get_List_SqlServer<Product>(BDConex.sCnx,sSql);
            }
            catch (Exception)
            {

                throw;
            }
            return oResp;
        }

        public string SGetProducts()
        {
            var sResp = "";
            try
            {
                string sSql = @"SELECT [CodProd] 
                                        ,[Descrip] AS name
                                        ,[CodInst] AS category
                                        ,[Precio1] AS price
                                        FROM[dbo].[SAPROD] WITH(NOLOCK)";

                var oResp = base.Get_List_SqlServer<Product>(BDConex.sCnx, sSql);
                sResp = oResp.TSerializeObjectOrDefault();
            }
            catch (Exception)
            {

                throw;
            }
            return sResp;
        }
    }
}
